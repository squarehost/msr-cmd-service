
'''
This module uses the pywin32 ServiceFramework to provide a Win32 service which
listens for control changes, and calls the msr-cmd utility to apply them.

The msr-utility is an separate open source project wholly unrelated to this,
and can be found at: https://github.com/cocafe/msr-utility

It requires elevated privileges to do so, so functionality has been
deliberately limited to only setting and retrieving specific registers
through a low-level socket interface to attempt to minimise potential security
issues.

Extended functionality, such as the profiles and Web API is included in the
msr_webapi service.

'''

import win32gui
import win32serviceutil
import win32service
import win32event
import servicemanager
import logging, logging.config, os, socket, sys, tempfile, threading, time
sys.coinit_flags = 0

import pythoncom, wmi

LOGGER = logging.getLogger("msr")

class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''
 
    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())
 
    def flush(self): pass

class MSRWebService(win32serviceutil.ServiceFramework):
    _svc_name_ = 'winmax-msr-web-svc'
    _svc_display_name_ = 'Squarehost CPU Web/Profile Service'
    _svc_description_ = (
        "This service handles the administration and application of profiles "
        "for the MSR service."
    )

    logfile_name = 'msr-web.log'

    LOGTYPE_GENERIC = 0xF000

    LOGLEVEL_DEBUG = 'debug'
    LOGLEVEL_INFO = servicemanager.EVENTLOG_INFORMATION_TYPE
    LOGLEVEL_WARN = LOGLEVEL_WARNING = servicemanager.EVENTLOG_WARNING_TYPE
    LOGLEVEL_ERROR = servicemanager.EVENTLOG_ERROR_TYPE
    
    ''' General ServiceFramework methods. '''
    def __init__(self, args):
        self.debug = servicemanager.Debugging(-1)
        if not self.debug:
            from msr_web.settings import DEBUG_LOG
            self.debug = DEBUG_LOG or os.environ.get('MSR_DEBUG') or False
        self.setup_logging( )
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60) 

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        self.shutdown( )
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        self.startup( )
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ' - Starting in {} mode...'.format(
                                  "DEBUG" if self.debug else "normal"
                              )))
        self.main( )

    @classmethod
    def log(cls, msg, level=LOGLEVEL_INFO, type=LOGTYPE_GENERIC):
        ''' Convenience logger method, wrapping around servicemanager
            LogMsg, but including rudimentary debug filtering. '''   
        if level == cls.LOGLEVEL_DEBUG:
            LOGGER.debug(msg)
            if not cls.is_debug( ): return
            level = cls.LOGLEVEL_INFO
        elif level == cls.LOGLEVEL_WARNING:
            LOGGER.warn(msg)
        elif level == cls.LOGLEVEL_ERROR:
            LOGGER.error(msg)
        else:
            LOGGER.info(msg)
        servicemanager.LogMsg(
            level, type, (msg, '')
        )

    @classmethod
    def is_debug(cls):
        if hasattr(cls, '__is_debug'): return cls.__is_debug
        cls.__is_debug = servicemanager.Debugging(-1)
        return cls.__is_debug

    ''' Callbacks for service events. '''
    def startup(self):
        try:
            pythoncom.CoInitialize()
        except:
            pass

    def apply_on_boot(self):
        ''' Check if we can apply any on-boot services, and apply if so. '''
        try:
            from profiles.models import Profile
            Profile.objects.apply_on_boot( )
        except Exception as ex:
            LOGGER.error("Unable to apply on boot: {}".format(
                ex
            ), exc_info=ex)

    def start_listeners(self):
        lst = getattr(self, 'listener_threads', [])
        if not lst: return

        for l in lst:
            if not l: continue
            try:
                LOGGER.debug("Attempting to start listener {}".format(l))
                l.isDaemon = True
                l.start( )
                LOGGER.info("Started listener: {}".format(l))
            except Exception as ex:
                LOGGER.warn("Unable to start listener {}: {}".format(
                    l, ex
                ), exc_info=ex)

    def stop_listeners(self):
        lst = getattr(self, 'listener_threads', [])
        if not lst: return

        for l in lst:
            try:
                l.stop( )
            except Exception as ex:
                LOGGER.warn("Unable to stop listener {}: {}".format(
                    l, ex
                ), exc_info=ex)

    def shutdown(self):
        ''' Shutdown any required services. This method should suppress
            any errors. '''
        try:
            ''' Legitimate shutdown, so remove our RestoreProfile records.
                On-boot profile should then be applied next time. '''
            from profiles.models import RestoreProfile
            RestoreProfile.objects.cleanup( )
        except Exception as ex:
            self.log(
                "Unable to cleanup RestoreProfile: {}".format(ex),
                self.LOGLEVEL_WARNING
            )
        try:
            ''' Remove all open windows to ensure we maintain in sync with
                the tray. '''
            from msr.models import CurrentOpenWindow
            CurrentOpenWindow.objects.all( ).delete( )
        except Exception as ex:
            self.log(
                "Unable to cleanup CurrentOpenWindows: {}".format(ex),
                self.LOGLEVEL_WARNING
            )
        try:
            ''' Clean-up any "sticky" shortcut profiles to prevent them
                interfering with the on-boot profiles. '''
            from profiles.models import AppProfile, Profile
            Profile.objects.filter(auto_force_test=True).update(
                force_test=False, auto_force_test=False
            )
            AppProfile.objects.filter(auto_force_test=True).update(
                force_test=False, auto_force_test=False
            )
        except Exception as ex:
            self.log(
                "Unable to clean-up sticky/auto force test profs: {}.".format(
                    ex
                ), self.LOGLEVEL_WARNING
            )
        self.log("Cleaned up profiles.")
        try:
            self.server.shutdown( )
        except:
            pass
        self.log("Shutdown server.")
        try:
            self.stop_listeners( )
        except:
            pass
        self.log("Stopped listeners.")
        try:
            self.comms.stop( )
        except:
            pass
        self.log("Stopped comms thread.")
        try:
            pythoncom.CoUninitialize( )
        except:
            pass

    def config_changed(self, **kwargs):
        self.log("Configuration has changed.")
        lthrd = getattr(self, 'listener_threads', [])
        for thrd in lthrd:
            thrd.update_from_settings( )

    def setup_listeners(self):
        if hasattr(self, 'listener_threads') and self.listener_threads:
            return

        from constance import signals
        from django.conf import settings
        from profiles.utils import get_listener
        signals.config_updated.connect(self.config_changed)

        self.listener_threads = []
        lists = settings.PROFILE_LISTENERS
        self.log("Listeners are: {}".format(lists))
        if lists:
            for klass, args in lists:
                gl = get_listener(self.comms, klass, args)
                if gl: self.listener_threads.append(gl)


    def main(self):
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'msr_web.settings')

        try:
            import django
            django.setup(set_prefix=False)
            from django.core.management import call_command
            from django.core.management.base import BaseCommand
        except Exception as exc:
            LOGGER.error(
                "Unable to init web framework: {}".format(
                    exc
                ), exc_info=exc
            )
            raise ImportError(
                "Framework setup has failed: {}".format(exc)
            ) from exc

        try:
            from msr.management.commands import runapp
            from msr import comms_thread
            from django.conf import settings

            self.comms = comms_thread.get_comms_thread(
                test_mode=settings.MSR_CLIENT_TEST_MODE
            )
            self.log("Comms thread configured, test mode: {}".format(
                settings.MSR_CLIENT_TEST_MODE
            ))
            self.comms.start( )
            self.setup_listeners( )
            self.server = call_command(runapp.Command( ), use_reloader=False)
            self.apply_on_boot( )
            self.start_listeners( )
            self.server.serve_forever( )
            self.shutdown( )
            import time
            time.sleep(3)
        except Exception as ex:
            LOGGER.error(
                "Unable to start required threads/services: {}".format(
                    ex
                ), exc_info=ex
            )
            self.shutdown( )
            raise

    def setup_logging(self):
        local_appdata = os.getenv('LOCALAPPDATA')
        if not local_appdata:
            import tempfile
            local_appdata = tempfile.gettempdir( )
            
        llevel = 'DEBUG' if self.debug else 'INFO'
        self.logfile = logfile = os.path.join(
            local_appdata, self.logfile_name
        )

        self.log("Logging to file {}, level {}".format(
            self.logfile, llevel
        ))
        cfg = dict(
            version=1,
            formatters=dict(
                full=dict(
                    format='[%(levelname)s] %(asctime)s - %(module)s/%(threadName)s - %(message)s'
                ),
                brief=dict(
                    format='[%(levelname)s] %(message)s'
                )
            ),  
            handlers=dict(
                file=dict(
                    formatter='full',
                    level='DEBUG',
                    maxBytes=10 * 1024 * 1024,
                    backupCount=3,
                    filename=logfile
                ),
                console=dict(
                    formatter='brief',
                    level='DEBUG'
                )
            ),
            loggers=dict(
                msr_svc=dict(
                    handlers=['file', 'console'],
                    propagate=True,
                    level=llevel
                )
            )
        )

        cfg['handlers']['file']['class'] = (
            'logging.handlers.RotatingFileHandler'
        )
        cfg['handlers']['console']['class'] = 'logging.StreamHandler'
        for log in ('msr', 'profiles', 'msr_web', 'django'):
            cfg['loggers'][log] = cfg['loggers']['msr_svc']
        logging.config.dictConfig(cfg)

        if not self.is_debug:
            stdout_logger = logging.getLogger('msr_svc.stdout')
            sl = StreamToLogger(stdout_logger, getattr(logging, llevel))
            sys.stdout = sl

            stderr_logger = logging.getLogger('msr_svc.stderr')
            sl = StreamToLogger(stderr_logger, getattr(logging, llevel))
            sys.stderr = sl

if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize( )
        servicemanager.PrepareToHostSingle(MSRWebService)
        servicemanager.StartServiceCtrlDispatcher( )
    else:
        win32serviceutil.HandleCommandLine(MSRWebService)
