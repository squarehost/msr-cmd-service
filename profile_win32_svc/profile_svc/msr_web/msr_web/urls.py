"""msr_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.utils.translation import ugettext_lazy as _

admin.site.site_header = settings.SITE_TITLE
admin.site.site_title = _("CPU Profile Switcher Administration")
admin.site.index_title = _("Welcome to the profile switcher")

from msr import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('logs/', views.LogView.as_view( ), name='logs'),
    path('profiles/', include('profiles.urls')),
    path('', include('msr.urls')),
    path('', views.IndexView.as_view( ), name='home'),
] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
) + static(
    settings.DOCS_URL, document_root=settings.DOCS_ROOT
)