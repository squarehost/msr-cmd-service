import os
from pathlib import Path

from django.utils.translation import ugettext_lazy as _

VERSION = "1.12.b4"

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
LOCAL_APPDATA_PATH = Path(os.getenv('LOCALAPPDATA')).resolve()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ni67vx@ocfu6s)s3by$_gs=u@nod=wx=*d-4(k=3@kt&6#8hu6'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# Used by the services to determine debug log level.
DEBUG_LOG = True

SITE_TITLE = _("CPU Profile Switcher")

ALLOWED_HOSTS = []

''' These will enforce some minimum validation on the offset values.
    If you change these please ensure you know what you are doing! '''
MIN_VOLTAGE_OFFSET = -150
MAX_VOLTAGE_OFFSET = 0

''' These will enforce some minimum validation on the PL wattages.
    If you change these please ensure you know what you are doing! '''
MIN_POWER_LIMIT = 1
MAX_POWER_LIMIT = 250

# If this cannot be detected from the client...
DEFAULT_MAX_PARKED_CORES = 4

DEFAULT_HIBERNATE_ENABLED_TIMER_MINS = 180

# Application definition

INSTALLED_APPS = [
    'msr.apps.MsrConfig',
    
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'constance',
    'constance.backends.database',
    'rest_framework',

    'profiles.apps.ProfilesConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'msr.middleware.LocalTZMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'msr_web.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'msr.context_processors.version'
            ],
        },
    },
]

WSGI_APPLICATION = 'msr_web.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': LOCAL_APPDATA_PATH / 'msr-web.sqlite3',
    }
}

CACHES = {
    'default': {
        'BACKEND': "django.core.cache.backends.locmem.LocMemCache",
        'LOCATION': "msr-cache"
    }
}
CACHES['cross_thread'] = CACHES['default']
CACHES['listener'] = CACHES['default']
CACHES['client'] = CACHES['default']
CACHES['profile_app'] = CACHES['default']

APP_LIST_CACHE_TIME = 30
APP_PROFILE_CACHE_SECONDS = 600
# A list of base globs we can use to match "game folders" on the filesystem
APP_FOLDER_FIND_GLOBS = [
    ('steamapps/common', _("Steam Apps")),
    ('origin games', _("EA Origin Games")),
    ('gog galaxy/games', _("GOG Games")),
    ('epic games', _("Epic Games"))
]
APP_FOLDER_FIND_MAX_DEPTH = 3
APP_FOLDER_FIND_CACHE_SECS = 60
APP_FOLDER_EXCLUDE_EXE_NAMES = [
    'installer', 'unins', 
    'setup', 'convert.exe', 'gfxcon.exe',
    'epicgameslauncher', 'crashreport',
    'overlayrenderer', 'vcredist', 'java.exe', 'javaw.exe',
    'steamworks shared', 'detectarchitecture',
    'unrealversion', 'unrealcef', 'unrealenginelauncher',
    'pack2', 'pack1'
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = False

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'

DEFAULT_ADMIN_PASSWORD = 'msr20'
DEFAULT_RUNSERVER_HOST = '127.0.0.1:8998'
DEFAULT_LOG_HARD_LIMIT_LINES = 50
LOG_PAGE_HARD_LIMIT_LINES = 500
LOG_FILE_CACHE_TIME = 10
STATIC_ROOT = BASE_DIR / "static/"

SHOW_TASKBAR_ICON = True

MSR_CLIENT = (
    "Win32NamedPipeMSRClient", ["msr"]
)
MSR_CLIENT_TEST_MODE = False

PROFILE_LISTENERS = [
    #("WMIProcessCreationListener", []),
    #("WMIProcessDeletionListener", []),
    ("ProcessModificationPollListener", [10]),
    ("WMIPowerEventResumeListener", []),
    ("WMIPowerEventBatteryListener", []),
    ("WMIPowerLevelBatteryListener", [20]),
    ("WMIDisplayHardwareEventListener", []),
]
REST_FRAMEWORK = {
    'DEFAULT_THROTTLE_CLASSES': [
        'rest_framework.throttling.AnonRateThrottle',
    ],
    'DEFAULT_THROTTLE_RATES': {
        'anon': '20/minute'
    }
}
EPP_DEFAULT = 33

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_CONFIG = {
    'DISABLE_AUTOMATION_BOOT': (
        False, "Disable Boot Automation"
    ),
    'DISABLE_AUTOMATION_POWER': (
        False, "Disable Power Event Automation"
    ),
    'DISABLE_AUTOMATION_POWER_POLL': (
        False, "Disable Power Battery Level Automation"
    ),
    'DISABLE_AUTOMATION_APP': (
        False, "Disable App Profile Automation"
    ),
    'DISABLE_AUTOMATION_HARDWARE': (
        False, "Disable Hardware Event Automation"
    ),
    'DISABLE_AUTOMATION_HOTKEY': (
        False, "Disable Profile Hotkey Detection"
    ),
    'DISABLE_AUDIO_NOTIFY': (
        False, "Disable Audio Alerts"
    )
}

RESERVED_HOTKEYS = [
    ('control_shift', 'a')
]

DOCS_URL = '/docs/'
DOCS_ROOT = BASE_DIR / "docs/"

try:
    from msr_settings import *
except:
    pass
