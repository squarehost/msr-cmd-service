# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
build_script_path = '..\\..\\..\\..\\build_scripts\\'

a = Analysis(
    ['service.py'],
    pathex=['.'],
    binaries=[
    ],
    datas=[
        ('msr/', 'msr'),
        ('msr_web/', 'msr_web'),
        ('profiles/', 'profiles'),
        ('_msr_settings.py', '.')
    ],
    hiddenimports=['win32timezone',
                   'constance',
                   'constance.admin',
                   'constance.apps',
                   'constance.backends.database',
                   'formtools.wizard.views',
                   'formtools.wizard.storage',
                   'formtools.wizard.storage.session',
                   'rest_framework',
                   'rest_framework.apps',
                   'rest_framework.authentication',
                   'rest_framework.parsers',
                   'rest_framework.metadata',
                   'rest_framework.negotiation',
                   'rest_framework.permissions',
                   'rest_framework.routers',
                   'rest_framework.serializers',
                   'rest_framework.templatetags',
                   'rest_framework.throttling',
                   'rest_framework.viewsets'],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='msr_profile_svc',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          icon='..\\..\\..\\taskbar_menu\\msr_taskbar\\ico\\green-chip.ico',
          version="file_version_info.txt")
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='msr_profile_svc')
