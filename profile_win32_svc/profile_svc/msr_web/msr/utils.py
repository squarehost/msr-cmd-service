import constance
from django.conf import settings
from django.core.cache import caches

import logging

CACHE = caches['client']
LOGGER = logging.getLogger(__name__)

def get_client(klass=None, args=None):
    klass = klass or settings.MSR_CLIENT[0]
    args = args or settings.MSR_CLIENT[1]

    if isinstance(klass, str):
        try:
            from . import client
            klass = getattr(client, klass)
        except AttributeError:
            LOGGER.error("Unknown client class {}".format(klass))
            return None

    try:
        return klass(*args)
    except Exception as ex:
        LOGGER.error("Unable to initialise MSR client: {}".format(
            ex
        ), exc_info=ex)
        return None

def get_settings_dict(force=False, cache_time=60):
    sts = CACHE.get('app_settings')
    if not sts or force:
        sts = { }
        for k in dir(constance.config):
            sts[k] = getattr(constance.config, k)
        CACHE.set('app_settings', sts, cache_time)
    return sts