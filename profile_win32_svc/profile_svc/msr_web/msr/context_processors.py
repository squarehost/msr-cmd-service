from django.conf import settings

import logging

LOGGER = logging.getLogger(__name__)

def version(request):
    return {'version': settings.VERSION}