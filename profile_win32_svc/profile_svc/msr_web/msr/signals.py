import django.dispatch
from constance import signals
from django.utils.timezone import now

from . import utils

import logging, traceback

apply_failed = django.dispatch.Signal( )
apply_success = django.dispatch.Signal( )

EXCEPTION_DETAILS = '''
Application Type: {}\n
Applied item: {}\n
Exception Class: {}\n
Exception Details: {}\n
Traceback: {}\n'''

LOGGER = logging.getLogger(__name__)

@django.dispatch.receiver(signals.config_updated)
def clear_settings_cache(sender, **kwargs):
    utils.get_settings_dict(force=True)

@django.dispatch.receiver(apply_failed)
def log_on_failure(sender, **kwargs):
    from profiles.models import ProfileLog
    ts = kwargs.get('timestamp') or now( )
    type = kwargs.get('type') or 'manual'
    profile = kwargs.get('profile') or None
    exc = kwargs.get('exception')
    test_mode = kwargs.get('test_mode', False)

    profile_name = ''
    if not profile:
        profile_name = 'Unknown Profile'
    else:
        profile.run_on_failure_script( )

    details = EXCEPTION_DETAILS.format(
        "{} ({})".format(type, "Test" if test_mode else "Live"),
        kwargs.get('queue_item') or 'Unknown',
        exc.__class__.__name__ if exc else 'Unknown',
        str(exc) if exc else 'Unknown',
        "\n".join(traceback.TracebackException.from_exception(
            exc
        ).format() if exc else '-')
    )

    ProfileLog.objects.create(
        applied=ts, profile=profile, type=type,
        success=False, profile_name=profile_name,
        messages="{}An error occurred applying this profile: {}".format(
            "[TEST] " if test_mode else "", exc or "Unknown Error"
        ), details=details
    )

SUCCESS_DETAILS = '''
Application Type: {}\n
Applied item: {}\n
Server Response: {}\n
Data Sent: {}\n'''

@django.dispatch.receiver(apply_success)
def log_on_success(sender, **kwargs):
    from profiles.models import ProfileLog, RestoreProfile
    ts = kwargs.get('timestamp') or now( )
    type = kwargs.get('type') or 'manual'
    profile = kwargs.get('profile') or None
    test_mode = kwargs.get('test_mode', False)
    sent_data = kwargs.get('data_sent')
    resp_data = kwargs.get('response_data')
    
    profile_name = ''
    if not profile:
        profile_name = 'Unknown Profile'
    else:
        profile.run_on_success_script( )

    details = SUCCESS_DETAILS.format(
        "{} ({})".format(type, "Test" if test_mode else "Live"),
        kwargs.get('queue_item') or 'Unknown',
        resp_data, sent_data
    )

    try:
        RestoreProfile.objects.create(
            applied=ts, profile=profile, apply_type=type
        )
    except Exception as ex:
        LOGGER.warn("Could not create RestoreProfile item: {}".format(
            ex
        )+". The profile will not be unapplied in the event of a crash.")

    try:
        ProfileLog.objects.create(
            applied=ts, profile=profile, type=type,
            success=True, profile_name=profile_name,
            messages="{}This profile was applied successfully.".format(
                "[TEST] " if test_mode else ""
            ), details=details
        )
    except Exception:
        pass
