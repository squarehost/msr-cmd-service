import constance
import constance.signals
from dateutil.parser import parse
from django.conf import settings
from django.core.cache import cache
from django.db import transaction
from django.http import (
    HttpResponse, JsonResponse,
    HttpResponseBadRequest
)
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView

import datetime, json, io, os, re, logging

from . import models, utils
from profiles.models import (
    AppProfile, Profile, ProfileLog, Register, RestoreProfile
)

LOGGER = logging.getLogger(__name__)

class BaseViewMixin(object):
    default_filter_level = 'INFO'
    log_files = (
        ('msr-service.log', _("MSR Service")),
        ('msr-web.log', _("Profile/Web Service")),
        ('msr-taskbar.log', _("Taskbar/Tray App"))
    )
    logline_regex = re.compile(
        r"^\s*\[(?P<level>\w+)\]\s*(?P<message>.*)$"
    )
    hard_limit_log_lines = settings.DEFAULT_LOG_HARD_LIMIT_LINES

    def get_hard_limit_log_lines(self):
        return self.hard_limit_log_lines

    def get_default_filter_level(self):
        return self.default_filter_level

    def get_current_filter(self):
        return self.request.GET.get('filter') or None

    def get_current_log_level(self):
        return self.request.GET.get('level') or self.get_default_filter_level()

    def get_log_queryset(self, force_level=None):
        '''
            Trick Django into reading from the logs instead of a queryset. Not 
            sure this is really advisable, but will do for now.
        '''
        # Cache these if we are not looking at the first page - saves load time
        page_kwarg = self.page_kwarg
        page = self.request.GET.get(page_kwarg) or 1
        if page != 1:
            data = cache.get('log_data')
            if data: return data

        lines = []
        reg = self.get_logline_regex( )
        ltz = datetime.datetime.utcnow().astimezone().tzinfo
        filt = self.get_current_filter( )
        level = force_level or self.get_current_log_level( )
        levels = []
        if level and level != 'DEBUG':
            if level == 'INFO':
                levels = ['INFO', 'WARNING', 'ERROR']
            elif level == 'WARNING':
                levels = ['WARNING', 'ERROR']
            elif level == 'ERROR':
                levels = ['ERROR']

        for fn, name in self.log_files:
            if filt and name != filt: continue
            fh = self.open_log_file(fn)
            if not fh: continue

            try:
                last = None
                dat = reversed(fh.readlines( ))
                ct = 0
                linec = self.get_hard_limit_log_lines( )
                for l in dat:
                    ct += 1
                    if linec and ct > linec: break
                    ll = self.parse_log_line(reg, l, ltz)
                    if isinstance(ll, str):
                        if last:
                            last['message'] += "\n{}".format(ll)
                            continue
                    else:
                        ll['type'] = name
                        last = ll
                        if levels and ll['level'] not in levels: continue
                        lines.append(ll)
            finally:
                fh.close( )
        
        lines.sort(key=lambda x: x['timestamp'], reverse=True)
        cache.set('log_data', lines, settings.LOG_FILE_CACHE_TIME)
        return lines

    def get_logline_regex(self):
        return self.logline_regex

    def open_log_file(self, log_path):
        path = os.path.join(settings.LOCAL_APPDATA_PATH, log_path)
        if not os.path.exists(path):
            LOGGER.warning("Log path {} does not exist.".format(path))
            return None
        
        try:
            return open(path)
        except Exception as ex:
            LOGGER.warning("Unable to open log file {}".format(
                path
            ), exc_info=ex)
            return None

    @classmethod
    def parse_log_line(cls, regex, line, localtz):
        mtch = regex.match(line)
        if not mtch: return line
        gd = mtch.groupdict()
        msg = gd['message']
        parts = msg.split(" ")
        date, time = parts[0:2]
        thread = None
        if len(parts) > 5:
            if parts[2] == '-' and parts[4] == '-':
                thread = parts[3]
                parts = parts[0:2] + parts[5:]
        gd['thread'] = thread or ''
        try:
            ts = parse("{} {}".format(date, time))
            # Assume timestamps are in local server time.
            gd['timestamp'] = ts.replace(tzinfo=localtz)
            gd['message'] = (" ".join(parts[2:])).strip( )
        except Exception as ex:
            LOGGER.warn("Unable to parse timestamp for log: {} {} ({})".format(
                date, time, ex
            ))
            gd['timestamp'] = datetime.datetime(1970, 1, 1)
            gd['message'] = " ".join(parts)
        return gd

    def get_context_data(self, **kwargs):
        return super( ).get_context_data(
            site_title=settings.SITE_TITLE,
            log_files=self.log_files,
            loglimit_lines=self.get_hard_limit_log_lines( ),
            current_log_level=self.get_current_log_level( ),
            current_filter=self.get_current_filter( ),
            recent_profiles=Profile.objects.filter(
                pk__in=RestoreProfile.objects.values_list(
                    'profile_id', flat=True
                )
            ),
            profiles=Profile.objects.filter(appprofile=None)[0:10],
            appdef_profiles=AppProfile.objects.filter(
                active=True, match_type='defl'
            ),
            app_profiles=AppProfile.objects.exclude(
                match_type='defl'
            ),
            has_more_profiles=Profile.objects.count( ) if (
                Profile.objects.count( ) > 10
            ) else False,
            profile_logs=ProfileLog.objects.all( )[0:3]
        )

class IndexView(BaseViewMixin, ListView):
    template_name = 'index.html'

    paginate_by = 20

    get_queryset = BaseViewMixin.get_log_queryset

    def get_context_data(self, **kwargs):
        kwargs = super( ).get_context_data(**kwargs)
        try:
            full_list = self.request.GET.get('full')
            cli = utils.get_client( )
            raw, current_settings = cli.get_current_registers(
                include_empty=full_list
            )
            kwargs['current_settings'] = Register.registers_to_display_list(
                current_settings, full=full_list
            )
        except Exception as ex:
            LOGGER.warn(
                "Unable to retrieve settings from client.",
                exc_info=ex
            )

        return kwargs

class LogView(BaseViewMixin, ListView):
    template_name = 'logs.html'
    paginate_by = 100
    hard_limit_log_lines = settings.LOG_PAGE_HARD_LIMIT_LINES
    get_queryset = BaseViewMixin.get_log_queryset

    def get_hard_limit_log_lines(self):
        if self.request.GET.get('all') or self.request.GET.get('download'):
            return None
        return super( ).get_hard_limit_log_lines( )

    def get(self, *args, **kwargs):
        if self.request.GET.get('download'):
            qs = self.get_log_queryset('DEBUG')
            
            fh = io.StringIO( )
            for q in qs:
                fh.write(
                    "[{}] {}{} {} {}\n".format(
                        q['level'],
                        q['timestamp'].strftime("%Y-%m-%d %H:%M:%S"),
                        " - {} -".format(q['thread']) if q['thread'] else '',
                        str(q['type']),
                        q['message']
                    )
                )
            rsp = HttpResponse(fh.getvalue())
            rsp['Content-Type'] = 'text/text'
            rsp['Content-Disposition'] = 'inline; filename=logs-{}.txt'.format(
                datetime.datetime.now( ).strftime("%Y%m%d%H%M")
            )
            return rsp
        return super( ).get(*args, **kwargs)

@method_decorator(csrf_exempt, name='dispatch')
class SettingsView(ListView):
    permission_required = 'constance.change_config'
    def get_queryset(self):
        return [(k, getattr(constance.config, k)) for k in (
            dir(constance.config)
        )]

    def get(self, *args, **kwargs):
        return JsonResponse(dict(self.get_queryset( )))

    def post(self, *args, **kwargs):
        dct = dir(constance.config)
        upd = 0
        for k, v in self.request.POST.items( ):
            if k not in dct:
                return HttpResponseBadRequest("Unknown setting '{}'".format(k))
            if v.lower( ) == "false":
                v = False
            elif v.lower( ) == "true":
                v = True
            if v != getattr(constance.config, k):
                setattr(constance.config, k, v)
                upd += 1
        return JsonResponse({"changed": upd})

@method_decorator(csrf_exempt, name='dispatch')
class CurrentWindowsView(ListView):
    queryset = models.CurrentOpenWindow.objects.all( )
    permission_required = 'msr.change_currentopenwindow'

    def get(self, *args, **kwargs):
        return JsonResponse({'windows': list(
            self.get_queryset( ).values( )
        )})

    def post(self, *args, **kwargs):
        with transaction.atomic( ):
            models.CurrentOpenWindow.objects.all( ).delete( )

            dat = json.loads(self.request.POST.get('windows'))
            ct = 0
            for wnd in dat:
                try:
                    models.CurrentOpenWindow(
                        hwnd=wnd['hwnd'],
                        process=wnd['process'],
                        text=wnd['text']
                    ).save( )
                    ct += 1
                except Exception as ex:
                    LOGGER.warn("Unable to store window {}".format(
                        wnd
                    ), exc_info=ex)
                    
        return JsonResponse({"updated": ct})