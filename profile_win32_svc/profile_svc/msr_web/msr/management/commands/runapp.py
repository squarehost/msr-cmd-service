from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import CommandError
from django.core.management.commands.runserver import Command, naiveip_re
from django.core.servers.basehttp import *
from django.db.models import Q

import datetime, errno, os, re, socketserver, sys, threading

class Command(Command):
    help = 'Perform any required setup and launch the dev server.'

    def runner(self, addr, port, wsgi_handler, ipv6=False, server_cls=WSGIServer):
        server_address = (addr, port)
        httpd_cls = type('WSGIServer', (socketserver.ThreadingMixIn, server_cls), {})
        self.httpd = httpd = httpd_cls(server_address, WSGIRequestHandler, ipv6=ipv6)
        # ThreadingMixIn.daemon_threads indicates how threads will behave on an
        # abrupt shutdown; like quitting the server by the user or restarting
        # by the auto-reloader. True means the server will not wait for thread
        # termination before it quits. This will make auto-reloader faster
        # and will prevent the need to kill the server manually if a thread
        # isn't terminating correctly.
        httpd.daemon_threads = True
        httpd.set_app(wsgi_handler)
        return httpd

    def inner_run(self, *args, **options):
        self.stdout.write("Performing system checks...\n\n")
        self.check(display_num_errors=True)
        now = datetime.datetime.now( ).strftime('%B %d, %Y - %X')
        self.stdout.write(now)
        self.stdout.write((
            "MSR running using version %(version)s, using settings %(settings)r\n"
            "Starting server at %(protocol)s://%(addr)s:%(port)s/\n"
        ) % {
            "version": self.get_version(),
            "settings": settings.SETTINGS_MODULE,
            "protocol": self.protocol,
            "addr": '[%s]' % self.addr if self._raw_ipv6 else self.addr,
            "port": self.port
        })

        try:
            handler = self.get_handler(*args, **options)
            return self.runner(self.addr, int(self.port), handler,
                ipv6=self.use_ipv6, server_cls=self.server_cls)
        except OSError as e:
            # Use helpful error messages instead of ugly tracebacks.
            ERRORS = {
                errno.EACCES: "You don't have permission to access that port.",
                errno.EADDRINUSE: "That port is already in use.",
                errno.EADDRNOTAVAIL: "That IP address can't be assigned to.",
            }
            try:
                error_text = ERRORS[e.errno]
            except KeyError:
                error_text = e
            self.stderr.write("Error: %s" % error_text)
        except KeyboardInterrupt:
            sys.exit(0)

    def run(self, **options):
        return self.inner_run(None, **options)

    def handle(self, *args, **options):
        self.stderr.write("Applying any required database migrations...\n")
        call_command("migrate", '--noinput',
                     verbosity=options.get('verbosity', 1))
        self.stderr.write("Creating default user (if required)...\n")
        au = get_user_model( ).objects.filter(
            Q(username='msr') | Q(
                is_superuser=True, is_staff=True, is_active=True
            )
        )
        if not au.count( ):
            um = get_user_model( )(
                username='msr', is_superuser=True, is_active=True,
                is_staff=True
            )
            um.set_password(settings.DEFAULT_ADMIN_PASSWORD)
            um.save( )
            self.stderr.write("\t... default user has been created.\n")
        else:
            self.stderr.write("\t... default user already exists.\n")
        call_command("collectstatic", "--noinput", "--force")


        if not options.get('addrport'):
            options['addrport'] = settings.DEFAULT_RUNSERVER_HOST

        if not settings.DEBUG and not settings.ALLOWED_HOSTS:
            raise CommandError('You must set settings.ALLOWED_HOSTS if DEBUG is False.')

        self.use_ipv6 = options['use_ipv6']
        if self.use_ipv6 and not socket.has_ipv6:
            raise CommandError('Your Python does not support IPv6.')
        self._raw_ipv6 = False
        if not options['addrport']:
            self.addr = ''
            self.port = self.default_port
        else:
            m = re.match(naiveip_re, options['addrport'])
            if m is None:
                raise CommandError('"%s" is not a valid port number '
                                   'or address:port pair.' % options['addrport'])
            self.addr, _ipv4, _ipv6, _fqdn, self.port = m.groups()
            if not self.port.isdigit():
                raise CommandError("%r is not a valid port number." % self.port)
            if self.addr:
                if _ipv6:
                    self.addr = self.addr[1:-1]
                    self.use_ipv6 = True
                    self._raw_ipv6 = True
                elif self.use_ipv6 and not _fqdn:
                    raise CommandError('"%s" is not a valid IPv6 address.' % self.addr)
        if not self.addr:
            self.addr = self.default_addr_ipv6 if self.use_ipv6 else self.default_addr
            self._raw_ipv6 = self.use_ipv6
        return self.run(**options)

    def execute(self, *args, **options):
        if self.requires_system_checks and not options['skip_checks']:
            self.check()
        if self.requires_migrations_checks:
            self.check_migrations()
        return self.handle(*args, **options)