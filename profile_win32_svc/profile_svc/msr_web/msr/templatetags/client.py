from django import template

from .. import utils

import logging, time

register = template.Library( )

LOGGER = logging.getLogger(__name__)

@register.simple_tag
def validate_client( ):
    cli = utils.get_client( )
    try:
        resl = cli.test( )
        resl2 = cli.support_level( )
        resl2['connect_info'] = resl
        return resl2
    except Exception as ex:
        LOGGER.error("Unable to verify MSR client.", exc_info=ex)
        return False