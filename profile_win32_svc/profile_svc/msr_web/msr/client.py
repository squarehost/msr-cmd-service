from django.conf import settings
from django.core.cache import caches
from django.core.exceptions import ObjectDoesNotExist

import json, logging, time

from profiles import models, utils

CACHE = caches['client']

GENERIC_LOGGER = logging.getLogger("{}.Generic".format(__name__))

class RetryError(Exception):
    ''' Perhaps a pipe is busy, so we should retry this operation later. '''
    pass

class MSRClient(object):
    retry_error = RetryError

    def __init__(self):
        self.logger = logging.getLogger("{}.{}".format(
            __name__, self.__class__
        ))

    def test(self):
        '''
            Test we can connect to the server, returns a string of info about
            the server if successful.
        '''
        raise NotImplementedError

    def support_level(self):
        '''
            Return detailed information about the level of support offered by
            the server.
        '''
        raise NotImplementedError

    def connect(self):
        raise NotImplementedError

    def interrogate_support_response(self, base_resp):
        partial = False
        if base_resp.get('unsupported_handlers') or not base_resp.get(
            'supported_handlers'
        ):
            partial = True
        if not partial:
            for h in base_resp.get('supported_handlers'):
                if h.get('support_issues') or h.get('support_level'):
                    partial = True
        base_resp['degraded_support'] = partial

        for h in base_resp.get('supported_handlers') or []:
            partial = h.get('support_issues') or h.get('support_level')
            attr = 'warn' if partial else 'info'
            getattr(self.logger, attr)(
                "Server indicates handler {} ({}) is {} supported.".format(
                    h['name'], h['class'], 'PARTIALLY' if partial else 'fully'
                )
            )
            for issue in h.get('support_issues'):
                self.logger.warn("Server support issue: {}, {}".format(
                    h['class'], issue
                ))
            sup = [
                "{}: {}".format(h.get('class'), h.get('name'))
                for h in h.get('supported_registers') or []
            ]
            if sup: self.logger.info(
                "Supported registers for {} are {}".format(
                    h.get('class'), ", ".join(sup)
                )
            )
            unsup = [
                "{}: {}".format(h.get('short_name'), h.get('name'))
                for h in h.get('unsupported_registers') or []
            ]
            if unsup: self.logger.warn(
                "Unsupported registers for {} are {}".format(
                    h.get('class'), ", ".join(unsup)
                )
            )

        for h in base_resp.get('unsupported_handlers') or []:
            self.logger.warn(
                "Server indicates handler {} is wholly unsupported.".format(h)
            )

    def get_current_registers(self, subset=[]):
        # Return a list of Register classes representing current proc. state.
        raise NotImplementedError

    def apply_profile(self, profile, test_mode=True):
        # Take a profile, apply any register changes required.
        raise NotImplementedError

    def clear_cache(self):
        # Clear any cached values.
        pass

    @classmethod
    def profile_to_regs(cls, profile):
        regs = profile.to_json( )
        pre_apply = []
        registry = []

        if 'PWR_RESET' in regs: pre_apply.append(['PWR_RESET'])
        gpu = getattr(profile, 'gpu_option', None)
        if gpu: registry.append(("gpu_option", gpu))

        return (regs, pre_apply, registry)

    @classmethod
    def apply_profile_proc_controls(cls, profile, launched_exe):
        controls = []
        try:
            acp = profile.process_control
            if acp.priority != None or acp.affinity_mask_list != []:
                controls.append((profile, acp.controls_to_dict, False))
        except ObjectDoesNotExist:
            pass
        except AttributeError:
            pass

        for pcg in profile.all_process_control_groups.all( ):
            if pcg.priority != None or pcg.affinity_mask_list != []:
                ctd = pcg.controls_to_dict
                for app in pcg.applications.all( ):
                    controls.append((app, ctd, True))

        GENERIC_LOGGER.debug(
            "Process controls for {} are {}".format(
                profile, controls
            )
        )

        existing = {}

        if controls:
            proc_info = utils.get_running_apps(
                include_windows=False, sort_key=None
            )
            process_pids = {}

            for item, to_set, store in reversed(controls):
                pid, name = item.get_proc_id_from_info(proc_info)
                if pid:
                    if pid in process_pids: continue

                    existing[pid] = {}
                    GENERIC_LOGGER.info(
                        "Setting control info for pid {} ({}) to {}".format(
                            pid, name, to_set
                        )
                    )
                    pri, aff = to_set.get('priority'), to_set.get('affinity')
                    if pri:
                        try:
                            existing[pid]['pri'] = (
                                utils.get_process_priority(pid)
                            )
                            res = utils.set_process_priority(pid, pri)
                            if not res:
                                raise ValueError("Priority was not updated.")
                            else:
                                process_pids[pid] = True
                        except Exception as ex:
                            GENERIC_LOGGER.warn(
                                "Cannot set priority for pid {} ({})".format(
                                    pid, name
                                ), exc_info=ex
                            )
                    if aff:
                        try:
                            existing[pid]['aff'] = (
                                utils.get_process_affinity(pid)
                            )
                            res = utils.set_process_affinity(pid, aff)
                            if not res:
                                raise ValueError("Affinity was not updated.")
                            else:
                                process_pids[pid] = True
                        except Exception as ex:
                            GENERIC_LOGGER.warn(
                                "Cannot set priority for pid {} ({})".format(
                                    pid, name
                                ), exc_info=ex
                            )
        
                    if not store: del existing[pid]
            return existing

    @classmethod
    def registry_action_gpu_option(cls, profile, value, launched_exe):
        if not launched_exe: return

        sids = utils.get_local_user_sids( )

        if value in (1, 2):
            import winreg
            reg = winreg.ConnectRegistry(None, winreg.HKEY_USERS)
            GENERIC_LOGGER.debug(
                "Retrieved {} users to update registry".format(len(sids))
            )
            for user, sid in sids:
                try:
                    key = winreg.OpenKeyEx(reg, sid)
                    winreg.CreateKey(key, cls.gpu_performance_key)
                    gpu_key = winreg.OpenKey(
                        key, cls.gpu_performance_key, 0, winreg.KEY_WRITE
                    )
                    reg_sz = "GpuPreference={};".format(value)
                    GENERIC_LOGGER.info(
                        "Setting reg key {} ({})\\{} value {} to {}".format(
                            user, sid, cls.gpu_performance_key,
                            launched_exe, reg_sz
                        )
                    )
                    winreg.SetValueEx(
                        gpu_key, launched_exe, 0,
                        winreg.REG_SZ, reg_sz
                    )
                except Exception as ex:
                    GENERIC_LOGGER.warn(
                        "Unable to set registry key {}/{}".format(
                            "HKU", sid
                        ), exc_info=ex
                    )
        else:
            raise ValueError("Unknown GPU option value {}".format(value))

class Win32NamedPipeMSRClient(MSRClient):
    protocol_version = "1.12.b4"
    current_register_cache_seconds = 20
    gpu_performance_key = r"SOFTWARE\Microsoft\DirectX\UserGpuPreferences"

    def __init__(self, pipe_name):
        import winerror, win32file
        self.w32f, self.werr = win32file, winerror
        self.pipe_name = pipe_name
        self.has_conn = False
        self.server_info = None
        super( ).__init__( )

    def _connect(self, tries=0, exc=None):
        if tries > 3:
            self.logger.error(
                "Failed to connect after {} tries.".format(tries-1)
            )
            raise exc or ValueError("Unable to connect after 3 tries.")

        try:
            self.pipe = self.w32f.CreateFile(
                r"\\.\pipe\{}".format(self.pipe_name),
                self.w32f.GENERIC_READ | self.w32f.GENERIC_WRITE, 0, None,
                self.w32f.OPEN_EXISTING, self.w32f.FILE_FLAG_OVERLAPPED, None
            )
            self.logger.debug("Created named pipe handle {}: {}".format(
                self.pipe_name, self.pipe
            ))
        except Exception as ex:
            if hasattr(ex, 'winerror'):
                if ex.winerror == 2 or (
                    ex.winerror > 229 and ex.winerror < 234
                ):
                    time.sleep(0.5)
                    return self._connect(
                        tries=(tries+1) if tries else 1,
                        exc=ex
                    )
                else:
                    raise
            else:
                raise

    def _read_obj(self):
        try:
            msg = self._read_msg( )
        except Exception as ex:
            self.logger.error(
                "Unable to read response from server: {}".format(ex),
                exc_info=ex
            )
            raise

        self.logger.debug("Read RAW message from server: {}".format(msg))
        try:
            msg = json.loads(msg.decode('utf-8'))
        except Exception as ex:
            self.logger.error(
                "Unable to decode response from server: {}".format(ex),
                exc_info=ex
            )
            raise

        if msg.get('result') > 0:
            self.logger.error(
                "Error response from server: {}".format(msg)
            )
            raise ValueError("Error from server: {}".format(msg))

        return msg

    def _write_obj(self, obj):
        msg = self._obj_to_bytes(obj)
        self.logger.debug("Sending message to server: {}".format(msg))
        self.w32f.WriteFile(self.pipe, msg)

    def _read_msg(self):
        buf = b''
        more = self.werr.ERROR_MORE_DATA
        while more == self.werr.ERROR_MORE_DATA:
            more, data = self.w32f.ReadFile(self.pipe, 2048)
            buf += data
        return buf

    @classmethod
    def _obj_to_bytes(cls, obj):
        return json.dumps(obj).encode('utf-8')

    def _close(self):
        try:
            if self.has_conn:
                self._write_obj({'cmd': 'done'})
                self._read_obj( )
        except:
            pass
    
        try:
            self.w32f.CloseHandle(self.pipe)
        finally:
            self.pipe = None

    def support_level(self):
        # Connect will autoclose, so we can just use that.
        try:
            self._get_session( )
            self._write_obj({'cmd': 'support'})
            base_resp = self._read_obj( )['response']
            self.interrogate_support_response(base_resp)
            return base_resp
        finally:
            self._close_session( )

    def test(self):
        # Connect will autoclose, so we can just use that.
        self.connect( )
        return self.server_info.get('message', False)

    def connect(self):
        '''
            We want to open and close the named pipe when we actually want
            to call the server, so this is just to check we can communicate. 
        '''
        self._get_session( )
        self._close_session( )

    def _get_session(self):
        try:
            self._connect( )
            self._write_obj({'ehlo': self.protocol_version})
            self.server_info = self._read_obj( )
            self.has_conn = True
        except Exception as ex:
            self.logger.error("Error whilst getting connection: {}".format(
                ex
            ), exc_info=ex)
            raise

    def _close_session(self):
        try:
            self._close( )
        except Exception as ex:
            self.logger.error("Error whilst closing connection: {}".format(
                ex
            ), exc_info=ex)
            raise
        

    def clear_cache(self):
        CACHE.delete("all")

    def get_current_registers(
        self, subset=[], include_empty=True, raw=False, force=False
    ):
        key = "_".join(subset) or "all"
        resp = CACHE.get(key)

        if force or not resp:
            if subset:
                cmd = 'read'
                args = {'registers': [str(s) for s in subset]}
            else:
                cmd = 'read_all'
                args = {}

            self._get_session( )
            self._write_obj({'cmd': cmd, 'args': args})
            resl = self._read_obj( )
            self._close_session( )

            resp = resl.get('response', {})
            CACHE.set(key, resp, self.current_register_cache_seconds)

        return (resp, models.Profile.registers_from_json(
            resp, include_empty=include_empty
        ) if not raw else [])

    def _send_regs(self, to_write, test_mode, profile):
        try:
            data = {
                'cmd': 'write', 'args': {
                    'registers': to_write,
                    'test_mode': test_mode
                }
            }
            self.logger.debug("Asking to write the following for {}: {}".format(
                profile, data
            ))
            self._get_session( )
            self._write_obj(data)
            res = self._read_obj( )
            self.logger.debug("Response from write is: {}".format(res))
            if not res.get('success'):
                raise Exception("Failure response from server: {}".format(
                    res
                ))
            return (res, data)
        except Exception as ex:
            if hasattr(ex, 'winerror'):
                if ex.winerror == 2 or (
                    ex.winerror > 229 and ex.winerror < 234
                ):
                    self.logger.warn("Named pipe {} error: {}.".format(
                        self.pipe_name, ex,
                    ), exc_info=ex)
                    raise RetryError(str(ex))
                else:
                    raise
            else:
                raise
        finally:
            try:
                self._close( )
            except:
                pass

    def apply_profile(self, profile, test_mode=True, launched_exe=None):
        if hasattr(profile, 'appprofile'):
            profile = profile.appprofile

        to_write, pre_apply, reg_items = self.profile_to_regs(profile)
        if not to_write and not reg_items:
            self.logger.info(
                "No changes to apply for {}, skipping.".format(
                    profile
                )
            )
            return

        if reg_items:
            for fnc, value in reg_items:
                self.logger.debug("Applying registry function {} = {}".format(
                    fnc, value
                ))
                try:
                    getattr(self, "registry_action_{}".format(fnc))(
                        profile, value, launched_exe
                    )
                except AttributeError as aex:
                    self.logger.warn("Missing registry action for {}".format(
                        fnc
                    ))
                except Exception as ex:
                    self.logger.warn("Unable to set registry item {}".format(
                        fnc
                    ), exc_info=ex)

        to_restore = self.apply_profile_proc_controls(profile, launched_exe)
        if to_restore:
            try:
                dt = json.dumps(to_restore)
                self.logger.info(
                    "Storing process restoration data for {}: {}".format(
                        profile, dt
                    )
                )
                profile.__class__.objects.filter(
                    pk=profile.pk
                ).update(_restore_process_control_data=dt)
                profile._restore_process_control_data = dt
            except Exception as ex:
                self.logger.warn(
                    "Unable to store process restoration data, "
                    "priority/affinity may not be restored on close.",
                    exc_info=ex
                )

        if not to_write: return

        self.logger.info("Test mode = {}".format(test_mode))
        res = {}
        data = {}
        if pre_apply:
            for pre_set in pre_apply:
                pre_regs = {}
                for p in pre_set:
                    pre_regs[p] = to_write[p]
                    del to_write[p]
                
                self.logger.info("Applying 'pre-apply' registers {}.".format(
                    pre_regs
                ))
            
                r, d = self._send_regs(pre_regs, test_mode, profile)
                res.update(r)
                data.update(d)

        r, d = self._send_regs(to_write, test_mode, profile)
        res.update(r)
        data.update(d)

        return res, data