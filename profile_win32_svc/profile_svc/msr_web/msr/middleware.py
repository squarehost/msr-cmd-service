from django.utils import timezone

import logging, datetime

LOGGER = logging.getLogger(__name__)

class LocalTZMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request):
        tz = datetime.datetime.utcnow().astimezone().tzinfo
        #LOGGER.info("Setting timezone to local time {}".format(tz))
        timezone.activate(tz)
        return self.get_response(request)