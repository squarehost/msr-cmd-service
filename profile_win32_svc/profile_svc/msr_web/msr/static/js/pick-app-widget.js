jQuery(function($){ 
    $('.buttons .button').on('click', function( ) {
        var type = $(this).data('type');
        $('.app-picker fieldset').css('display', 'none');
        switch(type) {
            case "running":
                $("fieldset.running tr.title-row").css('display', '');
                $("fieldset.running tr").css('display', '');
                $("fieldset.running").css('display', '');
                break;
            case "installed":
                $("fieldset.installed").css('display', '');
                break;
            default:
                $("fieldset.running input").closest("tr").css('display', 'none');
                $("fieldset.running tr.title-row").css('display', 'none');
                $("fieldset.running input[name=app-name]").closest("tr").css('display', '');
                $("fieldset.running input[name=app-name]").closest("tr").css('display', '');
                $("fieldset.running").css('display', '');
                break;


        }
        $('.buttons .button').removeClass("selected");
        $(this).addClass("selected");
    });
    $('.app-table tr').on('click', function(ev) {
        $(this).find('input[type=checkbox]').each(function( ) {
            if(window.select_multiple) {
                $(this).attr('checked', !$(this).is(":checked"));
            } else {
                var checked = !$(this).is(":checked");
                if(checked) {
                    $('.app-table input[type=checkbox]').attr('checked', false);
                }
                $(this).attr('checked', checked);
            }
        });
    });
    $('.buttons .button.selected').click( );
    if(!$('.buttons .button.selected').length) {
        $($('.buttons .button')[0]).click( );
    }

    $('[data-type="select"]').on('click', function( ) {
        var cb = $('.app-picker input[type=checkbox]');
        var sel = [];
        cb.each(function( ) {
            if(!window.select_multiple && sel.length) return;
            if($(this).is(":checked")) sel.push(this);
        });

        if(!sel.length) {
            alert("Please select one "+(window.select_multiple ? "or more " : "")+
                  "application to select, or close the window to cancel.");
        } else {
            var vals = [];
            for(var i = 0; i < sel.length; i++) {
                var o = $(sel[i]);
                vals.push([o.attr('name'), o.val( )]);
            }
            window.opener.selectApplicationInputs(vals);
        }
        return false;
    });
});