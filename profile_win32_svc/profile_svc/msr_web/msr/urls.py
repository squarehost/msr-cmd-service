from django.urls import include, path

from . import views

urlpatterns = [
    path('settings/', views.SettingsView.as_view( ),
         name='settings'),
    path('windows/', views.CurrentWindowsView.as_view( ),
         name='windows'),
]