from django.apps import apps
from django.core.cache import caches
from django.utils.timezone import now
import logging, queue, threading

from . import signals, utils

import os, time

LOGGER = logging.getLogger(__name__)
CACHE = caches['cross_thread']

class CommsThread(threading.Thread):
    queue_listen_timeout = 2

    def __init__(self, q, test=True):
        self.command_queue = q
        self.running = False
        self.papp_conf = apps.get_app_config('profiles')
        self.client = utils.get_client( )
        self.test_mode = test
        threading.Thread.__init__(self)
        self.name = 'Comms'

    def apply_profile(self, profile_obj, type='manual', exe=None, sync=False):
        klass, pk = profile_obj._meta.model_name, profile_obj.pk, 
        if sync:
            return self._apply(klass, pk, type, 0, exe, sync=True)
        else:
            self.command_queue.put(
                ["apply", (klass, pk, type, 0, exe)]
            )
            return True

    def _apply(self, klass, pk, type, count, executable, sync=False, item=None):
        try:
            akls = self.papp_conf.models[klass]
            obj = akls.objects.get(pk=pk)
            LOGGER.info(
                "Retrieved profile {} to apply.".format(obj)
            )
        
            if obj.run_on_launch or obj.linked_profile:
                LOGGER.info(
                    "Attempting to run on launch script."
                )
                try:
                    exe = os.path.basename(executable)
                except:
                    exe = ''
                try:
                    path = os.path.dirname(executable)
                except:
                    path = ''
                obj.run_on_launch_script(vars={
                    'path': path,
                    'exe': exe,
                    'full_path': executable
                })

            try:
                dt = self.client.apply_profile(
                    obj, test_mode=self.test_mode,
                    launched_exe=executable
                )
                try:
                    self.client.clear_cache( )
                except:
                    pass
                if dt == None:
                    LOGGER.warn(
                        "No changes to apply for this profile, marking as "
                        "applied anyway."
                    )
                    dt = {}, {}
                resp, data_sent = dt
                LOGGER.info(
                    "Profile {} applied successfully.".format(
                        obj
                    )
                )
                akls.objects.filter(pk=pk).update(
                    applied=now( )
                )
                signals.apply_success.send_robust(
                    self.__class__,
                    timestamp=now( ),
                    profile=obj,
                    queue_item=item,
                    test_mode=self.test_mode,
                    type=type,
                    data_sent=data_sent,
                    response_data=resp
                )
                return True
            except self.client.retry_error as ex:
                if count > 4:
                    raise
                else:
                    LOGGER.warn(
                        "Unable to apply profile {}: will ".format(
                            ex
                        )+"retry."
                    )
                    time.sleep(self.queue_listen_timeout)
                    
                    if sync:
                        return self._apply(
                            klass, pk, type, count+1, executable, True
                        )
                    else:
                        self.command_queue.put(
                            ["apply", (klass, pk, type, count+1, executable)]
                        )
        except Exception as ex:
            LOGGER.error("Failure to apply via comms: {}".format(
                ex
            ))
            signals.apply_failed.send(
                self.__class__,
                timestamp=now( ),
                message=str(ex),
                exception=ex,
                profile=obj,
                queue_item=item,
                test_mode=self.test_mode,
                type=type
            )

        return False

    def start(self):
        self.running = True
        LOGGER.info("Starting comms thread.")
        super( ).start( )

    def stop(self):
        self.running = False

    def run(self):
        while self.running:
            item = None
            try:
                item = self.command_queue.get(
                    timeout=self.queue_listen_timeout
                )
                obj, type = None, None
                LOGGER.debug("Retrieved item to process {}".format(item))
                if item[0] == "apply":
                    kn, pk, type, count, executable = item[1]
                    
                    self._apply(
                        kn, pk, type, count, executable, sync=False, item=item
                    )
                self.command_queue.task_done( )
            except queue.Empty:
                pass
            except Exception as ex:
                LOGGER.warn(
                    "Unhandled exception processing comms task {}.".format(
                        item
                    ), exc_info=ex
                )

QUEUES = {}
q = QUEUES.get('comms_thread_q')
if not q:
    q = queue.LifoQueue( )
    QUEUES.setdefault('comms_thread', q)
tq = QUEUES.get('comms_thread_q_test')
if not tq:
    tq = queue.LifoQueue( )
    QUEUES.setdefault('comms_thread_q_test', q)

def get_comms_thread(test_mode=False):
    if test_mode:
        return CommsThread(tq, test=test_mode)
    else:
        return CommsThread(q, test=test_mode)