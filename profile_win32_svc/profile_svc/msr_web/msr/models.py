from django.db import models

class CurrentOpenWindow(models.Model):
    text = models.CharField(max_length=512)
    hwnd = models.PositiveIntegerField(unique=True)
    process = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return "{} = {} [{}]".format(
            self.hwnd, self.text, self.process or "N/A"
        )