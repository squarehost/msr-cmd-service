from django.core.cache import caches
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from . import models

import logging

LOGGER = logging.getLogger(__name__)

def _clear_cache( ):
    try:
        lc = caches['listener']
        lc.delete('app_profiles')
        lc.delete('app_profiles_procdel')
    except:
        pass

@receiver(post_save, sender=models.Profile)
def clear_cache_on_profile_save(sender, instance, raw, **kwargs):
    if raw: return
    _clear_cache( )

@receiver(post_save, sender=models.AppProfile)
def clear_cache_on_appprofile_save(sender, instance, raw, **kwargs):
    if raw: return
    _clear_cache( )

@receiver(post_delete, sender=models.Profile)
def clear_cache_on_profile_del(sender, instance, **kwargs):
    _clear_cache( )

@receiver(post_delete, sender=models.AppProfile)
def clear_cache_on_appprofile_del(sender, instance, **kwargs):
    _clear_cache( )
