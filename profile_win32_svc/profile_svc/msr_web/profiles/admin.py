from django.contrib import admin, messages
from django.db import transaction
from django.forms import ModelForm, HiddenInput
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.urls import path, reverse
from django.utils.html import mark_safe
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from . import forms, models
from .utils import get_local_cpu_cores
from msr import utils

import logging, time
LOGGER = logging.getLogger(__name__)

class PLInlineAdmin(admin.TabularInline):
    model = models.PLRegister
    max_num = 2
    extra = 2

class UVInlineAdmin(admin.TabularInline):
    NUM_PLANES = len(models.UVRegister.PLANE_CHOICES)

    model = models.UVRegister
    max_num = NUM_PLANES
    extra = NUM_PLANES

class CoreParkingInlineAdmin(admin.StackedInline):
    model = models.CoreParkRegister
    form = forms.CoreParkingForm
    max_num = 1
    extra = 1
    
class PowerResetRegisterAdmin(admin.StackedInline):
    model = models.PowerResetRegister
    classes = ('collapse', )
    max_num = 1
    extra = 1
    
class PowerDisableWakeRegisterAdmin(admin.StackedInline):
    model = models.PowerDisableWakeRegister
    max_num = 1
    extra = 1

class PowerPriorityRegisterAdmin(admin.TabularInline):
    model = models.PowerPriorityRegister
    max_num = 1
    extra = 1

class ProcFreqRegisterAdmin(admin.StackedInline):
    model = models.ProcFreqRegister
    form = forms.ProcFreqForm
    max_num = 1
    extra = 1

class AppProfileProcessControlForm(ModelForm):
    class Meta:
        model = models.AppProfileProcessControl
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        get_cores = kwargs.get('get_cores')
        if get_cores == None: get_cores = True
        super( ).__init__(*args, **kwargs)

        if get_cores:
            num_cores = get_local_cpu_cores( )
        
            for i in range(num_cores, models.MAX_PROCESSOR_AFFINITY+1):
                self.fields['cpu_{}'.format(i)].widget = HiddenInput( )

class ProcessControlGroupForm(AppProfileProcessControlForm):
    class Meta:
        model = models.ProcessControlGroup
        fields = [
            'name', 'priority'
        ] + [
            'cpu_{}'.format(
                c
            ) for c in range(0, models.MAX_PROCESSOR_AFFINITY+1)
        ]

class ProcessControlGroupApplicationInlineAdmin(admin.StackedInline):
    form = forms.ProcessControlGroupApplicationForm
    model = models.ProcessControlGroupApplication
    extra = 1

class ProcessControlGroupAdmin(admin.ModelAdmin):
    form = ProcessControlGroupForm
    list_display = ('name', 'apps', 'details')
    inlines = (ProcessControlGroupApplicationInlineAdmin, )
    fieldsets = (
        (_("Basic Details"), {
            'fields': ('name', 'priority', )
        }),(_("CPU Affinity"), {
            'fields': [
                'cpu_{}'.format(
                    c
                ) for c in range(0, models.MAX_PROCESSOR_AFFINITY+1)
            ], 'classes': ('collapse', )
        })
    )

    def apps(self, obj):
        return mark_safe(
            "<br />".join([str(a) for a in obj.applications.all( )])
        )

    def details(self, obj):
        dts = obj.controls_to_dict
        if not dts:
            return _("No process controls made")
        else:
            parts = []
            if dts.get('affinity'):
                parts.append(
                    _("Affinity: %(af)s") % {'af': obj.affinity_mask_display}
                )
            if dts.get('priority'):
                parts.append(
                    _("Priority: %(pr)s") % {'pr': obj.get_priority_display( )}
                )
            return mark_safe("<br />".join(parts))
admin.site.register(models.ProcessControlGroup, ProcessControlGroupAdmin)

class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        'active', 'force_test', 'priority', 'name', 'hotkey_display',
        'applied', 'apply_on_boot', 'apply_on_power', 'used_by'
    )
    list_filter = (
        'applied', 'force_test', 'apply_on_boot', 'apply_on_power',
        'shortcut'
    )
    search_fields = ('name', )
    list_display_links = ('name', )
    filter_horizontal = ('process_control_groups', )

    fieldsets = (
        (_("Basic Details"), {
            'fields': (
                'name', 'active', 'shortcut', 'priority',
                'linked_profile'
            )
        }),(_("Hotkey Settings"), {
            'fields': ('hotkey_modifier', 'hotkey', 'hotkey_sticky', 
                       'reset_profile',)
        }), (_("Automation Details"), {
            'fields': ('apply_on_boot', 'apply_on_power',
                       'apply_on_video_adapters')
        }),
        (_("Additional Process Control"), {
            'fields': ('process_control_groups', ),
            'classes': ('collapse', )
        }),
        (_("Scripting"), {
            'fields': (
                'force_test', 'run_on_launch', 'run_on_success',
                'run_on_failure', 'run_on_close',
            ), 'classes': ('collapse', )
        }),
    )
    inlines = (
        UVInlineAdmin, PLInlineAdmin, PowerPriorityRegisterAdmin,
        CoreParkingInlineAdmin, ProcFreqRegisterAdmin,
        
        PowerDisableWakeRegisterAdmin,
        PowerResetRegisterAdmin
    )

    actions = ('apply_immediately', 'export' )
    export_profile_template = 'admin/profiles/export.html'
    import_profile_form_template = 'admin/profiles/import.html'
    import_profile_preview_template = 'admin/profiles/import-preview.html'

    def hotkey_display(self, obj): return obj.hotkey_display or "-"
    hotkey_display.short_description = _("Hotkey")

    @classmethod
    def export(cls, admin, request, queryset):
        return HttpResponseRedirect(
            reverse("admin:export_profiles")+"?profiles={}".format(
                ",".join([
                    str(s) for s in queryset.values_list('pk', flat=True)
                ])
            )
        )
    export.short_description = _("Export Selected Profiles")

    @classmethod
    def apply_immediately(cls, admin, request, queryset):
        if queryset.count( ) > 1:
            cls.message_user(
                cls, request, _("Please select only a single profile to apply."),
                messages.ERROR
            )
        else:
            queryset.first( ).apply( )
            cls.message_user(
                cls, request, _(
                    "Applying profile {}, please check the profile log for "
                    "results.".format(queryset.first( ))
                ), messages.SUCCESS
            )
    apply_immediately.short_description = _("Apply profile immediately")

    def response_change(self, request, obj):
        self.changed_obj = obj
        return super( ).response_change(request, obj)

    def response_add(self, request, obj):
        self.changed_obj = obj
        return super( ).response_add(request, obj)

    @transaction.atomic
    def create_from_current(self, request):
        try:
            cli = utils.get_client( )
            raw, regs = cli.get_current_registers(include_empty=False)
            prof = models.Profile(
                name=_("Current Settings %(dt)s") % {
                    'dt': now( ).strftime("%x %X")
                }, active=False
            )
            prof.save( )
            for reg in regs:
                reg.profile = prof
                reg.save( )

            messages.add_message(
                request, messages.SUCCESS, _(
                    "The profile below has been created from the current "
                    "settings. Please review and modify the profile, and "
                    "once you are happy with the configuration, mark it as "
                    "active and save. Alternatively you can delete the profile "
                    "if you do not wish to keep it."
                )
            )
            return HttpResponseRedirect(
                reverse("admin:profiles_profile_change", args=[prof.pk])
            )

        except Exception as ex:
            LOGGER.warn("Unable to get current settings.", exc_info=ex)
            messages.add_message(
                request, messages.WARNING, _(
                    "Sorry, I was unable to get the current settings to "
                    "create a profile. Please ensure all services are "
                    "running normally. Error was: %(err)s"
                ) % {'err': str(ex)}
            )
            return HttpResponseRedirect(reverse("home"))

    @transaction.atomic
    def export_profiles(self, request):
        if request.method == 'POST':
            profs = []
            for k in request.POST.keys( ):
                if 'profile_' in k:
                    profs.append(k.split("_")[-1])
            profs = models.Profile.objects.filter(
                pk__in=profs
            )
            if not profs.count( ):
                messages.add_message(
                    request, messages.ERROR, _(
                        "Please select some profiles to export."
                    )
                )
            else:
                json = profs.to_export_json(
                    include_linked=request.POST.get('include_linked'),
                    convert_app_paths=request.POST.get('remap')
                )
                if profs.count( ) == 1:
                    fname = "msr-profile-{}.json".format(
                        slugify(str(profs[0]))
                    )
                else:
                    fname = "msr-profiles-{}-{}.json".format(
                        profs.count( ), now( ).strftime("%Y%m%d_%H%M")
                    )
                
                resp = HttpResponse(json, content_type='application/json')
                resp['Content-Disposition'] = 'attachment; filename=' + fname
                return resp
            
        profs = models.Profile.objects.none( )
        if request.GET.get('profiles'):
            profs = models.Profile.objects.filter(
                pk__in=request.GET['profiles'].split(",")
            )
        if not profs.count( ):
            profs = models.Profile.objects.all( )

        if not profs.count( ):
            return HttpResponseRedirect(reverse("home"))

            
        context = {
            **self.admin_site.each_context(request),
            'profile_count': profs.filter(appprofile=None).count( ),
            'app_count': profs.exclude(appprofile=None).count( ),
            'profiles': profs.order_by('appprofile', 'name'),
            'cl': self.get_changelist_instance(request)
        }

        return render(
            request, self.export_profile_template, context
        )

    @transaction.atomic
    def import_profiles(self, request):
        if request.method == 'GET':
            context = {
                **self.admin_site.each_context(request),
                'cl': self.get_changelist_instance(request)
            }

            return render(
                request, self.import_profile_form_template, context
            )
        elif request.POST.get('process'):
            data = request.session['_import_data']
            with transaction.atomic( ):
                added, changed = models.Profile.objects.from_export_json(
                    data, verify_only=False
                )

                messages.add_message(request, messages.SUCCESS, _(
                    "Thank you, %(add)d profile(s) added (%(changed)d changed)."
                ) % {'add': len(added), 'changed': len(changed)})

            return HttpResponseRedirect(
                reverse("admin:profiles_profile_changelist")
            )
        else:
            prof = request.FILES.get('profile_file')
            try:
                to_add, to_change = models.Profile.objects.from_export_json(
                    prof, verify_only=True
                )
                prof.seek(0)
                request.session['_import_data'] = prof.read( ).decode("utf-8")
                context = {
                    **self.admin_site.each_context(request),
                    'cl': self.get_changelist_instance(request),
                    'to_add': to_add, 'to_change': to_change
                }

                return render(
                    request, self.import_profile_preview_template, context
                )
            except Exception as vex:
                LOGGER.error("Unable to process export JSON", exc_info=vex)
                messages.add_message(
                    request, messages.ERROR, _(
                        "Please ensure you have supplied a valid profile "
                        "file: %(exc)s"
                    ) % {'exc': vex}
                )
                return HttpResponseRedirect(request.path)


    def get_urls(self):
        urls = [
            path('from-current/',
                 self.admin_site.admin_view(self.create_from_current),
                 name='profiles_profile_add_from_current'
            ),
            path('export/',
                self.admin_site.admin_view(self.export_profiles),
                name='export_profiles'
            ),
            path('import/',
                self.admin_site.admin_view(self.import_profiles),
                name='import_profiles'
            )
        ]
        return urls + super( ).get_urls( )

    def _changeform_view(self, request, object_id, form_url, extra_context):
        view = super( )._changeform_view(
            request, object_id, form_url, extra_context
        )
        if request.method == 'POST' and '_saveapply' in request.POST:            
            changed = getattr(self, 'changed_obj', None)
            if changed:
                time.sleep(1)
                self.apply_immediately(
                    self, request,
                    self.get_queryset(request).filter(pk=changed.pk)
                )
        return view

    def used_by(self, obj):
        details = self.details(obj)
        ch = obj.child_profiles.all( )
        if not ch.count( ):
            return mark_safe("<abbr title='{}'>{}</abbr>".format(
                details, _("Standalone")
            ))
        else:
            return mark_safe("<abbr title='{}'>{}</abbr>".format(
                details, "<br />".join([p.name for p in ch])
            ))

    def get_queryset(self, request):
        return super( ).get_queryset(request).exclude(
            pk__in=models.AppProfile.objects.values_list('pk', flat=True)
        )

    def details(self, obj):
        empty = True
        for title, parts in obj.details:
            if parts:
                empty = False
                break
        
        if empty:
            return _("Empty Profile")

        strn = ''
        for title, parts in obj.details:
            if parts:
                strn += "{}: ".format(title)
                strn += ", ".join([str(p) for p in parts])
                strn += ". "
        
        return strn
admin.site.register(models.Profile, ProfileAdmin)

class AppProfileProcessControlInline(admin.TabularInline):
    model = models.AppProfileProcessControl
    form = AppProfileProcessControlForm
    extra = 0
    max_num = 1

class AppProfileAdmin(ProfileAdmin):
    list_display = ('active', 'force_test', 'name', 'applied', )
    list_filter = ProfileAdmin.list_filter + ('match_type', 'force_test')
    exclude = ('apply_on_boot', )
    list_display_links = ('name', )
    inlines = (
        AppProfileProcessControlInline,
    ) + ProfileAdmin.inlines

    form = forms.AppProfileForm
    fieldsets = (
        (_("Basic Details"), {
            'fields': ('active', 'name', 'linked_profile')
        }),
        (_("Application Details"), {
            'fields': ('match_info', 'match_type')
        }),
        (_("Strategy"), {
            'fields': ('apply_on_power', 'reset_profile',
                       'apply_on_video_adapters')
        }),
        (_("Scripting"), {
            'fields': ('gpu_option', 'run_on_launch', 'run_on_success',
                       'run_on_failure', 'run_on_close', 'force_test',),
            'classes': ('collapse', )
        }),
        (_("Additional Process Control"), {
            'fields': ('process_control_groups', ),
            'classes': ('collapse', )
        }),
    )

    actions = ('export', )
    export = ProfileAdmin.export

    def get_queryset(self, request):
        return admin.ModelAdmin.get_queryset(self, request)

admin.site.register(models.AppProfile, AppProfileAdmin)

class ProfileLogAdmin(admin.ModelAdmin):
    list_display = ('applied', 'success', 'type', 'profile_name', 'short_msg')
    list_filter = ('applied', 'type', 'success', )
    readonly_fields = (
        'applied', 'type', 'profile_name', 'success',

        'messages', 'details'
    )
    fieldsets = (
        (_('Basic Info'), {
            'fields': ('applied', 'type', 'success', 'profile_name', )
        }), (_('Details'), {
            'fields': ('messages', 'details')
        })
    )

    def short_msg(self, obj):
        return (obj.messages or '')[0:50]+"..."
    short_msg.description = _("Short Message")

    def has_add_permission(self, request): return False
    
admin.site.register(models.ProfileLog, ProfileLogAdmin)

'''class RestoreProfileAdmin(admin.ModelAdmin):
    def no_perm(self, request, obj=None): return False
    has_add_permission = has_change_permission = has_delete_permission = no_perm
admin.site.register(models.RestoreProfile, RestoreProfileAdmin)'''