from django import forms
from django.conf import settings
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from . import models, utils, widgets

from collections import OrderedDict
import copy, logging, os

LOGGER = logging.getLogger(__name__)

class ProcFreqForm(forms.ModelForm):
    class Meta:
        model = models.ProcFreqRegister
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        self.min_freq = kwargs.pop('min_freq', None)
        self.max_freq = kwargs.pop('max_freq', None)
        self.bus = kwargs.pop('bus', None)

        if not self.min_freq or not self.max_freq or not self.bus:
            try:
                from msr import utils as msr_utils
                cli = msr_utils.get_client( )
                raw, regs = cli.get_current_registers(
                    ['HWP', 'UNCORE'], raw=True
                )
                if not self.bus:
                    self.bus = raw['HWP']['bus'] or 100
                if not self.min_freq:
                    self.min_freq = raw['UNCORE']['min'] * self.bus
                if not self.max_freq:
                    self.max_freq = raw['HWP']['highest'] * self.bus

            except Exception as ex:
                LOGGER.warn(
                    "Unable to query read values for form validation.",
                    exc_info=ex
                )
                pass
        
        super( ).__init__(*args, **kwargs)

        if self.min_freq:
            self.fields['min_freq'].help_text += _(
                " Please note, your CPU indicates the minimum achievable "
                "speed is currently %(spd)dMHz. Setting values below this "
                "will likely have no effect."
            ) % {'spd': self.min_freq}

        if self.max_freq:
            self.fields['max_freq'].help_text += _(
                " Please note, your CPU indicates the maximum achievable "
                "speed is currently %(spd)dMHz, including single core turbo "
                "values. Setting values above this will likely have no effect."
            ) % {'spd': self.max_freq}

class CoreParkingForm(forms.ModelForm):
    class Meta:
        model = models.CoreParkRegister
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.max_cores = kwargs.pop('max_cores', None)
        if not self.max_cores:
            try:
                from msr import utils as msr_utils
                cli = msr_utils.get_client( )
                self.max_cores = (
                    cli.get_current_registers(['PARK'])[1][0].max_avail
                )
            except:
                pass

            #DEFAULT_MAX_PARKED_CORES
            if not self.max_cores:
                self.max_cores = settings.DEFAULT_MAX_PARKED_CORES

        super( ).__init__(*args, **kwargs)

        if self.max_cores:
            for fld in ('min_parked', 'max_parked'):
                self.fields[fld].help_text += _(
                    " Your system currently advertises a maximum "
                    "number of %(core)d physical core(s) available to park."
                ) % {'core': self.max_cores}

    def clean_min_parked(self):
        vl = self.cleaned_data.get('min_parked')
        if vl:
            if vl > self.max_cores:
                raise forms.ValidationError(
                    _("A maximum of %(core)d cores can be parked.") % {
                        'core': self.max_cores 
                    }
                )
        return vl

    def clean_max_parked(self):
        vl = self.cleaned_data.get('max_parked')
        if vl:
            if vl > self.max_cores:
                raise forms.ValidationError(
                    _("A maximum of %(core)d cores can be parked.") % {
                        'core': self.max_cores
                    }
                )
        return vl

class MatchTextInput(forms.TextInput):
    SIZE = 50
    template_name = 'forms/widgets/match-text.html'

    def __init__(self, attrs=None):
        if attrs == None:
            attrs = { }
        else:
            attrs = copy.copy(attrs)
        
        if 'size' not in attrs: attrs['size'] = self.SIZE
        super( ).__init__(attrs)

class MatchTextProcGroupInputMultiple(MatchTextInput):
    template_name = 'forms/widgets/match-text-proc-group.html'

class AppProfileForm(forms.ModelForm):
    class Meta:
        model = models.AppProfile
        fields = '__all__'
        widgets = {
            'app_path': widgets.AppListSelect
        }

    match_info = forms.CharField(
        label=_("Match Text"), help_text=_(
            "The text used to match this application. Should be either the "
            "filename or the application name."
        ), widget=MatchTextInput
    )

    def __init__(self, *args, **kwargs):
        super( ).__init__(*args, **kwargs)
        self.initial['match_info'] = self.instance.match_string

        # We auto name in model clean method if name isn't specified.
        self.fields['name'].required = False
        self.fields['name'].help_text= _(
            "Specify a name for this app profile, or leave blank for the "
            "system to generate one for you."
        )

    def clean(self):
        dat = super( ).clean( )
        dat.update({
            'manual_app_path': dat['match_info'],
            'app_path': None
        })
        self.instance.manual_app_path = dat['match_info']
        self.instance.app_path = None
        return dat

class ProcessControlGroupApplicationForm(forms.ModelForm):
    class Meta:
        model = models.ProcessControlGroupApplication
        exclude = (
            'app_path', 'manual_app_path'
        )
        widgets = {
            'app_path': widgets.AppListSelect
        }

    match_info = forms.CharField(
        label=_("Match Text"), help_text=_(
            "The text used to match this application. Should be either the "
            "filename or the application name."
        ), widget=MatchTextProcGroupInputMultiple
    )
    
    def __init__(self, *args, **kwargs):
        super( ).__init__(*args, **kwargs)
        self.initial['match_info'] = self.instance.match_string

    def clean(self):
        dat = super( ).clean( )
        dat.update({
            'manual_app_path': dat['match_info'],
            'app_path': None
        })
        self.instance.manual_app_path = dat['match_info']
        self.instance.app_path = None
        return dat

class AppPathChoiceForm(forms.Form):
    folder = forms.ChoiceField(
        help_text=_("Choose from a list of detected commonly used folders."),
        required=False
    )
    override_folder = forms.CharField(
        help_text=_(
            "If you cannot find the folder in the list, enter the full path "
            "here."
        ), required=False
    )

    def clean(self):
        if not models.Profile.objects.filter(
            active=True, appprofile=None
        ).count( ):
            raise forms.ValidationError(_(
                "There are no profiles configured. You must configure at "
                "least one standard profile before setting up an app path."
            ))
        cd = super( ).clean( )
        has = 0
        for key in ('folder', 'override_folder'):
            if key in cd and (cd.get(key) or '').strip( ):
                has += 1
                
        if not has:
            raise forms.ValidationError({
                'folder': _(
                    "Choose a folder from the list or enter a "
                    "valid override folder."
                )
            })
        elif has > 1:
            raise forms.ValidationError({
                'folder': _(
                    "Please choose a folder OR an override folder, not both."
                )
            })

        return cd

    def clean_override_folder(self):
        val = self.cleaned_data.get('override_folder')
        if val:
            drn = os.path.abspath(val)
            try:
                if not os.path.exists(drn):
                    raise ValueError
            except:
                raise forms.ValidationError(_(
                    "This is not a valid folder, or it cannot be "
                    "accessed."
                ))
            return drn
    
    def __init__(self, *args, **kwargs):
        folder_choices = kwargs.pop('folder_choices', [])
        super( ).__init__(*args, **kwargs)

        self.fields['folder'].choices = folder_choices
        self.fields['override_folder'].widget.attrs['size'] = 100

class AppPathFileForm(forms.Form):
    default = forms.MultipleChoiceField(
        label=_("Default Profile"),
        help_text=_(
            "Select one or more default profiles to apply to all apps within "
            "this folder. When multiple profiles are selected, the priority "
            "of the profiles and any relevant rules (such as AC/DC) will be "
            "used to determine which profile should be applied when a game is "
            "launched. To remove existing profiles, unselect any profiles."
        ), required=False
    )

    def _get_override_profile_for_app(self, full_path):
        if not full_path.endswith(os.path.sep):
            full_path += os.path.sep
            
        if not hasattr(self, 'folder_cache'):
            self.folder_cache = {}

        if full_path in self.folder_cache:
            return self.folder_cache[full_path]

        obj = None
        prof = models.AppProfile.objects.filter(
            Q(
                app_path__iexact=full_path
            ) | Q(
                manual_app_path__iexact=full_path
            ), active=True, match_type='defl'
        )
        if prof.count( ):
            self.folder_cache[full_path] = obj
            return obj
        
        return None

        '''if not obj:
            parts = os.path.dirname(full_path)
            while parts:
                if parts in self.folder_cache:
                    return self.folder_cache[parts]

                exact = models.AppProfile.objects.filter(
                    Q(
                        app_path__iexact=parts
                    ) | Q(
                        manual_app_path__iexact=parts
                    ), active=True, match_type='defl'
                )
                if exact.count( ):
                    obj = exact.first( )
                    self.folder_cache[parts] = obj
                    return obj
        
                parts = os.sep.join(parts.split(os.sep)[0:-1])

        return None'''

    def __init__(self, *args, **kwargs):
        main_folder = kwargs.pop('main_folder')
        self.main_folder = main_folder
        super( ).__init__(*args, **kwargs)
        
        main_folder = main_folder.rstrip(os.path.sep)
        if not main_folder.endswith(os.path.sep):
            main_folder += os.path.sep

        last_folder = None
        apps = []
        subl = []
        for path, folder, fls in utils.find_apps_in_folder(main_folder):
            if last_folder and folder != last_folder:
                apps.append((last_folder, subl))
                last_folder = folder
            elif not last_folder:
                last_folder = folder

            subl.append((path, fls))

        if subl: apps.append((folder, subl))
        
        apps.sort(key=lambda x: (x[0], x[1][-1]))

        base = models.AppProfile.objects.filter(
            Q(
                app_path__iexact=main_folder
            ) | Q(
                manual_app_path__iexact=main_folder
            ),
            active=True, match_type='defl'
        )
        base_profiles = models.Profile.objects.filter(
            active=True, appprofile=None
        )
        if base.count( ):
            defl = [
                item.linked_profile_id or item.pk
                for item in base
            ]
            self.fields['default'].initial = defl
        chc = [
            (_("Standard Profiles"), [
                (p.pk, p.name) for p in base_profiles
            ])
        ]

        self.fields['default'].choices = chc

        self.folder_fields = OrderedDict( )
        for folder, exes in apps:
            if not exes: continue
            self.folder_fields[folder] = []

            uniq_folders = set()
            for e in exes:
                d = os.path.dirname(e[0])
                uniq_folders.add(d)
            
            uniq_folders = list(uniq_folders)
            uniq_folders.sort( )
            for uf in uniq_folders:
                prof = self._get_override_profile_for_app(uf)
                if prof:
                    prof = [
                        item.linked_profile_id or item.pk
                        for item in prof
                    ]
                nm = uf[len(main_folder) + len(folder) + 2:] or ''
                self.fields[uf] = fld = forms.MultipleChoiceField(
                    label=nm, choices=chc,
                    required=False, initial=prof or None
                )
                fld.full_folder = uf
                self.folder_fields[folder].append(fld)