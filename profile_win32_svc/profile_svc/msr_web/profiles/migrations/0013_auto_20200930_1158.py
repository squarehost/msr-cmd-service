# Generated by Django 3.1.1 on 2020-09-30 10:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0012_auto_20200930_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='apply_on_power',
            field=models.CharField(blank=True, choices=[('ac_power', 'Charger attached (AC Power)'), ('dc_power', 'Charger removed (Battery/DC Power)'), ('battery_10', 'Battery below 10%'), ('battery_20', 'Battery below 20%'), ('battery_30', 'Battery below 30%'), ('battery_50', 'Battery below 50%'), ('battery_80', 'Battery below 80%'), ('battery_100', 'Battery below 100%'), ('battery_full', 'Battery Fully Charged')], db_index=True, help_text='Select an option from this list to automatically apply the profile on a power status change.', max_length=12, null=True, verbose_name='apply on power status change'),
        ),
        migrations.AlterField(
            model_name='profilelog',
            name='type',
            field=models.CharField(choices=[('manual', 'Manual (User) Applied'), ('auto_boot', 'Automatic (Applied on Boot)'), ('auto_susp', 'Automatic (Applied on Suspend)'), ('auto_app', 'Automatic (App Profile)'), ('auto_pwr', 'Automatic (Power Event)'), ('restore', 'Restored (App Profile)')], db_index=True, editable=False, max_length=9),
        ),
    ]
