# Generated by Django 3.1.1 on 2020-09-23 15:19

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_plregister_uvregister'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='plregister',
            options={'verbose_name': 'Power Limit Register'},
        ),
        migrations.AlterModelOptions(
            name='uvregister',
            options={'verbose_name': 'Voltage Offset (UV) Register'},
        ),
        migrations.AddField(
            model_name='profile',
            name='applied',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='uvregister',
            name='plane',
            field=models.CharField(choices=[('cpu', 'CPU Core'), ('gpu', 'GPU Core'), ('cache', 'CPU Cache'), ('sys', 'System Agent')], db_index=True, help_text='Select the component to undervolt.', max_length=5, verbose_name='component'),
        ),
    ]
