# Generated by Django 3.1.3 on 2021-01-28 18:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0042_auto_20210128_1841'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppProfileProcessControl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority', models.PositiveIntegerField(blank=True, choices=[(15, '+++ Realtime Critical Priority [!]'), (2, '++ Highest Priority'), (1, '+ Above Normal Priority'), (0, 'Normal Priority'), (-1, '- Below Normal Priority'), (-2, '-- Lowest Priority')], help_text='If you wish to adjust the system priority level of aprocess, select it here. Note, it will be applied once the application profile has been applied.', null=True)),
                ('cpu_0', models.BooleanField(default=False, help_text='Whether this process should run on CPU 0', verbose_name='CPU 0')),
                ('cpu_1', models.BooleanField(default=False, help_text='Whether this process should run on CPU 1', verbose_name='CPU 1')),
                ('cpu_2', models.BooleanField(default=False, help_text='Whether this process should run on CPU 2', verbose_name='CPU 2')),
                ('cpu_3', models.BooleanField(default=False, help_text='Whether this process should run on CPU 3', verbose_name='CPU 3')),
                ('cpu_4', models.BooleanField(default=False, help_text='Whether this process should run on CPU 4', verbose_name='CPU 4')),
                ('cpu_5', models.BooleanField(default=False, help_text='Whether this process should run on CPU 5', verbose_name='CPU 5')),
                ('cpu_6', models.BooleanField(default=False, help_text='Whether this process should run on CPU 6', verbose_name='CPU 6')),
                ('cpu_7', models.BooleanField(default=False, help_text='Whether this process should run on CPU 7', verbose_name='CPU 7')),
                ('cpu_8', models.BooleanField(default=False, help_text='Whether this process should run on CPU 8', verbose_name='CPU 8')),
                ('cpu_9', models.BooleanField(default=False, help_text='Whether this process should run on CPU 9', verbose_name='CPU 9')),
                ('cpu_10', models.BooleanField(default=False, help_text='Whether this process should run on CPU 10', verbose_name='CPU 10')),
                ('cpu_11', models.BooleanField(default=False, help_text='Whether this process should run on CPU 11', verbose_name='CPU 11')),
                ('cpu_12', models.BooleanField(default=False, help_text='Whether this process should run on CPU 12', verbose_name='CPU 12')),
                ('cpu_13', models.BooleanField(default=False, help_text='Whether this process should run on CPU 13', verbose_name='CPU 13')),
                ('cpu_14', models.BooleanField(default=False, help_text='Whether this process should run on CPU 14', verbose_name='CPU 14')),
                ('cpu_15', models.BooleanField(default=False, help_text='Whether this process should run on CPU 15', verbose_name='CPU 15')),
                ('cpu_16', models.BooleanField(default=False, help_text='Whether this process should run on CPU 16', verbose_name='CPU 16')),
                ('process', models.OneToOneField(blank=True, help_text='If you wish to ensure this application process runs only on specific CPU cores, you can adjust the affinity accordingly. Note, these are logical cores, so if your CPU supports hyperthreading there will be two logical cores here per physical core.', null=True, on_delete=django.db.models.deletion.CASCADE, to='profiles.appprofile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.DeleteModel(
            name='AppProfileAffinity',
        ),
    ]
