# Generated by Django 3.1.3 on 2021-01-27 16:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0035_auto_20210127_1305'),
    ]

    operations = [
        migrations.CreateModel(
            name='CPUPowerPolicy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('balance', models.PositiveIntegerField(blank=True, choices=[(1, 'r (Lowest Priority)'), (2, 'r'), (3, 'r'), (4, 'r'), (5, 'r'), (6, 'r'), (7, 'r'), (8, 'r'), (9, 'r (Default)'), (10, 'r'), (11, 'r'), (12, 'r'), (13, 'r'), (14, 'r'), (15, 'r'), (16, 'r'), (17, 'r'), (18, 'r'), (19, 'r'), (20, 'r'), (21, 'r'), (22, 'r'), (23, 'r'), (24, 'r'), (25, 'r'), (26, 'r'), (27, 'r'), (28, 'r'), (29, 'r'), (30, 'r'), (31, 'r (Highest Priority)')], help_text='Adjust the power priority for the CPU Power Balance. Higher values will request the SoC to prioritise the CPU cores over the GPU.', null=True)),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cpupowerpolicy_set', to='profiles.profile')),
            ],
            options={
                'verbose_name': 'CPU Power Balance Policy',
                'verbose_name_plural': 'CPU Power Balance Policy',
            },
        ),
        migrations.CreateModel(
            name='GPUPowerPolicy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('balance', models.PositiveIntegerField(blank=True, choices=[(1, 'r (Lowest Priority)'), (2, 'r'), (3, 'r'), (4, 'r'), (5, 'r'), (6, 'r'), (7, 'r'), (8, 'r'), (9, 'r'), (10, 'r'), (11, 'r'), (12, 'r'), (13, 'r (Default)'), (14, 'r'), (15, 'r'), (16, 'r'), (17, 'r'), (18, 'r'), (19, 'r'), (20, 'r'), (21, 'r'), (22, 'r'), (23, 'r'), (24, 'r'), (25, 'r'), (26, 'r'), (27, 'r'), (28, 'r'), (29, 'r'), (30, 'r'), (31, 'r (Highest Priority)')], help_text='Adjust the power priority for the FPU Power Balance. Higher values will request the SoC to prioritise the FPU over the CPU cores.', null=True)),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gpupowerpolicy_set', to='profiles.profile')),
            ],
            options={
                'verbose_name': 'GPU Power Balance Policy',
                'verbose_name_plural': 'GPU Power Balance Policy',
            },
        ),
        migrations.DeleteModel(
            name='PowerBalanceRegister',
        ),
    ]
