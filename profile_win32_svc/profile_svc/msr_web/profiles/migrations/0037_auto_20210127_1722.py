# Generated by Django 3.1.3 on 2021-01-27 17:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0036_auto_20210127_1645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gpupowerpolicy',
            name='balance',
            field=models.PositiveIntegerField(blank=True, choices=[(1, 'r (Lowest Priority)'), (2, 'r'), (3, 'r'), (4, 'r'), (5, 'r'), (6, 'r'), (7, 'r'), (8, 'r'), (9, 'r'), (10, 'r'), (11, 'r'), (12, 'r'), (13, 'r (Default)'), (14, 'r'), (15, 'r'), (16, 'r'), (17, 'r'), (18, 'r'), (19, 'r'), (20, 'r'), (21, 'r'), (22, 'r'), (23, 'r'), (24, 'r'), (25, 'r'), (26, 'r'), (27, 'r'), (28, 'r'), (29, 'r'), (30, 'r'), (31, 'r (Highest Priority)')], help_text='Adjust the power priority for the GPU Power Balance. Higher values will request the SoC to prioritise the GPU over the CPU Cores.', null=True),
        ),
    ]
