from django.apps import AppConfig
from django.conf import settings
from django.core.cache import caches

CACHE = caches['profile_app']

from . import utils

class ProfilesConfig(AppConfig):
    name = 'profiles'

    def ready(self):
        from . import signals
        self.setup_registers( )

    @property
    def installed_app_list(self):
        '''
            This is quite an expensive function, should we cache this for a
            period of time?
        '''
        return utils.get_installed_apps( )

    @property
    def running_app_list(self):
        '''
            This is quite an expensive function, should we cache this for a
            period of time?
        '''
        res = CACHE.get('running-apps')
        if res: return res
        res = utils.get_running_apps( )
        CACHE.set('running-apps', res, settings.APP_LIST_CACHE_TIME)
        return res

    def setup_registers(self):
        self.registers = []

        from .models import Register
        for name, model in self.models.items( ):
            if Register in model.__bases__:
                fkey = model._meta.get_field('profile')
                self.registers.append((
                    name, model, fkey.remote_field.get_accessor_name( )
                ))

    def get_registers(self):
        return self.registers
