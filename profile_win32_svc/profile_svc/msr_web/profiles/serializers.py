from .models import Profile

from rest_framework import serializers

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'id', 'name', 'slug', 'shortcut', 'priority',
            'apply_on_boot', 'apply_on_power', 'created', 'applied',
            'hotkey_value', 'details', 'admin_link'
        ]
        read_only_fields = fields

    admin_link = serializers.HyperlinkedIdentityField(
        view_name='admin:profiles_profile_change',
        lookup_field='pk', lookup_url_kwarg='object_id'
    )