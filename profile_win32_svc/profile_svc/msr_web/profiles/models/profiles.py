from deepmerge import always_merger
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.serializers import serialize
from django.db import models, transaction
from django.db.models import Q
from django.utils.text import slugify
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

import functools, logging, json, math, os, subprocess, string, threading, time

from .. import utils
from .registers import Register
from .process_groups import AppSelectionMixin, ProcessControlMixin
from msr.comms_thread import get_comms_thread

LOGGER = logging.getLogger(__name__)
COMMS = get_comms_thread(test_mode=settings.MSR_CLIENT_TEST_MODE)

class ProfileManagerQueryset(models.QuerySet):
    @classmethod
    def get_hardware_count_filters(cls):
        hcf = {}
        for lst in utils.get_hardware_count_listeners( ):
            hcf[lst.get_status_field_name( )] = (
                lst.get_required_model_field( ), lst.comparator
            )
        return hcf

    def filter_for_system_status(
        self, status={ }, exclude_status=[], wmi_instance=None,
        exact=False, include_linked=True
    ):
        if not self.count( ): return self

        if not status: status = utils.get_system_status(
            wmi_instance=wmi_instance, exclude=exclude_status
        )

        LOGGER.debug("Filtering for system status {}".format(status))
        filt = self.all( )
        if 'on_battery' in status or 'battery_percent' in status:
            if 'battery_percent' in status and 'on_battery' not in status:
                status['on_battery'] = True

            if not status.get('on_battery'):
                filt = filt.filter(
                    Q(apply_on_power=None) | Q(apply_on_power__startswith='ac')
                )
            else:
                bp = status.get('battery_percent')
                if bp >= 100:
                    qarg = Q(apply_on_power='battery_100')
                else:
                    if exact:
                        qarg = Q(apply_on_power='battery_{}'.format(bp))
                    else:
                        closest = math.ceil(bp / 10.0) * 10
                        qarg = Q(apply_on_power__in=[
                            'battery_{}'.format(p) for p in range(
                                closest, 101, 10
                            )
                        ])
                filt = filt.filter(
                    Q(apply_on_power=None) | Q(apply_on_power='dc_power') | qarg
                )
        
        hcf = self.get_hardware_count_filters( )
        for k in hcf:
            if k in status:
                hc = hcf[k]
                filt = filt.filter(Q(
                    **{hc[0]: None}
                ) | Q(
                    **{"{}__{}".format(*hc): status[k]}
                ))
                
        LOGGER.debug("Filtering query is {}".format(filt.query))

        LOGGER.debug("Matching profiles are ({}) {}".format(
            filt.count( ), filt
        ))
        return filt

    def to_export_json(self, include_linked=True, convert_app_paths=False):
        app_profs = AppProfile.objects.filter(
            pk__in=self.exclude(appprofile=None).values_list(
                'pk', flat=True
            )
        )
        profs = self.all( )

        items = json.loads(serialize(
            "json", list(profs) + list(app_profs),
            use_natural_primary_keys=True,
            use_natural_foreign_keys=False
        ))
        all_items = []
        for item in items:
            flds = item['fields']
            lp = flds.get('linked_profile')
            if lp:
                if not include_linked:
                    flds['linked_profile'] = None
                else:
                    flds['linked_profile'] = (
                        json.loads(
                            Profile.objects.filter(
                                pk=flds['linked_profile']
                            ).to_export_json(
                                include_linked=True,
                                convert_app_paths=convert_app_paths
                            )
                        )[0]
                    )

            if item['model'] == 'profiles.appprofile':
                if convert_app_paths and flds.get('match_type') == 'path':
                    flds['match_type'] = 'exe'
                item['profile_slug'] = Profile.objects.get(
                    pk=flds['profile_ptr']
                ).slug
            else:
                item['payload'] = Profile.objects.get_by_natural_key(
                    slug=flds['slug']
                ).to_json(with_linked=False)
            all_items.append(item)
        return json.dumps(items, indent=4)

class ProfileManager(models.Manager):
    def from_export_json(self, handle, verify_only=True):
        if isinstance(handle, str):
            dat = json.loads(handle)
        else:
            dat = json.loads(handle.read( ))

        processed = []
        linked_slugs = {}
        to_add, to_change = [], []
        add_slugs = []

        for prof in dat:
            flds = prof['fields']

            if flds.get('linked_profile'):
                if not isinstance(flds['linked_profile'], dict):
                    flds['linked_profile'] = None
                else:
                    linked_slugs[
                        flds['linked_profile']['fields']['slug']
                    ] = flds['linked_profile']

        for prof in dat:
            flds = prof['fields']
            if prof['model'] == "profiles.profile":
                try:
                    Profile.objects.get_by_natural_key(flds['slug'])
                    to_change.append(prof)
                except Profile.DoesNotExist:
                    to_add.append(prof)
                    add_slugs.append(flds['slug'])
                prof['payload']
            elif prof['model'] == "profiles.appprofile":
                if prof['profile_slug'] not in add_slugs:
                    try:
                        Profile.objects.get_by_natural_key(prof['profile_slug'])
                    except Profile.DoesNotExist:
                        raise ValueError(
                            "App profile {} does not have base {}".format(
                                flds['filename'], prof['profile_slug']
                            )+", cannot import."
                        )
                try:
                    ap = AppProfile.objects.filter(
                        app_path=flds['app_path']
                    ).first( )
                    if not ap: raise AppProfile.DoesNotExist
                    to_change.append(prof)
                except AppProfile.DoesNotExist:
                    to_add.append(prof)

        for slug, data in linked_slugs.items( ):
            if slug not in add_slugs:
                try:
                    Profile.objects.get(slug=slug)
                    to_change.append(data)
                except Profile.DoesNotExist:
                    raise ValueError(
                        "Linked profile {} does not exist, cannot ".format(
                            slug
                        )+"import."
                    )

        if verify_only:
            return (to_add, to_change)
        else:
            processed = []
            profs, apps = list(linked_slugs.values( )), []

            for prof in to_add + to_change:
                if prof['model'] == 'profiles.appprofile':
                    apps.append(prof)
                else:
                    profs.append(prof)

            added, changed = [], []
            profs.sort(
                key=lambda x: int(x['fields'].get('linked_profile') != None)
            )
            for p in profs:
                flds = p['fields']
                if flds['slug'] in processed: continue
                processed.append(flds['slug'])
                new = False
                try:
                    prf = Profile.objects.get_by_natural_key(
                        flds['slug']
                    )
                except Profile.DoesNotExist:
                    prf = Profile(slug=flds['slug'])
                    new = True

                for k, v in flds.items( ):
                    if k != 'linked_profile':
                        setattr(prf, k, v)
                    
                lp = flds.get('linked_profile')
                if lp: assert False, lp
                if lp and isinstance(lp, dict):
                    try:
                        lnkp = Profile.objects.get_by_natural_key(
                            slug=lp['fields']['slug']
                        )
                        try:
                            if lnkp.appprofile:
                                lnkp = lnkp.appprofile
                        except:
                            pass
                    except Profile.DoesNotExist:
                        raise ValueError(
                            "Could not find linked profile {} for {}, ".format(
                                lp['fields']['slug'], prf
                            )+" cannot import.")
                    
                    prf.linked_profile = lnkp
                
                prf.save( )
                prf.delete_registers( )
                regs = Profile.registers_from_json(
                    p['payload'], include_empty=False, profile=prf
                )
                for r in regs: r.save( )

                if new:
                    added.append(prf)
                else:
                    changed.append(prf)

            for p in apps:
                prof = Profile.objects.get_by_natural_key(
                    slug=p['profile_slug']
                )
                new = False
                try:
                    ap = prof.appprofile
                    if not ap: raise ValueError
                except:
                    ap = AppProfile(pk=prof.pk)
                    new = True

                flds = json.loads(serialize(
                    "json", [prof]
                ))[0]

                for k, v in flds['fields'].items( ):
                    setattr(ap, k, v)
                for k,v in p['fields'].items( ):
                    if k == "profile_ptr": continue
                    setattr(ap, k, v)
                
                ap.save() 

                if new:
                    added.append(ap)
                else:
                    changed.append(ap)

            return (added, changed)

    def get_queryset(self):
        return ProfileManagerQueryset(self.model, using=self._db)

    def get_by_natural_key(self, slug):
        return self.get(slug=slug)

    def apply_last(
        self, apply_type=None, restore_from=None, exclude=None, sync=False
    ):
        '''
            Re-apply the most recently applied standard profile, if it
            exists. Otherwise return None
        '''
        last = self.all( ).order_by('-applied').filter(
            active=True, appprofile=None
        )
        if exclude: last = last.exclude(pk=exclude)
        force = self.filter(active=True, force_test=True).order_by('-priority')
        if force.count( ):
            LOGGER.info(
                "Overriding last profile with force profile: {}".format(
                    force[0]
                )
            )
            last = force

        if last.count( ):
            last = last.first( )
            LOGGER.info(
                "Restoring last applied profile {}".format(
                    last
                )
            )
            last.apply(type=apply_type or 'manual', sync=sync)
            return last
        return None
            
    def apply_on_boot(self, force=True, apply_type=None, sync=False):
        '''
            Check to see we are allowed to (e.g. no RestoreProfile) apply
            a profile on boot, and if we are, iterate through all active
            on boot profiles, picking out the best matching candidate
            dependant on power profile. :-)
        '''
        rp = self.filter(active=True, apply_on_boot=True)

        forced= self.filter(active=True, force_test=True).order_by('-priority')
        if forced.count( ):
            LOGGER.info(
                "Overriding boot profile with force profile: {}".format(
                    forced[0]
                )
            )
            rp = forced
            force = True
        if not rp.count( ): return

        if not force and RestoreProfile.objects.all( ).count( ):
            LOGGER.warn(
                "Not applying boot profile {}, as RestoreProfiles {} ".format(
                    rp, self.all( )
                )+"exist."
            )
            RestoreProfile.objects.cleanup( )
            return
        
        if forced.count( ):
            p = forced.first( )
            p.apply(type=apply_type or 'auto_boot', sync=sync)
            return p

        rp = Profile.objects.filter(appprofile=None).exclude(
            apply_on_boot=False
        ).filter(active=True).filter_for_system_status( )
        p = None
        for prf in rp:
            LOGGER.info("Applying auto-boot profile {}".format(prf))
            p = prf
            prf.apply(type=apply_type or 'auto_boot', sync=sync)
            break
            
        if rp.count( ) and not p:
            LOGGER.warn(
                "No matching on boot profiles, either power or GPU count is "
                "filtering them. Not applying a profile."
            )
        return p

class Profile(models.Model):
    class Meta:
        ordering = ('-priority', 'name', )
        unique_together = (
            ('hotkey_modifier', 'hotkey'),
        )
    objects = ProfileManager( )

    POWER_STATUS_CHOICES = (
        ('ac_power', _("Charger attached (AC Power)")),
        ('dc_power', _("Charger removed (Battery/DC Power)")),
        ('battery_10', _("Battery <= 10%")),
        ('battery_20', _("Battery <= 20%")),
        ('battery_30', _("Battery <= 30%")),
        ('battery_40', _("Battery <= 40%")),
        ('battery_50', _("Battery <= 50%")),
        ('battery_60', _("Battery <= 60%")),
        ('battery_70', _("Battery <= 70%")),
        ('battery_80', _("Battery <= 80%")),
        ('battery_90', _("Battery <= 90%")),
        ('battery_100', _("Battery <= 100%")),
    )
    RESET_CHOICES = (
        ('last', _("The previously applied profile")),
        ('defl', _("The default on boot profile")),
    )
    HOTKEY_MODIFIER_CHOICES = (
        ('control_alt', _("Ctrl + Alt")),
        ('control_shift', _("Ctrl + Shift")),
        ('control_shift_alt', _("Ctrl + Shift + Alt")),
        ('shift_alt', _("Shift + Alt")),
    )
    HOTKEY_SPECIALS = [
        "up", "down", "left", "right", "space",
        "backspace", "delete", "escape"
    ]
    HOTKEY_CHOICES = list(
        zip(string.ascii_lowercase, string.ascii_lowercase)
    ) + list (
        zip(string.digits, string.digits)
    ) + list(
        zip(['f{}'.format(i) for i in range(1, 13)],
            ['F{}'.format(i) for i in range(1, 13)])
    ) + list(
        zip(HOTKEY_SPECIALS, [
            _(h.title( )) for h in HOTKEY_SPECIALS
        ])
    )
    linked_profile = models.ForeignKey(
        'profiles.Profile', related_name='child_profiles',
        help_text=_(
            "Linking a profile causes this app profile to inherit power limit "
            "and voltage offset fields from another profile. Any settings you "
            "assign directly within this profile will override the inherited "
            "values."
        ), blank=True, null=True, on_delete=models.SET_NULL
    )

    active = models.BooleanField(default=True, db_index=True)
    name = models.CharField(unique=True, help_text=_(
        "Please enter a unique name for this profile."
    ), max_length=150)
    slug = models.SlugField(unique=True, editable=False)
    shortcut = models.BooleanField(default=True, help_text=_(
        "Include this profile in the quick shortcut menu."
    ))
    force_test = models.BooleanField(
        verbose_name=_("Sticky Profile"), default=False, db_index=True,
        help_text=_(
            "Select this option to mark this profile as 'sticky', preventing "
            "any other profiles from being applied even if automation events, "
            "such as an application launch or power status change occur. "
            "You most likely want to leave this checkbox and utilise the "
            "'Sticky Hotkey' functionality instead, but it could also be "
            "useful to check this option if you are testing and experimenting "
            "with a profile and do not want other profiles to interfere."
        )
    )
    auto_force_test = models.BooleanField(
        editable=False, default=False, db_index=True
    )
    priority = models.PositiveBigIntegerField(
        default=0, db_index=True, help_text=_(
            "When responding to automated events (such as standby resume, "
            "battery power change, etc.) the profiles will be processed in "
            "descending priority order. The first matching profile will be "
            "utilised."
        )
    )

    apply_on_boot = models.BooleanField(
        default=False, db_index=True, help_text=_(
            "Select this option to automatically apply the profile when the "
            "system boots OR resumes from suspend. Note, you should ensure "
            "stability of this profile before selecting this option. "
            "The system will detect if an unsafe shutdown occurred and, if so,"
            " will not apply the profile upon boot."
        )
    )
    apply_on_power = models.CharField(
        max_length=12, verbose_name=_("apply on power status change"),
        db_index=True, blank=True, null=True, choices=POWER_STATUS_CHOICES,
        help_text=_(
            "Select an option from this list to automatically apply the "
            "profile on a power status change. Battery levels will match "
            "in two scenarios - a) the battery level falls exactly to the "
            "value specified (e.g. 90%, 80%, 70%), b) the device is "
            "switched from AC to DC and the battery level falls below the "
            "value specified (e.g. 81-89% etc.)"
        )
    )
    apply_on_video_adapters = models.PositiveSmallIntegerField(
        blank=True, null=True,
        verbose_name=_("apply when GPUs are available"), help_text=_(
            "If you have more than one GPU available on the system, apply "
            "this profile when at least this number are connected and "
            "available. For example, if you have an eGPU and wish this "
            "profile to apply when it is connected, you could set this value "
            "to 2, as your integrated GPU AND the external GPU would be "
            "connected."
        )
    )

    created = models.DateTimeField(auto_now_add=now, editable=False)
    applied = models.DateTimeField(
        blank=True, null=True, verbose_name=_("last applied"),
        editable=False
    )

    run_on_launch = models.CharField(
        max_length=500,
        verbose_name=_("run on apply"),
        blank=True, null=True, help_text=_(
            "Execute a script or executable immediately BEFORE attempting to "
            "apply this profile. This should be a full, absolute path to a "
            "file, unless it is within your system PATH. "
            "Note, this BAT will be run with elevated privileges, so use at "
            "your own risk!"
        )
    )
    run_on_success = models.CharField(
        max_length=500,
        verbose_name=_("run on successful apply"),
        blank=True, null=True, help_text=_(
            "Execute a script or executable prior once this profile is "
            "successfully applied. "
            "This should be a full, absolute path to a file, unless it is "
            "within your system PATH. "
            "Note, this BAT will be run with elevated privileges, so use at "
            "your own risk!"
        )
    )
    run_on_failure = models.CharField(
        max_length=500,
        verbose_name=_("run on failure to apply"),
        blank=True, null=True, help_text=_(
            "Execute a script or executable prior if this profile fails to "
            "apply. "
            "This should be a full, absolute path to a file, unless it is "
            "within your system PATH. "
            "Note, this BAT will be run with elevated privileges, so use at "
            "your own risk!"
        )
    )

    hotkey_modifier = models.CharField(
        max_length=max([len(k[0]) for k in HOTKEY_MODIFIER_CHOICES]),
        choices=HOTKEY_MODIFIER_CHOICES, blank=True, null=True, help_text=_(
            "A modifier to use in conjunction with the hotkey. To add a "
            "hotkey to a profile, both a modifier and hotkey are required."
        )
    )
    hotkey = models.CharField(
        max_length=max([len(k[0]) for k in HOTKEY_CHOICES]),
        choices=HOTKEY_CHOICES, blank=True, null=True, help_text=_(
            "A hotkey to use to apply/unapply this profile. To add a "
            "hotkey to a profile, both a modifier and hotkey are required."
        )
    )
    hotkey_sticky = models.BooleanField(
        verbose_name=_("Sticky Hotkey"), default=True, help_text=_(
            "Select this option to mark a profile as 'sticky' when using "
            "the hotkey. A sticky profile will prevent any other automated "
            "profiles, such as app or power-based profiles, being applied. "
            "Pressing the hotkey again will unapply the profile, and remove "
            "its sticky nature."
        )
    )
    reset_profile = models.CharField(
        verbose_name=_("when closed set profile to"), choices=RESET_CHOICES,
        max_length=4, help_text=_(
            "When unapplying from a hotkey or app choose a profile that the "
            "system should restore to. If for any reason this is not available,"
            " the system will reset to the default/on boot profile."
        ), default='last'
    )
    run_on_close = models.CharField(
        max_length=500, blank=True, null=True, help_text=_(
            "Execute a script or executable when this profile is restored, for "
            "example on hotkey or application close. "
            "Note, this BAT will be run with elevated privileges, so "
            "use at your own risk! Also note, the app will already be closed "
            "by the time this script is launched."
        )
    )

    process_control_groups = models.ManyToManyField(
        'profiles.ProcessControlGroup', blank=True, help_text=_(
            "Choose one or more process control groups to apply when this "
            "profile is activated. This allows you to set other application "
            "controls, such as priority and CPU affinity when applying a "
            "profile or launching an application."
        )
    )

    _restore_process_control_data = models.TextField(
        blank=True, null=True, editable=False
    )

    @property
    def hotkey_display(self):
        if self.hotkey_modifier and self.hotkey:
            return "{} + {}".format(
                self.get_hotkey_modifier_display( ), self.hotkey.upper( )
            )
        return ""

    @property
    def hotkey_value(self):
        if self.hotkey_modifier and self.hotkey:
            return tuple(self.hotkey_modifier.split("_")) + (self.hotkey, )
        return None

    @property
    def all_process_control_groups(self):
        from .process_groups import ProcessControlGroup
        pks = []
        if self.linked_profile:
            pks = self.linked_profile.all_process_control_groups.values_list(
                'pk', flat=True
            )
            pks = list(pks)
        return ProcessControlGroup.objects.filter(
            pk__in=pks + list(
                self.process_control_groups.values_list('pk', flat=True)
            )
        ).distinct( )

    @classmethod
    def _dict_to_keys(cls, dct, prefix=''):
        dat = {}
        for k, v in dct.items( ):
            k = k.lower( )
            if isinstance(v, dict):
                dat.update(cls._dict_to_keys(
                    v, '{}__'.format(k)
                ))
            else:
                dat["{}{}".format(prefix, k)] = v
        return dat

    @classmethod
    def _escape_script_arg(cls, arg):
        '''
            You can't actually escape double quotes in BAT scripts (really?!),
            so replace with a single quote.

            Unlikely we'll ever have such an argument but better safe than
            sorry!
        '''
        return '"{}"'.format(str(arg).replace('"', "'"))

    def _run_script(self, command, type=None, vars={}, include_data=True):
        if not command: return None

        class FormatDict(dict):
            def __missing__(self, key): return '""'
        
        if include_data:
            try:
                jkeys = self._dict_to_keys(self.to_json( ))
                for k, v in jkeys.items( ):
                    vars.setdefault(k, self._escape_script_arg(v))
            except Exception as ex:
                LOGGER.warn(
                    "Unable to convert profile variables to command "
                    "script variables.", exc_info=ex
                )

        vars = FormatDict(vars)
        command = command.format_map(vars)
        LOGGER.info("Run script is {} (vars={})".format(command, vars))
        try:
            if command.lower( ).endswith(".lnk"):
                LOGGER.info("Spawning link as startfile.")
                threading.Thread(target=functools.partial(
                    os.startfile, command, 'open'
                )).start( )
                LOGGER.info("Startfile spawned.")
            else:
                threading.Thread(target=functools.partial(
                    subprocess.Popen, command, shell=True
                )).start( )
                LOGGER.info("{}Script '{}' spawned successfully.".format(
                    "{}: ".format(type) if type else "", command
                ))
        except Exception as ex:
            LOGGER.warn(
                "{}Unable to execute script '{}': {}".format(
                    "{}: ".format(type) if type else "", command, ex
                ), exc_info=ex
            )

    def run_on_launch_script(self, vars={}):
        if not self.run_on_launch:
            if self.linked_profile:
                return self.linked_profile.run_on_launch_script(vars=vars)
        self._run_script(self.run_on_launch, "launch", vars=vars)
    def run_on_success_script(self, vars={}):
        if not self.run_on_success:
            if self.linked_profile:
                return self.linked_profile.run_on_success_script(vars=vars)
        self._run_script(self.run_on_success, "success", vars=vars)
    def run_on_failure_script(self, vars={}):
        if not self.run_on_failure:
            if self.linked_profile:
                return self.linked_profile.run_on_failure_script(vars=vars)
        self._run_script(self.run_on_failure, "failure", vars=vars)
    def run_on_close_script(self, vars={}):
        if not self.run_on_close:
            if self.linked_profile:
                return self.linked_profile.run_on_close_script(vars=vars)
        self._run_script(self.run_on_close, "close", vars=vars)

    def natural_key(self):
        return (self.slug, )

    def clean(self):
        if self.linked_profile and self.pk and (
            self.linked_profile_id == self.pk
        ):
            raise ValidationError({
                "linked_profile": _("A profile cannot be linked to itself.")
            })
        
        if self.linked_profile and self.pk and (
            self.linked_profile.linked_profile_id
        ) and self.linked_profile.linked_profile_id == self.pk:
            raise ValidationError({
                "linked_profile": _(
                    "Profiles cannot be recursively linked to each other."
                )
            })

        if len(list(
            filter(lambda h: bool(h), [self.hotkey, self.hotkey_modifier])
        )) == 1:
            raise ValidationError({
                'hotkey_modifier': _(
                    "Both a modifier and a hotkey must be specified to enable "
                    "hotkeys on a profile."
                )
            })
        
        if self.hotkey and self.hotkey_modifier:
            if (self.hotkey_modifier,self.hotkey) in settings.RESERVED_HOTKEYS:
                raise ValidationError({
                    'hotkey_modifier': _(
                        "This hotkey combination is reserved for system usage. "
                        "Please choose another modifier or hotkey."
                    ), 'hotkey': _(
                        "This hotkey combination is reserved for system usage. "
                        "Please choose another modifier or hotkey."
                    )
                })

    def cleanup(self):
        ''' Perform any required cleanup when un-applying this profile,
            e.g. restoring affinity or priority. '''

        rpcd = None
        if self.pk:
            rpcd = self.__class__.objects.filter(
                pk=self.pk
            ).values_list('_restore_process_control_data', flat=True)[0]

        LOGGER.info(
            "Applying any cleanup for profile {}: {} ({})".format(
                self, rpcd, rpcd.__class__
            )
        )
        if rpcd:
            try:
                dat = json.loads(rpcd)
                if dat:
                    LOGGER.info(
                        "Restoring settings for {} process(es): {}".format(
                            len(dat), dat
                        )
                    )
                    ProcessControlMixin.restore_process_data(dat)
                self.__class__.objects.filter(
                    pk=self.pk
                ).update(_restore_process_control_data=None)
            except Exception as ex:
                LOGGER.warn(
                    "Unable to apply cleanup to profile {}".format(self),
                    exc_info=ex
                )

    def apply(
        self, type='manual', force=None, exe=None, sync=False, hotkey=False
    ):
        profile = self

        if RestoreProfile.objects.filter(
            profile=self, apply_type__endswith='hk'
        ).count( ) and hotkey and not exe:
            LOGGER.info(
                "Attempting to apply already applied profile {}, assuming "
                "restore/unapply instead.".format(self)
            )
            return self.restore(hotkey=hotkey, sync=sync)

        # Un-stick any other auto-sticky profiles. I think this is
        # probably desired behaviour.
        self.__class__.objects.filter(
            force_test=True, auto_force_test=True
        ).update(force_test=False, auto_force_test=False)
        RestoreProfile.objects.filter(
            apply_type__endswith='hk'
        ).delete( )

        if hotkey: type += "hk"

        if type != 'manual' and not self.force_test:
            force = Profile.objects.filter(
                active=True, force_test=True
            ).order_by('-priority')
            if force.count( ):
                LOGGER.info(
                    "Overriding {} profile with sticky profile: {}".format(
                        type, force[0]
                    )
                )
                profile = force.first( )
        elif type == 'manual' and force:
            LOGGER.info("Marking profile {} as a sticky profile.".format(
                self
            ))
            Profile.objects.filter(pk=self.pk).update(force_test=True)
        elif type == 'manual' and force == False:
            LOGGER.info("Unmarking profile {} as a sticky profile.".format(
                self
            ))
            Profile.objects.filter(pk=self.pk).update(force_test=False)
            return self.restore(hotkey=False, sync=sync)

        LOGGER.info("Attempting to apply profile {} (Sync={})".format(
            profile, sync
        ))

        if hotkey:
            # Un-stick any other auto-sticky profiles. I think this is
            # probably desired behaviour.
            self.__class__.objects.filter(
                force_test=True, auto_force_test=True
            ).update(force_test=False, auto_force_test=False)

            if self.hotkey_sticky:
                LOGGER.info(
                    "Applying hotkey sticky profile {}, marking as force "
                    "test.".format(self)
                )
                self.__class__.objects.filter(
                    pk=self.pk
                ).update(force_test=True, auto_force_test=True)
        COMMS.apply_profile(profile, type, exe=exe, sync=sync)

    def restore(self, vars={}, hotkey=False, sync=False):
        if hotkey:
            if self.hotkey_sticky:
                self.__class__.objects.filter(
                    pk=self.pk, auto_force_test=True
                ).update(force_test=False, auto_force_test=False)

        if self.run_on_close or self.linked_profile:
            LOGGER.info(
                "Attempting to run on close script: {}".format(
                    self.run_on_close
                )
            )
            executable = vars['full_path'] = vars.get(
                'ExecutablePath', ''
            )
            if executable:
                try:
                    exe = os.path.basename(executable)
                except:
                    exe = ''
                try:
                    path = os.path.dirname(executable)
                except:
                    path = ''
                vars['exe'] = exe
                vars['path'] = path
            self.run_on_close_script(vars=app)

        try:
            self.cleanup( )
        except Exception as ex:
            LOGGER.warn(
                "Unable to clean up profile {}".format(self),
                exc_info=ex
            )

        LOGGER.debug("Cleanup has been applied for {}".format(self))

        last = None
        apply_type = 'restorehk' if hotkey else 'restore'
        if self.reset_profile == "last":
            LOGGER.debug("Resetting to last profile.")
            last = Profile.objects.apply_last(
                apply_type=apply_type, restore_from=self,
                exclude=self.pk, sync=sync
            )
            if last:
                LOGGER.debug("Profile {} applied.".format(last))
                RestoreProfile.objects.filter(profile=self).delete( )
                return last

        if not last:
            last = Profile.objects.filter(
                active=True, apply_on_boot=True
            ).count( )
            if last:
                LOGGER.info("Restoring on boot profile.")
                Profile.objects.apply_on_boot(
                    force=True, apply_type=apply_type, sync=sync
                )                        
                RestoreProfile.objects.filter(profile=self).delete( )
                return last

        return None

    def matches_power_profile(self, ac, battery_level):
        aop = self.apply_on_power
        if not aop: return False
        if ac:
            return aop == 'ac_power'
        else:
            if aop.startswith('battery'):
                if aop == 'battery_full':
                    if battery_level == 100:
                        return True
                else:
                    try:
                        comp = int(aop.split("_")[-1])
                    except:
                        return False
                    
                    return battery_level < comp
            elif aop == 'dc_power':
                return True

        return False

    def delete_registers(self):
        app = apps.get_app_config('profiles')

        for name, klass, accessor in app.get_registers( ):
            data = getattr(self, accessor).all( )
            if data.count( ): data.delete( )

    def to_json(self, with_linked=True, with_nulled=True):
        app = apps.get_app_config('profiles')

        payload = { }
        if with_linked and self.linked_profile and self.linked_profile.active:
            payload = self.linked_profile.to_json(with_nulled=with_nulled)
            LOGGER.debug("Linked profile {} payload is {}".format(
                self.linked_profile, payload
            ))
        has = False
        for name, klass, accessor in app.get_registers( ):
            data = getattr(self, accessor).all( )
            if not data.count( ):
                if with_nulled and (
                    not with_linked or not self.linked_profile
                ):
                    js = klass.nulled_json( )
                    if js: payload = always_merger.merge(payload, js)
            else:
                has = True
                for d in data:
                    js = d.to_json( )
                    if js: payload = always_merger.merge(payload, js)

        if not has:
            LOGGER.info("Empty payload, not including nullable.")
            return { }
        LOGGER.debug("Profile {} payload is {}".format(self, payload))
        return payload

    @classmethod
    def registers_from_json(self, body, include_empty=True, profile=None):
        '''
            Given a JSON payload, construct (unsaved) registers for this 
            information and return as a list.
        '''
        app = apps.get_app_config('profiles')

        regs = []
        for name, klass, accessor in app.get_registers( ):
            resl = klass.from_json(body, include_empty=include_empty)
            if not resl: continue
            if not isinstance(resl, list): resl = [resl]
            if profile:
                for r in resl: r.profile = profile
            regs.extend(resl)
        
        return regs
        
    def _set_slug(self, force=False):
        if not self.slug or force:
            slug = None
            base = slugify(self.name)
            while not slug:
                oth = self.__class__.objects.filter(slug=base)
                if self.pk: oth = oth.exclude(pk=self.pk)
                if not oth.count( ):
                    slug = base
                    break
                else:
                    base = slugify("{} {}".format(self.name, oth.count( ) + 1))
            
            self.slug = slug
            return slug

    def save(self, *args, **kwargs):
        self._set_slug( )
        return super( ).save(*args, **kwargs)
    
    def __str__(self):
        return self.name

    @property
    def details(self):
        app = apps.get_app_config('profiles')

        regs = []
        for name, klass, accessor in app.get_registers( ):
            data = getattr(self, accessor).all( )
            if data.count( ):
                regs.extend(list(data.all( )))
 
        return Register.registers_to_display_list(regs)

class AppProfileProcessControlManager(models.Manager):
    def get_by_natural_key(self, *process_key):
        return AppProfile.objects.get_by_natural_key(
            *process_key
        ).process_control

class AppProfileProcessControl(ProcessControlMixin):
    objects = AppProfileProcessControlManager( )

    def natural_key(self):
        return self.profile.natural_key( )

    profile = models.OneToOneField(
        'profiles.AppProfile', blank=True, null=True,
        help_text=_(
            "If you wish to ensure this application process runs only on "
            "specific CPU cores, you can adjust the affinity accordingly. "
            "Note, these are logical cores, so if your CPU supports "
            "hyperthreading there will be two logical cores here per "
            "physical core."
        ), on_delete=models.CASCADE, related_name='process_control'
    )

    def __str__(self):
        return "{}: {}".format(self.profile, super( ).__str__( ))

class AppProfile(AppSelectionMixin, Profile):
    GPU_OPTION_CHOICES = (
        (1, _("Power Saving/Integrated GPU")),
        (2, _("High Performance/Discrete GPU"))
    )
    ALLOWED_CHOICES = AppSelectionMixin.MATCH_TYPE_CHOICES

    ''' Track PID so we can bypass window detection on close. '''
    _pid = models.CharField(
        max_length=24, blank=True, null=True,
        db_index=True, editable=False
    )

    gpu_option = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=GPU_OPTION_CHOICES,
        verbose_name=_("Graphics Performance Mode"),
        help_text=_(
            "If your system has more than one graphics adapter and you wish "
            "to ensure applications within this profile use a specific adapter"
            ", select it here. This will be set for every local user on this "
            "machine. **NOTE**, this may not work the very first "
            "time you launch this application. If so, restart the application "
            "and it should work going forward."
        )
    )


    def _auto_name(self, unique=True):
        try:
            ap = os.path.normpath(self.app)
        except Exception:
            return ""

        if ap.endswith("**"):
            base = ap.split(os.sep)[-2]
        else:
            base = "\\".join(ap.split(os.sep)[-2:])
        if unique:
            others = self.__class__.objects.filter(
                name=base
            )
            if self.pk: others = others.exclude(pk=self.pk)
            ct = others.count( ) + 1
            while others.count( ):
                base = "{} - {}".format(base, ct)
                others = self.__class__.objects.filter(
                    name=base
                )
                if self.pk: others = others.exclude(pk=self.pk)
                ct += 1

        if self.match_type == "defl":
            base = "Default: {}".format(base)   
            if self.linked_profile:
                base += ", {}".format(self.linked_profile)         
        return base

    def clean(self):
        super( ).clean( )
        if not self.name: self.name = self._auto_name( )
        
        if self.linked_profile_id and self.pk and (
            self.linked_profile == self.pk
        ):
            raise ValidationError({
                'linked_profile': _("A profile cannot be linked to itself.")
            })


        if self.linked_profile_id and self.pk:
            if self.linked_profile.linked_profile_id and (
                self.pk == self.linked_profile.linked_profile_id
            ):
                raise ValidationError({
                    'linked_profile': _(
                        "Profiles cannot be linked to each other recursively."
                    )
                })

        if not self.name:
            raise ValidationError({
                'name': _("Specify a name for this profile.")
            })

        if self.match_type == 'exe':
            others = self.__class__.objects.filter(
                filename__iexact=self.filename
            )
            if self.pk: others = others.exclude(pk=self.pk)
            if others.count( ):
                raise ValidationError({
                    'manual_app_path': _(
                        "When using Filename Only matching, the executable "
                        "name ('%(exe)s') must be unique. It is already in use"
                        " for the profile '%(prof)s'."
                    ) % {'exe': self.filename, 'prof': ", ".join([
                        o.exe_name for o in others
                    ])}
                })

    @property
    def exe_name(self):
        return "{}, {}".format(self.name, self.filename)

    def save(self, *args, **kwargs):
        self.apply_on_boot = False
        self.shortcut = False
        if not self.name: self.name = self._auto_name( )

        uniq = Profile.objects.filter(name=self.name)
        if self.pk: uniq = uniq.exclude(pk=self.pk)
        
        if uniq.count( ):
            self.name = "{} {}".format(
                self.name, now( ).strftime("%Y%m%d %H%M")
            )

        if self.priority == 0:
            if self.match_type == 'path':
                self.priority = 200
            elif self.match_type == 'exe':
                self.priority = 100

        if self.match_type == 'defl':
            if self.linked_profile:
                if not self.priority:
                    self.priority = self.linked_profile.priority
                self.apply_on_power = self.linked_profile.apply_on_power
    
        super( ).save(*args, **kwargs)

AppProfile._meta.get_field('match_type').choices = AppProfile.ALLOWED_CHOICES

class RestoreProfileManager(models.Manager):
    def cleanup(self):
        '''
            Remove any RestoreProfile models.
        '''
        self.all( ).delete( )

class RestoreProfile(models.Model):
    '''
        When we apply a profile we store it in this model. These are then 
        removed on service exit. If the system fails (due to instability etc),
        then records will be left in this model and we know not to auto
        apply a profile on boot.

        In addition, anything that needs a force restore (e.g. keyboard
        input tips), can be pulled out from here on startup.
    '''
    class Meta:
        ordering = ('-applied', )
    objects = RestoreProfileManager( )

    applied = models.DateTimeField(
        auto_now_add=now, db_index=True, editable=False
    )
    profile = models.ForeignKey(
        'profiles.Profile', related_name='current_applied',
        on_delete=models.SET_NULL, blank=True, null=True
    )
    apply_type = models.CharField(
        max_length=20, blank=True, null=True,
        db_index=True, editable=False
    )
    _to_restore = models.TextField(blank=True, null=True)

    @property
    def to_restore(self):
        return json.loads(self._to_restore)
    @to_restore.setter
    def to_restore(self, data):
        self._to_restore = json.dumps(data)

    def __str__(self):
        return "{}: {}".format(self.applied, self.profile)

class ProfileLog(models.Model):
    class Meta:
        ordering = ('-applied', )

    TYPE_CHOICES = (
        ('manual', _("Manual (User) Applied")),
        ('manualhk', _("Manual (User) Applied via Hotkey")),
        ('auto_boot', _("Automatic (Applied on Boot)")),
        ('auto_susp', _("Automatic (Applied on Resume)")),
        ('auto_app', _("Automatic (App Profile)")),
        ('auto_pwr', _("Automatic (Power Event)")),
        ('auto_bat', _("Automatic (Battery Level)")),
        ('auto_hw', _("Automatic (Hardware Event)")),
        ('restore', _("Restored (App Profile)")),
        ('restorehk', _("Restored (Hotkey)"))
    )

    applied = models.DateTimeField(
        auto_now_add=now, db_index=True, editable=False
    )
    type = models.CharField(
        max_length=9, choices=TYPE_CHOICES, db_index=True, editable=False
    )
    profile = models.ForeignKey(
        'profiles.Profile', related_name='log_message',
        on_delete=models.SET_NULL, blank=True, null=True,
        editable=False
    )
    profile_name = models.CharField(
        max_length=200, db_index=True, editable=False
    )
    success = models.BooleanField(db_index=True, editable=False)
    messages = models.CharField(max_length=500, editable=False)
    details = models.TextField(editable=False)

    def save(self, *args, **kwargs):
        if self.profile: 
            self.profile_name = self.profile.name
        elif not self.profile_name:
            self.profile_name = "N/A"
        return super( ).save(*args, **kwargs)

    def __str__(self):
        return "{}: {} ({})".format(
            self.applied, self.profile, _("Success") if (
                self.applied
            ) else _("Failed")
        )
