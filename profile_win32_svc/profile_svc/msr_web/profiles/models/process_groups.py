from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Q
from django.utils.text import slugify
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from .. import utils
from msr.models import CurrentOpenWindow

import logging, json, math, os, subprocess, threading, time

LOGGER = logging.getLogger(__name__)

MAX_PROCESSOR_AFFINITY = 32

priority_choices, priority_defaults = utils.get_process_priority_choices( )

class AppSelectionMixin(models.Model):
    class Meta:
        abstract = True

    MATCH_TYPE_CHOICES = (
        ('name', _("Application Window Name: Full Match")),
        ('pnme', _("Application Window Name: Partial Match")),
        ('path', _("Filename: Full File Path (Default)")),
        ('exe', _("Filename: Filename Only")),
        ('defl', _("Filename: The start of the path (Folder Default)")),
    )
    NORMAL_MATCH_TYPE_CHOICES = MATCH_TYPE_CHOICES
    ALLOWED_CHOICES = NORMAL_MATCH_TYPE_CHOICES
    ALLOW_DEFAULT_MATCH = True
    DEFAULT_MATCH_TYPE = 'path'

    app_path = models.CharField(
        verbose_name=_("Application"), db_index=True, max_length=1024,
        help_text=_(
            "Select the application which, when launched, will cause this "
            "profile to be automatically applied. If the application is not "
            "listed, please use the override setting to browse and choose the "
            "actual executable."
        ), blank=True, null=True
    )
    manual_app_path = models.CharField(
        verbose_name=_("Application (Override)"), db_index=True,
        max_length=1024, help_text=_(
            "If the application cannot be found in the application list, use "
            "this option to locate the actual executable within your file "
            "system. Only use this option if you cannot find the application "
            "in the main list. You will need to enter the full path to the "
            "relevant executable, e.g. C:\\Program Files\\MyApp\\File.exe."
        ), blank=True, null=True
    )
    
    match_type = models.CharField(
        max_length=4, choices=ALLOWED_CHOICES,
        db_index=True, default=DEFAULT_MATCH_TYPE, help_text=_(
            "Choose how to match this application. The default is to match "
            "the exact file path to determine which application has launched, "
            "for example, 'C:\\Program Files\\MyApp\\File.exe'."
            "Sometimes the Operating System will not detect the full launch "
            "path, meaning that profiles will not automatically apply. If "
            "this seems to be the case you may use the 'Filename Only' option,"
            "which will cause the system to just match on the filename alone, "
            "e.g. File.exe. Note, this could cause problems if more than one "
            "application has the same filename, but in different paths, as "
            "the system will match both of these, so this option should only "
            "be used in the event the Full File Path matching does not work, "
            "and the executable name is unique."
        )
    )
    match_string = models.CharField(
        max_length=1024, editable=False, db_index=True
    )
    filename = models.CharField(
        max_length=500, editable=False, db_index=True
    )

    @property
    def app(self):
        return self.manual_app_path or self.app_path

    @property
    def is_exe_match_type(self):
        return self.match_type in ('path', 'exe', 'defl')

    def process_info_to_path(self, process_info):
        if self.is_exe_match_type:
            return [process_info['ExecutablePath']]
        else:
            wins = process_info['Windows']
            wn = []
            if wins:
                for w in wins:
                    if isinstance(w, dict):
                        wn.append(w.get('text'))
                    else:
                        wn.append(w)
            return wn

    def app_is_active(self, app_list=None):
        app_list = app_list or utils.get_running_apps( )
        for a in app_list:
            if self.matches(a): return True
    
        return False

    def _update_filename(self):
        apath = self.app
        if apath:
            self.match_string = apath
            self.filename = os.path.basename(apath)

    def matches(self, process_info, extra_details=None):
        pth = self.process_info_to_path(process_info)

        if self.is_exe_match_type:
            return self.matches_exe(pth)
        else:
            return self.matches_name(pth)

    def matches_name(self, names):
        if not isinstance(names, (list, tuple)):
            names = [names]

        for name in names:
            if self.match_type == 'name':
                if name == self.match_string:
                    return True
            elif self.match_type == 'pnme':
                if self.match_string in name:
                    return True
        
        return None

    def matches_exe(self, exe):
        if isinstance(exe, (list, tuple)): exe = exe[0]
        full_path = exe
        if self.match_type == 'exe':
            fn = self.filename.lower( )
            if fn.endswith(".exe"): fn = fn[0:-4]
            if fn == "**":
                # This would match everything...
                return False

            exe = os.path.basename(exe).lower( )
            if exe.endswith(".exe"): exe = exe[0:-4]
            return full_path if fn == exe else None
        elif self.match_type == 'defl':
            # Looking to see if the start of the path matches.
            fn = self.app.lower( )
            return full_path if exe.lower( ).startswith(fn) else None
        elif self.match_type == 'path':
            if not os.path.dirname(exe).strip( ):
                # We can't match, so exit out quickly.
                return None

            fn = self.filename.lower( )
            if fn == "**":
                # Match path parts.
                return full_path if (
                    os.path.dirname(exe) == os.path.dirname(self.app).lower( )
                ) else None
            else:
                exe = exe.lower( )
                pth = self.app.lower( )
                if exe.endswith(".exe"): exe = exe[0:-4]
                if pth.endswith(".exe"): pth = pth[0:-4]

                return full_path if exe == pth else None
        
        return None

    def get_proc_id_from_info(self, info=None):
        if not info: info = utils.get_running_apps(include_windows=False)
        open_windows = [(c['process'], c) for c in (
            CurrentOpenWindow.objects.values( )
        )]
        for inf in info:
            inf['Windows'] = [
                w[1]['text'] for w in open_windows if w[0] == inf['ProcessId']
            ]
            matches = self.matches(inf)
            if matches: return inf['ProcessId'], inf.get('Title')
        return None, None

    def clean(self):
        if not self.app_path and not self.manual_app_path:
            raise ValidationError(_(
                "Please select an application."
            ))

        self._update_filename( )
        super( ).clean( )

    def save(self, *args, **kwargs):
        self._update_filename( )
        return super( ).save(*args, **kwargs)

    def __str__(self):
        return "{} - {}".format(
            self.get_match_type_display( ), self.app
        )

class ProcessControlMixin(models.Model):
    class Meta:
        abstract = True

    priority = models.PositiveIntegerField(
        blank=True, null=True, choices=priority_choices,
        help_text=_(
            "If you wish to adjust the system priority level of aprocess,"
            " select it here. Note, it will be applied once the application "
            "profile has been applied."
        )
    )

    @classmethod
    def restore_process_data(cls, data):
        for pid, rest in data.items( ):
            pri, aff = rest.get('pri'), rest.get('aff')
            if pri != None:
                LOGGER.debug(
                    "Restoring priority {} for process {}".format(
                        pri, pid
                    )
                )
                try:
                    pp = utils.set_process_priority(int(pid), int(pri))
                    if not pp: raise ValueError("Priority not set.")
                except Exception as ex:
                    LOGGER.warn(
                        "Unable to set process priority {} for {}".format(
                            pri, pid
                        ), exc_info=ex
                    )
            if aff != None:
                LOGGER.debug(
                    "Restoring affinity {} for process {}".format(
                        aff, pid
                    )
                )
                try:
                    aa = utils.set_process_affinity(int(pid), int(aff))
                    if not aa: raise ValueError("Affinity not set.")
                except Exception as ex:
                    LOGGER.warn(
                        "Unable to set process affinity {} for {}".format(
                            aff, pid
                        ), exc_info=ex
                    )


    @property
    def controls_to_dict(self):
        controls = {}
        if self.priority != None:
            controls['priority'] = self.priority
        if self.affinity_mask_list != []:
            controls['affinity'] = self.affinity_mask_list
        return controls

    @property
    def affinity_mask_list(self):
        has_true = False
        bin_str = ""
        for i in range(0, MAX_PROCESSOR_AFFINITY+1):
            val = getattr(self, 'cpu_{}'.format(i), False)
            bin_str += ("1" if val else "0")
            if val: has_true = True

        if not has_true:
            # All CPUs
            return []
        else:
            return [
                True if c == "1" else False for c in bin_str.rstrip("0")
            ]

    @property
    def affinity_mask_display(self):
        aml = self.affinity_mask_list
        if not aml: 
            return _("All CPUs")
        
        ranges = []
        rng = [aml[0]]
        for c in aml[1:]:
            if c != rng[-1]:
                ranges.append(rng)
                rng = []
            rng.append(c)

        if rng: ranges.append(rng)
        
        ct = 0
        nums = []
        for rg in ranges:
            if [r2 for r2 in rg if r2]:
                nums.append((ct, ct+len(rg)-1))
            ct += len(rg)

        msgs = []
        for start, end in nums:
            if start == end:
                msgs.append(_("CPU %(num)d") % {'num': start})
            else:
                msgs.append(_("CPU %(st)d-%(ed)d") % {'st': start, 'ed': end})
        
        return ", ".join(msgs)

    def __str__(self):
        base = []
        if self.priority:
            base.append(self.get_priority_display( ))
        mask = self.affinity_mask_list
        if mask:
            base.append(self.affinity_mask_display)
        return ", ".join([str(b) for b in base])

for i in range(0, MAX_PROCESSOR_AFFINITY+1):
    models.BooleanField(
        verbose_name=_("CPU {}".format(i)),
        default=False, help_text=_(
            "Whether this process should run on CPU %(ct)s"
        ) % {'ct': i}
    ).contribute_to_class(ProcessControlMixin, 'cpu_{}'.format(i))

class ProcessControlGroupManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)

class ProcessControlGroup(ProcessControlMixin):
    class Meta:
        ordering = ('name', )

    objects = ProcessControlGroupManager( )

    def natural_key(self):
        return (self.slug, )

    name = models.CharField(unique=True, help_text=_(
        "Please enter a unique name for this process control group."
    ), max_length=150)
    slug = models.SlugField(unique=True, editable=False)

    def __str__(self): return self.name

    def _set_slug(self, force=False):
        if not self.slug or force:
            slug = None
            base = slugify(self.name)
            while not slug:
                oth = self.__class__.objects.filter(slug=base)
                if self.pk: oth = oth.exclude(pk=self.pk)
                if not oth.count( ):
                    slug = base
                    break
                else:
                    base = slugify("{} {}".format(self.name, oth.count( ) + 1))
            
            self.slug = slug
            return slug

    def save(self, *args, **kwargs):
        self._set_slug( )
        return super( ).save(*args, **kwargs)

class ProcessControlGroupApplicationManager(models.Manager):
    def get_by_natural_key(self, group_slug, app_path, manual_app_path):
        return self.get(
            group__slug=group_slug,
            app_path=app_path, manual_app_path=manual_app_path
        )

class ProcessControlGroupApplication(AppSelectionMixin):
    class Meta:
        ordering = ('match_string', )
        unique_together = ('group', 'app_path', 'manual_app_path')

    objects = ProcessControlGroupApplicationManager( )

    def natural_key(self):
        return self.group.natural_key( ) + (
            self.app_path, self.manual_app_path
        )

    group = models.ForeignKey(
        "profiles.ProcessControlGroup", related_name="applications",
        on_delete=models.CASCADE
    )

