from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models, transaction
from django.template.defaultfilters import floatformat
from django.utils.encoding import force_str
from django.utils.text import slugify
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

import copy, logging, json, math, os, threading, time

LOGGER = logging.getLogger(__name__)

class Register(models.Model):
    class Meta:
        abstract = True

    profile = models.ForeignKey(
        'profiles.Profile', related_name='%(class)s_set',
        on_delete=models.CASCADE
    )

    @classmethod
    def registers_to_display_list(cls, regs, sort=True, full=True):
        ''' Given a list of registers, convert this to a friendly list of
            Class name, registers to be displayed accordingly. '''
        reg_klass = {}
        for reg in regs:
            kls = reg.__class__
            if kls not in reg_klass: reg_klass[kls] = []
            reg_klass[kls].append(reg)
        
        reg_by_group = []
        for klass, regs in reg_klass.items( ):
            reg_by_group.append((
                klass._meta.verbose_name,
                klass.registers_to_friendly_name(regs, full=full)
            ))

        if sort:
            reg_by_group.sort(key=lambda x: x[0])
        
        return reg_by_group

    @classmethod
    def registers_to_friendly_name(cls, regs, full=True):
        ''' Given a list of registers of this class, convert this to a list
            of strings for each required item.
            
            Setting full to True gives a full-length string rep of the
            registers, otherwise a somewhat shorted form is used.
        '''
        if full:
            fll = []
            for r in regs: fll.extend(str(r).split("\n"))
            return fll
        else:
            return [r.short_value for r in regs]

    @classmethod
    def nulled_json(cls):
        ''' If this register should still return some sort of payload, even 
            when it is not specified in a profile, it should be specified here.

            Return None to indicate no payload should be specified. '''
        return None

    def to_json(self):
        ''' Return the JSON payload of this register and configuration. '''
        return { }

    @classmethod
    def from_json(self, json, include_empty=False):
        ''' Given the JSON payload, return an unsaved set of registers if
            required, or None to indicate there is no relevant data in
            the payload.

            If include_empty is True, include a register even if it is the
            "default"/nulled value. E.g. if core parking is 0, include anyway.
        '''
        return None

    @property
    def comparators(self): return None

    @property
    def display_name(self): return self._meta.verbose_name.title( )

    def short_value(self): return self.display_details

    @property
    def display_details(self): return "-"

CPU_PRIORITY_EXPLAINERS = {
    1: _(" (Lowest Priority)"),
    9: _(" (Default)"),
    31: _(" (Highest Priority)")
}

GPU_PRIORITY_EXPLAINERS = {
    1: _(" (Lowest Priority)"),
    13: _(" (Default)"),
    31: _(" (Highest Priority)")
}

EXPLAINERS = {
    'PP0_POLICY': CPU_PRIORITY_EXPLAINERS,
    'PP1_POLICY': GPU_PRIORITY_EXPLAINERS
}

TYPES = {
    'PP0_POLICY': {
        'name': _("CPU Power Priority"),
        'short_name': _("CPU"),
        'field': 'cpu_balance',
        'default': 9,
        'help_text': _(
            "Set the power priority balance for the CPU cores. Higher "
            "values will ask the SoC to supply power to the CPU cores as "
            "a higher priority to the GPU. Please note, raising this "
            "value to high levels may cause system instability, so unless you "
            "know what you are doing we do not recommend raising this above "
            "a priority of 20."
        )
    }, 'PP1_POLICY': {
        'name': _("GPU Power Priority"),
        'short_name': _("GPU"),
        'field': 'gpu_balance',
        'default': 13,
        'help_text': _(
            "Set the power priority balance for the GPU. Higher "
            "values will ask the SoC to supply power to the GPU as "
            "a higher priority to the CPU Cores. Please note, raising this "
            "value to high levels may cause system instability, so unless you "
            "know what you are doing we do not recommend raising this above "
            "a priority of 20."
        )
    }
}

class PowerPriorityRegister(Register):
    class Meta:
        verbose_name = _("CPU and GPU Power Balance Priority")
        verbose_name_plural = verbose_name

    ALL_TYPES = ['PP0_POLICY', 'PP1_POLICY']

    cpu_balance = models.PositiveSmallIntegerField(
        verbose_name=TYPES['PP0_POLICY']['name'], blank=True, null=True,
        help_text=TYPES['PP0_POLICY']['help_text'],
        choices=[
            (r, _("%(pri)s%(ecl)s%(expl)s") % {
                'pri': r, 'ecl': " [!]" if r > 20 else "",
                'expl': EXPLAINERS['PP0_POLICY'].get(r) or ''
            }) for r in range(1, 32)
        ]
    )
    gpu_balance = models.PositiveSmallIntegerField(
        verbose_name=TYPES['PP1_POLICY']['name'], blank=True, null=True,
        help_text=TYPES['PP1_POLICY']['help_text'],
        choices=[
            (r, _("%(pri)s%(ecl)s%(expl)s") % {
                'pri': r, 'ecl': " [!]" if r > 20 else "",
                'expl': EXPLAINERS['PP1_POLICY'].get(r) or ''
            }) for r in range(1, 32)
        ]
    )

    @classmethod
    def nulled_json(cls):
        obj = { }
        for key in cls.ALL_TYPES:
            t = TYPES[key]
            obj[key] = {
                'balance': t['default'], 'type': key.split("_")[0]
            }
        return obj
        
    @classmethod
    def from_json(cls, data, include_empty=False):
        obj = { }
        for key in cls.ALL_TYPES:
            t = TYPES[key]
            vl = data.get(key)
            if vl != None:
                bl = vl.get('balance')
                if bl != None: obj[t['field']] = bl
        
        if obj:
            return [cls(**obj)]
        else:
            if include_empty:
                return cls.from_json(cls.nulled_json( ))
            else:
                return None

    def to_json(self):
        obj = { }
        for key in self.ALL_TYPES:
            t = TYPES[key]
            val = getattr(self, t['field'], None)
            if val != None: obj[key] = {
                'balance': val, 'type': key.split("_")[0]
            }
        return obj if obj else None

    @property
    def comparators(self): return [f['field'] for f in TYPES.values( )]

    @property
    def display_details(self):
        msg = []
        for type in TYPES.values( ):
            vl = getattr(self, type['field'], None)
            if vl == None:
                vl = type['default']
            msg.append("%(nm)s Power Priority = %(num)d / 31 (%(prc)s%%)" % {
                'nm': type['name'],
                'num': vl, 'prc': round((vl / 31.0) * 100)
            })
        return ", ".join(msg)

    @property
    def short_value(self):
        msg = []
        
        for type in TYPES.values( ):
            vl = getattr(self, type['field'], None)
            if vl == None:
                vl = type['default']
            msg.append("%(nm)s PPB = %(num)d / 31 (%(prc)s%%)" % {
                'nm': type['short_name'], 'num': vl,
                'prc': round((vl / 31.0) * 100)
            })
        return ", ".join(msg)

    def __str__(self):
        return self.display_details

class CoreParkRegister(Register):
    class Meta:
        verbose_name = _("CPU Core Parking")
        verbose_name_plural = verbose_name

    min_parked = models.PositiveIntegerField(
        verbose_name=_("Minimum Parked Cores"), default=0,
        help_text=_(
            "Enter the number of physical CPU cores to park when CPU usage "
            "is at a lower level. Note, parking all cores is allowed and "
            "will simply encourage the OS to park as many cores as it can "
            "as quickly as possible."
        )
    )
    max_parked = models.PositiveIntegerField(
        verbose_name=_("Maximum Parked Cores"), default=0,
        help_text=_(
            "Enter the number of physical CPU cores to park at all times, "
            "regardless of CPU level. This must be greater than, or equal to, "
            "this minimum number of parked cores. "
            "Note, parking all cores is allowed and "
            "will simply encourage the OS to park as many cores as it can "
            "as quickly as possible."
        )
    )
    # Holder for validation and retrieval from server.
    max_avail = models.PositiveIntegerField(
        editable=False, blank=True, null=True
    )

    @classmethod
    def nulled_json(cls):
        return {'PARK': {'min_parked': 0, 'max_parked': 0}}

    def to_json(self):
        return {'PARK': {
            'min_parked': self.min_parked, 'max_parked': self.max_parked
        }}

    @classmethod
    def from_json(cls, data, include_empty=False):
        if 'PARK' in data:
            p = data['PARK']
            mn, mx = p.get('min_parked') or 0, p.get('max_parked') or 0
            if not mn and not mx and not include_empty: return None

            return [cls(min_parked=mn, max_parked=mx,
                        max_avail=p.get('avail_to_park') or None)]
        elif include_empty:
            return [cls(**cls.nulled_json( )['PARK'])]

    @property
    def comparators(self):
        return ['min_parked', 'max_parked']

    @property
    def display_details(self):
        return _("Min. Parked: %(min)d, Max. Parked: %(max)d%(of)s") % {
            'min': self.min_parked or 0, 'max': self.max_parked or 0,
            'of': _(
                " (Of %(ct)d)"
            ) % {'ct': self.max_avail} if self.max_avail else ""
        }

    @property
    def short_value(self):
        if not self.min_parked and not self.max_parked:
            return ""

        ret = ""
        if self.min_parked:
            ret += "Park {} core(s)".format(self.min_parked)
        if self.max_parked and self.max_parked != self.min_parked:
            if self.min_parked:
                ret += ", "
            else:
                ret += "Park "
            ret += "upto {} cores".format(self.max_parked)
        if self.max_avail:
            ret += " (of {})".format(self.max_avail)
        return ret

    def __str__(self):
        return self.display_details

    def clean(self):
        if self.min_parked and self.max_parked:
            if self.max_parked < self.min_parked:
                raise ValidationError({
                    'max_parked': _(
                        "This must be greater than, or equal to, the minimum "
                        "number of parked cores."
                    )
                })

class ProcFreqRegister(Register):
    class Meta:
        verbose_name = _("Processor Frequency Scaling")
        verbose_name_plural = verbose_name

    EPP_CHOICES = [
        [i, "{}%".format(i)] for i in range(0, 101)
    ]
    EPP_CHOICES[0][1] = _("%(val)s%% - Maximum Performance") % {'val': 0}
    EPP_CHOICES[33][1] = (
        _("%(val)s%% - Default 'Better' Performance (AC)/'Best' (DC)") % {
            'val': 33
        }
    )
    EPP_CHOICES[50][1] = (_(
        "%(val)s%% - Default 'Better' Performance (DC)") % {'val': 50}
    )
    EPP_CHOICES[60][1] = (_(
        "%(val)s%% - Default 'Standard' Performance (AC)") % {'val':60}
    )
    EPP_CHOICES[70][1] = (_(
        "%(val)s%% - Default 'Standard' Performance (DC)") % {'val': 70}
    )
    EPP_CHOICES[100][1] = (_(
        "%(val)s%% - Maximum Power Saving") % {'val': 100}
    )

    min_freq = models.PositiveIntegerField(
        verbose_name=_("Minimum Frequency (MHz)"),
        blank=True, null=True,
        help_text=_(
            "Enter the minimum frequency you wish the processor to scale to, "
            "in MHz. Note, setting this value to 0 (zero) will enforce the "
            "default minimum frequency as stipulated by the Operating System. "
        )
    )
    max_freq = models.PositiveIntegerField(
        verbose_name=_("Maximum Frequency (MHz)"),
        blank=True, null=True,
        help_text=_(
            "Enter the maximum frequency you wish the processor to scale to, "
            "in MHz. Note, setting this value to 0 (zero) will enforce the "
            "default maximum frequency as stipulated by the Operating System. "

        )
    )
    epp = models.PositiveIntegerField(
        verbose_name=_("energy performance profile"),
        choices=EPP_CHOICES, blank=True, null=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)], help_text=_(
            "EPP allows you to adjust the balance of performance versus "
            "energy saving. Lower percentages indicate higher performance and "
            "lower power saving, whereas higher percentages indicate higher "
            "power saving but lower performance."
        )
    )


    @property
    def msr_epp(self):
        if self.epp == None: return None
        return int((self.epp / 100) * 255)
    @property
    def comparators(self):
        return ['max_freq']
    @classmethod
    def nulled_json(cls):
        return {
            'MAX_FREQ': {'mhz': 0},
            'MIN_FREQ': {'mhz': 0},
            'EPP': {'factor': settings.EPP_DEFAULT},
            'HWP_REQ': {
                'minimum': None,
                'maximum': None,
                'desired': None,
                'epp': None
            }
        }

    def to_json(self):
        pl = {}
        # PowrProf registers
        if self.max_freq != None:
            pl['MAX_FREQ'] = {'mhz': self.max_freq}
        if self.min_freq != None:
            try:
                pl['MIN_FREQ'] = {'mhz': self.min_freq}
            except Exception as ex:
                LOGGER.warn(
                    "Unable to determine PowerCfg MIN_FREQ, ignoring.",
                    exc_info=ex
                )
        if self.epp != None:
            pl['EPP'] = {'factor': self.epp}
        # MSR
        if pl:
            pl['HWP_REQ'] = {
                'minimum': self.min_freq,
                'maximum': self.max_freq,
                'desired': None,
                'epp': self.msr_epp
            }

        return pl

    @classmethod
    def _dat_to_kwargs(cls, data):
        kw = { }
        for key in ('MAX_FREQ', 'MIN_FREQ'):
            dat = data.get(key)
            if dat and dat.get('mhz') and dat['mhz'] != 0:
                kw[key.lower( )] = dat['mhz']
        
        hwp = data.get('HWP_REQ')
        if hwp and hwp.get('minimum') and 'minimum' in hwp:
            minm = hwp.get('minimum')
            if 'min_freq' not in kw or minm > kw['min_freq']:
                kw['min_freq'] = minm
        if hwp and hwp.get('maximum') and 'maximum' in hwp:
            maxm = hwp.get('maximum')
            if 'max_freq' not in kw or maxm < kw['max_freq']:
                kw['max_freq'] = maxm

        epp = data.get('EPP')
        if epp and epp.get('factor') != None:
            kw['epp'] = epp['factor']

        return kw

    @classmethod
    def from_json(cls, data, include_empty=False):
        kw = cls._dat_to_kwargs(data)
        if not kw and not include_empty: return None
        if not kw: kw = cls._dat_to_kwargs(cls.nulled_json( ))
        return cls(**kw)           

    @property
    def display_details(self):
        items = []
        items.append(
            _("Minimum Frequency (MHz): %(freq)s") % {
                'freq': '{}MHz'.format(self.min_freq) if (
                    self.min_freq
                ) else "OS Default"
            }
        )
        items.append(
            _("Maximum Frequency (MHz): %(freq)s") % {
                'freq': '{}MHz'.format(self.max_freq) if (
                    self.max_freq
                ) else "OS Default"
            }
        )
        items.append(
            _("EPP (Energy Performance Profile): %(epp)s") % {
                'epp': self.get_epp_display( )
            }
        )
        return "\n".join(items)

    @classmethod
    def _mhz_or_default(cls, mhz):
        if mhz:
            return "{}MHz".format(mhz)
        return _("Default")

    @property
    def short_value(self):
        if not self.max_freq and not self.min_freq and self.epp == None:
            return _("OS Defaults")
        return _("Min: %(min)s, Max: %(max)s, EPP: %(epp)s%%") % {
            'min': self._mhz_or_default(self.min_freq),
            'max': self._mhz_or_default(self.max_freq),
            'epp': _("Default") if self.epp == None else self.epp
        }

    def __str__(self):
        return self.display_details


class PowerResetRegister(Register):
    class Meta:
        verbose_name = _("reset power settings")
        verbose_name_plural = verbose_name

    NULL_CHOICES = (
        (None, _("Do not Change")),
        (False, _("No")),
        (True, _("Yes")),
    )

    reset_defaults = models.BooleanField(
        blank=True, null=True, choices=NULL_CHOICES,
        verbose_name=_("Reset Power Settings to Default Values"),
        help_text=_(
            "Select this option to reset all Operating System power settings "
            "to their default values. This can help if features such as core "
            "parking and power sensitivity features do not seem to be "
            "functioning. Please note, current power settings can not be "
            "restored as this reset will PERMANENTLY PERSIST even when the "
            "system restarts. Therefore, it is recommended to use this option "
            "once to reset power settings if you are experiencing issues, but "
            "should be disabled thereafter."
        )
    )
    @property
    def comparators(self):
        return ['reset_defaults']

    def to_json(self):
        if self.reset_defaults:
            return {'PWR_RESET': 'RESET'}
        return None

    @property
    def display_details(self):
        return _("Reset Power Settings to System Defaults") if (
            self.reset_defaults
        ) else  _("Leave Power Settings as Current")

    @property
    def short_value(self): return self.display_details

    def __str__(self): return force_str(self.display_details)


class PowerDisableWakeRegister(Register):
    class Meta:
        verbose_name = _("Hibernate Options")
        verbose_name_plural = verbose_name

    NULL_CHOICES = PowerResetRegister.NULL_CHOICES
    
    disable_wake_to_hibernate = models.BooleanField(
        blank=True, null=True, choices=NULL_CHOICES,
        help_text=_(
            "The Operating System contains a feature which, when a device "
            "has been on standby for some time (for example the lid shut on "
            "a portable device), will wake the system from standby and put it "
            "into a more permananent power saving state known as hibernation. "
            "If hibernation fails for any reason, which can be common on some "
            "systems, the Operating System will not return the system to "
            "sleep, resulting in devices turning on in cases/bags and draining "
            "the battery entirely. Select this option to prevent the OS from "
            "waking the device from sleep for hibernation purposes."
        )
    )

    def to_json(self):
        if self.disable_wake_to_hibernate == False:
            return {'WAKE_HIB': {
                'minutes': settings.DEFAULT_HIBERNATE_ENABLED_TIMER_MINS
            }}
        elif self.disable_wake_to_hibernate == True:
            return {'WAKE_HIB': {'seconds': 0}}
        else:
            return { }

    @classmethod
    def from_json(cls, data, include_empty=False):
        if 'WAKE_HIB' in data:
            wh = data.get('WAKE_HIB') or {}
            if not wh and not include_empty: return None

            scs = wh.get('seconds')
            if scs and not include_empty: return None

            return [cls(disable_wake_to_hibernate=True)]
        return None

    @property
    def comparators(self):
        return ['disable_wake_to_hibernate']

    @property
    def display_details(self):
        return _("Wake to Hibernate Disabled") if (
            self.disable_wake_to_hibernate
        ) else _("Wake to Hibernate Enabled")

    @property
    def short_value(self): return self.display_details

    def __str__(self): return force_str(self.display_details)


class PLRegister(Register):
    class Meta:
        unique_together = ('profile', 'pl')
        verbose_name = _("CPU Power Limit")

    PL_CHOICES = (
        ('pl1', _("TDP/Power Long Window (PL1)")),
        ('pl2', _("Turbo/Power Short Window (PL2)")),
    )
    pl = models.CharField(
        verbose_name=_("Power Limit Type"), 
        max_length=3, choices=PL_CHOICES, db_index=True
    )
    enabled = models.BooleanField(
        default=True, help_text=_(
            "Whether to enable this power limit window."
        ), db_index=True
    )
    clamped = models.BooleanField(
        default=False, help_text=_(
            "Whether this power limit should be clamped. This means "
            "the CPU will throttle performance to keep the power limit "
            "from being exceeded, which could result in your CPU being "
            "scaled to a low frequency and speed. If this option is "
            "not set, the CPU will only throttle during the power window."
        )
    )
    limit = models.DecimalField(
        max_digits=7, decimal_places=3,
        verbose_name=_("Power Limit (W)"), help_text=_(
            "Specify the power limit in Watts."
        ), validators = [
            MinValueValidator(settings.MIN_POWER_LIMIT),
            MaxValueValidator(settings.MAX_POWER_LIMIT)
        ]
    )

    @property
    def short_value(self):
        if not self.limit: return ""

        return "{} = {}W".format(self.pl.upper( ), floatformat(self.limit, -3))

    def to_json(self):
        return {'TPL': {self.pl: {
            'enabled': self.enabled,
            'clamped': self.clamped,
            'limit': float(self.limit),
            'limit_mw': int(float(self.limit) * 1000),
            'duration': 0, #TODO: For now, this is ignored
        }}}

    @classmethod
    def from_json(cls, data, include_empty=False):
        if 'TPL' in data:
            tp = data['TPL'] or {}
            rl = []
            for pl, data in tp.items( ):
                d = copy.copy(data)
                for a in ('duration', 'locked'):
                    try:
                        del d[a]
                    except KeyError:
                        pass
                rl.append(cls(pl=pl, **d))
            return rl
        return None

    @property
    def comparators(self): return ['pl', 'limit', 'clamped', 'enabled']

    def save(self, *args, **kwargs):
        self.limit = math.ceil(float(self.limit) * 8.0) / 8.0
        return super( ).save(*args, **kwargs)
    
    @property
    def display_name(self): return self.get_pl_display( )

    @property
    def display_details(self):
        return _("Enabled = %(e)s, Clamped = %(c)s, Limit = %(l)sW") % {
            'e': _("Yes") if self.enabled else _("No"),
            'c': _("Yes") if self.clamped else _("No"),
            'l': self.limit
        }

    def __str__(self):
        return "{}, Enabled = {}, Clamped = {}, Limit = {}W".format(
            self.get_pl_display( ), "Yes" if self.enabled else "No",
            "Yes" if self.clamped else "No", self.limit
        )

class UVRegister(Register):
    class Meta:
        unique_together = ('profile', 'plane')
        verbose_name = _("CPU Voltage Offset (UV)")

    PLANE_CHOICES = (
        ('cpu', _("CPU Core")),
        ('gpu', _("GPU Core")),
        ('cache', _("CPU Cache")),
        ('sys', _("System Agent"))
    )
    plane = models.CharField(
        max_length=5, db_index=True, verbose_name=_("component"),
        help_text=_("Select the component to undervolt."),
        choices=PLANE_CHOICES
    )
    mv = models.SmallIntegerField(
        verbose_name=_("voltage offset (mV)"), help_text=_(
            "Specify the voltage offset in milliVolts to apply to this "
            "component. To undervolt, specify a negative offset. Note, "
            "changing these values can lead to system instability."
        ), validators=[
            MinValueValidator(settings.MIN_VOLTAGE_OFFSET),
            MaxValueValidator(settings.MAX_VOLTAGE_OFFSET)
        ]
    )

    def to_json(self):
        return {'FIVR': {self.plane: {'mV': self.mv, 'code': self.plane}}}

    @classmethod
    def from_json(cls, data, include_empty=False):
        if 'FIVR' in data:
            tp = data['FIVR']
            rl = []
            for pl, data in tp.items( ):
                if data.get('mV') != 0 or include_empty:
                    rl.append(cls(plane=data['code'], mv=data['mV']))
            return rl
        return None

    @property
    def comparators(self): return ['plane', 'mv']

    @property
    def short_value(self):
        if not self.mv: return ""
        return "{} = {}".format(self.plane.upper( ), self.display_details)

    @property
    def display_name(self):
        return _("%(plane)s Voltage") % {'plane': self.get_plane_display( )}

    @property
    def display_details(self):
        return "{}mV".format(int(round(self.mv)))

    def __str__(self):
        return "{} = {}".format(self.get_plane_display( ), self.display_details)

