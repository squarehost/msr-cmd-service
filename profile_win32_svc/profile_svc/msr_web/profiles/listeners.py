import logging, os, threading, sys, time
import pythoncom

from django.conf import settings
from django.core.cache import caches

from .models import AppProfile, Profile, RestoreProfile
from .utils import get_running_apps, get_open_window_info, get_system_status
from msr import utils
cache = caches['listener']

LOGGER = logging.getLogger(__name__)

class Listener(threading.Thread):
    reset_watcher_on_profile_change = False
    app_profile_cache_key = 'app_profiles'
    settings_disable_key = None

    @classmethod
    def get_system_status(cls, wmi_instance=None, exclude=[]):
        ''' Return any relevant hardware values in a dict. This can be used to
            filter down profiles accordingly. Exclude fields in the exlcude
            parameter - listeners should self inspect and see if their 
            fields are in there and exclude accordingly.
            
            wmi_instance is an optional WMI class instance to pass in for those
            listeners that use WMI to get status variables. This should be
            instantiated if not supplied.    
        '''
        return { }

    def __init__(self, comms_thread):
        self.settings_dict = { }
        self.comms_thread = comms_thread
        threading.Thread.__init__(self)
        self.name = self.__class__.__name__

    def check_active_app_profile(self):
        act = None
        rest = RestoreProfile.objects.exclude(
            profile=None, profile__active=True
        ).exclude(profile__appprofile=None)
        if rest.count( ): 
            app_list = get_running_apps( )
            for pr in rest:
                prf = pr.profile.appprofile
                if prf.app_is_active(app_list=app_list):
                    if not act: act = prf
                else:
                    self.logger.warn(
                        "Deleting stale App RestoreProfile: {}".format(
                            pr
                        )
                    )
                    pr.delete( )

        return act

    def update_from_settings(self):
        self.settings_dict = utils.get_settings_dict( )

    def check_setting_disabled(self):
        if getattr(self, 'settings_disable_key'):
            return self.settings_dict.get(self.settings_disable_key)
        return False

    def run_loop(self):
        pass

    def start(self):
        self.running = True
        self.update_from_settings( )
        threading.Thread.start(self)

    def stop(self):
        self.running = False
        
    def get_app_profiles(self):
        self.apc = False
        val = cache.get(self.app_profile_cache_key)
        if val == None:
            self.apc = True
            if self.reset_watcher_on_profile_change:
                self.watcher = None
            LOGGER.info("Retrieving app profiles from DB.")
            profs = AppProfile.objects.filter(
                active=True
            ).order_by('-priority', '-match_type')
            cache.set(
                self.app_profile_cache_key, profs,
                settings.APP_PROFILE_CACHE_SECONDS
            )
            return profs
        return val

    def get_standard_profiles(self):
        val = cache.get('standard_profiles')
        if val == None:
            if self.reset_watcher_on_profile_change:
                self.watcher = None
            LOGGER.info("Retrieving profiles from DB.")
            profs = Profile.objects.filter(active=True, appprofile=None)
            cache.set(
                'standard_profiles', profs, settings.APP_PROFILE_CACHE_SECONDS
            )
            return profs
        return val

class ProcessModificationPollListener(Listener):
    default_poll_delay_seconds = 15
    settings_disable_key = 'DISABLE_AUTOMATION_APP'

    def __init__(self, comms_thread, poll_delay_secs=None, poll_windows=True):
        self._current_pids = set( )
        self._current_pid_info = []
        self._current_windows = set( )
        self._current_window_info = []
        self.poll_delay_seconds = poll_delay_secs or (
            self.default_poll_delay_seconds
        )
        self.poll_windows = poll_windows
        self._inc_windows = False

        self.logger = logging.getLogger("{}.process".format(__name__))
        self.logger.info("Listener {} created ({}s poll time).".format(
            self.__class__, self.poll_delay_seconds
        ))
        super( ).__init__(comms_thread)

    def get_running_pids(self):
        # Sets don't care about ordering, and we can get intersections.
        return set(get_running_apps(pids_only=True))

    def get_open_hwnds(self):
        dat = get_open_window_info( )
        return (set([d['hwnd'] for d in dat]), dat)

    def _restore_profile(self, profile, app, window_object=None):
        app = app or { }
        if window_object and isinstance(window_object, dict):
            app.update(window_object)

        return profile.restore(vars=app)

    def _process_app_creation_pids(self, new_pids):
        app_dets = get_running_apps(
            include_windows=False, sort_key=None, pids=new_pids
        )
        if not app_dets: return None

        profs = self.get_app_profiles( ).exclude(
            match_type__in=('name', 'pnme')
        )
        if not profs.count( ): return None

        self.logger.debug("C: Running app inf. to check is {}".format(app_dets))
        for app in app_dets:
            self.logger.debug("CREATE:Title is {}, ExePath is {}".format(
                app['Title'], app['ExecutablePath']
            ))

            stats = get_system_status( )
            profs = profs.filter_for_system_status(stats)

            for p in profs:
                match = p.matches(app)
                if match:
                    pn = str(p)
                    self.logger.info("C: Exec {}, match profile {} ({})".format(
                        p, p.match_type, pn
                    ))
                    p.apply(type='auto_app', exe=match)
                    return p

        return None

        
    def _process_app_deletion_pids(self, del_pids):
        info = self._current_pid_info
        pid_info = dict([
            (i['ProcessId'], i) for i in info if i['ProcessId'] in del_pids
        ])
        self.logger.info(
            "D: Checking profiles matching deleted processes: {}".format(
                pid_info
            )
        )
        if not pid_info: return

        profs = self.get_app_profiles( )
        rest = RestoreProfile.objects.exclude(profile=None)
        if not rest.count( ): return None
        rpfs = [r.profile_id for r in rest if r.profile_id]

        has_match = False
        last = None

        for dpid in del_pids:
            app = pid_info.get(dpid)
            for p in profs:
                if p.pk not in rpfs: continue
            
                LOGGER.info("Profile {} pid {}".format(p, p._pid))
                last = None
                if p._pid == dpid or (app and p.matches(app)):
                    if p._pid:
                        p.__class__.objects.filter(pk=p.pk).update(_pid=None)
                    
                    pn = str(p)
                    has_match = True
                    self.logger.info("D: Exec {}, matched prof {} ({})".format(
                        p, p.match_type, pn
                    ))
                    last = self._restore_profile(p, app)
                    if last: break

        if has_match and not last:
            self.logger.warn(
                "{} matches profile, but cannot restore. Ensure there is a "
                "valid restore profile (on boot or last applied). Existing "
                "profile will remain in place.".format(pn)
            )

        return last

    def _process_app_creation_hwnds(self, new_hwnds):
        self._current_windows, self._current_window_info = (
            self.get_open_hwnds( )
        )
        if not self._current_window_info: return None

        profs = self.get_app_profiles( ).filter(
            match_type__in=('name', 'pnme')
        )
        if not profs.count( ): return None

        stats = get_system_status( )
        profs = profs.filter_for_system_status(stats)

        winfo_dict = dict([(w['hwnd'], w) for w in self._current_window_info])
        proc_dict = dict([(d['ProcessId'], d) for d in self._current_pid_info])
        for hwnd in new_hwnds:
            window = winfo_dict.get(hwnd)
            if not window:
                self.logger.warn(
                    "C: Missing window information for new hwnd {}".format(
                        hwnd
                    )
                )
                continue
            if not (window.get('text') or '').strip( ): continue

            app = (proc_dict.get(window.get('process')) or {})
            self.logger.debug("CREATE:Title is {}, ExePath is {}".format(
                window['text'], app.get('ExecutablePath', 'Unknown')
            ))

            for p in profs:
                pn = str(p)
                match = p.matches({'Windows': [window['text']]}, app)
                if match:
                    self.logger.info("C: Exec {}, match profile {} ({})".format(
                        p, p.match_type, pn
                    ))
                    p.apply(type='auto_app', exe=app.get('ExecutablePath'))
                    pid = app.get('ProcessId')
                    if pid != None:
                        p.__class__.objects.filter(pk=p.pk).update(_pid=pid)
                    return p

        return None

    def _process_app_deletion_hwnds(self, del_hwnds):
        info = self._current_window_info
        hwnd_info = [i for i in info if i['hwnd'] in del_hwnds]
        self.logger.info(
            "D: Checking profiles matching deleted windows: {}".format(
                hwnd_info
            )
        )
        if not hwnd_info: return

        profs = self.get_app_profiles( )
        rest = RestoreProfile.objects.exclude(profile=None)
        if not rest.count( ):
            self.logger.info(
                "D: No active app profiles, none to restore."
            )
            return None
        rpfs = [r.profile_id for r in rest if r.profile_id]

        has_match = False
        last = None
        pn = None

        winfo_dict = dict([(w['hwnd'], w) for w in hwnd_info])
        proc_dict = dict([(d['ProcessId'], d) for d in self._current_pid_info])
        for hwnd in del_hwnds:
            window = winfo_dict.get(hwnd)
            if not (window.get('text') or '').strip( ): continue
            
            for p in profs:
                if p.pk not in rpfs: continue
                
                last = None
                
                app = (proc_dict.get(window.get('process')) or {})
                obj = {'Windows': [window['text']]}
                if p.matches(obj, app):
                    pn = str(p)
                    has_match = True
                    self.logger.info("D: Window {}, matched prof {} ({})".format(
                        window['text'], p.match_type, pn
                    ))
                    last = self._restore_profile(p, app, obj)
                    if last: break

        self.logger.info("D: HM {}, LAST {}, PN {}".format(
            has_match, last, pn
        ))
        if has_match and not last:
            self.logger.warn(
                "{} matches profile, but cannot restore. Ensure there is a "
                "valid restore profile (on boot or last applied). Existing "
                "profile will remain in place.".format(pn)
            )

        return last

    def run(self):
        err_count = 0

        while self.running:
            if not self._current_pids:
                self._current_pids = self.get_running_pids( )
                self._current_pid_info = get_running_apps(
                    include_windows=False, sort_key=None,
                    pids=self._current_pids
                )

            if self.check_setting_disabled( ):
                time.sleep(self.poll_delay_seconds)
                continue

            aps = self.get_app_profiles( )
            if not aps: 
                time.sleep(self.poll_delay_seconds)
                continue

            if self.poll_windows:
                self._inc_windows = aps.filter(
                    match_type__in=('name', 'pnme')
                ).count( )

                if not self._current_windows and self._inc_windows:
                    self._current_windows, self._current_window_info = (
                        self.get_open_hwnds( )
                    )
                    
            upda, new_apps, del_apps = False, set( ), set( )
            cpids = self.get_running_pids( )
            if cpids != self._current_pids:
                # Firstly check "executable/filename" profiles

                # Praise Python sets
                new_apps = cpids - self._current_pids
                del_apps = self._current_pids - cpids
                new_apps -= del_apps

                self.logger.debug(
                    "Newly created proc IDs are {}, deleted are {}".format(
                        new_apps, del_apps
                    )
                )
                upda = False
                if len(new_apps):
                    upda = self._process_app_creation_pids(new_apps)
                if not upda and len(del_apps):
                    upda = self._process_app_deletion_pids(del_apps)

                self._current_pids = cpids
                self._current_pid_info = get_running_apps(
                    include_windows=False, sort_key=None, pids=cpids
                )

            if self._inc_windows and not upda:
                # If we have profiles with window (or partial) name match, 
                # check those too.
                chwnd, chinfo = self.get_open_hwnds( )

                new_windows = chwnd - self._current_windows
                del_windows = self._current_windows - chwnd

                if new_windows or del_windows:
                    self.logger.debug(
                        "Windows: New = {}, Del = {}".format(
                            new_windows, del_windows
                        )
                    )

                upda = False
                if len(new_windows):
                    upda = self._process_app_creation_hwnds(new_windows)
                if not upda and len(del_windows):
                    upda = self._process_app_deletion_hwnds(del_windows)

                self._current_windows, self._current_window_info = (
                    self.get_open_hwnds( )
                )
                                

            time.sleep(self.poll_delay_seconds)
            self.run_loop( )

    def run_loop(self):
        # This will update the watcher to use new WQL if cache has expired.
        self.get_app_profiles( )

class WMIListener(Listener):
    wmi_class = None
    event_type = "operation"
    err_limit = 10
    delay_secs = 10
    watch_timeout = delay_secs * 1000
    fields = []
    reset_watcher_on_profile_change = False

    def __init__(self, comms_thread):
        self.logger = logging.getLogger("{}.{}".format(
            __name__, self.event_type)
        )
        self.logger.info("Listener {} created.".format(self.__class__))
        super( ).__init__(comms_thread)

    @classmethod
    def escape_wql_value(cls, value):
        return value.replace("\\", "\\\\").replace("'", "\\'")

    def get_wql_where(self):
        return "WHERE TargetInstance ISA '%s'" % (self.wmi_class)

    def _get_raw_wql(self):
        fields = self.get_fields( )
        field_list = ", ".join(fields)
        notification_type = self.event_type
        delay_secs = self.delay_secs
        class_name = self.wmi_class
        where = self.get_wql_where( )
        return (
            "SELECT %s FROM __Instance%sEvent WITHIN %d %s" % (
                field_list, notification_type, delay_secs,
                where
            )
        )

    def process_result(self, wmi_res):
        return None

    def get_watch_timeout(self):
        return self.watch_timeout

    def get_fields(self):
        return self.fields

    def get_watcher(self, **kwargs):
        return getattr(
            self.wmi, self.wmi_class
        ).watch_for(
            self.event_type,
            fields=self.get_fields( ),
            delay_secs=self.delay_secs,
            **kwargs
        )

    def run(self):
        err_count = 0

        pythoncom.CoInitialize( )
        import wmi
        self.wmi = wmi.WMI(find_classes=False)
        self.watcher = None
        while self.running:
            if self.check_setting_disabled( ):
                time.sleep(2)
                continue

            self.run_loop( )
            try:
                if not self.watcher:
                    self.watcher = self.get_watcher( )
                if not self.watcher:
                    # Not ready yet, or nothing to listen to.
                    time.sleep(1)
                else:
                    resl = self.watcher(timeout_ms=self.get_watch_timeout( ))
                    self.process_result(resl)
                    time.sleep(2)

            except wmi.x_wmi_timed_out:
                continue
            except Exception as ex:
                self.logger.warn(
                    "Error whilst listening to watcher: {}".format(ex),
                    exc_info=ex
                )
                err_count += 1
                if err_count > self.err_limit:
                    self.logger.error(
                        "Stopping listener thread {} as error limit ".format(
                            self
                        )+"exceeded."
                    )
                    self.stop( )
                    break
                time.sleep(1)
        
        try:
            del self.watcher
        except:
            pass
        try:
            del self.wmi
        except:
            pass
        try:
            pythoncom.CoUninitialize()
        except:
            pass
    
class WMIPowerEventResumeListener(WMIListener):
    '''  WMI Power Event Types:

        4
        Entering suspend

        7
        Resume from suspend

        10
        Power status change

        11
        OEM event

        18
        Resume automatic

    '''
    wmi_class = 'Win32_PowerManagementEvent'
    event_type = "operation"
    fields = ["EventType"]
    settings_disable_key = 'DISABLE_AUTOMATION_BOOT'

    def process_result(self, wmi_res):
        if wmi_res.EventType == 7:
            time.sleep(2)
            prf = self.check_active_app_profile( )
            if prf:
                self.logger.info(
                    "App profile {} is still active, applying this "
                    "instead of detecting a boot profile.".format(prf)
                )
                prf.apply(type='auto_susp')
                return

            brpf = Profile.objects.apply_on_boot(
                force=True, apply_type='auto_susp'
            )
            if brpf:
                self.logger.info(
                    "System has resumed from suspend, applied on boot "
                    "profile {}".format(brpf)
                )

class WMIPowerEventBatteryListener(WMIListener):
    wmi_class = 'Win32_PowerManagementEvent'
    event_type = "operation"
    fields = ["EventType"]
    settings_disable_key = 'DISABLE_AUTOMATION_POWER'

    def __init__(self, *args, **kwargs):
        super( ).__init__(*args, **kwargs)
        from . import ctype
        self.sys_status = ctype.GetSystemPowerStatus
        self.status_struct_data = ctype.SYSTEM_POWER_STATUS( )

    def get_battery_status(self):
        try:
            import ctypes
            stat = self.sys_status(
                ctypes.pointer(self.status_struct_data)
            )
            if not stat: raise Exception("Empty response")
            return self.status_struct_data
        except Exception as ex:
            self.logger.warn(
                "Unable to query system power status: {}".format(ex)
            )
            return None

    @classmethod
    def get_power_status(cls):
        ''' Useful method to be called from class context (e.g. when 
            checking on-boot profiles). '''
        import ctypes
        from . import ctype
        sys_status = ctype.GetSystemPowerStatus
        data = ctype.SYSTEM_POWER_STATUS( )
        sys_status(ctypes.pointer(data))
        return {
            'on_battery':  not data.ACLineStatus,
            'battery_percent': data.BatteryLifePercent
        }

    @classmethod
    def get_system_status(cls, wmi_instance=None, exclude=[ ]):
        if 'on_battery' in exclude or 'battery_percent' in exclude: return { }
        return cls.get_power_status( )

    def process_result(self, wmi_res):
        if wmi_res.EventType == 10:
            stat = self.get_battery_status( )
            if stat:
                on_battery = not stat.ACLineStatus
                bat_percent = stat.BatteryLifePercent
                last_perc = getattr(self, 'last_perc', None)
                pv = "{}_{}".format(on_battery, bat_percent)
                if last_perc == pv: return

                stats = get_system_status(
                    exclude=['on_battery', 'battery_percent']
                )
                stats.update({
                    'on_battery': on_battery, 'battery_percent': bat_percent
                })

                self.last_perc = pv
                self.logger.debug("EV: Sys. on battery: {}, percent {}".format(
                    on_battery, bat_percent
                )+", WMI: {}".format(wmi_res))

                prf = self.check_active_app_profile( )
                if prf:
                    self.logger.info(
                        "App profile {} is still active, not applying "
                        "the power change profile.".format(
                            prf
                        )
                    )
                    return

                profs = self.get_standard_profiles( ).exclude(
                    apply_on_power=None
                ).filter_for_system_status(stats)

                for p in profs:
                    has_pwr = False
                    if p.matches_power_profile(not on_battery, bat_percent):
                        self.logger.info("POWER: Profile {} matched ".format(
                            p
                        )+"DC Status {}, Battery Level {}".format(
                            "DC" if on_battery else "AC", bat_percent
                        ))
                        has_pwr = True

                    if has_pwr:
                        p.apply(type='auto_pwr')
                        break

class WMIPowerLevelBatteryListener(WMIPowerEventBatteryListener):
    settings_disable_key = 'DISABLE_AUTOMATION_POWER_POLL'

    def __init__(self, thread, poll_delay_seconds=15, *args, **kwargs):
        self.poll_delay_seconds = poll_delay_seconds
        
        self.logger = logging.getLogger("{}.battery".format(__name__))
        self.logger.info("Power level poll delay is {} seconds on DC.".format(
            self.poll_delay_seconds
        ))
        super( ).__init__(thread, *args, **kwargs)

        self.last_polled = None
        self.last_battery = None
        self.poll = False

    def get_poll_delay(self):
        return self.poll_delay_seconds

    def run_loop(self):
        if not self.poll: return

        poll = not self.last_polled
        if not poll:
            poll = (time.time( ) - self.last_polled) >= self.get_poll_delay( )

        if poll:
            self.last_polled = time.time( )
            cstat = self.get_battery_status( )
            if cstat:
                on_battery = not cstat.ACLineStatus
                bat_percent = cstat.BatteryLifePercent
                if on_battery and bat_percent != 100 and(
                    self.last_battery != bat_percent
                ):
                    self.logger.debug("Poll at {}: Battery={}, %={}".format(
                        self.last_polled, on_battery, bat_percent
                    ))
                    self.last_battery = bat_percent
                    upd = bat_percent % 10 == 0
                    if upd:
                        stats = get_system_status(
                            exclude=['on_battery', 'battery_percent']
                        )
                        stats.update({
                            'on_battery': on_battery,
                            'battery_percent': bat_percent
                        })

                        self.last_battery = bat_percent
                        self.logger.info(
                            "Bat. level is {}, checking profiles.".format(
                                bat_percent
                            )
                        )

                        prf = self.check_active_app_profile( )
                        if prf:
                            self.logger.info(
                                "App profile {} is still "
                                "active, not applying power "
                                "profile.".format(prf)
                            )
                            return 
                                                
                        profs = self.get_standard_profiles( ).exclude(
                            apply_on_power=None
                        ).filter_for_system_status(stats, exact=True)

                        for p in profs:
                            p.apply(type='auto_bat')
                            return          

    def process_result(self, wmi_res):
        if wmi_res.EventType == 10:
            stat = self.get_battery_status( )
            p = not stat.ACLineStatus
            if p != self.poll:
                self.poll = not stat.ACLineStatus
                self.logger.info(
                    "POWER POLL: Setting poll to {}".format(self.poll)
                )


    def run(self):
        stat = self.get_battery_status( )
        self.poll = not stat.ACLineStatus
        self.logger.info(
            "POWER POLL: Starting with poll = {}".format(self.poll)
        )
        super( ).run( )

class WMIHardwareEventAbstractListener(WMIListener):
    '''
        For now this class will just look at the quantity of Display
        adaptors (for tentative eGPU detection). Could be extended into
        something more "exciting".
    '''
    wmi_class = 'Win32_PnpEntity'
    event_type = "operation"
    fields = ["Caption", "DeviceID", "Service", "Status"]
    comparator = None
    required_model_field = None
    status_field_name = None
    settings_disable_key = 'DISABLE_AUTOMATION_HARDWARE'
    pnp_class = None
    pnp_fields = ["Name", "Manufacturer"]

    @classmethod
    def get_pnp_fields(cls): return cls.pnp_fields
    @classmethod
    def get_pnp_class(cls): return cls.pnp_class
    @classmethod
    def get_status_field_name(cls):
        return cls.status_field_name or cls.required_model_field
    @classmethod
    def get_required_model_field(cls): return cls.required_model_field

    def get_watcher(self, **kwargs):
        ps = self.get_standard_profiles( )
        if not ps: return None

        rmf = self.get_required_model_field( )
        has = not rmf
        if rmf and not has:
            for p in ps:
                if getattr(p, rmf, None):
                    has = True
                    break
        
        if not has: return None

        return super( ).get_watcher(
            PNPClass=self.get_pnp_class( ), **kwargs
        )

    def run_loop(self):
        # This will update the watcher to use new WQL if cache has expired.
        self.get_standard_profiles( )

    @classmethod
    def get_hardware(cls, wmi_klass=None):
        d = False
        if not wmi_klass:
            pythoncom.CoInitialize( )
            import wmi
            wmi_klass = wmi.WMI(find_classes=False)
            d = True

        ''' Get the current matching hardware. '''
        res = wmi_klass.Win32_PnPEntity(
            PNPClass=cls.get_pnp_class( ),
            fields=cls.get_pnp_fields( )
        )
        if d: wmi_klass = None
        return res

    def process_result(self, wmi_res):
        raise NotImplementedError

class WMIHardwareCountEventListener(WMIHardwareEventAbstractListener):
    comparator = "lte"

    def process_result(self, wmi_res):
        self.logger.debug("Hardware event WMI result: {}".format(wmi_res))
        hw = self.get_hardware(self.wmi)
        hw_len = len(hw)

        kls = self.get_pnp_class( )
        self.logger.info("{} items of {} hardware connected.".format(
            hw_len, kls
        ))
        self.logger.debug("  - Hardware is: {}".format(hw))

        fn = self.get_status_field_name( )
        stats = utils.get_system_status(exclude=fn)
        stats[fn] = hw_len
        prf = self.get_standard_profiles( ).exclude(**{
            "{}__{}".format(
                self.get_required_model_field( ), self.comparator
            ): hw_len
        }).filter_for_system_status(stats)

        for p in prf:
            p.apply(type='auto_hw')
            return         
        
        self.logger.debug("No matching profiles for hardware event.")

    @classmethod
    def get_system_status(cls, wmi_instance=None, exclude=[]):
        mf = cls.get_status_field_name( )
        if not mf or mf in exclude: return { }

        return {
            mf: len(cls.get_hardware(wmi_klass=wmi_instance))
        }

class WMIDisplayHardwareEventListener(WMIHardwareCountEventListener):
    pnp_class = "Display"
    status_field_name = "video_adapter_count"
    required_model_field = "apply_on_video_adapters"
