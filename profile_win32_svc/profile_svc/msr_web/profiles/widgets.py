from django.apps import apps
from django.forms.widgets import Select, FileInput
from django.utils.translation import ugettext_lazy as _

import functools, logging
LOGGER = logging.getLogger(__name__)

class AppListSelect(Select):
    pass

class _AppListSelect(Select):
    def __init__(self, attrs=None, choices=()):
        super( ).__init__(attrs)
        self._choices = list(choices)

    @property
    def choices(self):
        if not self._choices:
            self._choices = self._get_app_list_choices( )
        return self._choices
    @choices.setter
    def choices(self, chc):
        self._choices = chc

    @property
    def flat_only_choices(self):
        ac = apps.get_app_config('profiles')
        try:
            rap = ac.running_app_list
        except Exception as ex:
            LOGGER.warn("Unable to get running apps.", exc_info=ex)
            rap = []
        try:
            aac = ac.installed_app_list
        except Exception as ex:
            LOGGER.warn("Unable to get installed apps.", exc_info=ex)
            aac = []

        return (rap, aac)        

    def _get_app_list_choices(self):
        rap, aac = self.flat_only_choices

        if rap:
            rap = [(_("* Running Apps"), rap), (_("* Installed Apps"), [])]
        else:
            rap = []
        return [
            ('', _(' - Choose App or Select Override Below'))
        ] + rap + aac
