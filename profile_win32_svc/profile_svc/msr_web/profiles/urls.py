from django.contrib.auth.decorators import login_required
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter( )
router.register(r'', views.ProfileViewSet, basename='profile')

urlpatterns = [
    path('select-app/', login_required(
        views.ApplicationSelectorView.as_view( )
    ), name='select-app-popup'),
    path('app-profiles/', views.AppProfileWizard.as_view( ),
         name='setup-app-profiles'),
    path('api/v1/', include(router.urls))
]