from django import template

register = template.Library( )

@register.simple_tag
def render_named_field(form, name):
    return form[name]

