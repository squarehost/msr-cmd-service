import constance
import pytz
from django.conf import settings
from django.contrib import messages
from django.core.cache import cache
from django.db import transaction
from django.db.models import Q
from django.http import (
    HttpResponse, HttpResponseRedirect, HttpResponseBadRequest,
    HttpResponseNotModified
)
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView
from formtools.wizard.views import SessionWizardView
from rest_framework import viewsets
from rest_framework.decorators import action, throttle_classes
from rest_framework.response import Response

from . import forms, models, serializers, utils
from msr.utils import get_client

import logging, os, time

LOGGER = logging.getLogger(__name__)

class ApplicationSelectorView(TemplateView):
    template_name = 'profiles/app-selector.html'

    def get_context_data(self, **kwargs):
        running = utils.get_running_apps( )
        installed = utils.get_installed_apps( )

        return super( ).get_context_data(
            multiple=self.request.GET.get('multiple'),
            running=running, installed=installed,
            is_popup=True, **kwargs
        )

class ProfileViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Profile.objects.filter(appprofile=None)
    serializer_class = serializers.ProfileSerializer

    @action(detail=True, methods=['post'])
    def apply(self, request, pk=None, sync=True):
        try:
            obj = self.get_object( )
            if not obj:
                raise HttpResponseBadRequest("Invalid profile.")
                
            LOGGER.debug("Applying profile {} with arguments {}".format(
                obj, request.POST
            ))
            force = None
            if 'force' in request.POST:
                force = request.POST['force'] == '1'

            try:
                cli = get_client( )
                cli.clear_cache( )
            except Exception as ex:
                LOGGER.warn(
                    "When applying profile {}, cannot get client.".format(obj),
                    exc_info=ex
                )
                pass

            hotkey = bool(request.POST.get('hotkey') or False)
            try:
                resl = obj.apply(force=force, sync=sync, hotkey=hotkey)
            except Exception as ex:
                LOGGER.error(
                    "Error whilst applying profile {}".format(obj),
                    exc_info=ex
                )
                resl = False

            
            # Not sure why this is causing issues. commenting for now.
            '''if sync:
                if not resl:
                    LOGGER.warn(
                        "Sync apply process indicates profile {} did ".format(
                            obj
                        )+"not apply."
                    )
                    return HttpResponseNotModified( )'''

        except Exception as ex:
            LOGGER.error(
                "Unhandled error applying profile.", exc_info=ex
            )
            return HttpResponseNotModified( )            

        return self.retrieve(request)

    @action(detail=False, methods=['get'])
    def current(self, request, format=None):
        settings_only = request.GET.get('settings_only')
        vals = []
        try:
            cli = get_client( )
            cli.connect( )
        except Exception:
            return HttpResponse("Client unavailable", status=503)
        if not settings_only:
            raw, regs = cli.get_current_registers(include_empty=False)
            for reg in regs:
                if not reg: continue
                vals.append({
                    'model': reg._meta.model_name,
                    'display_name': reg.display_name,
                    'display_details': reg.display_details,
                    'short_value': reg.short_value
                })
        vals.append({'bookmarks': models.Profile.objects.filter(
            active=True, appprofile=None
        ).filter(
            Q(shortcut=True) | Q(force_test=True) | Q(
                hotkey_modifier__isnull=False, hotkey__isnull=False
            )
        ).order_by('-name').values('id', 'name', 'force_test')})
        vals.append({'hotkeys': [{
                'id': p.pk,
                'name': p.name,
                'hotkey': p.hotkey_value
            } for p in models.Profile.objects.filter(
                active=True
            ).exclude(
                Q(hotkey_modifier=None) | Q(hotkey=None)
            ).order_by('-name')
        ]})

        vals.append({
            'settings': {
                'config': dict(
                    (k, constance.settings.CONFIG.get(k)) for k in (
                        dir(constance.config)
                    )
                ), 'current': dict(
                    (k, getattr(constance.config, k)) for k in (
                        dir(constance.config)
                    )
                )
            }
        })
        last = []
        for p in models.ProfileLog.objects.filter(success=True)[
            0:(3 if not settings_only else 1)
        ]:
            last.append({
                'name': p.profile_name,
                'applied': p.applied.astimezone(pytz.UTC).strftime(
                    "%Y/%m/%d %H:%M:%S"
                ),
                'type': p.get_type_display( ),
            })
        vals.append({'last': last})

        return Response(vals)

class AppProfileWizard(SessionWizardView):
    form_list = [
        ('path_choice', forms.AppPathChoiceForm),
        ('profile_choice', forms.AppPathFileForm)
    ]
    template_name = 'profiles/app-profile-wizard.html'

    @transaction.atomic
    def done(self, form_list, **kwargs):
        fd = self.get_cleaned_data_for_step('path_choice')
        fld = fd.get('override_folder') or fd.get('folder')
        
        if not fld.endswith(os.path.sep): fld += os.path.sep
        # Delete the default.
        models.AppProfile.objects.filter(
            Q(
                app_path__iexact=fld
            ) | Q(manual_app_path__iexact=fld),
            match_type='defl'
        ).delete( )

        profs = self.get_cleaned_data_for_step('profile_choice')

        defl_value = None
        for key, value in profs.items( ):
            if key == "default":
                defl_value = value
                if value:
                    for vl in value:
                        models.AppProfile(
                            manual_app_path=fld, match_type='defl',
                            active=True, linked_profile_id=vl
                        ).save( )
                    messages.add_message(self.request, messages.SUCCESS,
                        _("Thank you, the default profile was set.")
                    )

        prof_add = False
        for key, value in profs.items( ):
            if not key.endswith(os.path.sep):
                key += os.path.sep

            if key != "default":
                models.AppProfile.objects.filter(
                    Q(app_path__iexact=key) | Q(
                        manual_app_path__iexact=key
                    ), match_type='defl'
                ).delete( )
                if value and value != defl_value:
                    for vl in value:
                        models.AppProfile(
                            manual_app_path=key, match_type='defl',
                            active=True, linked_profile_id=vl
                        ).save( )
                        prof_add = True

        if prof_add:
            messages.add_message(self.request, messages.SUCCESS,
                _("Thank you, one or more specific default profiles "
                "were added.")
            )
    
        return HttpResponseRedirect(reverse("home"))

    def get_template_names(self):
        if self.steps.current == "profile_choice":
            return "profiles/app-profile-wizard-profile-choice.html"
        return super( ).get_template_names( )

    def get_form_kwargs(self, step, **kwargs):
        kw = super( ).get_form_kwargs(step, **kwargs)
        if step == 'path_choice':
            chcs = cache.get('scanned_paths')
            if not chcs:
                chcs = []
                paths = utils.find_app_folders( )
                grped = {}
                for p, title in paths:
                    if title not in grped:
                        grped[title] = []
                    
                    grped[title].append((
                        p, p
                    ))

                chcs = list(grped.items( ))
                chcs.sort(key=lambda x: x[0])

                cache.set(
                    'scanned_paths', chcs, settings.APP_FOLDER_FIND_CACHE_SECS
                )
            if not chcs:
                chcs = [
                    ('', _(" - No common folders found, please choose "
                            "manually below."))
                ]
            else:
                chcs = [
                    ('', _(" - Choose a folder below"))
                ] + chcs
            kw['folder_choices'] = chcs
        elif step == 'profile_choice':
            cd = self.get_cleaned_data_for_step('path_choice')
            if cd:
                kw['main_folder'] = (
                    cd.get('override_folder') or cd.get('folder')
                )
        return kw
                
