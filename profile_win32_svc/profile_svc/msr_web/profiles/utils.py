from django.conf import settings

import copy, logging, os

try:
    import pythoncom
    pythoncom.CoInitialize( )
except:
    pass

LOGGER = logging.getLogger(__name__)

def _walk_path(path, folder_list, match_globs, max_levels):
    if max_levels <= 1: return
    try:
        paths = list(os.listdir(path))
    except:
        # Permission error or similar
        return

    for pth in paths:
        if pth in (".", ".."): continue
        bpth = os.path.join(path, pth)
        if not os.path.isdir(bpth): continue

        for sub, mg in match_globs:
            try:
                fn = os.path.join(bpth, sub)
                if os.path.exists(fn):
                    # Don't add empty folders for now
                    fp = [p for p in os.listdir(fn) if p not in (".", "..")]
                    if fp: folder_list.append((fn, mg))
            except:
                pass

        _walk_path(
            bpth, folder_list, match_globs, max_levels-1
        )

def get_local_user_sids(min_uid=1000):
    import win32net, win32netcon, win32security
    filter = win32netcon.FILTER_NORMAL_ACCOUNT
    resume_handle = 0
    level = 20
    server = None
    user_list = []
    while True:
        result = win32net.NetUserEnum(server, level, filter, resume_handle)
        user_list += [
            [user['name']] for user in result[0] if user['user_id'] > min_uid
        ]
        resume_handle = result[2]
        if not resume_handle:
            break


    for u in user_list:
        sid = win32security.LookupAccountName(server, u[0])
        u.append(":".join(str(sid[0]).split(":")[1:]))
    
    return user_list

def find_app_folders(globs=None, max_depth=None):
    ''' Find a list of app folders on local filesystems. This can be used to
        pick out steam folders, origin folders, whatever. 
    '''
    globs = globs or settings.APP_FOLDER_FIND_GLOBS
    max_depth = max_depth or settings.APP_FOLDER_FIND_MAX_DEPTH

    import win32api
    drives = [d for d in (
        win32api.GetLogicalDriveStrings( ).split('\000')
    ) if d]
    p_globs = [
        (os.sep.join(os.path.split(g[0])).lstrip(os.path.sep), g[1])
        for g in globs
    ]
    folder_list = []
    for root in drives:
        _walk_path(root, folder_list, p_globs, max_depth)
    return set(folder_list)

def _walk_app_path(game_path, exe_list, exclusions):
    for path, dirs, files in os.walk(game_path):
        if path in (".", ".."): continue

        for f in files:
            l = os.path.join(path, f)
            lf = l.lower( )
            if not lf.endswith("exe"): continue

            for excl in exclusions:
                if excl in lf: break
            else:
                fld_path = path.replace(game_path, "").lstrip(os.path.sep)
                parts = fld_path.split(os.path.sep)
                folder_name = parts[0] or parts[-1] or lf.replace(".exe", "")
                exe_list.append((l, folder_name, f))

def find_apps_in_folder(
    app_folder, exclusions=None
):
    exclusions = exclusions or settings.APP_FOLDER_EXCLUDE_EXE_NAMES
    files = []
    _walk_app_path(app_folder, files, exclusions)
    files.sort(key=lambda x: (x[1], x[2]))
    return files

def get_open_window_info(hwnd_only=False):
    ''' This is now delivered from the taskbar client as it is the only 
        task that runs under the end user's profile. '''
    from msr.models import CurrentOpenWindow
    if hwnd_only:
        return list(CurrentOpenWindow.objects.values('hwnd'))
    else:
        return list(CurrentOpenWindow.objects.values( ))

def get_running_apps(
    skip_exes=('unins0', 'uninstall', 'svchost'),
    fields=['ProcessId', 'ExecutablePath', 'Name'],
    include_windows=True, sort_key='Title', short_len=60,
    skip_windows=('msctfime ui', 'default ime', 'cspnotify', 'gdi+'),
    group_by_exe=True, pids_only=False, pids=[]
):

    import win32api, win32con, win32process
    try:
        pids = pids or win32process.EnumProcesses( )
    except Exception as ex:
        LOGGER.error(
            "Unable to enumerate running processes.", exc_info=ex
        )
        return []

    if pids_only: return pids

    windows = []
    if include_windows:
        windows = get_open_window_info( )

    windows_by_proc_id = {}
    window_text_by_proc_id = {}
    for w in windows:
        t = w['text']
        if not t or not t.strip( ): continue
        ln = t.lower( )

        skip = False
        for sw in skip_windows:
            if sw in ln:
                skip = True
                break

        if skip: continue

        p = w['process']
        if p not in windows_by_proc_id:
            window_text_by_proc_id[p] = {}
            windows_by_proc_id[p] = []
        
        if t not in window_text_by_proc_id[p]:
            windows_by_proc_id[p].append(w)
            window_text_by_proc_id[p][t] = True

    processes = []
    for pid in pids:
        hproc = None
        try:    
            hproc = win32api.OpenProcess(
                win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_VM_READ,
                0, pid
            )
            exe = win32process.GetModuleFileNameEx(hproc, 0)
            skip = False
            for skp in skip_exes:
                if skp in exe.lower( ): 
                    skip = True
                    break
            if skip: continue

            bn = os.path.basename(exe)
            proc_dict = {
                'ProcessId': pid,
                'ExecutablePath': exe,
                'ShortExecutablePath': exe if len(exe) <= short_len else (
                    "...{}".format(exe[-short_len-3:])
                ),
                'Name': bn,
                'Title': bn
            }
            proc_dict['Windows'] = wins = windows_by_proc_id.get(pid, [])
            if wins: proc_dict['Title'] = wins[0]['text']
            processes.append(proc_dict)
        except Exception as ex:
            pass
        finally:
            if hproc: win32api.CloseHandle(hproc)
    
    if group_by_exe:
        exe_procs = {}

        for proc in processes:
            exe = proc['ExecutablePath']
            if exe not in exe_procs:
                exe_procs[exe] = copy.copy(proc)
                exe_procs[exe]['_wintext'] = {}
                exe_procs[exe]['Windows'] = []

            for w in proc.get('Windows', []):
                if w['text'] not in exe_procs[exe]['_wintext']:
                    exe_procs[exe]['Windows'].append(w)
                    exe_procs[exe]['_wintext'][w['text']] = True

        processes = list(exe_procs.values( ))

    if sort_key:
        processes.sort(key=lambda x: x[sort_key])

    return processes

def get_process_priority(pid):
    import win32process, win32con, win32api
    hproc = win32api.OpenProcess(
        win32con.PROCESS_QUERY_INFORMATION,
        0, pid
    )
    try:
        return win32process.GetPriorityClass(hproc)
    finally:
        win32api.CloseHandle(hproc)
   
def set_process_priority(pid, priority):
    import win32process, win32con, win32api
    hproc = win32api.OpenProcess(
        win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_SET_INFORMATION,
        0, pid
    )
    try:
        win32process.SetPriorityClass(hproc, priority)
        kls = win32process.GetPriorityClass(hproc)
        return priority == kls
    finally:
        win32api.CloseHandle(hproc)

def get_process_affinity(pid):
    import win32process, win32con, win32api
    hproc = win32api.OpenProcess(
        win32con.PROCESS_QUERY_INFORMATION,
        0, pid
    )
    try:
        am, asm = win32process.GetProcessAffinityMask(hproc)
        return am
    finally:
        win32api.CloseHandle(hproc)
   

def set_process_affinity(pid, processors=[], all_mask=255):
    '''
        Processors should either be an empty list (which means affinity to all
        processors), or a list of True/False values indicating if a CPU should
        have affinity. Index 0 is interpreted as CPU 0, Index 1 as CPU 1, and 
        so on. For example:

        [False, True, True] means affinity to CPU 1 and 2 (but not 0)
        [True] means affinity to CPU 0 only.

        Windows supports up to 64 processors in the affinity mask.
    '''        

    if isinstance(processors, int):
        a_mask = processors or all_mask
    else:
        if not processors:
            # Allow to run on all processors
            a_mask = all_mask
        else:
            a_mask = int("".join([
                "1" if bool(c) else "0" for c in reversed(processors)
            ]), 2)

    import win32process, win32con, win32api
    hproc = win32api.OpenProcess(
        win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_SET_INFORMATION,
        0, pid
    )
    try:
        win32process.SetProcessAffinityMask(hproc, a_mask)
        am, asm = win32process.GetProcessAffinityMask(hproc)
        return am == a_mask
    finally:
        win32api.CloseHandle(hproc)

def get_process_priority_choices( ):
    from django.utils.translation import ugettext_lazy as _
    import win32con

    return ([
        (win32con.REALTIME_PRIORITY_CLASS,
            _("+++ Realtime Critical Priority [!]")
        ),
        (win32con.HIGH_PRIORITY_CLASS, _("++ Highest Priority")),
        (0x00008000, _("+ Above Normal Priority")),
        (win32con.NORMAL_PRIORITY_CLASS, _("Normal Priority")),
        (0x00004000, _("- Below Normal Priority")),
        (win32con.IDLE_PRIORITY_CLASS, _("-- Lowest Priority")),
    ], win32con.NORMAL_PRIORITY_CLASS)

def get_local_cpu_cores(default=8):
    try:
        import pythoncom
        pythoncom.CoInitialize( )
        import wmi
        wmi_inst = wmi.WMI( )

        for p in wmi_inst.Win32_Processor(["NumberOfLogicalProcessors"]):
            return int(p.NumberOfLogicalProcessors)
    except Exception as ex:
        LOGGER.warn(
            "Unable to query WMI for number of logical processors.", exc_info=ex
        )
        return default



def get_installed_apps(
    subkey=r'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall',
    skip_exes=('unins0', 'uninstall')
):
    skip_exes = [e.lower( ) for e in skip_exes] if skip_exes else []
    app_list = []
    try:
        import winreg
        reg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
        key = winreg.OpenKeyEx(reg, subkey)

        count = -1
        while True:
            count += 1
            try:
                sub = winreg.OpenKey(key, winreg.EnumKey(key, count))
                iloc, t = winreg.QueryValueEx(sub, "InstallLocation")
                name, t = winreg.QueryValueEx(sub, "DisplayName")
                iloc = str(iloc).strip( )
                if not iloc: continue
                
                exes = []
                apps = []
                for item in os.listdir(iloc):
                    l = item.lower( )

                    try:
                        ext = os.path.splitext(l)
                        if ext[-1] in ('.exe', 'exe'):      
                            ''' Skip common EXEs we are not interested in, 
                                e.g. uninstallers. '''
                            skip = False
                            for c in skip_exes:
                                if c in l:
                                    skip = True
                                    break
                            if not skip: exes.append(item)
                    except:
                        pass
                

                if exes:
                    for e in exes:
                        apps.append((
                            os.path.join(iloc, e), e
                        ))
                    
                    apps.sort(key=lambda x: x[1])
                    if len(apps) > 1:
                        apps.append((
                            os.path.join(iloc, "**"),
                            "[Any Executable in this App]"
                        ))
                    app_list.append((name, apps))
            
            except FileNotFoundError:
                # The key doesn't have the required fields
                continue
            except OSError as ex:
                # No more sub keys (or another unknown error)
                break
            except Exception as ex:
                LOGGER.warn("Unable to iterate uninstall key: {}".format(
                    ex
                ), exc_info=ex)

        app_list.sort(key=lambda x: x[0])
    except Exception as ex:
        LOGGER.error("Unable to iterate installed apps: {}".format(
            ex
        ), exc_info=ex)
        return []

    return app_list
    
def get_listener(comms, klass, args):

    if isinstance(klass, str):
        try:
            from . import listeners
            klass = getattr(listeners, klass)
        except AttributeError:
            LOGGER.error("Unknown listener class {}".format(klass))
            return None

    try:
        return klass(comms, *args)
    except Exception as ex:
        LOGGER.error("Unable to initialise listener client: {}".format(
            ex
        ), exc_info=ex)
        return None

def get_system_status(wmi_instance=None, exclude=[ ]):
    from . import listeners

    status = { }
    for k in dir(listeners):
        if k[0] != "_":
            try:
                obj = getattr(listeners, k)
                if hasattr(obj, 'get_system_status'):
                    status.update(obj.get_system_status(
                        wmi_instance=wmi_instance, exclude=exclude
                    ))
            except Exception as ex:
                LOGGER.warn("Unable to get_system_status for {}".format(
                    k
                ), exc_info=ex)
    LOGGER.debug("Current system listener status is {}".format(status))
    return status

def get_hardware_count_listeners( ):
    from . import listeners

    hc = []
    for k in dir(listeners):
        if k[0] != "_":
            try:
                obj = getattr(listeners, k)
                if hasattr(obj, 'required_model_field'):
                    hc.append(obj)
            except:
                pass
    
    return hc
