import json, wmi

w = wmi.WMI( )

controllers = {}

usb = w.Win32_USBController( )
for u in usb:
    controllers[u.DeviceId] = {
        'Details': str(u),
        'Devices': []
    }

print("Retrieved {} USB controllers.".format(len(controllers)))

usb_d = w.Win32_USBControllerDevice( )
for dev in usb_d:
    ant = dev.Antecedent
    controllers[ant.DeviceId]['Devices'].append(str(dev.Dependent))

f = open("output.json", "w")
f.write(json.dumps(controllers))
f.close( )
