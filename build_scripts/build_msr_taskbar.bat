@ECHO OFF

set src_path=%~dp0
CD %src_path%

cd ..\taskbar_menu\msr_taskbar
pyinstaller -D  --noconfirm --noconsole --windowed --workpath=..\..\build\build --distpath=..\..\dist taskbar_menu.spec

CD %src_path%