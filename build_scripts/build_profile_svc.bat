@ECHO OFF

set src_path=%~dp0
CD %src_path%

cd ..\profile_win32_svc\profile_svc\msr_web
pyinstaller -D --noconfirm  --workpath=..\..\..\build\buildprof --distpath=..\..\..\dist profile_svc.spec

CD %src_path%