@ECHO OFF
set signtool="C:\Program Files (x86)\Windows Kits\10\App Certification Kit\signtool.exe"
set src_path=%~dp0
CD %src_path%\..\
ECHO Building Documentation...
CALL docs/make.bat html
ECHO Building Application Bundle...

pyinstaller -D  --noconfirm --workpath=build\build --distpath=dist\_bundle pyinstaller-bundle.spec
IF EXIST %signtool% (
    ECHO Signing Individual Applications...
    %signtool% sign /a /fd SHA256 dist\_bundle\msr_profile_svc.exe
    %signtool% sign /a /fd SHA256 dist\_bundle\msr_svc\msr_svc.exe
    %signtool% sign /a /fd SHA256 dist\_bundle\msr_taskbar\msr_taskbar.exe
)