@ECHO OFF

set src_path=%~dp0
CD %src_path%

cd ..\msr_win32_svc\
pyinstaller -D  --noconfirm --workpath=..\build\build --distpath=..\dist msr_svc.spec

CD %src_path%