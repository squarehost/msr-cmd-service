@ECHO OFF

set src_path=%~dp0
CD %src_path%

ECHO Building MSR Service...
CALL build_msr_svc.bat
ECHO Building Web/Profile Service...
CALL build_profile_svc.bat
ECHO Building Taskbar Application....
CALL build_msr_taskbar.bat