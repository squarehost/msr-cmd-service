!include "MUI2.nsh"
!include nsDialogs.nsh

Name "MSR CPU and Power Management Service"
!define MSR_VERSION "1.12.b4"
OutFile "InstallMSR${MSR_VERSION}.exe"
Unicode True

SetCompressor /SOLID /FINAL lzma

RequestExecutionLevel admin

InstallDir "$LOCALAPPDATA\MSR"
InstallDirRegKey HKCU "Software\MSR" ""

!define MUI_ICON "..\taskbar_menu\msr_taskbar\ico\green-chip.ico"

!define MUI_ABORTWARNING
!define MUI_TEXT_LICENSE_TITLE "MSR and Power Management Service"
!define MUI_TEXT_LICENSE_SUBTITLE "(C) 2021 Squarehost Ltd."

!insertmacro MUI_PAGE_LICENSE "Docs\license.txt"

!insertmacro MUI_PAGE_DIRECTORY

!define MSR_SERVICE_NAME "winmax-msr-tdpuv-svc"
!define WEB_SERVICE_NAME "winmax-msr-web-svc"
!define TASKBAR_EXE "msr_taskbar.exe"
!define WEB_PATH "http://127.0.0.1:8998"
#!define SIGN_TOOL "ECHO Replace this with path to Windows SDK SignTool"
!define SIGN_TOOL "C:\Program Files (x86)\Windows Kits\10\App Certification Kit\signtool.exe"
!finalize '"${SIGN_TOOL}" sign /a /fd SHA256 "%1"'

Var Dialog
Var Label
#var Details
var BootCheckbox
var StartCheckbox
var TrayCheckbox

var BootSetting
var StartSetting
var TraySetting
var Startup

Page custom optsDialogPage optsDialogPageLeave
Page instfiles

Function optsDialogPage
    nsDialogs::Create 1018
	Pop $Dialog

	${If} $Dialog == error
		Abort
	${EndIf}

	${NSD_CreateLabel} 0 0 100% 12u "Installation Options"
	Pop $Label

	#${NSD_CreateLabel} 0 13u 100% 24u "Please note, installing over an existing installation will result in an upgrade. No settings or profiles will be lost."
	#Pop $Details
   
	${NSD_CreateCheckbox} 0 25u 100% 10u "& Run On Boot"
	Pop $BootCheckbox
    # TODO: We should probably read this from the service startup type, but SCService plugin does not appear to work
	${NSD_Check} $BootCheckbox
	${NSD_CreateLabel} 0 35u 100% 24u "Enabling services to run on boot allows profiles to automatically be applied. If you do not start the services on boot, they will need to be manually started in order for you to use the MSR services."

	${NSD_CreateCheckbox} 0 73u 100% 10u "& Start Services After Installation"
	Pop $StartCheckbox
	${NSD_Check} $StartCheckbox


  ${NSD_CreateCheckbox} 0 88u 100% 12u "& Launch Status/Quick Launch Icon On Boot (Required for Hotkey and Window Detection)"
	Pop $TrayCheckbox
	${NSD_Check} $TrayCheckbox

	nsDialogs::Show
FunctionEnd

Function optsDialogPageLeave
	${NSD_GetState} $BootCheckbox $BootSetting
	${NSD_GetState} $StartCheckbox $StartSetting
	${NSD_GetState} $TrayCheckbox $TraySetting
FunctionEnd

Section
  DetailPrint "Stopping any relevant running services..."
  nsExec::Exec 'TaskKill /IM "${TASKBAR_EXE}" /F'
  nsExec::Exec 'net stop "${MSR_SERVICE_NAME}"'
  nsExec::Exec 'net stop "${WEB_SERVICE_NAME}"'

  Sleep 3000
  SetOutPath "$INSTDIR"
  
  File /r /x *.pyc /x locale /x __pycache__ /x *.mo /x *.po ..\dist\_bundle\*
  File /r /x *.pyc /x __pycache__ /x *.mo /x *.po ..\dist\_bundle\msr_svc\*
  File /r /x *.pyc /x __pycache__ /x *.mo /x *.po ..\dist\_bundle\msr_taskbar\*

  RMDir /r "$INSTDIR\msr_svc"
  RMDir /r "$INSTDIR\msr_taskbar"

  ${If} $BootSetting == 1
    StrCpy $Startup "auto"
  ${Else}
    StrCpy $Startup "manual"
  ${EndIf}

  DetailPrint "Installing services (startup = $Startup)..."
  nsExec::Exec '"$INSTDIR\msr_svc.exe" --startup=$Startup install'
  Pop $0
  Pop $1
  ${If} $0 != 0
      MessageBox MB_OK "Unable to install msr_svc.exe: $1"
  ${Endif}
  nsExec::Exec '"$INSTDIR\msr_profile_svc.exe" --startup=$Startup install'
  Pop $0
  Pop $1
  ${If} $0 != 0
      MessageBox MB_OK "Unable to install msr_profile_svc.exe: $1"
  ${Endif}
 

  DetailPrint "Enabling LIVE mode :-O"
  CopyFiles $INSTDIR\msr_profile_svc\_msr_settings.py $INSTDIR\msr_profile_svc\msr_settings.py

  ${If} $StartSetting == 1
    DetailPrint "Starting MSR service..."
    nsExec::Exec 'net start "${MSR_SERVICE_NAME}"'
    Pop $0
    Pop $1
    ${If} $0 != 0
        MessageBox MB_OK "Unable to start msr_svc.exe: $1"
    ${Endif}
    DetailPrint "Starting Web service..."
    nsExec::Exec 'net start "${WEB_SERVICE_NAME}"'
    Pop $0
    Pop $1
    ${If} $0 != 0
        MessageBox MB_OK "Unable to start msr_profile_svc.exe: $1"
    ${Else}
      ${If} $TraySetting == 0
        DetailPrint "Waiting to launch browser..."
        Sleep 3000
        ExecShell "open" "${WEB_PATH}"
      ${Endif}
    ${Endif}
  ${EndIf}  


	createDirectory "$SMPROGRAMS\MSR"
	createShortCut "$SMPROGRAMS\MSR\MSRTray.lnk" "$INSTDIR\${TASKBAR_EXE}" "" "$INSTDIR\msr_taskbar\ico\green-chip.ico"
 
  ${If} $TraySetting == 1
    DetailPrint "Installing Tray Icon to Start on Boot..."
    CreateShortCut "$SMSTARTUP\MSRTray.lnk" '"$INSTDIR\${TASKBAR_EXE}"'
    nsExec::Exec '"$WINDIR\explorer.exe" "$INSTDIR\${TASKBAR_EXE}"'
  ${EndIf}  

  ;Store installation folder
  WriteRegStr HKCU "Software\MSR" "" $INSTDIR

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MSR" \
                  "DisplayName" "${MUI_TEXT_LICENSE_TITLE}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MSR" \
                  "DefaultIcon" "$INSTDIR\msr_taskbar\ico\green-chip.ico"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MSR" \
                  "UninstallString" "$\"$INSTDIR\Uninstall.exe$\""

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

Section "Uninstall"

  DetailPrint "Stopping any relevant running services..."
  nsExec::Exec 'TaskKill /IM "${TASKBAR_EXE}" /F'
  nsExec::Exec 'net stop "${MSR_SERVICE_NAME}"'
  nsExec::Exec 'net stop "${WEB_SERVICE_NAME}"'
  nsExec::Exec '"$INSTDIR\msr_profile_svc.exe" remove'
  nsExec::Exec '"$INSTDIR\msr_svc.exe" remove'

  Delete "$INSTDIR\Uninstall.exe"
  RMDir /r "$INSTDIR"
  
  Delete "C:\Windows\System32\config\systemprofile\AppData\Local\msr-service.log"
  Delete "C:\Windows\System32\config\systemprofile\AppData\Local\msr-web.log"
  Delete "C:\Windows\System32\config\systemprofile\AppData\Local\msr-web.sqlite3"

	RMDir /r  "$SMPROGRAMS\MSR"

  DeleteRegKey /ifempty HKCU "Software\MSR"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MSR"
  
SectionEnd


!insertmacro MUI_LANGUAGE "English"