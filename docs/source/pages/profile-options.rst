=========================
Profile Options Reference
=========================

Hopefully you are up and running with some basic profiles at this point,
but you will have noticed there are several different options that are
available.

Whilst the usage is fairly self-explanatory, this page documents each setting,
along with technical information on how it is applied at the subsystem level.

^^^^^^^^^^^^^^^^
CPU Power Limits
^^^^^^^^^^^^^^^^

""""""""
Synopsis
""""""""

Modern Intel CPU/SoCs contain a Turbo architecture (Turbo 2.0 or Running Average Power
Limit [RAPL]), providing temporary additional performance when required, at the 
expense of additional heat. Several different mechanism are provided to ensure
the CPU remains within a set power limit (usually specified in watts) over different
periods of time, whilst maximising possible performance.

Two mechanisms provided by *RAPL*, known as **PL1** (or Long Power Window) and **PL2**
(or Short Power Window) can be tuned by the project services.

**PL1** is used to control power limits over significant periods of time (e.g. 
seconds), whilst **PL2** is used to control power limits over much shorter periods
of time.

In theory, your system will aim to sustain the **PL1** power over longer periods of
time, whilst *bursting* to **PL2** if it is able to within the overall thermal
constraints given.

There are many reasons why you may wish to limit or increase the PL1/PL2 limits
set by your OEM, but consider one scenario whereby you are looking to extend
battery life as much as possible, whilst still maintaining an appropriate level of
performance for portable gaming.

Your system will always try and achieve the best performance/frame rate possible,
within the power limits, regardless of whether this is actually required. For example,
older games or emulators may be able to reach usable frame rates at lower power
limits, but the system will still try and get the best FPS and performance possible,
using power (up to power limits), generating heat, and reducing battery life without
any real gain for the end user.

One method to prevent this happening is to set a lower limit on the
PL1 and PL2 windows, artificially limiting the CPU/SoC and preventing it from 
scaling above a certain level of power consumption. In effect you can tell your SoC
to not scale above a certain power level. Of course, maximum performance will suffer,
but in scenarios where you know maximum performance is not required this can bring
huge benefits in terms of battery life and cooling.

As a real-world use case, my GPD Win Max has an Intel Core i5-1035G7 processor - 
capable of scaling up to at least 25W out-of-the-box. When the system is taxed at
a high-level (e.g. gaming), I will typically get 90 minutes battery life, and very
warm hands. 

One game that I play regularly is Metal Gear Solid V: The Phantom Pain, which seems
to have a rather well-optimised engine and PC port. Given that the Win Max has an
800P screen, I have built a profile that sets the PL1 and PL2 windows to a maximum
of 11.75W, and configured the game to have a frame rate cap of 30FPS. This is 
absolutely fine for my needs, and with some other settings tweaks, enables me to
get around 4 hours of battery life, without much fan noise or sweaty fingers.

Conversely, you may be looking to ensure that your system maintains the maximum
power limits possible in certain scenarios (such as when on AC power), and as such
you can combine the TDP settings with the power automation to achieve this.

"""""""""""""""""
Available Options
"""""""""""""""""

As per the rationale of this project, we aim to give the user an easy-to-use method
of setting some of the key controls available within the CPU, without overloading
the user with technical/advanced options.

Therefore, within the administration system, you can enable/disable power limits, enable/disable
clamping (allowing the CPU to go below the requested state to meet power limits),
and set the actual power limit directly, as a decimal wattage value to three decimal
places. Nominally, the power limits will be rounded to the nearest eighth of a watt
(e.g. 0.125W) in order to comply with hardware limitations.

  .. image:: ../img/ref/rapl.png
    :width: 600
    :alt: RAPL profile configuration

Select the relevant power limit from the drop-down, and enter the values accordingly.
Setting a limit above what your CPU can physically draw will have no effect; it will
still be limited to the maximum the hardware can provide.

Each power limit can be specified once per profile, so within the current release you
can have a PL1 and a PL2 value.

""""""""
Defaults
""""""""

If a power limit is not specified in a profile, a default is **NOT** set, and the
system simply leaves the current value in place. If no previous profile has been
applied this will be the OEM default. If a previous profile has been applied it
will be whatever is specified within this.

"""""""""""""""""
Technical Details
"""""""""""""""""

  .. table::
    :align: left

    +--------------------------+--------------------------------------------------+
    | Underlying Subsystem(s)  | Intel MSR                                        |
    +--------------------------+--------------------------------------------------+
    | Register(s) Set          | 0x610 (MSR_PKG_POWER_LIMIT)                      |
    +--------------------------+--------------------------------------------------+
    | Other Utilised Registers | 0x606 (MSR_RAPL_POWER_UNIT)                      |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Parameter   | TPL                                              |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Format      | A JSON object with at least one of PL1 or PL2    |
    |                          | key, which is an object with the following keys: |
    |                          |                                                  |
    |                          |    *enabled*: true if enabled else false         |
    |                          |                                                  |
    |                          |    *clamped*: true if enabled else false         |
    |                          |                                                  |
    |                          |    *limit*: floating point number, power limit in|
    |                          |           Watts (max 3 D.P.). Should be rounded  |
    |                          |           to the nearest quarter watt but will   |
    |                          |           be fixed by the MSR service if not.    |
    |                          |                                                  |
    |                          |    *duration*: Power window length in seconds,   |
    |                          |              *currently ignored!*                |
    |                          |                                                  |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Example     | .. code-block:: json                             |
    |                          |                                                  |
    |                          |    {"TPL": {                                     |
    |                          |        "PL1": {                                  |
    |                          |            "enabled": true, "clamped": true,     |
    |                          |            "limit": 15, "duration": 0            |
    |                          |        }, "PL2": {                               |
    |                          |            "enabled": true, "clamped": true,     |
    |                          |            "limit": 20, "duration": 0            |
    |                          |        }                                         |
    |                          |    }}                                            |
    +--------------------------+--------------------------------------------------+
    | Methodology              | PL1=Read/write using 32-bit int (EAX Register)   |
    |                          | PL2=Read/write using 32-bit int (EDX Register)   |
    |                          |                                                  |
    |                          | Power limits are specified in units given in the |
    |                          | MSR RAPL_POWER_UNIT (606h), which is read when   |
    |                          | a request is made to read or write the package   |
    |                          | power limit. For example, power unit (watts) is  |
    |                          | stored as an integer representing 0.125W. E.g.   |
    |                          | 1 = 0.125W, 2 = 0.250W etc. This also determines |
    |                          | the maximum resolution of the limits that can be |
    |                          | set.                                             |
    |                          |                                                  |
    |                          | Power limit settings are encoded as a 32-bit     |
    |                          | value, with bits 0 to 14 being the user-set PL   |
    |                          | wattage divided by the power units. For example, |
    |                          | 10W / PU of 0.125W = 80.                         |
    |                          | Enablement/clamping are stored as single bits,   |
    |                          | with 0 being off and 1 being on (bits 15 and 16) |
    |                          | The time window is stored in bits 17:23, but as  |
    |                          | of the current release we do **NOT** support     |
    |                          | alteration of the time window.                   |
    |                          | EDX bit 32 determines whether the MSR has been   |
    |                          | locked, and therefore cannot be updated by the   |
    |                          | user. This is read from the register accordingly,|
    |                          | and can be used by the web/profile service or    |
    |                          | another client to specify if RAPL control is     |
    |                          | allowed with the SoC.                            |
    |                          |                                                  |
    |                          | In an effort to improve compatibility, any bits  |
    |                          | we do not modify (e.g. time limits) are read from|
    |                          | the existing MSR and written-back accordingly,   |
    |                          | NB they are not modified at all.                 |
    +--------------------------+--------------------------------------------------+

^^^^^^^^^^^^^^^^^^^^^^^^
CPU Voltage Offset (UV)s
^^^^^^^^^^^^^^^^^^^^^^^^

""""""""
Synopsis
""""""""

** Note, the majority of the important research with regards to UV/FIVR was obtained from
the `linux-intel-undervolt <https://github.com/mihic/linux-intel-undervolt>`_ team. **

Since Intel's 4th generation of SoCs, *Fully Integrate Voltage Regulators* (FIVRs)
have been included, which in effect, allow the user to reduce the voltage supplied 
to different components of their CPU/SoC. Most people have heard of the term
**Undervolting** (UV) with regards to system tuning.

Nominally, a specific component of a specific model of CPU will operate at a
standard voltage. Depending on the "Silicon Lottery", quite literally the luck of
the draw, your individual component may be able to operate at a
normal level of performance and stability, but at less than the standard voltage.
This offers a whole load of benefits, such as lower temperatures, improved battery
life, and so on without any real drawbacks.

In our experience, most CPUs are able to sustain a moderate level of UV, even if
you don't have the best silicone available.

**It is important to note that the optimium undervolt for any single instance of 
a processor can vary wildly. Regardless of whether you have the same model of 
processor as somebody else, you may be able to achieve significantly better or 
worse results just depending on how good your SoC is!**

"""""""""""""""""""""
Components and Planes
"""""""""""""""""""""

Each Intel Core SoC has several components, known as *planes*. The services support
the FIVR for four different planes (numbered from zero):

0. **CPU Core**

As the name suggests, this is the the FIVR for the main CPU cores.

1. **GPU Core (Integrated)**

This is the FIVR for the integrated GPU, if applicable to your SoC.

2. **CPU Cache**

This is the FIVR for the CPU cache (L1/L2 etc).

4. **System Agent / Uncore**

This is the FIVR for the system agent (also called Uncore by Intel), which governs **non-core**
CPU functions outside of the cache, such as the memory controller and Thunderbolt controller.

.. topic:: Maybe leave the SA alone?!

    The System Agent seems to be the most sensitive plane, which can cause a lot
    of instability even at minimal UV. I suspect this is because the memory 
    controller is clearly key to the other planes within the SoC, so has some
    sort of cumulative effect. Either way, it seems there are not a huge amount of
    gains to be had by undervolting the System Agent anyway, so it could be wise
    to leave this alone if you have reasonable UVs configured for the other 
    planes anyway.

"""""""""""""""""
Available Options
"""""""""""""""""

Within the administration system, there is a section called **CPU Voltage Offset (UV)s**,
which in a similar fashion to the CPU Power Limits, allows you to choose the plane/
component to undervolt from an easy-to-use drop-down menu, and then enter the voltage
offset you wish to apply, in millivolts. Undervolts are specified using a negative
offset, so in the example below we can see the CPU, GPU and Cache have been undervolted
accordingly.


  .. image:: ../img/ref/uv.png
    :width: 600
    :alt: FIVR (UV) profile configuration

** Positive offsets (overvolting) are disabled in the current build for safety reasons,
as overvolting can cause SoC damage. We may consider re-instanting this in the future,
but it would require the user to activate a specific setting to prevent any unhappy
accidents! **

.. topic:: The optimal undervolt

    Without question, reducing the voltage to a component of an SoC by too great
    a value will cause system instability, likely resulting in app crashes in the
    best case, and commonly, complete system lockups.

    Whilst it is unlikely that undervolting will cause any real damage, other than
    inconvenient crashes if you try to push things too far, you should be aware
    that we are not responsible for any issues that may occur by misuse of this
    functionality. That said, we simply set standard Intel hardware registers to
    achieve this.

    As mentioned previously, different chips of the same model can achieve wildly
    different Undervolt values so it really is a case of trial and error to 
    see what gives you the best performance whilst maintaining pure stability.

    We recommend you start with a profile that has a moderate undervolt on the
    three key planes discussed above (you can probably ignore System Agent (SA) as
    it is more trouble than it is worth), such as -40mV, and then perform a
    variety of tasks. One easy method would be to use the benchmark mode in a game,
    such as Bioshock Infinite, to try and get the SoC working hard for periods,
    but still scaling down in quiter moments.

    If you are happy that the system is completely stable, you can increase the
    undervolt value by an increment (5mV if you have time on your hands, 10/15mV if
    you are feeling brave) and repeat. 

    **If you experience any instability you should reduce the UV accordingly. If games
    tend to crash back to Windows it is a good indicator that the GPU undervolt is
    too high, if you experience complete lock ups, it probably means the CPU or cache
    undervolt is too great.**

    It is also recommended to try the undervolt at different Power Limits (as 
    determined by the CPU Power Limit setting in the profile), as sometimes a UV
    might be stable at a higher limit, but not a lower limit, or vice versa.

    Using this process, you should eventually get to a profile that has a good
    balance of stability and performance/battery improvements for your specific
    system.

    As per our `Quickstart guide <quickstart.html>`_, it is a very good idea to
    store your UV values in a base profile so you can link to, and utilise them
    in other profiles  without having to re-enter the same UV values.
    **This also has the added benefit that if you do discover down the line that
    the UV for, say, your GPU is too great, you just have to update your base
    profile to adjust it for everything, rather than have to go through the
    painstaking process of changing every profile you have defined!**

""""""""
Defaults
""""""""

If an FIVR plane offset is not specified in a profile, a default is **NOT** set, and
the system simply leaves the current value in place. If no previous profile has been
applied this will be the OEM default (zero). If a previous profile has been applied it
will be whatever is specified within this.

"""""""""""""""""
Technical Details
"""""""""""""""""

  .. table::
    :align: left

    +--------------------------+--------------------------------------------------+
    | Underlying Subsystem(s)  | Intel MSR                                        |
    +--------------------------+--------------------------------------------------+
    | Register(s) Set          | 0x150 (MSR_PKG_POWER_LIMIT)                      |
    +--------------------------+--------------------------------------------------+
    | Other Utilised Registers | None                                             |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Parameter   | FIVR                                             |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Format      | A JSON object with at least one of with at least |
    |                          | one of the following keys, relating to a plane:  |
    |                          | ``cpu, gpu, cache, sys``                         |
    |                          |                                                  |
    |                          | A given key's value should be a JSON object with |
    |                          | a mV key and a relative integer value in mV.     |
    |                          |                                                  |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Example     | .. code-block:: json                             |
    |                          |                                                  |
    |                          |    {"FIVR": {                                    |
    |                          |        "cpu": {                                  |
    |                          |            "mV": -50                             |
    |                          |        },                                        |
    |                          |        "gpu": {                                  |
    |                          |            "mV": -70                             |
    |                          |        },                                        |
    |                          |        "cache": {                                |
    |                          |            "mV": -20                             |
    |                          |        }                                         |
    |                          |    }}                                            |
    +--------------------------+--------------------------------------------------+
    | Methodology              | EAX contains the relative voltage offset, encoded|
    |                          | as an 11-bit integer, according to rules given   |
    |                          | at the linux-intel-undervolt guide [1]_.         |
    |                          |                                                  |
    |                          | The EAX always contains this encoded voltage,    |
    |                          | so reading contains the current value, writing   |
    |                          | expects the new value to be set.                 |
    |                          |                                                  |
    |                          | Access to this MSR is unusual, we call this a    |
    |                          | "readback" register (for want of a better term)  |
    |                          | as even for a read-only operation, you must first|
    |                          | write a "control" value to EDX with the plane    |
    |                          | you wish to read. Once issued, immediately       |
    |                          | reading from the 0x150 register gives you the    |
    |                          | desired encoded voltage within the EAX register. |
    |                          | EAX should be 0 for read operations.             |
    |                          |                                                  |
    |                          | The control value is packed in EDX as follows:   |
    |                          |                                                  |
    |                          |   ``0x80000[PLANE_IDX]1[READ/WRITE]``            |
    |                          |                                                  |
    |                          | Where PLANE_IDX is the plane index between 0 and |
    |                          | 4 as per the Components/Planes documentation     |
    |                          | above, read/write = 1 for write, 0 for read,     |
    |                          | and voltage is the 11 bit encoded voltage, or 0  |
    |                          | for a read operation.                            |
    +--------------------------+--------------------------------------------------+

.. [1] `Linux Intel Undervolt guide GIT repo. <https://github.com/mihic/linux-intel-undervolt>`_

^^^^^^^^^^^^
Core Parking
^^^^^^^^^^^^

""""""""
Synopsis
""""""""

Modern versions of Windows have provided support for **core parking**. On multi-core CPUs,
it is possible to put certain cores to sleep to attempt to save power and increase
battery life. In addition, this may also allow power to reassigned to other core 
components (e.g. the integrated GPU) which would otherwise be utilised by the CPU.
Typically, the OS will determine whether to park any cores based on workload - so if the
system is idling it will put cores to sleep to save power.

In recent Windows releases the core parking options have been hidden from the user by
default, with the onus on the CPU vendor to determine if core parking should be enabled
and how it should be implemented.

Our services allow you to determine a minimum number and maximum number of cores to be
parked - where minimum is cores that are *ALWAYS* parked, and maximum is the cores that 
*MAY* be parked when the CPU is in a low-powered state. **Note, the number of parked
cores refers to the physical cores, not threads.**

One use case for this may be whilst utilising an old game or application that is not
well multi-threaded, and therefore does not make use of additional CPU cores. Parking
a certain number of cores (for example, minimum 2, maximum 2) might result in lower
power consumption, or the ability to boost GPU clocks accordingly.

These options utilise Windows Power Profile settings only - they do **not** modify any
CPU registers.

"""""""""""""""""
Available Options
"""""""""""""""""

Within the administration system, there is a section called **CPU Core Parking**,
which allows you to specify the minimum number of cores to park, and the maximum
number of cores. As mentioned previously, these refer to **physical cores**, not
threads.

The maximum number of parked cores is retrieved from Windows via the MSR service,
and as such, is based on the maximum number of cores available within your system
processor.

It is possible to set the minimum or maximum to the total number of cores available
within your CPU. This does not automatically cause all cores to be parked, but
does encourage Windows to be as aggressive as possible when determining when to park
the cores.

In the below example, the system will always have three physical cores parked, 
regardless of workload. You can note that the system has detected the maximum 
available cores (see the help text for each option), and will validate this
accordingly.

  .. image:: ../img/ref/core-parking.png
    :width: 600
    :alt: Example Core Parking Configuration

""""""""
Defaults
""""""""

If core parking is not specified within a profile, it will set the minimum/maximum
cores to park to the system default (e.g. minimum = 0, maximum = 0). This is the
equivalent of not changing the core parking options from boot.

"""""""""""""""""
Technical Details
"""""""""""""""""

  .. table::
    :align: left

    +--------------------------+--------------------------------------------------+
    | Underlying Subsystem(s)  | Windows Power Profile (via PowrProf.DLL)         |
    +--------------------------+--------------------------------------------------+
    | Register(s) Set          | CPMINCORES, CPMAXCORES                           |
    +--------------------------+--------------------------------------------------+
    | Other Utilised Registers | None                                             |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Parameter   | PARK                                             |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Format      | A JSON object with at least one of with at least |
    |                          | one of the following keys,                       |
    |                          | ``min_parked, max_parked``                       |
    |                          |                                                  |
    |                          | A given key's value should be an integer between |
    |                          | 0 (OS default parking) and the maximum number of |
    |                          | physical cores.                                  |
    |                          |                                                  |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Example     | .. code-block:: json                             |
    |                          |                                                  |
    |                          |    {"PARK": {                                    |
    |                          |        "min_parking": 0,                         |
    |                          |        "max_parking": 4,                         |                         
    |                          |        "avail_to_park": 4                        |
    |                          |    }}                                            |
    +--------------------------+--------------------------------------------------+
    | Methodology              | Windows power profiles contain two aliases,      |
    |                          | *CPMINCORES* and *CPMAXCORES*, which control the |
    |                          | CPU core parking.                                |
    |                          |                                                  |
    |                          | These aliases are read and written as an index   |
    |                          | value between 0 and 100, where 0 = default       |
    |                          | parking, 100 = agressive parking, and the numbers|
    |                          | in between indicative of a CPU thread. Therefore,|
    |                          | on a 4-core system 25 would be 1 core parked,    |
    |                          | 50, 2 cores parked, and 75 3 cores parked. On    |
    |                          | an 8-core system 13 would be 1 core parked, 25   |
    |                          | 2 cores and so on.                               |
    |                          |                                                  |
    |                          | We utilise WMI to determine the maximum available|
    |                          | physical cores, and on reading the PARK register,|
    |                          | return this as an "avail_to_park" parameter so it|
    |                          | can be used for validation by the client. This   |
    |                          | parameter is not expected on write operations,   |
    |                          | and is ignored.                                  |
    +--------------------------+--------------------------------------------------+

^^^^^^^^^^^^^^^^^^^^^^^^^^^
Processor Frequency Scaling
^^^^^^^^^^^^^^^^^^^^^^^^^^^

""""""""
Synopsis
""""""""

Whilst core parking can be useful in some scenarios, it is usually preferable to 
control CPU minimum and maximum performance, and therefore power consumption, heat
and overall battery life using Processor Frequency Scaling Control.

In effect, it is possible to tell the OS and the CPU that you only wish it to scale
between a minimum and maximum frequency. For example, whilst your CPU may hit Turbo
speeds of 4GHz, you could limit it to 2GHz if you know that extra clock speed is 
likely to be a waste of resources.

Furthermore, most CPUs and Operating Systems have a concept of Energy Performance
Profiles (EPP), which tells the system whether to prefer power saving scaling, or
high performance scaling. Obviously this has a considerable impact on the CPU power
consumption. You could consider this analagous to the "power saving/high performance"
slider.

Our services allow you to enforce values for all three of these parameters, namely:

  * Minimum CPU Frequency
  * Maximum CPU Frequency
  * Energy Performance Profile

Note, we only offer "package-level" control over these - that is a minimum/maximum 
limit regardless of the number of cores. At time of writing, we do not offer 
per-core minimums/maximums based on number of active or turbo cores.

Furthermore, Intel CPUs AND the Windows OS offer their own controls for each of the
above, and setting, for example, OS controls alone can often conflict with what is
set for the CPU, and vice versa. In effect, we control both subsystems simultaneously
and set them with the equivalent values to ensure that the CPU and OS are not in 
competition, and ensuring that the overall CPU scaling behaviour is consistent and
as expected. Further details of this are described within the *Technical Details*
section below.

"""""""""""""""""
Available Options
"""""""""""""""""

Within the administration system, there is a section called **Processor Frequency Scaling**,
which allows you to specify *Minimum Frequency (MHz)*, *Maximum Frequency (MHz)* and
*Energy Performance Profile*.

Whilst you can set all options within a profile, it is not a requirement to set them all.
Leaving the minimum/maximum blank will ensure the OS and CPU defaults are utilised.

Minimum and Maximum Frequency should be set as an integer value represent the required MHZ,
e.g. 1000 for 1.0GHz and so on. The apparent minimum and maximum supported by the CPU are
read directly from an MSR and presented in the help text to give you an indication of what
the minimum and maximium could be, but **setting the minimum/maximum values outside of this
range seem to have no effect ** - the CPU will not scale above or below its physical range.

EPP is set as a percentage value, with 0% being Maximum Performance and 100% being Maximum
Power Saving.
Various Windows defaults are listed under the drop-down to give you an idea of what values
are utilised by the OS as a default.

Shown below is an example of a configuration where the minimum frequency is not changed,
maximium is limited to 2000MHz, and EPP is set to 80%, which is the equivalent of quite
agressive power saving.

  .. image:: ../img/ref/freq-scaling.png
    :width: 600
    :alt: Example CPU Frequency Scaling Configuration

""""""""
Defaults
""""""""

If minimum and maximum frequencies are not specified, these will be zeroed in both
the CPU and OS options, meaning that the OS/CPU default values will be used.

If the EPP is not specified, a default value will be utilised. This is currently 
33%, equivalent to "Best" performance on DC, and "Better" performance on AC.


"""""""""""""""""
Technical Details
"""""""""""""""""

  .. table::
    :align: left

    +--------------------------+--------------------------------------------------+
    | Underlying Subsystem(s)  | Intel MSR                                        |
    |                          | Windows Power Profile (via PowrProf.DLL)         |
    +--------------------------+--------------------------------------------------+
    | Register(s) Set          | Windows: PROCTHROTTLEMIN, PROCFREQMAX,           |
    |                          |          PROCTHROTTLEMAX                         |
    |                          |                                                  |
    |                          | Intel: 0x772 (IA32_HWP_REQUEST_PKG)              |
    |                          |                                                  |
    +--------------------------+--------------------------------------------------+
    | Other Utilised Registers | Intel: 0x771 (IA32_HWP_CAPABILITIES)             |
    |                          | Used to determine minimum, maximum, preferred    |
    |                          | multipliers.                                     |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Parameters  | MIN_FREQ = Windows Subsystem Minimum Frequency   |
    |                          | MAX_FREQ = Windows Subsystem Maximum Frequency   |
    |                          | EPP = Windows Subsystem EPP Control              |
    |                          | HWP_REQ = Intel MSR HWP Request                  |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Format      | The processor scaling is controlled via 4        |
    |                          | separate MSR protocol keys. It is anticipated    |
    |                          | that these should be supplied by the client in a |
    |                          | single write request (all 4 keys supplied        |
    |                          | simultaneously) to prevent any competition       |
    |                          | between the OS and the MSRs set, but this is not |
    |                          | technically required.                            |
    |                          |                                                  |
    |                          | **MAX_FREQ**                                     |
    |                          |                                                  |
    |                          | This is expected to be a JSON object with a *mhz*|
    |                          | key and an integer value determining the maximum |
    |                          | frequency to be set for the OS CPU controls.     | 
    |                          |                                                  |
    |                          | **MIN_FREQ**                                     |
    |                          |                                                  |
    |                          | This is expected to be a JSON object with a *mhz*|
    |                          | key and an integer value determining the minimum |
    |                          | frequency to be set for the OS CPU controls.     |
    |                          |                                                  | 
    |                          | **EPP**                                          | 
    |                          |                                                  |
    |                          | This is expected to be a JSON object with a      |
    |                          | *factor* key and a value between 0 and 100.      |
    |                          |                                                  |
    |                          | **HWP_REQ**                                      | 
    |                          |                                                  |
    |                          | This is expected to be a JSON object with four   |
    |                          | keys:                                            |
    |                          |                                                  |
    |                          | * minimum = Minimum CPU frequency desired, as an |
    |                          |   integer MHz value (or null to use CPU default) |
    |                          |   Should be the same as set in MIN_FREQ key, or  |
    |                          |   null if not specified.                         |
    |                          | * maximum = Maximum CPU frequency desired, as an |
    |                          |   integer MHz value (or null to use CPU default) |
    |                          |   Should be the same as set in MAX_FREQ key, or  |
    |                          |   null if not specified.                         |
    |                          | * preferred = Preferred CPU frequency desired,   |
    |                          |   as an integer MHz value (or null to use CPU    |
    |                          |   default). Note, this is never currently        |
    |                          |   specified by the web/profile service as is     |
    |                          |   therefore always null.                         |
    |                          | * epp = An 8-bit integer (between 0 and 255)     |
    |                          |   representing the CPU EPP setting. 0 is maximum |
    |                          |   performance, 255 is maximum power saving, as   |
    |                          |   per the OS EPP control, but is scaled across   |
    |                          |   0-255 rather than 0-100. Note, it is expected  |
    |                          |   that the EPP values in a request must          |
    |                          |   correlate, so if the user has picked an EPP    |
    |                          |   value of 33, the HWP_REQ epp key should be 84. |
    +--------------------------+--------------------------------------------------+
    | MSR Protocol Example     | .. code-block:: json                             |
    |                          |                                                  |
    |                          |    {                                             |
    |                          |        "MAX_FREQ": {"mhz": 2000},                |
    |                          |        "MIN_FREQ": {"mhz": 0},                   |
    |                          |        "EPP": {"factor": 33},                    |
    |                          |        "HWP_REQ": {                              |
    |                          |            "maximum": 2000,                      |
    |                          |            "minimum": null,                      |
    |                          |            "desired": null,                      |
    |                          |            "epp": 84,                            |
    |                          |        }                                         |
    |                          |    }                                             |
    +--------------------------+--------------------------------------------------+
    | Methodology              | As already mentioned, two subsystems are         |
    |                          | controlled to ensure that consistent behaviour   |
    |                          | is exhibited.                                    |
    |                          |                                                  |
    |                          | *It is important to note that Windows itself     |
    |                          | sets the IA32_HWP_REQUEST_PKG MSR when applying a|
    |                          | power profile, conflicting with the options we   |
    |                          | set below. In order to prevent this, Windows     |
    |                          | Power Profile options are always set first, and  |
    |                          | the power profile applied, before any CPU MSR    |
    |                          | registers are set within our service.            |
    |                          | This applies to any profiles that contain both   |
    |                          | Windows and MSR keys - the Windows settings are  |
    |                          | always applied first.*                           |
    |                          |                                                  |
    |                          | **Windows Power Profile**                        |
    |                          |                                                  |
    |                          | When the MSR service starts, it queries WMI to   |
    |                          | retrieve information about the first processor,  |
    |                          | namely number of logical and physical cores, and |
    |                          | the maximum "non-turbo" speed in MHz. This is    |
    |                          | stored for usage within future calculations.     |
    |                          | The CPU bus speed is also determined, although   |
    |                          | this tends to always be 100MHZ on modern         |
    |                          | processors.                                      |
    |                          |                                                  |
    |                          | The CPU frequency options are controlled by the  |
    |                          | **MIN_FREQ** and **MAX_FREQ** keys.              |
    |                          |                                                  |
    |                          | MIN_FREQ is most straightforward, as the MHz     |
    |                          | value supplied is converted directly to an index |
    |                          | between 0 and 100 and and applied as the         |
    |                          | PROCTHROTTLEMIN. Effectively this is a percentage|
    |                          | of the maximum non-turbo speed determined from   |
    |                          | WMI, so if the required minimum is specified as  |
    |                          | 1000, and the non-turbo maximum is 2600. This is |
    |                          | floored to the nearest "bus speed" unit, so if   |
    |                          | the user was to specify 1050MHz on a CPU with a  |
    |                          | 100MHz bus, it would be floored to 1000MHz.      |
    |                          | This value is then dividied by the maximum       |
    |                          | no-turbo speed to give the percentage index, in  |
    |                          | this case 38. Or at least that's how it should   |
    |                          | function, but due to unusual/unexplained         |
    |                          | behaviour in Windows Power Profiles, sometimes   |
    |                          | the minimum is set to a "bus unit" below the     |
    |                          | desired - e.g. it may be set to 900MHz when      |
    |                          | 1000MHz is requested. To compensate for this, a  |
    |                          | static fudge factor of 50MHz is added onto the   |
    |                          | floored value and used to determine the index.   |
    |                          | In testing this seems to give the desired        |
    |                          | scaling.                                         |
    |                          |                                                  |
    |                          | MAX_FREQ is more complex, as this requires       |
    |                          | setting two PowrProf aliases, namely             |
    |                          | PROCTHROTTLEMAX and PROCFREQMAX.                 |
    |                          | PROCTHROTTLEMAX is set using the same algorithm  |
    |                          | as PROCTHROTTLEMIN in the above MIN_FREQ key.    |
    |                          | Of course, most CPUs are capable of bursting     |
    |                          | above the "non-turbo" maximum, so if the user    |
    |                          | request a value above this, it still ends up     |
    |                          | with the maximum percentage of 100. For example, |
    |                          | setting MAX_FREQ to 3000MHz or 2600MHz on a CPU  |
    |                          | with 2600MHz "non-turbo" max will both result in |
    |                          | a PROCTHROTTLEMAX index of 100.                  |
    |                          | Along with the PROCTHROTTLEMAX percentage,       |
    |                          | the maximum MHz is supplied as an integer value  |
    |                          | to the PROCFREQMAX register. For example, a      |
    |                          | MAX_FREQ of 3000MHz would result in a value of   |
    |                          | 3000 being set to the PROCFREQMAX register.      |
    |                          |                                                  |
    |                          | Given our example CPU has a baseline "non-turbo" |
    |                          | MHz of 2600, here are some examples of what the  |
    |                          | underlying PROCTHROTTLEMAX and PROCFREQMAX       |
    |                          | aliases will be set to given a few MAX_FREQs:    |
    |                          |                                                  |
    |                          | * MAX_FREQ = 1300, PROCFREQMAX = 1300,           |
    |                          |   PROCTHROTTLEMAX = 52                           |
    |                          | * MAX_FREQ = 1500, PROCFREQMAX = 1500,           |
    |                          |   PROCTHROTTLEMAX = 60                           |
    |                          | * MAX_FREQ = 2600, PROCFREQMAX = 2600,           |
    |                          |   PROCTHROTTLEMAX = 100                          |
    |                          | * MAX_FREQ = 3200, PROCFREQMAX = 3200,           |
    |                          |   PROCTHROTTLEMAX = 100                          |
    |                          |                                                  |
    |                          | The Windows Power Profile EPP value is set using |
    |                          | the PERFEPP alias. The factor value between 0 and|
    |                          | 100 is passed directly to this alias.            |
    |                          |                                                  |
    |                          | **Intel MSR**                                    |
    |                          |                                                  |
    |                          | When reading/writing the HWP_REQ key to request  |
    |                          | a specified level of performance, firstly a      |
    |                          | read-only MSR (0x771) is read from the CPU. If   |
    |                          | this fails to read, it means the CPU is locked or|
    |                          | does not allow HWP_REQ requests from the user.   |
    |                          | This is used to determine if the user's system   |
    |                          | supports the dynamic frequency scaling.          |
    |                          | Assuming the user's system does support it, the  |
    |                          | call results in a 32-bit value being set in the  |
    |                          | EAX register indicating the default CPU scaling  |
    |                          | capabilities:                                    |
    |                          |                                                  |
    |                          | * Bits 0-7 are the highest performance of the CPU|
    |                          | * Bits 8-15 are the "guaranteed" performance of  |
    |                          |   the CPU                                        |
    |                          | * Bits 16-23 are the "most efficient" performance|
    |                          |   of the CPU                                     |
    |                          | * Bits 24-31 are the lowest performance of the   |
    |                          |   CPU                                            |
    |                          |                                                  |
    |                          | Each performance value is an 8-bit integer,      |
    |                          | representing the bus speed multiplier for each   |
    |                          | capability. E.g a lowest performance of 8        |
    |                          | indicates a bus-speed multiple of 8, so likely an|
    |                          | 800MHz lowest possible performance on a CPU with |
    |                          | 100MHz bus.                                      |
    |                          |                                                  |
    |                          | At the current time of writing we do not make any|
    |                          | use of guaranteed and most efficient performance,|
    |                          | but consider the lowest performance equivalent of|
    |                          | the lowest/highest frequency the CPU can scale to|
    |                          | at a hardware level. For example, a lowest       |
    |                          | performance of 2 and a highest performance of 37 |
    |                          | indicates a minimum frequency of 200MHz and      |
    |                          | maximum frequency of 3700MHz on our example CPU. |
    |                          |                                                  |
    |                          | When setting the HWP_REQ key, the MSR service    |
    |                          | obtains the capabilites MSR 0x771 as above. If   |
    |                          | any of the keys passed in by the user are null,  |
    |                          | these are substituted with these values. For     |
    |                          | example, if minimum is null on the HWP_REQ,      |
    |                          | the lowest performance value is substituted.     |
    |                          |                                                  |
    |                          | Say, for example, we have a user request of:     |
    |                          |                                                  |
    |                          |  * maximum = 2000, minimum = 0, desired = 0      |
    |                          |                                                  |
    |                          | This would result in the following merge:        |
    |                          |                                                  |
    |                          |  * maximum = 2000, minimum = 800, desired = 0    |
    |                          |                                                  |
    |                          | Note, desired will always be zero at the time of |
    |                          | writing.                                         |
    |                          |                                                  |
    |                          | Once the settings have been determined, the      |
    |                          | existing value of the 0x772 (HWP_REQ) MSR is     |
    |                          | read from the CPU. This is a 64-bit value        |
    |                          | occupying both EDX and EAX registers, but the    |
    |                          | EAX register is what is used to make the request.|
    |                          | The meaning of the EDX register is not understood|
    |                          | but is stored so it can be written back when the |
    |                          | write request is actually made.                  |
    |                          |                                                  |
    |                          | The EAX value is encoded as follows:             |
    |                          |                                                  |
    |                          | * Bits 0-7 are the minimum performance of the CPU|
    |                          | * Bits 8-15 are the maximum performance of the   |
    |                          |   CPU                                            |
    |                          | * Bits 16-23 are the desired performance of the  |
    |                          |   CPU                                            |
    |                          | * Bits 24-31 are the EPP of the CPU [0-255]      |
    |                          |                                                  |
    |                          | At this point if the HWP_REQ epp key supplied by |
    |                          | the user is null, the **current** EPP read from  |
    |                          | the 0x772 register is substitued in. The         |
    |                          | request is then encoded back into the relevant   |
    |                          | form, so MHz divided by bus speed (with NO fudge |
    |                          | factor because Intel seem to know maths), and    |
    |                          | shifted to the appropriate bits.                 |
    |                          | This 32-bit value is then written to the EAX     |
    |                          | register, with the current (RAW) data in EDX also|
    |                          | written back.                                    |
    +--------------------------+--------------------------------------------------+

^^^^^^^^^^^^^^^^^^^^^^^^^^^
Miscellaneous Power Options
^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are a couple of other useful features that can be set. **Hibernate Options**
allows you to disable the Windows "Wake to hibernate" timer via PowrProf alias 
*HIBERNATEIDLE*. Setting the *Disable wake to hibernate* option to **yes** ensures
that the system will never be woken from suspend (e.g. lid shut) to hibernate.

This can help prevent common issues where a user puts their machine to sleep, 
only to find the battery has been entirely exhausted by the time they open it 
the next day. This is typically because Windows wakes a device from sleep to put it
in a permanent suspend state (e.g. hibernation) after a period of time. However, it
is not uncommon for the hibernation to fail, leaving the machine on and running and
not in any form of suspend state. This can be even worse if the machine is in a bag
or closed environment where it cannot be ventilated.

Setting this to **no** restores the Windows default timer of 180 minutes. Leaving
as **Do not change** allows you to customise the *MSR Power* power profile with 
your own timer.

The second option is the **Reset Power Settings** (hidden by default). When applied
this will simply reset the current MSR power profile to default values, so if you
have manually changed some power profile settings and wish to reset to the MSR
defaults, select this option.

When the profile is applied, the settings will first be reset to default. Once they
have been reset, you can remove this option to prevent them being reset every time
the profile is applied. Alternatively, you can just removed the *MSR Power* profile
at any time and it will be recreated with default settings when a profile is to be
applied or the MSR service is launched on boot.