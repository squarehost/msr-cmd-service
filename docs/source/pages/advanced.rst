===============
Advanced Topics
===============

Forcing Profiles
----------------

In the event you are trying to test a specific profile and determine the
best power settings for a specific application, it is possible to mark a profile
as a "Forced Profile", either within the Profile/App Profile administration,
or by using the Force Apply option within the tray icon.

This simply means that the profile will be chosen when ANY automation trigger 
occurs, be it on boot, power event, or application launch. Other profiles will
be ignored, and "forced profiles" will always be chosen even if they have
no triggers configured themselves. Simply put, other profiles will never be
applied, even if their automation triggers match - the forced profile will 
always be chosen and applied instead.

Obviously this is geared towards testing and temporary usage, so you should ensure
that the Force profile checkbox is removed (again either via the admin or tray 
app) if you wish to resume normal service.

Import/Export Profiles
----------------------

Documentation coming soon.