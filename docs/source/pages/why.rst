===================
What, How, and Why?
===================

TLDR; this application is a pair of services to automate the application of
CPU performance and power saving functionality, based on a variety of triggers.
For example, a specific performance profile can be applied when a device is booted,
when it changes from AC/DC power (and vice versa), and even when specific
applications launch.

A performance profile consists of a variety of settings, such as Power Limits (PL1/PL2),
minimum/maximum CPU frequencies, CPU undervolting, core parking, and EPP/power saving. These are set using a
combination of Intel MSR (Model Specific Registers) and Windows Power Profile 
subsystems.

Multiple profiles can be setup in a straightforward and easy-to-use web interface,
inheriting from other profiles for ease of administration and maintenance. Profiles
can be exported and imported from other users, and we have even included an optional
tray application to monitor current settings and quickly apply profiles.

Uh, what now? What is an "MSR"?
===============================

Modern Intel CPUs provide some pretty impressive functionality, allowing the
OS and end-user to configure the processors and alter their behaviour. This
is achieved by reading and writing directly to special control registers on the CPU,
known as Model-Specific Registers (MSRs).

Depending on the exact Intel processor within your machine, and whether the
vendor allows you to access all the registers, all sorts of useful functionality
can be controlled, such as toggling CPU features, controlling power consumption
and scaling, and general performance regulations.

Controlling these registers directly can have a massive impact on heat dissapation
and power consumption, which is particularly important for controlling both
temperatures and battery consumption on portable devices.

For example, you can limit and increase the Power Limits (PL1/PL2) of chips,
control minimum and maximum clock frequencies, and directly set performance scaling,
ranging between 100% performance and 100% power saving, just by writing specific
values to the MSRs.

If you know you have a specific use-case or two; for example you may want to run
your laptop at the highest performance possible when on AC power, but limit it to
a more reasonable level of performance to get maximum battery power on battery, 
you could set aformentioned MSRs depending on what you hope to achieve.

Whilst lots of programs exist which allow you to use Intel Model-Specific Registers
to change things like Power Limits and Undervolting in real-time from Windows, most of them tend to either be over the top in terms of UI
(looking at you XTU) or include loads of other features that I don't want or need.

Furthermore, there can sometimes be competition between these registers that are set, and Windows' underlying Power Management systems,
meaning you end up with conflicting settings and unexpected behaviour.

In addition, whilst you can semi-automate the setting of these MSRs using a 
multitude of complicated batch scripts or scheduled task triggers, this tends
to be quite complex to implement and maintain, and not really too useful to the
average user.

Okay, so why do I need this?
============================

MSRs are either set using aformentioned complex and bulky tools, or you need
to learn which register values to set, and how to decode and encode their
values using EAX and EDX 32-bit values. Not too easy...

In addition, automation tends to be limited. The CPU/OS will usually reset some
settings you have changed, particularly when resuming from standby or booting the
system from cold boot. Unless you want to delve into the murky world of complex (but admittedly
0% CPU using) batch scripts, it really isn't that straightforward to turn
the underlying functionality into something that useful from an end-user 
perspective.

Our system provides two main services, which interact to provide an *easy-to-use
but feature rich* method of defining profiles and values, and *automating these
to a high-level*.

* MSR Service - the low level service which sits and listens on a named pipe for 
  commands. It takes these commands, and translates them to the underlying
  subsystem calls (Intel MSR and Windows PowrProf). It can also be used
  to query the current system status.

* Profile Service - the user-facing service, which provides a web interface
  allowing a user to log in and create a multitude of profiles, with very
  simple and easy to use settings and a variety of automation options (
  apply on boot, apply on power status change, apply on app launch etc). As an
  example of how simple this is, check out the following screenshot showing part of the
  main administration interface. Click for a full screen view.
           
  .. image:: ../img/admin-interface.png
      :width: 800
      :alt: Administration Interface

* In addition, the Profile Service also uses efficient WMI (Windows Management 
  Instrumentation) queries, and various system DLLs to provide advanced automation 
  functionality, allowing profiles to be automatically applied in a variety
  of scenarios, including:

    * On boot or on resume
    * On power status change (e.g. changing from charging to battery power)
    * On application launch. You can set profiles for individual applications
      and games, and even a default profile for things like Steam and Origin
      games.

      Fancy putting your system into a lower power mode for on-the-go gaming
      on a certain emulator? Or high performance mode when running an FPS?
      And for this to be done automatically when you run the programs or take 
      your machine off charge (set and forget?). If so, this may be the tool for you.
    
* Finally, a tray icon app is also included, which allows you to quickly
  check the currently applied settings and profile, apply profiles manually,
  disable automation, and quickly jump to the administration service:
    
  .. image:: ../img/tray-app.png
     :width: 800
     :alt: Tray App Utility

Interested? Head on over to 
`the Installation Guide <installation.html>`_ to get started.