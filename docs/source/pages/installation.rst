===================
System Requirements
===================

The MSR system utilises several underlying subsystems, so for full
functionality, the target system should consist of the following:

 * Windows 10 (or Windows 7) Operating System
 * Intel 6th Generation onwards processor

Whilst you may be able to install the system on other Windows-based
platforms, functionality will be limited if the relevant Intel MSR (Model
Specific Registers) are not supported by the CPU.

=============================
Requirements and Installation
=============================

Installation is straightforward. Download the most recent installer from our
`Releases Page <https://bitbucket.org/squarehost/msr-cmd-service/downloads/>`_, 
and execute this on the machine you wish to control with the MSR services.

You must read and accept the license to proceed with the install. Importantly,
you should understand that the services will read and write low-level registers
within your system, and whilst these are commonly used registers and the MSR
services have been tested across a range of systems, there is always a chance 
that damage may be caused to your system. We provide these services in good faith
as an open-source project, and **will not be held responsible for any damage,
perceived or actual, caused by usage or mis-usage of this project.**

The system will also prompt for an installation path. It is recommended to leave
this as the default.

A final step, before installation proceeds, is to choose the installation
options. These are shown in the image below:

.. image:: ../img/install-options.png
  :width: 600
  :alt: Installation Options

Each option is discussed below:

 * *Test Mode*

   If you are unsure as to whether you want the services to physically apply
   changes to the underlying subsystems, but would like to test the full range
   of functionality provided by the services, you can enable test mode. This
   simply means that the system will function as normal, allowing profiles to be
   built and communicating accordingly, but will *not* apply the changes at the
   appropriate time. This will not make any changes to power/CPU options within
   your system.

   You can always re-install and disable test mode when you are ready to take the
   plunge. You won't lose any configured settings or profiles whenever you install
   or upgrade the system.

 * *Run on Boot*

   By enabling this option, the relevant services will be started on-boot,
   allowing boot profiles to be automatically applied when your machine starts
   up. If you do not select this option, MSR services will be required to be
   manually started within the Windows services control panel.

   **Note, the MSR system does have a fail-safe option, so that if the system
   detects an unclean shutdown (which may be caused be profile instability with
   regards to undervoltage or similar), on-boot profiles will not be automatically
   applied, even when the services are set to start on-boot.**

 * *Start services after Installation*

   Enabling this option will start the relevant services when installation
   has completed.

 * *Launch Status/Quick Launch Icon On Boot*
 
   The system also provides a quick launch and status app, which runs as a taskbar
   icon and provides quick access to a variety of settings. It uses minimal CPU as
   it only queries the main services when interacted with by the user. However,
   it is not essential for automated profile management and application, and some
   users prefer not to run it on boot. Disabling this option will prevent it
   launching automatically.

Once installation is completed, you will be able to manually enabled/disable/stop/start
the services through the standard Windows Services Control Panel (services.msc). The
services are listed under Squarehost CPU MSR Service and Squarehost CPU Profile/Web Service.

In addition, an MSRTray shortcut to launch the tray application will be added to
your Start Menu. The services can be uninstalled using the Uninstaller.exe file 
within the installation directory.

Head over to `the Quickstart Guide <quickstart.html>`_ to learn how to get started
with the configuration services.

=========
Upgrading
=========

The MSR system is designed so that upgrades and quick, pain-free and do not
require any form of uninstallation of previous versions. Therefore, to upgrade to
a later version of the system, simply download the most recent installer.

Once the services are launched, they will automatically apply any changes 
and required migrations to the settings database. In addition, you do not need
to upgrade in order - you could, for example, jump directly from the 0.6 build to
the 0.9 build just by downloading and executing the 0.9 installer.

=====================================
Locked Processors and Partial Support
=====================================

The Intel MSR can be locked by the OEM of your machine, either in the
BIOS or via other means. In addition, some CPUs may not support all the
required registers. In either case, some functionality of the platform,
(such as configurable TDPs) will not be available. Where this is the case,
the system will highlight this information to you on the profile dashboard:


.. image:: ../img/unsupported-registers.png
  :width: 600
  :alt: Unsupported Registers

Clicking on the *Show Full Service Details* link will give much more detail on
which registers are not available, and the potential impact of this. In the below
example, the HWP_REQ register is locked, which indicates that minimum/maximum/EPP
settings cannot be changed.

.. image:: ../img/unsupported-registers-detail.png
  :width: 600
  :alt: Unsupported Registers Detail

Bored of reading documentation and looking to get started *post haste*?
Sounds like you need `the Quickstart Guide <quickstart.html>`_.