==========================
Profile Automation Options
==========================

Whilst creating profiles that are applied consistently is useful, the real
power of the MSR services is realised in the variety of automation options
that are available. This allows you to define profiles that should be 
applied when certain system events occur (such as on boot/resume, power 
event changes, and application launches) in a "set and forget" fashion.

Automation types can be individually disabled from within the taskbar
application *Automation* menu.

A summary of different automation types and concepts is given in this
section.

Automation Triggers
-------------------

The following triggers are available for Standard profiles.

1. Apply on boot

   This can be activated by selecting the "Apply on Boot" checkbox within
   the profile administration. Boot profiles are applied with the MSR web
   profile is launched, and when the system resumes from suspend/hibernate.

2. Apply on power status change

   These can be activated by selecting the "Apply on Power Status Change"
   drop-down within the profile menu, and selecting the appropriate option:

    * Charger Attached (AC Power)
    * Charger removed (DC/Battery Power)
    * Battery Levels between 10 and 100% (10% intervals)

   The profile service will monitor battery level changes in the background,
   and apply any battery levels events accordingly.

   **Note**, apply on boot and power status change automation can be 
   combined. This means that a profile will only be applied on boot/resume
   if it **ALSO** matches the selected power criteria.

   Please be aware, "battery level" power automation events are applied in 
   two scenarios.

   1. When the exact power level (e.g. 50%) is reached.
   2. When a device is unplugged (onto DC power) and the power level is
      BELOW the selected level.

3. Apply on Application Launch

   An App Profile is very similar to a Standard Profile, but with 
   automation triggers that occur when a specific application is launched.

   To create an application profile, use the App Profile administration
   section and select an application from the Appliation drop-down. The
   drop-down will contain a list of currently running applications AND 
   installed applications, making it easy to select the appropriate
   program. If the application cannot be found, you can enter a manual 
   override within the "Appliation (Override)" box. This should be either
   a full path to an executable (e.g. C:\\Program Files\\My Game\\Game.exe),
   or an executable name alone (Game.exe).

   Application events can be matched using several strategies, which 
   again, can be chosen from a drop-down (Match type) within the app profile
   administration. The different match types are as follows:

   * Full File Path (Default)

     Only match if the application/override is an exact, full-path match to
     the launched program. NB. The complete path must match.

   * Filename Only
     
     Match if the application/override executable matches the launch program.
     Do not require the path to match. For example, a profile with "MyApp.exe"
     and match type=Filename Only would match launched applications 
     "C:\\Program Files\\My Game\\MyApp.exe" and "D:\\MyApp.exe", and so on.
     This makes the profile more portable, as you do not need to worry about
     exactly where the program is installed, but you should be cautious about
     filename clashes. For example, it could be possible that two entirely
     different programs could have the same name "App.exe", so be careful of
     unintended profile applications.

   * Folder Default

     This allows you to match on the folder path of an executable, and therefore
     set a profile for all apps and games located in a folder. This can be 
     useful for setting a default for, for example, your Steam apps folder.

     An easier wizard is available to configure default app profiles. This can
     be accessed from the main Status Dashboard, under the "Default Application 
     Profiles" section.

   The final option is to choose what happens when the relevant application is 
   closed. The two options here are to reset to the previously applied profile
   (the default), or reset to a boot profile. Pick the appropriate option from
   the "When closed set profile to" drop-down.

   **Note**, if you resume your machine from standby and a triggered application
   is still running, the system will re-apply the app profile rather than any
   boot profiles you may have configured. This is to ensure that you can suspend
   your device during a game (e.g. shutting the lid of your device), and have the
   expected profile resumed when you resume/re-open the lid.

   Combining App Profiles and Power Triggers
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   As well as app automation, it is also possible to choose power options from 
   within the App profile administration section. The "Apply on power status change"
   drop-down has the same options available as the standard Profiles, but these
   will be tested only when an application launches. 

   For example, you could create a profile for "MyGame.exe" with the AC power status
   option selected, and another profile for "MyGame.exe" with the DC power status
   selected. When launching the game, the system would check to see whether you are
   running on AC or DC power and apply the appropriate profile.

   This allows you to, for example, have a high-power profile applied when an 
   application is launched on AC power, but a more modest power profile applied when on
   DC power to enhance battery life and reduce temperatures.

Priority
--------

The profiles also allow you to specify a numeric priority. This is only relevant if
you have two profiles which could match simultaneously and you wish to be able to
dictate which profile is applied.

In general, the system will iterate through all profiles in a "reverse priority" order - 
that is, a profile with a priority of 1000 is checked before a profile with priority 100.

Whilst the system will, of course, check if any relevant automation triggers match before
applying the profile, it is certainly possible to have two profiles with matching triggers.

For example, you could have an on boot profile which is applied on 50% or below battery 
level and an on boot profile which is applied on DC power. If you boot your system on 
battery with a level of less than 50%, BOTH of these profiles will actually match this
automation event - as you are running on both DC power and have less than 50% battery.

Presumably, you would want the 50% battery level profile to apply rather than the DC power
profile. In this scenario, you could manually give the 50% battery level profile a higher
priority than the DC power profile - which would mean the system would check and apply 
this profile before the DC power profile. If your battery level is above 50% on boot,
the system will recognise that the 50% trigger should not apply, skip over this profile,
and apply the standard DC one.


Scripting
---------

The profile service also offers a basic level of scripting support. This simply means that
a batch or executable command can be automatically run at any of the following triggers:

1. On Apply
   
   The script/executable will be launched immediately *BEFORE* a profile is to be applied. 

2. On Successful Apply

   The script/executable will be launched immediately *AFTER* a profile is successfully applied.

3. On Failure to Apply

   The script/executable will be launched immediately *AFTER* a profile has failed to apply for any reason.

4. On Close

   The script/executable will be launched immediately *AFTER* a matched application has closed. This is only
   applicable to App Profiles.

Simply enter the full path to the batch script or executable file (and any arguments/parameters) in the
relevant boxes within the *Scripting* section of the Profile or AppProfile administration, and the
relevant script or executable will be launched as appropriate. It is, of course, possible to combine 
multiple triggers within a profile - simply complete mutliple boxes within the scripting section.

This allows you to run certain commands, for example, just after an application has launched, and to
apply another command just after an application has closed.

.. topic:: Privileged Execution: Use the Options with Caution!

    The profile service executes the scripts/executables declared within the Scripting boxes, and 
    as such, they will be executed as the privileged System user.

    This has obvious security implications, as any batch/executables will have access to both the
    MSR pipe and any other privileged components on your Windows system, so you should ensure you
    entirely trust the applications you are launched. Use this at your own risk.

    In addition, applications launched by the System user will not have access to the Desktop,
    so applications that launch tray icons and so on will not work. In addition, the application
    will not be launched under the logged-in user's profile, but under the system profile instead.
    This may cause unexpected behaviour if the scripted application/script expects to find configuration
    files within your profile folder. You may be able to overcome this by using the USERPROFILE
    environment variable, but any files opened/written will be done using the system profile, so please
    be aware of this.
   