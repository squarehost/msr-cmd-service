=============
System Basics
=============

The MSR CPU Profile Switcher provides two main services, and an optional tray icon app.
Each service plays a specific role, the basics of which are described below:

Squarehost CPU MSR Service
--------------------------

The CPU MSR service runs as a privileged service (running using the System account), with 
its primary goal to receive commands (loosely known as *registers*) and manage the
appropriate subsystems in a unified fashion. It could be considered the "lower level" service
of the two including with this software. The currently utilised subsystems are:

* Intel Model-Specific Registers (MSRs), via `WinRing0.dll <https://github.com/QCute/WinRing0>`_
* Windows Power Profiles, via the core Windows PowrProf.dll file.

This service does not rely on external executables or batch scripts, instead opening the
relevant DLLs directly.

When launched, the MSR service performs two notable actions:

* Creates a named pipe, allowing it to listen to commands/register requests from a client.
  The named pipe is only accessible by fellow privileged services, preventing other 
  executables and services from accessing the server and issuing commands. This restriction
  is only relaxed when running the services in `development mode <development.html>`.

* Creates a profile profile named "MSR Power", if it does not already exist. This is cloned
  from the current "Balanced" system profile. Once created, multiple power options are reset
  to specific default values, including enabling the High Performance personality. This
  ensures the MSR service has consistent and expected behaviour, without other Windows 
  power settings interfering with the desired result.

  The profile is automatically activate on boot, and prior to any command/register being 
  applied. Profile setting are currently only reset when the profile is created, so it is
  safe to delete the MSR Power profile if you wish to reinitialise it - the MSR service will
  detect it no longer exists and create it accordingly.

  For a full list of the settings that are reset when the profile is created, see the 
  relevant `source code <https://bitbucket.org/squarehost/msr-cmd-service/src/0978a78934aab2c90224ee1e0564f067ce8c3830/msr_win32_svc/msr_svc/controllers/_powercfgsetup.py#lines-179>`.

It then proceeds to listen on its named pipe to client commands. For details of the low
level protocol it utilises, see the `MSR protocol documentation <msr-protocol.html>`.

Squarehost CPU Web/Profile Service
----------------------------------

The CPU Web/Profile Service communicates directly with the MSR service as a client, 
instructing the MSR to perform the appropriate subsystem actions, but also handles
all profile definition, User Interface, and automation events.

It consists of several components:

* Web dashboard/administration interface (written using the Django web framework)
 
  This runs on port 8998 on localhost - if you already have a service bound to this
  port you may not be able to access this. All profile configuration is performed
  from the web interface. All profile changes require an authenticated login.

  Any profile settings are stored in a SQLite database within the System user's
  profile folder. The database will be automatically upgraded with any new MSR
  service installation, so you should never need to remove this and re-install.

* Listeners

  Various automation options exist within the service, such as automatically
  applying profiles when the system boots/resumes, the power source changes (e.g. (dis)connecting a 
  charger), when the battery level changes (e.g. drops below 50%), and when
  specific applications/games are launched.

  The Web/Profile service contains listener threads, which listen to various
  system events, and trigger any appropriate profiles to be applied. The
  listeners all use the Windows Management Instrumentation (WMI) service
  built into modern Windows versions, and utilise efficient queries to do so.

* MSR Client

  The profile service also contains the main MSR client, which connects to the
  CPU MSR service and queries/updates the relevant registers.

  The profile service converts a profile, and the specified options, into a
  series of registers in JSON format. These are then sent over the named pipe
  to the CPU MSR service.

Logging
-------

Both the CPU MSR and Profile services have extensive logging, which is extremely
useful for understanding when/why profiles are applied, the exact registers in
the underlying subsystems that are modified, and general debugging.

The logs can be access using the "System Logs" link in the top right of the 
Dashboard page, filtered by level (for example warning/errors often are the
first port of call if something is not working correctly) and service. The log
files can also be downloaded as a combined file. If you report any bugs, you 
should always supply an up-to-date combined log file to allow us to 
replicate or diagnose any problems.

The Tray App
------------

Optionally, you can install and launch the tray app on boot. This allows you to
quickly check the current settings applied on your machine, apply profiles manually,
and disable/enable specified automation events.

Full documentation on the tray app is coming soon, but this should be quite 
straightforward and easy to use!