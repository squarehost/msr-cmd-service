.. MSR CPU Profile Switcher documentation master file, created by
   sphinx-quickstart on Mon Nov  2 12:50:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MSR CPU Profile Switcher Documentation
======================================

Please note, this software is supplied entirely without warranty and installed/operated
  at the user's risk. All of our code is open source, and can be found at the
  `Project Homepage <https://bitbucket.org/squarehost/msr-cmd-service>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/why
   pages/installation
   pages/quickstart
   pages/basics

   pages/automation
   pages/profile-options
   pages/advanced
   pages/development

Acknowledgements
================

* All the teams who have done the heavy lifting in terms of research and behaviour,
  including the linux-intel-msr team and @Ciphray on the Win Max channel, whose
  enthusiasm and wholehearted adoption of the MSR service concept has kept us going
  when challenges have arisen.

* `OpenLibSys <https://openlibsys.org/manual/HowToUse.html>`_ for their WinRing DLL
  which we use to read/write the MSRs.

* All the alpha testers who put their hardware at risk (joking?!?!), including:

  ibebyi, Ciphray, AyubTagh, jurassicjim, Lug Steakface, tatane and xomm from our
  `Discord Channel <https://discord.gg/pDXsgF>`_. Newcomers and testers welcome!

