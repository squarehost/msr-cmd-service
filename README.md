# FUN DISCLAIMER #

* I'm not responsible for bricked devices, dead SD cards, thermonuclear war, etc. Use this software at your own risk.
 
* The latest builds can be found at the project homepage: https://bitbucket.org/squarehost/msr-cmd-service, under the **Downloads** section.

* The latest documentation can always be found at https://www.squarehost.co.uk/msr/

* Join us in the testing/chat Discord channel for the profile: https://discord.gg/pDXsgF

### What is this repository? ###

* Lots of programs exist which allow you to use Intel Model-Specific Registers to change things like Power Limits and Undervolting in real-time from Windows, but most of them tend to either be over the top in terms of UI (looking at you XTU) or include loads of other features that I don't want or need.

* Somebody wrote a batch script which uses a very simple command (msr-cmd) from [cocafe's msr-utility repo](https://github.com/cocafe/msr-utility) to set some parameters, which is extremely quick and lightweight.

* I thought it would be a nice idea to use a similar methodology to produce a couple of services and a UI so that we can have enhanced features, such as the ability to define and quickly apply separate profiles, apply defaults on boot and so on.

### What is it made up of? ###

Currently three main components:

* A lower-level MSR service, which listens to commands and applies them using msr-cmd. Implements MSR read/write via WinRing DLL, and core parking via PowrProf DLL (Windows power manager).

* A web app, which talks to the lower-level MSR service, but adds higher level functionality, such as profiles, apply-on-boot, apply on power status change (e.g. AC to DC) and automatic app launch. It does this by using efficient WMI queries to establish when apps are launched and closed.

* A taskbar icon, which gets status from the web app and can manually apply profiles. This isn't required to be running for the profile services to work.

### Basic Features ###

* Define profiles, allowing a variety of registers to be specified. These can help reduce package power consumption and increase battery life, and currently include:

    - TDP PL1 (Long Window)/PL2 (Short Window) power limits, clamping and enablement.
    - CPU Voltage Offset Adjustment. Used for undervolting package components, such as CPU, GPU, Cache, and System Agent.
    - Core parking (using Windows PowrProf) to specify a minimum and maximum number of parked cores.

* Automate profiles on certain triggers, including:

    - On boot, or on resume
    - On power status change, such as connecting to an AC power supply, or disconnecting to battery (DC) power.
    - When a specific app is launched, including re-applying the previous power profile when the app is closed.
    - When any app is run from a certain path. This allows you to set a default profile for things such as Steam apps or everything in your emulators folder.

* Manual application of profiles, and quick review of currently applied settings from the optional tray application.

* Linking profiles. inheriting the settings of one profile in another. This allows you to, for example, specify a profile with your undervolt values and link that to any other power profile you may make. You do not have to duplicate UV values in multiple profiles, and if you wish to update them, you can just update your undervolt values profile.

### Can I help? ###

Yes. We are taking feature and enhacement requests and appreciate any alpha/beta testing you can offer. Join us on our discord channel. https://discord.gg/pDXsgF

Feel free to contact support @ squarehost.co.uk for any e-mail enquiries.

### Attributions and Acknowledgements ###

* All the teams who have done the heavy lifting in terms of research and behaviour,
  including the linux-intel-msr team and @Ciphray on the Win Max channel, whose
  enthusiasm and wholehearted adoption of the MSR service concept has kept us going
  when challenges have arisen.

* OpenLibSys https://openlibsys.org/manual/HowToUse.html for their WinRing DLL
  which we use to read/write the MSRs.

* All the alpha testers who put their hardware at risk (joking?!?!), including:

    ibebyi, Ciphray, AyubTagh, jurassicjim, Lug Steakface, tatane and xomm from our
    Discord Channel https://discord.gg/pDXsgF_. Newcomers and testers welcome!

* Taskbar icon based on an icon by dinosoftlabs.com, thanks!
