import win32api

import copy, datetime, logging, logging.config, os, sys, threading, time
from itertools import groupby

sys.coinit_flags = 0

LOGGER = logging.getLogger('taskbar')

class Taskbar(object):
    title = "MSR Taskbar"
    logfile_name = 'msr-taskbar.log'
    version = '1.12.b4'

    notify_if_applied_less_than = 60
    project_uri = 'https://bitbucket.org/squarehost/msr-cmd-service'

    status_success, status_warn, status_error = None, None, None

    starting_up_message = "MSR is starting."
    sfx_path = 'sfx'
    sfx_play_delay = 0.33
    enable_sfx = True
    disable_sfx_key = 'DISABLE_AUDIO_NOTIFY'

    def __init__(self, web_service_base_url):   
        self.debug = os.getenv('MSR_DEBUG') or False
        self.logfile = None
        self.taskbar_menu = None

        self._last_bookmark_state = set( )
        self._bookmarks = []
        self._last_force_bookmark_state = set( )
        self._force_bookmarks = []

        self._last_msr_status_state = set( )
        self._msr_status = []
        self._last_applied = None

        self._last_settings = set( )
        self._settings = []

        self._wav_cache = {}
        self._sfx_thread = None

        self.service_uri = web_service_base_url
        self.docs_uri = "{}/docs/index.html".format(web_service_base_url)

        self._setup_logging( ) 
        self._setup_locale( )
        self._setup_comms( )
        self._setup_taskbar_menu( )
        self.set_status(self.starting_up_message, status=self.status_warn)

    def _setup_locale(self):
        import locale
        lcle = locale.setlocale(locale.LC_ALL, "")
        LOGGER.info("System locale is {} ({})".format(
            lcle, datetime.datetime.now( ).strftime("%x %X")
        ))

    def _get_wav_path(self, type, force=False):
        name, ext = os.path.splitext(type)
        ext = ext or ".wav"
        wav_name = "{}{}".format(name, ext)

        if not force and self._wav_cache.get(wav_name):
            return self._wav_cache[wav_name]

        dn = os.path.abspath(os.path.dirname(__name__))
        edn = os.path.abspath(os.path.dirname(sys.executable))
        wav_path = os.path.abspath(
            os.path.join(dn, self.sfx_path, wav_name)
        )
        exe_wav_path = os.path.abspath(
            os.path.join(edn, self.sfx_path, wav_name)
        )

        wavp = None
        for path in (wav_path, exe_wav_path):
            LOGGER.debug("Attempting to find wav {}".format(path))
            if os.path.exists(path):
                wavp = path
                break    

        if not wavp:
            LOGGER.warn("Unable to locate a valid wav '{}'".format(
                type
            ))
            return None
        
        self._wav_cache[wav_name] = wavp
        return wavp

    def play_notify(self, key, no_delay=False):
        if not self.enable_sfx: return False

        sts = self.get_settings( ) or []
        for s in sts:
            if s.get('key') == self.disable_sfx_key and s.get('checked'):
                LOGGER.debug(
                    "Not playing notify '{}' as sfx setting disabled.".format(
                        key
                    )
                )
                return False

        wav = self._get_wav_path(key)
        if not wav:
            LOGGER.warn(
                "Not playing notify '{}' - no WAV exists for type.".format(
                    key
                )
            )
            return False
        if self._sfx_thread: self._sfx_thread.active = False
        self._sfx_thread = SFXPlayThread(
            wav, self.sfx_play_delay if not no_delay else 0.01
        )
        self._sfx_thread.start( )
        
    def _setup_comms(self):
        try:
            from comms_thread import CommsThread
            self.comms_thread = CommsThread(
                self, self.service_uri
            )
        except Exception as ex:
            LOGGER.error(
                "Error whilst creating comms thread.", exc_info=ex
            )
            raise

    def _setup_taskbar_menu(self):
        try:
            from ui import taskbar_menu
            self.taskbar_menu = taskbar_menu.TaskbarMenu(self)
            self.status_success = self.taskbar_menu.success_icon
            self.status_warn = self.taskbar_menu.warn_icon
            self.status_error = self.taskbar_menu.error_icon

        except Exception as ex:
            LOGGER.error(
                "Error whilst creating taskbar menu.", exc_info=ex
            )
            raise

    def _setup_logging(self):
        local_appdata = os.getenv('LOCALAPPDATA')
        if not local_appdata:
            import tempfile
            local_appdata = tempfile.gettempdir( )
            
        llevel = 'DEBUG' if self.debug else 'INFO'
        self.logfile = logfile = os.path.join(
            local_appdata, self.logfile_name
        )

        sys.stderr.write("Logging to file {}, level {}\n".format(
            self.logfile, llevel
        ))
        cfg = dict(
            version=1,
            formatters=dict(
                full=dict(
                    format='[%(levelname)s] %(asctime)s %(message)s'
                ),
                brief=dict(
                    format='[%(levelname)s] %(message)s'
                )
            ),  
            handlers=dict(
                file=dict(
                    formatter='full',
                    level='DEBUG',
                    maxBytes=10 * 1024 * 1024,
                    backupCount=3,
                    filename=logfile
                ),
                console=dict(
                    formatter='brief',
                    level='DEBUG'
                )
            ),
            loggers=dict(
                taskbar=dict(
                    handlers=['file', 'console'],
                    propagate=True,
                    level=llevel
                )
            )
        )

        cfg['handlers']['file']['class'] = (
            'logging.handlers.RotatingFileHandler'
        )
        cfg['handlers']['console']['class'] = 'logging.StreamHandler'
        logging.config.dictConfig(cfg)

    def _bookmarks_to_state(self, bookmark_json, hotkey_json):
        state = set( )
        for item in bookmark_json:
            hk = [h['hotkey'] for h in hotkey_json if h['id'] == item['id']]
            hkd = "_".join(hk[0]) if hk else "-"
            if hk: item['hotkey'] = hk[0]
            key = '{}_{}_{}'.format(item['id'], item['name'], hkd)
            state.add(key)
        return state

    def get_bookmarks(self):
        # Return curre  nt list of active bookmarks from the server.
        return self._bookmarks
        
    def set_bookmarks(self, bookmark_json, hotkey_json):
        nstate = self._bookmarks_to_state(bookmark_json, hotkey_json)
        update = nstate != self._last_bookmark_state

        self._last_bookmark_state = nstate
        self._bookmarks = bookmark_json

        if update:
            LOGGER.debug("Bookmark JSON is {} (Hotkeys {})".format(
                bookmark_json, hotkey_json
            ))
            self.taskbar_menu.update( )

    def get_force_bookmarks(self):
        # Return current list of active 'sticky' bookmarks from the server.
        return self._force_bookmarks

    def set_force_bookmarks(self, bookmark_json, hotkey_json):
        nstate = self._bookmarks_to_state(bookmark_json, hotkey_json)
        update = nstate != self._last_force_bookmark_state

        self._last_force_bookmark_state = nstate
        self._force_bookmarks = bookmark_json

        if update:
            LOGGER.debug("Force Bookmark JSON is {} (Hotkeys {})".format(
                bookmark_json, hotkey_json
            ))
            self.taskbar_menu.update( )

    def get_current_registers(self):
        # Return current list of active MSR status from the server.
        return self._msr_status

    def set_current_registers(self, registers):
        LOGGER.info("Current regs, {}".format(registers))
        state = set([r.get('short_value') for r in registers])
        if state != self._last_msr_status_state:
            registers.sort(key=lambda x: x['model'])
            try:
                groups = groupby(
                    registers, key=lambda x: x['model']
                )
                regs = []

                for k, rgs in groups:
                    regs.append(", ".join([r['short_value'] for r in rgs]))
                
                LOGGER.info("New grouped registers are {}".format(regs))
                regs.sort( )
                registers = regs
            except Exception as ex:
                LOGGER.error(
                    "Unable to update register groupings.",
                    exc_info=ex
                )
                return
            self._msr_status = registers
            self.taskbar_menu.update( )

        self._last_msr_status_state = state

    def get_last_applied(self):
        return self._last_applied

    def set_last_applied(self, appl_json, force_notify=False):
        LOGGER.debug(
            "New last applied json is {}, current is {}".format(
                appl_json, self._last_applied
            )
        )
        if appl_json:
            if self._last_applied:
                _la = self._last_applied
                if (
                    _la.get('name') == appl_json.get('name') and
                    _la.get('applied') == appl_json.get('applied')
                ): return False

            notify = force_notify
            try:
                dt = datetime.datetime.strptime(
                    appl_json['applied'],
                    "%Y-%m-%dT%H:%M:%S.%fZ"
                )
                delta = datetime.datetime.now( ) - dt
                notify = delta.total_seconds( ) < self.notify_if_applied_less_than
            except Exception as ex:
                LOGGER.warn(
                    "Unable to calculate notification delta time for {}".format(
                        appl_json
                    )+". Suppressing notification.", exc_info=ex
                )

            if notify and self._last_applied:
                prf = appl_json.get('name') or ''
                if prf == self._last_applied.get('name') and not force_notify:
                    notify = False

            try:
                dt = datetime.datetime.strptime(
                    appl_json['applied'],
                    "%Y-%m-%dT%H:%M:%S.%fZ"
                )
                lct = dt.astimezone().strftime("%H:%M:%S")
                kw = {
                    'message':  "Profile '{}' successfully applied.".format(
                        appl_json.get('name') or '<Unknown Profile>'
                    )
                }
                if notify:
                    kw.update({
                        'notify_title': kw['message'],
                        'notify_msg': "'{}' event caused the application.".format(
                            (appl_json.get('type') or 'Manual').title( )
                        ), 'force': True
                    })
                    
                    lt = (appl_json.get('type') or 'manual').lower( )
                    sfx = None
                    if 'manual' in lt or 'restored' in lt:
                        sfx = 'profile-apply'
                    else:
                        sfx = 'appprofile-apply'
                    self.set_status(**kw)
                    if sfx:
                        LOGGER.info("Playing sfx {} from type {}".format(
                            sfx, lt
                        ))
                        self.play_notify(sfx)
                
                # Queue a full refresh to grab registers.
                self.comms_thread.queue.put(["status_update"])
            except Exception as ex:
                LOGGER.warn(
                    "Error whilst setting taskbar last applied.",
                    exc_info=ex
                )

        self._last_applied = appl_json

    def get_system_hotkeys(self):
        try:
            return dict([
                (s[0], s) for s in (
                    self.comms_thread.hotkey.get_system_hotkeys( )
                )
            ])
        except Exception as ex:
            LOGGER.warn("Unable to get system hotkeys.", exc_info=ex)

    def get_settings(self):
        return self._settings

    def set_settings(self, sts):
        settings = []
        for setting, info in sts['config'].items( ):
            settings.append({
                'name': info[1],
                'key': setting,
                'checked': sts['current'].get(setting)
            })

        state = set(["{}_{}".format(r['key'], r['checked']) for r in settings])
        if state != self._last_settings:
            LOGGER.info("Updating settings to {}".format(settings))
            self._settings = settings
            self._last_settings = state
            self.taskbar_menu.update( )

    def update_setting(self, key, truthy):
        LOGGER.info("Updating setting {} = {}".format(key, truthy))
        self.comms_thread.queue.put([
            "apply_setting", [key, True if truthy else False]
        ])

    def _launch_url(self, url):
        LOGGER.info("Launching URL '{}' via Shell".format(url))
        win32api.ShellExecute(
            self.taskbar_menu.hwnd, "open", url, None, "", 1
        )

    def launch_project_url(self):
        self._launch_url(self.project_uri)

    def launch_docs_url(self):
        self._launch_url(self.docs_uri)

    def launch_admin_url(self):
        self._launch_url(self.service_uri)

    def start(self):
        self.comms_thread.start( )
        self.taskbar_menu.run( )

    def exit(self):
        try:
            self.taskbar_menu.stop( )
        except:
            pass
        try:
            self.comms_thread.stop( )
        except:
            pass

    def set_status(
        self, message, status=None, notify_title=None, notify_msg=None,
        force=False
    ):
        status = status or self.status_success
        self.taskbar_menu.set_icon_status(status, message, force=force)

        if notify_title:
            notify_msg = notify_msg or message
            self.taskbar_menu.notify(notify_title, notify_msg, force=force)

    def apply_profile(self, pk, force=None):
        args = [pk]
        if force != None: args.append(force)
        self.comms_thread.queue.put([
            "apply_profile", args
        ])


class SFXPlayThread(threading.Thread):
    def __init__(self, wav_path, delay=0.5):
        self.wav_path = wav_path
        self.delay = delay
        self.active = True
        super( ).__init__( )
    
    def run(self):
        time.sleep(self.delay)
        if self.active:
            try:
                LOGGER.info("Attempting to play {}".format(
                    self.wav_path
                ))
                import winsound
                winsound.PlaySound(None, winsound.SND_ALIAS)
                winsound.PlaySound(
                    self.wav_path,
                    winsound.SND_FILENAME | winsound.SND_ASYNC
                )
            except Exception as ex:
                LOGGER.warn(
                    "Unable to play sound {}.".format(self.wav_path),
                    exc_info=ex
                )

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        service = sys.argv[-1]
    else:
        service = 'http://127.0.0.1:8998'
    try:
        w = Taskbar(service)
        w.start( )
    except KeyboardInterrupt:
        try:
            w.exit( )
        except:
            pass
