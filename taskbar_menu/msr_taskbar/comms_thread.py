from datetime import datetime
import json, logging, queue, requests, threading, time, urllib.parse

LOGGER = logging.getLogger('taskbar.comms')

def get_open_window_info(hwnd_only=False):
    import win32gui, win32process

    def enum_windows(hwnd, window_list):
        try:
            if win32gui.GetParent(hwnd): return
            if hwnd_only:
                windows.append(hwnd)
            else:
                text = win32gui.GetWindowText(hwnd)
                thread, proc = win32process.GetWindowThreadProcessId(hwnd)
                windows.append({
                    'hwnd': hwnd,
                    'thread': thread,
                    'process': proc,
                    'text': text
                })
        except Exception as ex:
            LOGGER.error(
                "Unable to query window handle {}".format(hwnd),
                exc_info=ex
            )

    try:
        windows = []
        win32gui.EnumWindows(enum_windows, windows)
        return windows
    except Exception as ex:
        LOGGER.error(
            "Unable to enumerate open window list.", exc_info=ex
        )
        return []


class CommsThread(threading.Thread):
    current_status_uri = "profiles/api/v1/current/"
    apply_profile_uri = "profiles/api/v1/{}/apply/"
    apply_settings_uri = "settings/"
    open_windows_uri = "windows/"
    throttle_sleep_time = 10
    status_delay_time = 15
    force_full_poll_seconds = 300
    disable_window_poll_settings_key = 'DISABLE_AUTOMATION_APP'
    disable_hotkey_poll_settings_key = 'DISABLE_AUTOMATION_HOTKEY'

    def __init__(self, taskbar, service_uri, window_detect=10):
        self.taskbar = taskbar
        self.service_uri = service_uri
        self.window_detect = self.hotkey_detect = window_detect
        self.window_last_poll = self.hotkey_last_poll = None
        self.window_info = set( )
        self.status_last_queued = None
        self.status_full_last_queued = None
        self.queue = queue.LifoQueue( )
        self.hotkey = None
        self._has_had_error = False
        self._first_conn = True
        
        try:
            from hotkey import HotkeyListener
            self.hotkey = HotkeyListener(self)
            LOGGER.info("Created hotkey listener ({})".format(self.hotkey))
        except Exception as ex:
            LOGGER.error(
                "Unable to create hotkey listener - hotkeys will be inactive.",
                exc_info=ex
            )

        self._settings = {}
        threading.Thread.__init__(self)

    def start(self):
        self.is_running = True
        threading.Thread.start(self)

    def _poll_hotkeys(self):
        upd = not self.hotkey_last_poll
        if not upd:
            settings = (self._settings or {}).get('current', {})
            if settings.get(self.disable_hotkey_poll_settings_key):
                if self.hotkey:
                    self.hotkey.set_hotkeys([])
                    return

            t = time.time( )
            if t - self.hotkey_last_poll > self.hotkey_detect:
                upd = True
                self.hotkey_last_poll = t

        if upd:
            self._queue_status_update(settings=True)
            self.hotkey_last_poll = time.time( )

    def _queue_status_update(self, force=False, settings=False):
        upd = force or not self.status_last_queued
        first = not self.status_last_queued
        if not upd:
            t = time.time( )
            if t - self.status_last_queued > self.status_delay_time:
                upd = True

        if upd:
            args = []
            if settings and not first:
                args = ['settings']
                if self.status_full_last_queued:
                    t = time.time( )
                    if t - self.status_full_last_queued > (
                        self.force_full_poll_seconds
                    ):
                        # Force a full poll as it's been a while...
                        LOGGER.info(
                            "Forcing full (including register) poll of "
                            "current server settings."
                        )
                        args = []

            self.queue.put(["status_update", args])
            self.status_last_queued = time.time( )
            if not args:
                self.status_full_last_queued = time.time( )

    def _poll_windows(self):
        upd = not self.window_info
        if not upd:
            settings = (self._settings or {}).get('current', {})
            if settings.get(self.disable_window_poll_settings_key):
                return

            t = time.time( )
            if t - self.window_last_poll > self.window_detect:
                upd = True
                self.window_last_poll = t

        if upd: 
            hwnds = set(get_open_window_info(hwnd_only=True))
            rem = self.window_info - hwnds
            new = [h for h in (hwnds - self.window_info) if h not in rem]

            self.window_info = hwnds
            self.window_last_poll = time.time( )

            if new or rem:
                LOGGER.debug(
                    "Open windows have changed (new: {}, closed: {})".format(
                        new, rem
                    )
                )
                LOGGER.info(
                    "Open windows have changed (new: {}, closed: {})".format(
                        len(new), len(rem)
                    )
                )
                w_data = json.dumps([
                    w for w in get_open_window_info( ) if w.get('text')
                ])
                
                try:
                    res = self._load_url(
                        self.open_windows_uri, "post", data={"windows": w_data}
                    )
                except Exception as ex:
                    LOGGER.error(
                        "Unable to update windows on profile service.",
                        exc_info=ex
                    )

    def run(self):
        while self.is_running:
            if self.window_detect:
                self._poll_windows( )
            if self.hotkey_detect and self.hotkey:
                self._poll_hotkeys( )
            try:
                itm = self.queue.get_nowait( )
                try:
                    self._handle_queue_item(itm)
                finally:
                    self.queue.task_done( )
            except queue.Empty:
                pass
            except Exception as ex:
                LOGGER.error("Unhanded exception.", exc_info=ex)
            time.sleep(0.5)

    def stop(self):
        self.is_running = False
    
    def _load_url(self, rel_url, method="get", data=None):
        uri = urllib.parse.urljoin(
            self.service_uri, rel_url
        )
        LOGGER.debug("Attempting to call URI {}".format(uri))
        try:
            res = getattr(requests, method)(uri, data=data)
        except Exception as ex:
            LOGGER.error("Unable to connect to service: {}".format(
                ex
            ), exc_info=ex)
            self._has_had_error = True
            self.taskbar.set_status(
                "Unable to connect to the MSR profile service.",
                status=self.taskbar.status_error,
                notify_title="Unable to connect to MSR profile service.",
                notify_msg=(
                    "Ensure the msr_profile_svc service is running. "
                    "Review the logs for further details."
                )
            )
            return False

        try:
            json = res.json( )
        except Exception as ex:
            LOGGER.error("Unable to decode response: {}".format(
                ex
            ), exc_info=ex)
            json = {}

        LOGGER.debug("Response is {}".format(json))
        if res.status_code == 429:
            # Throttled
            LOGGER.warn("Response request has been throttled.")
            self.taskbar.set_status(
                "Too many requests, please wait and the status "
                "will update.\n{}".format(
                    json.get('detail', '')
                ), status=self.taskbar.status_warn
            )
            time.sleep(self.throttle_sleep_time)
            return None
        elif res.status_code == 304:
            # Profile could not be applied.
            LOGGER.warn("Server indicates profile could not be applied.")
            self.taskbar.set_status(
                "Unable to apply profile correctly.",
                status=self.taskbar.status_error,
                notify_title="Unable to apply profile.",
                notify_msg=(
                    "The profile could not be applied successfully. Please "
                    "review the administration logs for further details, and "
                    "report any issues."
                ), force=True
            )
            return False     

        elif res.status_code == 503:
            # Web up, MSR not available.
            LOGGER.warn("MSR service is unavailable.")
            self._has_had_error = True
            self.taskbar.set_status(
                "MSR service is unavailable.",
                status=self.taskbar.status_error,
                notify_title="Unable to connect to MSR hardware service",
                notify_msg=(
                    "Whilst the profile service is available, it cannot "
                    "connect to the hardware service. Ensure "
                    "services are running and review admin logs "
                    "for more info."
                )
            )
            return False
        elif res.status_code != 200:
            self._has_had_error = True
            LOGGER.error("Invalid status response {}: {}".format(
                res.status_code, res.text
            ))
            self.taskbar.set_status(
                "Error communicating with the profile server.",
                status=self.taskbar.status_error,
                notify_title="Error speaking to the profile service",
                notify_msg=(
                    "An unexpected error has occurred in communication with "
                    "the profile service. "
                    "Review the administration logs for further details."
                )
            )
            return False
        else:
            # TODO: Make this detection a bit more elegant :-)
            if self.open_windows_uri not in uri and self._has_had_error:
                self.taskbar.set_status(
                    "Successfully connected to MSR profile service.",
                    notify_title="Successfully connected to MSR.",
                    notify_msg=(
                        "Service issues have been resolved, and I have "
                        "successfully connected to the MSR services."
                    )
                )
                self._has_had_error = False
                # Force a full update to get MSR registers.
                self.status_last_queued = None
            elif self.open_windows_uri not in uri and self._first_conn:
                self._first_conn = False
                self.taskbar.set_status(
                    "Connected to MSR services."
                )

        return json

    def _handler_status_update(self, args):
        url = self.current_status_uri
        sonly = False
        if args and args[0] == 'settings':
            url += "?settings_only=1"
            sonly = True

        resl = self._load_url(url)
        if resl == None:
            # Throttled, so requeue
            self.queue.put(["status_update"])
            return
        elif resl == False:
            # Error, status has been updated if required.
            return
        else:
            hotkeys = None
            if self.hotkey:
                hotkeys = [b['hotkeys'] for b in resl if b.get('hotkeys')]
                if hotkeys: hotkeys = hotkeys[0]
                LOGGER.debug("Hotkey information is {}".format(hotkeys))
                self.hotkey.set_hotkeys(hotkeys)

            bmks = [b['bookmarks'] for b in resl if b.get('bookmarks')]
            bmks = bmks[0] if bmks else []
            self.taskbar.set_bookmarks(bmks, hotkeys)
            self.taskbar.set_force_bookmarks(bmks, hotkeys)

            last = [b['last'] for b in resl if b.get('last')]
            if last: self.taskbar.set_last_applied(last[0][0])
            if not sonly:
                self.taskbar.set_current_registers(
                    [r for r in resl if r.get('short_value')]
                )
            settings = [b['settings'] for b in resl if b.get('settings')]
            if settings:
                self.taskbar.set_settings(settings[0])
                self._settings = settings[0]

    def _handler_apply_profile(self, args):
        base_url = self.apply_profile_uri.format(args[0])

        data = {}
        try:
            data['force'] = 1 if args[1] else 0
        except IndexError:
            pass

        try:
            data['hotkey'] = 1 if args[2] else 0
        except IndexError:
            pass

        if data.get('hotkey'): self.taskbar.play_notify('hotkey-apply')
        self.taskbar.set_status(
            "Attempting to {}apply profile ({})...".format(
                "force " if data.get('force') else "",
                "hotkey=True" if data.get('hotkey') else ""
            ), status=self.taskbar.status_warn
        )
        resl = self._load_url(base_url, method="post", data=data)

        if resl:
            if data.get('force') == 0:
                self.taskbar.set_status(
                    "Profile {} unmarked as force.".format(
                        resl.get('name')
                    )
                )
                self.taskbar.play_notify('profile-unmark', no_delay=True)
                self._handler_status_update([])
                return
            self.taskbar.set_status(
                "Profile {} applied.".format(resl.get('name'))
            )

            dets = {
                'name': resl.get('name') or "Unknown Profile",
                'type': 'Manual',
                'applied': datetime.now( ).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            }
            self.taskbar.set_last_applied(dets, force_notify=True)
            # Force a status update
            self._handler_status_update([])
        elif resl == None:
            # Throttled, so requeue
            self.queue.put(["apply_profile", args])
        else:
            self.taskbar.set_status(
                "Unable to apply profile.", status=self.taskbar.status_error
            )

    def _handler_apply_setting(self, args):
        base_url = self.apply_settings_uri
        key, value = args
        sts = dict([(r['key'], r['checked']) for r in self.taskbar.get_settings( )])
        sts[key] = value
        self._settings = {'current': sts}

        self.taskbar.set_status(
            "Attempting to update settings...", status=self.taskbar.status_warn
        )
        resl = self._load_url(base_url, method="post", data={
            key: value
        })
        if resl:
            self.taskbar.set_status("Settings updated.")
            self._handler_status_update(['settings'])
        elif resl == None:
            # Throttled, so requeue
            self.queue.put(["apply_setting", args])
        else:
            self.taskbar.set_status(
                "Unable to apply settings.", status=self.taskbar.status_error
            )
        
    def _handle_queue_item(self, itm):
        try:
            try:
                cmd, args = itm[0], itm[1]
            except IndexError:
                cmd, args = itm[0], []
        except (ValueError, TypeError) as ex:
            LOGGER.error("Invalid queue item {}: {}".format(itm, ex))
            return
        
        LOGGER.debug("Processing queue item {}: {}".format(cmd, args))
        try:
            cmd = getattr(self, '_handler_{}'.format(cmd))
        except AttributeError:
            LOGGER.error("No queue handler for {}".format(cmd))
            return

        try:
            cmd(args)
        except Exception as ex:
            LOGGER.error(
                "Error whilst executing handler.", exc_info=ex
            )