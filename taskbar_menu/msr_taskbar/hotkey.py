from system_hotkey import SystemHotkey

import functools, logging

LOGGER = logging.getLogger("taskbar.hotkey")

class HotkeyListener(object):
    system_hotkeys = [
        ("Admin", ('control', 'shift', 'a'), 'launch_admin_url')
    ]

    def __init__(self, comms_thread):
        self.comms_thread = comms_thread
        self.taskbar = self.comms_thread.taskbar
        self._current_state = set( )
        self._current_hkeys = []
        self.hk = SystemHotkey( )
        # Set default system hotkeys
        self._set_hotkeys(set( ), [])

    def get_system_hotkeys(self): return self.system_hotkeys

    @classmethod
    def data_to_state(cls, data):
        dt = ["_".join(
            [str(p) for p in (d['id'], d['name'], "_".join(d['hotkey']))]
        ) for d in data]
        dt.sort( )
        return set(dt)

    def _sys_hk_callback(self, event, hkey, callback, *args, **kwargs):
        LOGGER.info(
            "Applying system callback {} for hotkey {}".format(
                callback, hkey
            )
        )
        try:
            cmd = getattr(self.taskbar, callback, None)
            if not cmd:
                LOGGER.warn(
                    "Unable to call system hotkey callback {}, it does not "
                    "exist on the main class.".format(callback)
                )
                return False
            cmd( )
            return True
        except Exception as ex:
            LOGGER.warn(
                "Error whilst applying system callback '{}'".format(callback),
                exc_info=ex
            )
            return False

    def _hk_callback(self, event, hkey, profile_id, name=None, *args, **kwargs):
        LOGGER.info(
            "Applying profile {}={} for hotkey combination {}".format(
                profile_id, name, hkey
            )
        )

        self.taskbar.set_status(
            "Applying profile from hotkey...",
            status=self.taskbar.status_warn,
            notify_title="Applying profile {} from hotkey.".format(
                name
            ), notify_msg=(
                "Applying profile {} as hotkey '{}' has been detected.".format(
                    name, " + ".join([h.title( ) for h in hkey])
                )
            )
        )
        self.comms_thread.queue.put([
            "apply_profile", [profile_id, None, True]
        ])

    def _set_hotkeys(self, state, hotkeys):
        LOGGER.info(
            "Updating hotkey state {}, details {}".format(
                state, hotkeys
            )
        )
        sys_hks = self.get_system_hotkeys( )
        had = False
        if self._current_hkeys:
            if len(self._current_hkeys) != len(sys_hks or []): had = True
            LOGGER.info(
                "Removing existing hotkeys: {}".format(self._current_hkeys)
            )
            for hkc in self._current_hkeys:
                try:
                    self.hk.unregister(hkc)
                except Exception as ex:
                    LOGGER.warn(
                        "Unable to unregister hotkey {}".format(hkc),
                        exc_info=ex
                    )

        hkeys = []
        for id, hotkey, action in sys_hks:
            try:
                LOGGER.info("Registering system hotkey {}={} => {}".format(
                    id, hotkey, action
                ))
                self.hk.register(hotkey, callback=functools.partial(
                    self._sys_hk_callback, hkey=hotkey, callback=action
                ))
                hkeys.append(tuple(hotkey))
            except Exception as ex:
                LOGGER.warn(
                    "Unable to register system hotkey {}".format(hotkey),
                    exc_info=ex
                )


        for item in hotkeys:
            try:
                if tuple(item.get('hotkey')) in hkeys:
                    LOGGER.warn(
                        "Hotkey {} is already registered for {}".format(
                            item['hotkey'], item
                        )
                    )
                    continue
                self.hk.register(item['hotkey'], callback=functools.partial(
                    self._hk_callback, hkey=item['hotkey'],
                    profile_id=item['id'], name=item.get('name')
                ))
                hkeys.append(item['hotkey'])
                LOGGER.info(
                    "Registered hotkey {} to profile {}/{}".format(
                        " + ".join(item['hotkey']), item['id'],
                        item.get('name') or 'Unknown Name'
                    )
                )
            except Exception as ex:
                LOGGER.warn(
                    "Unable to register hotkey {}".format(item),
                    exc_info=ex
                )

        if had:
            self.taskbar.set_status(
                "Hotkey configuration has been updated.",
                notify_title="Hotkey configuration has been updated.",
                notify_msg=(
                    "New hotkeys have been detected and applied. You "
                    "can now use the {} registered hotkey(s).".format(
                        len(hkeys)
                    )
                )
            )

        self._current_state = state
        self._current_hkeys = hkeys


    def set_hotkeys(self, hkey):
        try:
            ds = self.data_to_state(hkey)
            LOGGER.debug("New hotkey state: {}, current state: {}".format(
                ds, self._current_state
            ))
            if not ds and not self._current_state: return
            if not self._current_state or ds != self._current_state:
                LOGGER.info(
                    "Detected hotkey configuration change, updating."
                )
                self._set_hotkeys(ds, hkey)
        except Exception as ex:
            LOGGER.error(
                "Error whilst setting hotkeys {}".format(hkey),
                exc_info=ex
            )

    def __str__(self):
        return "SHK: {}".format(self.hk)