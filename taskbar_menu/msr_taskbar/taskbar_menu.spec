# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
build_script_path = '..\\build_scripts\\'

a = Analysis(
    ['taskbar.py'],
    pathex=['.'],
    binaries=[],
    datas=[
        ("ico/*", "ico"),
        ("sfx/*.wav", "sfx")
    ],
    hiddenimports=[
        'win32timezone', 'system_hotkey', 'pystray',
        'pystray._win32'
    ],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='msr_taskbar',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          icon='ico/green-chip.ico',
          version="file_version_info.txt")
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='msr_taskbar')
