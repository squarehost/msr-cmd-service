import pytz
from PIL import Image
from pystray import Icon, Menu, MenuItem

import datetime, functools, logging, os, sys

from taskbar import Taskbar

LOGGER = logging.getLogger('taskbar.menu')

class LeftMouseShowIcon(Icon):
    ''' Slight adaption to the standard Icon to show to the menu on 
        left-mouse click as well as right. '''
        
    def __call__(self):
        super( ).__call__( )
        from pystray._util.win32 import WM_RBUTTONUP
        self._on_notify(None, WM_RBUTTONUP)

class TaskbarMenu(object):
    MAX_MSR_ITEMS = 10
    MAX_PROFILES = 20

    default_icon = 'green-chip.ico'
    success_icon = 'green-chip.ico'
    warn_icon = 'amber-chip.ico'
    error_icon = 'red-chip.ico'

    profiles_text = 'Bookmarked Profiles'
    last_applied_text = 'Last Applied'

    menu_items = [
        Menu.SEPARATOR,
        dict(id='LastApplied', text=''),
        dict(id='LastAppliedType', text=''),
        dict(id='LastAppliedDate', text=''),
        Menu.SEPARATOR,
        dict(id='ApplyProfile', text=profiles_text),
        dict(id='Settings', text='Automation Settings'),
        dict(id='Admin', text='Admin/Setup Profiles'),
        Menu.SEPARATOR,
        dict(
            id='Version',
            text='MSR Version {} - (C) 2021 Squarehost Ltd'.format(
                Taskbar.version
            )
        ),
        dict(id='Docs', text='Help And Documentation'),
        dict(id='Exit', text='Exit')
    ]

    def __init__(self, taskbar):
        self.taskbar = taskbar
        # Ensure this thread is on the system locale.
        self.taskbar._setup_locale( )

        self._icon_cache = {}
        self._last_notify = None
        self.menu = self._setup_menu( )
        self.tray = LeftMouseShowIcon(
            self.taskbar.title, None, menu=self.menu
        )
        self.tray.icon = self._get_icon( )
        if not self.tray.icon:
            raise ValueError("Unable to load default icon for tray.")

    def set_tray_icon(self, type, force=False):
        self.tray.icon = self._get_icon(type, force=force)

    def set_icon_status(self, type, message, force=False):
        self.tray.icon = self._get_icon(type, force=force)
        self.tray.title = message

    def notify(self, title, message, force=False):
        title = str(title)[0:64]
        message = str(message)[0:256]
        strn = "{}{}".format(title, message)
        if not force and self._last_notify:
            if strn == self._last_notify: return False
        self.tray.notify(message, title)
        self._last_notify = strn
        return True

    def update(self): return self.tray.update_menu( )

    @property
    def hwnd(self): return getattr(self.tray, '_hwnd', None)

    @classmethod
    def _hotkey_to_string(cls, hk):
        d = " + ".join([h.title( ) for h in hk])
        return d.replace("Control", "Ctrl").replace("Shift", "Shft")

    @classmethod
    def _get_bookmark_text(cls, json):
        txt = json['name']
        ft = json.get('force_test')
        if ft:
            txt = '>> {} (Sticky)'.format(txt)
        hk = json.get('hotkey')
        if hk:
            txt = '{} [{}]'.format(
                txt, cls._hotkey_to_string(hk)
            )
        return txt

    def _get_icon(self, type=None, force=False):
        type = type or self.default_icon
        name, ext = os.path.splitext(type)
        ext = ext or ".ico"
        ico_name = "{}{}".format(name, ext)

        if not force and self._icon_cache.get(ico_name):
            return self._icon_cache[ico_name]

        dn = os.path.abspath(os.path.dirname(__name__))
        edn = os.path.abspath(os.path.dirname(sys.executable))
        icon_path = os.path.abspath(os.path.join(dn, "ico", ico_name))
        exe_icon_path = os.path.abspath(os.path.join(edn, "ico", ico_name))

        icon = None
        for path in (icon_path, exe_icon_path):
            LOGGER.debug("Attempting to load icon {}".format(path))
            try:
                icon = Image.open(path)
                if icon: break
            except FileNotFoundError as fex:
                pass
            except Exception as ex:
                LOGGER.warn("Unable to load icon file '{}'".format(
                    icon_path
                ), exc_info=ex)

        if not icon:
            LOGGER.warn("Unable to locate a valid icon for '{}'".format(
                type
            ))
            return None
        
        self._icon_cache[ico_name] = icon
        return icon

    ''' Dynamic MSR status methods. '''
    def _msr_status_item_text(self, item, index):
        return self.taskbar.get_current_registers( )[index]
    def _msr_status_item_visible(self, item, index):
        try:
            return bool(self.taskbar.get_current_registers( )[index])
        except IndexError:
            return False

    ''' Dynamic profile application methods. '''
    def _apply_profile_item_menu_text(self, menu):
        text = self.profiles_text
        bm = [True for b in self.taskbar.get_bookmarks( ) if b.get(
            'force_test'
        )]
        if bm:
            text = "{} <{} sticky>".format(text, len(bm))
        return text

    def _apply_profile_item_text(self, menu):
        text = self.profiles_text
        bm = [True for b in self.taskbar.get_bookmarks( ) if b.get(
            'force_test'
        )]
        if bm:
            text = "{} <{} sticky>".format(text, len(bm))
        return text
    def _apply_profile_item_checked(self, item, index):
        try:
            return self.taskbar.get_bookmarks( )[index].get(
                'force_test'
            )
        except IndexError:
            return False
    def _apply_profile_item_visible(self, item, index):
        try:
            return bool(self.taskbar.get_bookmarks( )[index])
        except IndexError:
            return False
    def _apply_profile_item_text(self, item, index):
        try:
            bi = self.taskbar.get_bookmarks( )[index]
            return self._get_bookmark_text(bi)
        except IndexError:
            return ""
    def _apply_profile_item_callback(self, menu, item, index):
        LOGGER.debug("Apply profile callback index={}, {} ,{}".format(
            index, menu, item
        ))
        try:
            sts = self.taskbar.get_force_bookmarks( )[index]
            value = sts.get('force_test')
            args = [sts['id']]
            if value: args.append(False)
            self.taskbar.apply_profile(*args)
        except IndexError:
            LOGGER.warn("Profile item {} no longer exists.".format(index))
    
    ''' Last applied dynamic methods. '''
    def _last_applied_item_menu_text(self, menu):
        la = self.taskbar.get_last_applied( )
        if la:
            appl = None
            txt = "{}: {}".format(
                self.last_applied_text,
                la.get('name') or 'Unknown Profile'
            )
            return txt
        return ""
    def _last_applied_item_type_menu_text(self, menu):
        la = self.taskbar.get_last_applied( )
        if la:
            appl = None
            txt = " - Type: {}".format(la.get('type') or 'Unknown Profile')
            return txt
        return ""
    def _last_applied_date_item_menu_text(self, menu):
        la = self.taskbar.get_last_applied( )
        if la:
            appl = None
            try:
                appl = datetime.datetime.strptime(
                    la.get('applied'), "%Y/%m/%d %H:%M:%S"
                ).replace(tzinfo=pytz.UTC)
                # Convert to user's local time
                appl = appl.astimezone( )
            except Exception as ex:
                LOGGER.warn(
                    "Unable to parse applied date time {}".format(la),
                    exc_info=ex
                )
            ds = ""
            if appl:
                today = datetime.date.today( )
                if today != appl.date( ):
                    ds = appl.strftime(" - Applied Date/Time: %x %X")
                else:
                    ds = appl.strftime(" - Applied Time: %X")
            return ds
        return ""

    def _last_applied_item_menu_visible(self, menu):
        return bool(self.taskbar.get_last_applied( ))
    ''' Dynamic settings application methods. '''
    def _settings_item_visible(self, item, index):
        try:
            return bool(self.taskbar.get_settings( )[index])
        except IndexError:
            return False
    def _settings_item_checked(self, item, index):
        try:
            return self.taskbar.get_settings( )[index].get(
                'checked'
            )
        except IndexError:
            return False
    def _settings_item_text(self, item, index):
        try:
            return self._get_bookmark_text(
                self.taskbar.get_settings( )[index]
            )
        except IndexError:
            return ""
    def _settings_item_callback(self, menu, item, index):
        LOGGER.debug("Apply settings callback index={}, {} ,{}".format(
            index, menu, item
        ))
        sts = self.taskbar.get_settings( )[index]
        value = not sts.get('checked')
        self.taskbar.update_setting(sts['key'], value)
 
    ''' Setup methods for creating MenuItem instances. '''
    def _setup_menu_item_ApplyProfile(self, item):
        '''
            Due to how the menu plugin works, make up to 10 dynamic 'bookmarks'
            which can be updated and/or hidden depending on which profiles are
            retrieved from the menu.
        '''
        profiles = []
        for i in range(0, self.MAX_PROFILES):
            profiles.append(MenuItem(
                text=functools.partial(
                    self._apply_profile_item_text, index=i
                ), visible=functools.partial(
                    self._apply_profile_item_visible, index=i
                ), action=functools.partial(
                    self._apply_profile_item_callback, index=i
                ), checked=functools.partial(
                    self._apply_profile_item_checked, index=i
                )
            ))
        return {
            'action': Menu(*profiles),
            'text': functools.partial(
                self._apply_profile_item_menu_text
            )
        }
    def _setup_menu_item_LastApplied(self, item):
        return {
            'text': functools.partial(
                self._last_applied_item_menu_text
            ), 'visible': functools.partial(
                self._last_applied_item_menu_visible
            ), 
        }
    def _setup_menu_item_LastAppliedType(self, item):
        return {
            'text': functools.partial(
                self._last_applied_item_type_menu_text
            ), 'visible': functools.partial(
                self._last_applied_item_menu_visible
            ), 
        }
    def _setup_menu_item_LastAppliedDate(self, item):
        return {
            'text': functools.partial(
                self._last_applied_date_item_menu_text
            ), 'visible': functools.partial(
                self._last_applied_item_menu_visible
            ), 
        }
    def _setup_menu_item_Settings(self, item):
        profiles = []
        for i in range(0, 50):
            profiles.append(MenuItem(
                text=functools.partial(
                    self._settings_item_text, index=i
                ), visible=functools.partial(
                    self._settings_item_visible, index=i
                ), action=functools.partial(
                    self._settings_item_callback, index=i
                ), checked=functools.partial(
                    self._settings_item_checked, index=i
                )
            ))
        return {'action': Menu(*profiles)}
    def _setup_menu_item_ForceApplyProfile(self, item):
        '''
            Due to how the menu plugin works, make up to 10 dynamic 'bookmarks'
            which can be updated and/or hidden depending on which profiles are
            retrieved from the menu.
        '''
        profiles = []
        for i in range(0, self.MAX_PROFILES):
            profiles.append(MenuItem(
                text=functools.partial(
                    self._force_apply_profile_item_text, index=i
                ), visible=functools.partial(
                    self._force_apply_profile_item_visible, index=i
                ), action=functools.partial(
                    self._force_apply_profile_item_callback, index=i
                ), checked=functools.partial(
                    self._force_apply_profile_item_checked, index=i
                )
            ))
        return {
            'action': Menu(*profiles),
            'text': functools.partial(
                self._force_apply_menu_text
            )
        }

    ''' Callback actions from menu items. '''
    def _callback_Admin(self, menu, item):
        self.taskbar.launch_admin_url( )
    _callback_LastApplied = _callback_Admin
    _callback_LastAppliedDate = _callback_Admin
    def _callback_Docs(self, menu, item):
        self.taskbar.launch_docs_url( )
    def _callback_Exit(self, menu, item):
        LOGGER.info("Exiting from tray menu icon.")
        self.taskbar.exit( )
    def _callback_Version(self, menu, item):
        self.taskbar.launch_project_url( )

    def _menu_callback(self, menu, item):
        '''
            Menu callback handler. This method will call defined callback
            methods on the class if they exist, or warn gracefully otherwise.
        '''
        LOGGER.debug("Menu Callback, menu: {}, item: {}".format(
            menu, item
        ))

        id = getattr(item, '_id')
        if id:
            callback = getattr(self, '_callback_{}'.format(id), None)
            if callback:
                return callback(menu, item)

        LOGGER.warn("No callback for id={}: item {}".format(
            id or '<Unknown>', item
        ))

    def _get_msr_status_items(self):
        return [MenuItem(
            text=functools.partial(
                self._msr_status_item_text, index=i
            ), visible=functools.partial(
                self._msr_status_item_visible, index=i
            ), enabled=False, action=lambda x: ''
        ) for i in range(0, self.MAX_MSR_ITEMS)]
        
    def _setup_menu(self, items=None):
        '''
            Setup menu items - wrap values accordingly.
            If a _setup_menu_item_<ID> method exists, we will call this to 
            determine additional parameters to be passed into the MenuItem
            class.
        '''
        menu_list = self._get_msr_status_items( )
        items = items or self.menu_items
        sys_hk = self.taskbar.get_system_hotkeys( )
        menu = None
        for item in items:
            if isinstance(item, dict):
                id = item.pop('id')
                setup = getattr(self, '_setup_menu_item_{}'.format(id), None)
                if id in sys_hk:
                    txt = item.get('text') or id
                    txt = "{} [{}]".format(
                        txt, self._hotkey_to_string(sys_hk[id][1])
                    )
                    item['text'] = txt
                if setup: item.update(setup(item))
                item['text'] = item.get('text') or id
                act = item.get('action')
                if isinstance(act, list):
                    act = self._setup_menu(act)
                elif act == None:
                    def local_callback(menu, item):
                        self._menu_callback(menu, item)
                    act = local_callback
                item['action'] = act
                LOGGER.debug("Creating menu item {}".format(item))
                mi = MenuItem(**item)
                mi._id = id
            else:
                mi = item
            menu_list.append(mi)
        return Menu(*menu_list)
            
    def run(self):
        self.tray.run( )
    def stop(self): self.tray.stop( )