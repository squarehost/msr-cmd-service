# -*- mode: python ; coding: utf-8 -*-

import os
j = os.path.join

block_cipher = None

build_relpath = "build_scripts\\"
docs_relpath = "docs\\"
prof_relpath = "profile_win32_svc\\profile_svc\\msr_web\\"
msr_relpath = 'msr_win32_svc\\'
taskbar_relpath = 'taskbar_menu\\msr_taskbar\\'

profile_svc = Analysis(
    [j(prof_relpath, "service.py")],
    pathex=[prof_relpath],
    binaries=[
    ],
    datas=[
        (j(docs_relpath, 'build', 'html'), 'docs'),
        (j(prof_relpath, 'msr/'), 'msr'),
        (j(prof_relpath, 'msr_web/'), 'msr_web'),
        (j(prof_relpath, 'profiles/'), 'profiles'),
        (j(prof_relpath, '_msr_settings.py'), '.')
    ],
    hiddenimports=['win32timezone',
                   'constance',
                   'constance.admin',
                   'constance.apps',
                   'constance.backends.database',
                   'formtools.wizard.views',
                   'formtools.wizard.storage',
                   'formtools.wizard.storage.session',
                   'rest_framework',
                   'rest_framework.apps',
                   'rest_framework.authentication',
                   'rest_framework.parsers',
                   'rest_framework.metadata',
                   'rest_framework.negotiation',
                   'rest_framework.permissions',
                   'rest_framework.routers',
                   'rest_framework.serializers',
                   'rest_framework.templatetags',
                   'rest_framework.throttling',
                   'rest_framework.viewsets'],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)

msr_svc = Analysis(
    [j(msr_relpath, 'service.py')],
    pathex=[msr_relpath],
    binaries=[
        (j(build_relpath, 'msr\\WinRing0x64.dll'), '.'),
        (j(build_relpath, 'msr\\WinRing0x64.sys'), '.'),
        ],
    datas=[],
    hiddenimports=['win32timezone'],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False)

taskbar = Analysis(
    [j(taskbar_relpath, 'taskbar.py')],
    pathex=[taskbar_relpath],
    binaries=[],
    datas=[
        (j(taskbar_relpath, "ico/*"), "ico"),
        (j(taskbar_relpath, "sfx/*.wav"), "sfx"),
    ],
    hiddenimports=[
        'win32timezone', 'system_hotkey', 'pystray',
        'pystray._win32'
    ],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False)

MERGE(
    (profile_svc, 'profile_svc', 'profile_svc'),
    (msr_svc, 'msr_svc', 'msr_svc'),
    (taskbar, 'taskbar', 'taskbar'),
)

profile_pyz = PYZ(
    profile_svc.pure, profile_svc.zipped_data,
    cipher=block_cipher
)
profile_exe = EXE(
          profile_pyz,
          profile_svc.scripts,
          [],
          exclude_binaries=True,
          name='msr_profile_svc',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          icon=j(taskbar_relpath, 'ico/green-chip.ico'),
          version=j(prof_relpath, 'file_version_info.txt'))
profile_coll = COLLECT(
               profile_exe,
               profile_svc.binaries,
               profile_svc.zipfiles,
               profile_svc.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='.')

msr_pyz = PYZ(
    msr_svc.pure, msr_svc.zipped_data,
    cipher=block_cipher
)
msr_exe = EXE(msr_pyz,
          msr_svc.scripts,
          [],
          exclude_binaries=True,
          name='msr_svc',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          icon=j(taskbar_relpath, 'ico/green-chip.ico'),
          version=j(msr_relpath, 'file_version_info.txt'))
msr_coll = COLLECT(msr_exe,
               msr_svc.binaries,
               msr_svc.zipfiles,
               msr_svc.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='msr_svc')

taskbar_pyz = PYZ(
    taskbar.pure, taskbar.zipped_data,
    cipher=block_cipher
)
taskbar_exe = EXE(
          taskbar_pyz,
          taskbar.scripts,
          [],
          exclude_binaries=True,
          name='msr_taskbar',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          icon=j(taskbar_relpath, 'ico/green-chip.ico'),
          version=j(taskbar_relpath, 'file_version_info.txt'))
taskbar_coll = COLLECT(
               taskbar_exe,
               taskbar.binaries,
               taskbar.zipfiles,
               taskbar.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='msr_taskbar')