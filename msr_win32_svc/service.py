
'''
This module uses the pywin32 ServiceFramework to provide a Win32 service which
listens for control changes, and calls the msr-cmd utility to apply them.

The msr-utility is an separate open source project wholly unrelated to this,
and can be found at: https://github.com/cocafe/msr-utility

It requires elevated privileges to do so, so functionality has been
deliberately limited to only setting and retrieving specific registers
through a low-level socket interface to attempt to minimise potential security
issues.

Extended functionality, such as the profiles and Web API is included in the
msr_webapi service.

'''

import win32serviceutil
import win32service
import win32event
import servicemanager
import logging, logging.config, os, socket, tempfile, time, sys

from msr_svc import msr_thread

LOGGER = logging.getLogger(__name__)

class MSRService(win32serviceutil.ServiceFramework):
    _svc_name_ = 'winmax-msr-tdpuv-svc'
    _svc_display_name_ = 'Squarehost CPU MSR Service'
    _svc_description_ = (
        "This service sets and retrieves MSR registers related to UnderVolting"
        " and TDP configuration, using msr-utility/WinRing0."
    )

    logfile_name = 'msr-service.log'

    LOGTYPE_GENERIC = 0xF000

    LOGLEVEL_DEBUG = 'debug'
    LOGLEVEL_INFO = servicemanager.EVENTLOG_INFORMATION_TYPE
    LOGLEVEL_WARN = LOGLEVEL_WARNING = servicemanager.EVENTLOG_WARNING_TYPE
    LOGLEVEL_ERROR = servicemanager.EVENTLOG_ERROR_TYPE

    ''' General ServiceFramework methods. '''
    def __init__(self, args):
        self.debug = os.environ.get('MSR_DEBUG') or servicemanager.Debugging(-1)
        self.setup_logging( )
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)  
        self.listener_thread = None

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        self.shutdown( )
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        self.startup( )
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ' - Starting in {} mode...'.format(
                                  "DEBUG" if self.debug else "normal"
                              )))
        self.main( )

    @classmethod
    def is_debug(cls):
        if hasattr(cls, '__is_debug'): return cls.__is_debug
        cls.__is_debug = servicemanager.Debugging(-1)
        return cls.__is_debug

    @classmethod
    def log(cls, msg, level=LOGLEVEL_INFO, type=LOGTYPE_GENERIC):
        ''' Convenience logger method, wrapping around servicemanager
            LogMsg, but including rudimentary debug filtering. '''   
        if level == cls.LOGLEVEL_DEBUG:
            LOGGER.debug(msg)
            if not cls.is_debug( ): return
            level = cls.LOGLEVEL_INFO
        elif level == cls.LOGLEVEL_WARNING:
            LOGGER.warn(msg)
        elif level == cls.LOGLEVEL_ERROR:
            LOGGER.error(msg)
        else:
            LOGGER.info(msg)
        servicemanager.LogMsg(
            level, type, (msg, '')
        )
        
    ''' Callbacks for service events. '''
    def startup(self):
        self.listener_thread = msr_thread.MSRCommandThread(
            service_klass=self.__class__,
            secure_mode=not servicemanager.Debugging(-1)
        )
        try:
            self.listener_thread.verify( )
        except Exception as ex:
            self.log(
                "Failure to verify listener thread: {} ".format(
                    ex
                )+"Aborting.",
                level=self.LOGLEVEL_ERROR
            )
            
            self.shutdown( )
            raise

        self.listener_thread.start( )

    def shutdown(self):
        ''' Shutdown any required services. This method should suppress
            any errors. '''
        try:
            self.listener_thread.stop( )
        except Exception as ex:
            if self.listener_thread:
                self.log(
                    "Unable to stop listener thread: {}".format(
                        ex
                    ), level=self.LOGLEVEL_WARN
                )
    
    def main(self):
        try:
            self.listener_thread.join( )
        except KeyboardInterrupt:
            LOGGER.info("CTRL+C/Interrupt pressed")
            self.shutdown( )
        except Exception as ex:
            LOGGER.error("Unknown fatal exception: {}".format(ex))
            self.shutdown( )

    def setup_logging(self):
        local_appdata = os.getenv('LOCALAPPDATA')
        if not local_appdata:
            import tempfile
            local_appdata = tempfile.gettempdir( )
            
        llevel = 'DEBUG' if self.debug else 'INFO'
        self.logfile = logfile = os.path.join(
            local_appdata, self.logfile_name
        )

        self.log("Logging to file {}, level {}".format(
            self.logfile, llevel
        ))
        cfg = dict(
            version=1,
            formatters=dict(
                full=dict(
                    format='[%(levelname)s] %(asctime)s %(message)s'
                ),
                brief=dict(
                    format='[%(levelname)s] %(message)s'
                )
            ),  
            handlers=dict(
                file=dict(
                    formatter='full',
                    level='DEBUG',
                    maxBytes=10 * 1024 * 1024,
                    backupCount=3,
                    filename=logfile
                ),
                console=dict(
                    formatter='brief',
                    level='DEBUG'
                )
            ),
            loggers=dict(
                msr_svc=dict(
                    handlers=['file', 'console'],
                    propagate=True,
                    level=llevel
                )
            )
        )
        cfg['loggers']['msr'] = cfg['loggers']['msr_svc']

        cfg['handlers']['file']['class'] = (
            'logging.handlers.RotatingFileHandler'
        )
        cfg['handlers']['console']['class'] = 'logging.StreamHandler'
        logging.config.dictConfig(cfg)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize( )
        servicemanager.PrepareToHostSingle(MSRService)
        servicemanager.StartServiceCtrlDispatcher( )
    else:
        win32serviceutil.HandleCommandLine(MSRService)