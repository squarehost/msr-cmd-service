# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
build_script_path = '..\\build_scripts\\'

a = Analysis(
    ['service.py'],
    pathex=['.'],
    binaries=[
        (os.path.join(build_script_path, 'msr\\WinRing0x64.dll'), '.'),
        (os.path.join(build_script_path, 'msr\\WinRing0x64.sys'), '.'),
        ],
    datas=[],
    hiddenimports=['win32timezone'],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='msr_svc',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          icon='..\\taskbar_menu\\msr_taskbar\\ico\\green-chip.ico',
          version="file_version_info.txt")
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='msr_svc')
