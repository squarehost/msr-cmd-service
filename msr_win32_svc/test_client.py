'''

    A basic file to simulate a client connection to the service
    named pipe. Ensure the service is running and pass in the pipe name
    as the first argument.

'''

import winerror, win32file

import json, time

def _read_obj(hdl):
    try:
        msg = _read_msg(hdl)
    except Exception as ex:
        print("Unable to read response from server: {}".format(ex))
        raise

    try:
        msg = json.loads(msg.decode('utf-8'))
    except Exception as ex:
        print("Unable to decode response from server: {}".format(ex))
        raise

    if msg.get('result') > 0:
        print("Error response from server: {}".format(msg))
        raise ValueError("Error from server")

    return msg

def _write_obj(hdl, obj):
    msg = _obj_to_bytes(obj)
    print("Sending message to server: {}".format(msg))
    win32file.WriteFile(hdl, msg)

def _read_msg(hdl):
    buf = b''
    more = winerror.ERROR_MORE_DATA
    while more == winerror.ERROR_MORE_DATA:
        more, data = win32file.ReadFile(hdl, 2048)
        buf += data
    return buf

def _obj_to_bytes(obj):
    return json.dumps(obj).encode('utf-8')

def simulate_client(pipe, version='0.1.a1'):
    hdl = win32file.CreateFile(
        r"\\.\pipe\{}".format(pipe),
        win32file.GENERIC_READ | win32file.GENERIC_WRITE, 0, None,
        win32file.OPEN_EXISTING, win32file.FILE_FLAG_OVERLAPPED, None
    )
    print("Sending ehlo string (version {}) to server.".format(version))
    _write_obj(hdl, {'ehlo': version})
    resp = _read_obj(hdl)
    print("Server response is: {}".format(resp))

    for cmd, params in (
        ('avail', {}),
        ('support', {}),
        ('read_all', {}),
        ('read', {'registers': 'TPL'}),
        ('write', {
            'registers': {
                'PP0_POLICY': {
                    'balance': 13
                },
                'WAKE_HIB': {
                    'seconds': 1500
                },
                'PARK': {
                    'min_parked': 0,
                    'max_parked': 6
                },
                'MAX_FREQ': {
                    'mhz': 4000
                }
            },
            # Careful when setting this to False as it will actually do stuff.
            'test_mode': True
        })
    ):
        print("Sending command '{}' to server (params: {})".format(
            cmd, params if params else "-"
        ))
        _write_obj(hdl, {'cmd': cmd, 'args': params})
        resp = _read_obj(hdl)
        print("Server response is ", resp)
        if resp['result'] != -1:
            raise RuntimeError("Unexpected response from server: {}. Expected ready command.".format(
                resp
            ))

    _write_obj(hdl, {'cmd': 'done'})
    resp = _read_obj(hdl)
    print("Server summary is {}".format(resp))

    win32file.CloseHandle(hdl)

if __name__ == "__main__":
    import sys
    try:
        pipe = sys.argv[1]
    except IndexError:
        print("Pass the pipe name (e.g. msr) in as the first argument.")
        sys.exit(100)
    
    kw = {}
    if len(sys.argv) > 2: kw['version'] = sys.argv[2]
    
    simulate_client(pipe, **kw)