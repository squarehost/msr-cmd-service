'''

This class listens synchronously on a win32 named pipe, decodes the request,
and performs the relevant actions associated with it.

'''
from ntsecuritycon import (
    SECURITY_CREATOR_SID_AUTHORITY, SECURITY_CREATOR_GROUP_RID,
    SECURITY_CREATOR_OWNER_RID, SECURITY_NT_AUTHORITY,

    SECURITY_WORLD_SID_AUTHORITY, SECURITY_WORLD_RID,
    SECURITY_LOCAL_SYSTEM_RID,

    FILE_ALL_ACCESS, FILE_GENERIC_READ, FILE_GENERIC_WRITE
)
import pywintypes, win32api, win32event, win32file, win32pipe, winerror

import json, logging, pkg_resources, threading, time

from .controllers import RegisterJSONEncoder

LOGGER = logging.getLogger(__name__)

class HandlerError(Exception):
    def __init__(self, code, err_msg):
        self.code = code
        self.err_msg = err_msg
        super( ).__init__(err_msg)
    
    def __str__(self):
        return "{}: {}".format(self.code, self.err_msg)

class Handler(object):
    ERROR_SERVER_UNKNOWN = 500

    ERROR_CLIENT_VERSION_MISMATCH = 100
    ERROR_CLIENT_MALFORMED_MESSAGE = 99
    ERROR_CLIENT_UNEXPECTED_CLOSE = 98

    ERROR_CLIENT_UNKNOWN_COMMAND = 999
    ERROR_CLIENT_INVALID_PARAMETERS = 998
    ERROR_CLIENT_UNKNOWN_REGISTERS = 997
    ERROR_CLIENT_INVALID_REGISTER_PARAMETERS = 996

    ERROR_REGISTER_SET_FAILED = 9999
    ERROR_UNSUPPORTED_PLATFORM = 10000

    RESULT_READY = -1
    RESULT_FINISHED = 0
    RESULT_ERROR = 1

    SUCCESS_ALL = 1
    SUCCESS_PARTIAL = 10

    BULK_OPERATION_LIMIT = 10

    json_encoder_class = RegisterJSONEncoder

    def __init__(
        self, handler, name, version, secure_mode=True, strict_mode=False
    ):
        self.handler = handler
        self.version = pkg_resources.parse_version(version)
        self.secure = secure_mode
        self.strict = strict_mode

    def get_command(self, command):
        try:
            return getattr(self, 'cmd_{}'.format(command))
        except AttributeError:
            raise HandlerError(
                self.ERROR_CLIENT_UNKNOWN_COMMAND,
                "Unknown command: '{}'".format(command)
            )

    def get_support_info(self):
        resp = {
            'version': self.version,
            'supported_handlers': [],
            'unsupported_handlers': []
        }
        for cnt in self.handler.get_supported_controllers( ):
            resp['supported_handlers'].append({
                'name': cnt.name,
                'class': cnt.__class__.__name__,
                'support_level': cnt.get_support_level( ),
                'supported_registers': [{
                    'short_name': r.short_name,
                    'name': r.name,
                    'rw': cnt.is_writeable(r)
                } for r in cnt.get_supported_registers( )],
                'unsupported_registers': [{
                    'short_name': r.short_name,
                    'name': r.name,
                } for r in cnt.get_unsupported_registers( )],
                'support_issues': cnt.get_support_issues( )
            })
        for cnt, issues in self.handler.get_unsupported_controllers( ):
            resp['unsupported_handlers'].append({
                'name': cnt.name,
                'class': cnt.__name__,
                'support_level': cnt.SUPPORT_LEVEL_UNSUPPORTED,
                'supported_registers': [],
                'unsupported_registers': [],
                'support_issues': issues
            })
        
        return resp

    def listen(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError

    '''
        Commands that can be actioned. A command must return a tuple of:
            - True/False for success
            - A JSON-encodable object, which will be sent directly to the client.
            - A list of additional messages to be sent (feedback)

        If you wish to break the client connection, raise a HandlerError in
        this block. Any exception will break connection, but only a HandlerError
        will result in a relevant error code being sent to the client.
    '''
    def cmd_avail(self):
        LOGGER.info("Sending available commands to client.")
        return (True, self.get_available_commands( ), [])

    def cmd_support(self):
        LOGGER.info("Sending support details to client.")
        resp = self.get_support_info( )
        return (True, resp, [])

    def cmd_read_all(self):
        LOGGER.info("Reading all registers for client.")
        return (True, self.handler.read_registers( ), [])

    def _sanitise_reg_list(self, registers):
        sanitised = self.handler.get_read_registers(registers)
        unsup = [
            u.short_name for u in self.handler.get_unsupported_registers( )
        ]
        unsupported = []
        names = [s.short_name for s in sanitised]
        invalid = []
        for r in registers:
            if r not in names:
                if r in unsup:
                    unsupported.append(r)
                else:
                    invalid.append(r)
        return (sanitised, invalid, unsupported)
        

    def cmd_read(self, registers):
        if not isinstance(registers, (tuple, list)):
            registers = [registers]

        sanitised, invalid, unsup = self._sanitise_reg_list(registers)
        if invalid:
            if self.strict:
                raise HandlerError(
                    self.ERROR_CLIENT_UNKNOWN_REGISTERS,
                    "The following registers are unknown/"
                    "unsupported: {}".format(
                        ", ".join(["'{}'".format(i) for i in invalid])
                    )
                )
            else:
                LOGGER.warn(
                    "The following registers are unknown/unsupported and "
                    "will not be actioned: {}".format(
                        ", ".join(["'{}'".format(i) for i in invalid])
                    )
                )
        if len(sanitised) > self.BULK_OPERATION_LIMIT:
            raise HandlerError(
                self.ERROR_CLIENT_INVALID_PARAMETERS,
                "Please specify a maximum of {} registers, not {}.".format(
                    self.BULK_OPERATION_LIMIT, len(sanitised)
                )
            )

        res = True
        if unsup: res = self.SUCCESS_PARTIAL
        return (res, self.handler.read_registers(sanitised), [])

    def cmd_write(
        self, registers, test_mode=True, stop_on_fail=True, verify=True
    ):
        ''' 
            Pass in a dict of Register short name -> parameter pairs to
            attempt to write these registers.

            Success will be aggregated across all registers,
            with the stop_on_fail argument causing the system to return an
            error if an individual register fails to set. Setting this
            to False will cause the system to skip failures and continue.

            The response object will be a dict of the passed in register
            short names, and True/False depending on if the set was 
            successful. Feedback will contain further information on 
            the set where appropriate.

        '''
        register_dict = registers
        if not isinstance(register_dict, dict):
            raise HandlerError(
                self.ERROR_CLIENT_INVALID_PARAMETERS,
                "This command requires a dictionary of registers/values, "
                "with the key being the register short name."
            )

        if len(register_dict) > self.BULK_OPERATION_LIMIT:
            raise HandlerError(
                self.ERROR_CLIENT_INVALID_PARAMETERS,
                "Please specify a maximum of {} registers, not {}.".format(
                    self.BULK_OPERATION_LIMIT, len(register_dict)
                )
            )

        sanitised, invalid, unsup = self._sanitise_reg_list(
            register_dict.keys()
        )
        if invalid:
            if self.strict:
                raise HandlerError(
                    self.ERROR_CLIENT_UNKNOWN_REGISTERS,
                    "The following registers are unknown: {}".format(
                        ", ".join(["'{}'".format(i) for i in invalid])
                    )
                )
            else:
                LOGGER.warn(
                    "The following registers are unknown/unsupported and "
                    "will not be actioned: {}".format(
                        ", ".join(["'{}'".format(i) for i in invalid])
                    )
                )


        feedback = []
        to_apply = []
        if unsup:
            LOGGER.warn(
                "Client is asking for unsupported registers {}. These "
                "will be discarded and no response returned for them.".format(
                    ", ".join(unsup)
                )
            )
            for u in unsup:
                feedback.append(
                    "{}: Register is unsupported, ignoring.".format(u)
                )

        
        for reg in sanitised:
            params = register_dict.get(reg.short_name)
            try:
                vals = reg.json_to_value(params)
            except Exception as ex:
                raise HandlerError(
                    self.ERROR_CLIENT_INVALID_REGISTER_PARAMETERS,
                    "Invalid parameter for '{}': {}".format(
                        reg.short_name, ex
                    )
                )
            
            feedback.append(
                "{}: Sanitised parameters are {}".format(
                    reg.short_name, vals        
                )
            )
            LOGGER.debug(feedback[-1])
            to_apply.append((reg, vals))

        compound_regs = []
        controller_regs = {}

        values = {}
        all_success = not unsup
        for klass, params in to_apply:
            controllers = self.handler.get_controllers_for_register(klass)
            if len(controllers) != 1:
                LOGGER.warn(
                    "Unable to apply compound register {} in specific "
                    "controller priority. This may cause conflicting "
                    "results.".format(klass)
                )
                compound_regs.append((klass, params))
            else:
                ctrl = controllers[0]
                if ctrl not in controller_regs:
                    controller_regs[ctrl] = []
                controller_regs[ctrl].append((klass, params))

        regs = [(None, compound_regs)] + list(controller_regs.items( ))
        regs.sort(key=lambda x: x[0].apply_priority if x[0] else -1)

        for controller, registers in regs:
            self.handler.start_session( )
            try:
                LOGGER.debug("ORDERED: Writing {} registers {}".format(
                    controller if controller else "COMPOUND", registers
                ))
                for klass, params in registers:
                    LOGGER.info("{}Applying register {}, with params {}".format(
                        "TEST: " if test_mode else "", klass, params
                    ))
                    try:
                        ro = False
                        if not controller.is_writeable(klass):
                            ro = True
                            if self.strict:
                                raise ValueError(
                                    "Unable to write register {}, it is not "
                                    "writeable.".format(klass.short_name)
                                )
                            else:
                                LOGGER.warn(
                                    "Unable to write register {}, controller "
                                    " reports it is not writeable.".format(
                                        klass.short_name
                                    )
                                )
                                res = None
                        else:
                            res = self.handler.write_register(
                                klass, params, verify=verify, test_mode=test_mode
                            )
                        values[klass.short_name] = res
                        if not res and not ro:
                            LOGGER.warn(
                                "Write register for {} returned False. "
                                "Could be a validation failure?".format(
                                    klass.short_name
                                )
                            )
                            all_success = False
                            if stop_on_fail:
                                raise HandlerError(
                                    self.ERROR_REGISTER_SET_FAILED,
                                    "Register {} could not be set, or "
                                    " verification failed.".format(
                                        klass.short_name
                                    )
                                )

                    except Exception as ex:
                        err = (
                            "There was a failure when setting register "
                            "{} - {}".format(klass.short_name, ex)
                        )
                        LOGGER.warn(err, exc_info=ex)
                        if stop_on_fail:
                            raise HandlerError(
                                self.ERROR_REGISTER_SET_FAILED, err
                            )
                        else:
                            feedback.append(err)
            finally:
                self.handler.finish_session( )

        res = all_success
        if not res and values: res = self.SUCCESS_PARTIAL
        return res, values, feedback

    def get_available_commands(self):
        cmds = []
        for k in dir(self):
            if k[0:4] == 'cmd_':
                # Allow for "hidden" commands
                if k[4] != '_':
                    cmds.append(k[4:])
        return cmds

class Win32PipeHandler(Handler):
    thread_timeout = 8
    secure_mode = True
    strict_mode = False

    def __init__(
        self, handler, pipe_name, version, secure_mode=secure_mode,
        strict_mode=strict_mode
    ):
        super( ).__init__(handler, pipe_name, version)
        self.overlapped = pywintypes.OVERLAPPED()
        self.overlapped.hEvent = win32event.CreateEvent(None, 0, 0, None)
        self.stopEvent = win32event.CreateEvent(None, 0, 0, None)
        self.pipe_path = r'\\.\pipe\{}'.format(pipe_name)
        self.running = False
        self.killed = False
        self.secure = secure_mode
        self.strict = strict_mode


    def create_security_context(self):
        '''
            Create the pipe so only the Owner group can access it,
            and only the Owner user can modify it.

            This should help prevent the interface being hijacked
            by other non-privileged applications.
        '''

        sa = pywintypes.SECURITY_ATTRIBUTES( )
        if self.secure:
            # Only allow group R/W access
            sidGroup = pywintypes.SID( )
            sidGroup.Initialize(SECURITY_NT_AUTHORITY, 1)
            sidGroup.SetSubAuthority(0, SECURITY_LOCAL_SYSTEM_RID)
        else:
            # Allow world R/W access to the pipe. Dangerous...
            LOGGER.warn(
                "Proceeding with world read/write access to named pipes. "
                "Please note, this will allow any process on this machine "
                "to access the register read/write process without further "
                "authentication required."
            )
            sidGroup = pywintypes.SID()
            sidGroup.Initialize(SECURITY_WORLD_SID_AUTHORITY,1)
            sidGroup.SetSubAuthority(0, SECURITY_WORLD_RID)

        sidUser = pywintypes.SID( )
        sidUser.Initialize(SECURITY_CREATOR_SID_AUTHORITY, 1)
        sidUser.SetSubAuthority(0, SECURITY_CREATOR_OWNER_RID)

        acl = pywintypes.ACL( )
        acl.AddAccessAllowedAce(
            FILE_GENERIC_READ | FILE_GENERIC_WRITE, sidGroup
        )
        acl.AddAccessAllowedAce(
            FILE_ALL_ACCESS, sidUser
        )

        sa.SetSecurityDescriptorDacl(1, acl, 0)
        return sa

    def listen(self):
        self.running = True
        self._listen( )

    def close(self):
        self.running = False
        try:
            win32event.SetEvent(self.stopEvent)
        except:
            pass

    def _read_msg(self):
        '''
            Read from the named pipe, and return a byte string.
        '''
        buf = b''
        more = winerror.ERROR_MORE_DATA
        while more == winerror.ERROR_MORE_DATA:
            more, data = win32file.ReadFile(self.pipe, 1024)
            buf += data
        
        LOGGER.debug("Read {} bytes from pipe\n\t{}".format(len(buf), buf))
        return buf
        
    def _write_msg(self, bytest):
        '''
            Write a byte string to the named pipe.
        '''
        LOGGER.debug("Sending byte string to client:\n\t{}".format(bytest))
        win32file.WriteFile(self.pipe, bytest)

    def _read_obj(self):
        '''
            Read from the named pipe and decode JSON into native
            Python format. 
        '''
        buf = None
        try:
            buf = self._read_msg( )
            msg = json.loads(buf.decode('utf-8'))
        except Exception as ex:
            if buf:
                LOGGER.debug("Raw bytes are {}".format(buf))
                LOGGER.warn(
                    "Unable to decode client message: {}".format(ex),
                    exc_info=ex
                )
            raise
        LOGGER.debug("Decoded message is {}".format(msg))
        return msg

    def _write_obj(self, obj):
        '''
            Encode the native Python object into JSON, and send to the
            named pipe.
        '''
        self._write_msg(
            json.dumps(obj, cls=self.json_encoder_class).encode('utf-8')
        )

    def _write_ready(self, msg=None):
        ''' 
            Write a basic "continue" message to the named pipe to tell
            the client we are ready for more input.
        '''
        msg = msg or 'Ready'
        handler = self.__class__.__name__
        unsup = [
            s[0].name for s in self.handler.get_unsupported_controllers( )
        ] 
        sup = [
            s.name for s in self.handler.get_supported_controllers( )
        ]
        msg = 'Server version {}, using {} communication.'
        if sup:
            msg += " Supporting {}.".format(", ".join([
                "'%s'" % s for s in sup
            ]))
        self._write_obj({
            'result': self.RESULT_READY, 'msg': msg,
            'supported': sup,
            'unsupported': unsup,
            'version': self.version, 'comms': handler,
            'message': msg.format(
                self.version, handler,
                "IS NOT" if unsup else "is"
            )
        })

    def _parse_ehlo(self, msg):
        try:
            version = msg['ehlo']
            v = pkg_resources.parse_version(str(version))
            
            if v > self.version:
                raise HandlerError(
                    self.ERROR_CLIENT_VERSION_MISMATCH,
                    "Client version {} is greater than I support ({})".format(
                        v, self.version
                    )
                )

            if v != self.version:
                LOGGER.warn(
                    "Client version {} not exact match for me ({}). I will "
                    "continue as I should be backwards compatible.".format(
                        v, self.version
                    )
                )

            return v
        except (KeyError, TypeError):
            raise HandlerError(
                self.ERROR_CLIENT_MALFORMED_MESSAGE,
                "Client version must be specified in ehlo key"
            )

    @classmethod
    def _exc_to_msg(cls, ex, killed=False):
        send = True
        err_code = cls.ERROR_SERVER_UNKNOWN
        if isinstance(ex, HandlerError):
            err_code = ex.code
            msg = ex.err_msg
        elif isinstance(ex, win32api.error):
            if ex.winerror == winerror.ERROR_BROKEN_PIPE:
                send = False
                if not killed:
                    LOGGER.warn("Broken pipe, client terminated early?")
                    err_code = cls.ERROR_CLIENT_UNEXPECTED_CLOSE
                    msg = "Why have you left me? :-("
                else:
                    msg = (
                        "Response handle timeout - forcibly killed. "
                        "Either something has gone horribly wrong, or our "
                        "client is hanging."
                    )
                    LOGGER.warn(msg)
            else:
                msg = str(ex)[0:150]
        else:
            msg = str(ex)[0:150]
        return (send, {
            'result': cls.RESULT_ERROR,
            'error': err_code,
            'msg': msg
        })

    def action_command(self, msg):
        success = None
        brk = True
        cmd = msg.get('cmd')
        if not cmd:
            raise HandlerError(
                self.ERROR_CLIENT_MALFORMED_MESSAGE,
                "Expecting command in 'cmd' key."
            )
        
        if cmd == 'done':
            brk = True
        else:
            func = self.get_command(cmd)
            kwargs = msg.get('args') or {}
            if not isinstance(kwargs, dict):
                raise HandlerError(
                    self.ERROR_CLIENT_INVALID_PARAMETERS,
                    "Parameters must be specified as a dictionary object."
                )
            try:
                success, response, feedback = func(**kwargs)
                brk = False

                if success == True or success == None:
                    success = self.SUCCESS_ALL
                elif success:
                    success = self.SUCCESS_PARTIAL
                else:
                    success = False
                resp = {
                    'result': self.RESULT_READY,
                    'success': success,
                    'response': response,
                }
                if feedback: resp['feedback'] = feedback
                self._write_obj(resp)
            except TypeError as vex:
                if 'unexpected keyword argument' in str(vex):
                    invalid = vex.args[0].split("argument ")[1].strip( )
                    raise HandlerError(
                        self.ERROR_CLIENT_INVALID_PARAMETERS,
                        "{} is not a valid parameter.".format(invalid)
                    )


                elif 'required positional argument' in str(vex):
                    missing = vex.args[0].split(":")[1].strip( )
                    raise HandlerError(
                        self.ERROR_CLIENT_INVALID_PARAMETERS,
                        "Required parameters have not been set: {}".format(
                            missing
                        )
                    )
                raise


        return success, brk

    def _write_goodbye(self, success=0, failed=0):
        self._write_obj({
            'result': self.RESULT_FINISHED,
            'success': success, 'msg': 'Adios amigo.',
            's': success, 'f': failed
        })
        

    def handle(self):
        # We have a connection from the client.
        try:
            self._parse_ehlo(self._read_obj( ))
            self._write_ready( )

            self.handler.start_session( )
            success = 0
            failed = 0
            while True:
                s, brk = self.action_command(self._read_obj( ))
                if s == True:
                    success += 1
                elif s == False:
                    failed += 1

                if brk: break


            self.handler.finish_session( )
            self._write_goodbye(success, failed)
        except Exception as ex:
            LOGGER.warn(
                "Error in handling client response {}".format(ex), exc_info=ex
            )
            try:
                send, msg = self._exc_to_msg(ex, killed=self.killed)
                if send:
                    self._write_obj(msg)
                else:
                    LOGGER.warn("Not sending error to client: {}".format(
                        msg
                    ))
            except Exception as ex:
                LOGGER.warn(
                    "Unable to return error message to client: {}".format(ex),
                    exc_info=ex
                )
        finally:
            self._close( )

    def _close(self):
        LOGGER.debug("Closing any handles/pipes...")
        self.killed = True
        try:
            win32file.CloseHandle(self.pipe)
        except:
            pass
        try:
            win32pipe.DisconnectNamedPipe(self.pipe, None)
        except:
            pass
    
    def _listen(self):
        has = False
        self.killed = False
        while self.running:
            try:
                self.pipe = win32pipe.CreateNamedPipe(
                    self.pipe_path,
                    win32pipe.PIPE_ACCESS_DUPLEX | win32file.FILE_FLAG_OVERLAPPED,
                    win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_BYTE,
                    1, 65536, 65536,
                    300, self.create_security_context( )
                )
                LOGGER.debug("Created named pipe {}".format(self.pipe_path))
                hr = win32pipe.ConnectNamedPipe(self.pipe, self.overlapped)
                has = True
            except Exception as ex:
                if isinstance(ex, win32api.error):
                    if ex.winerror == winerror.ERROR_PIPE_BUSY:
                        if not has:
                            LOGGER.warn(
                                "Unable to create named pipe: {}, is another "
                                "instance of the service running?".format(
                                    self.pipe_path
                                )
                            )
                            win32api.Sleep(2000)
                        else:
                            win32api.Sleep(500)
                        continue

                LOGGER.warn("Unable to connect to pipe: {}".format(
                    ex
                ))
                self._close( )
                continue

            LOGGER.debug("Connection to named pipe: {}".format(hr))
            try:
                if hr == winerror.ERROR_PIPE_CONNECTED:
                    win32event.SetEvent(self.overlapped.hEvent)
            
                rc = win32event.WaitForMultipleObjects(
                    (self.stopEvent, self.overlapped.hEvent), 0,
                    win32event.INFINITE
                )
                if rc == win32event.WAIT_OBJECT_0:
                    # Stop event has been reached.
                    break
                else:
                    sTime = time.time( )
                    hthread = threading.Thread(target=self.handle)
                    hthread.start( )
                    while True:
                        win32api.Sleep(200)
                        if hthread.is_alive( ):
                            if time.time( ) - sTime > self.thread_timeout:
                                LOGGER.error(
                                    "Handle thread timeout reached ({}), "
                                    "killing pipe.".format(self.thread_timeout)
                                )
                                self._close( )
                                hthread.join()
                        else:
                            break

            except Exception as ex:
                LOGGER.warn(
                    "Error whilst sending/receiving pipe: {}".format(ex),
                    exc_info=ex
                )
            finally:
                self._close( )

        self._close( )