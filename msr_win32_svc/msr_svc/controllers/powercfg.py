from ctypes import wintypes
import ctypes, ctypes.wintypes, json, logging, os, shutil, subprocess, sys
import time
import pythoncom, winerror, wmi

# Fix for COM multithreading
sys.coinit_flags = 0

from ._guid_utils import GUID
from .base import BaseController, DllLoader, Register, SerializableResult

LOGGER = logging.getLogger(__name__)

'''
    Controller to handle settings within Windows' Power Profile system.

    Used for core parking and the like.

    TODO: We currently screw around with the current power scheme - is this
    wise, as we could be permanently changing settings?
'''

from ._powercfgsetup import ALIASES, ALIAS_MAP, POWER_DEFAULTS


class PowerCfgRegister(Register):
    def _read_reg(self, proc_thread=None):
        res = []
        for alias in self.get_register( ):
            res.append(self.controller.read_register(alias))
        
        return self.raw_to_dict(*res)

class MaxCPUFrequency(SerializableResult):
    def __init__(self, freq_mhz):
        self.freq_mhz = freq_mhz or 0
    
    @property
    def raw_value(self):
        return self.freq_mhz or 0

    def __str__(self):
        return "{}MHz".format(self.freq_mhz)

    def __repr__(self): return "MaxCPUFrequency({}MHz)".format(self.freq_mhz)
    
    def to_json(self):
        return {
            'mhz': self.freq_mhz
        }

    @classmethod
    def from_json(cls, dict):
        return MaxCPUFrequency(
            freq_mhz=dict.get('mhz')
        )      

class WakeHibernateTimer(SerializableResult):
    MAX_SECONDS = 4294967295
    
    def __init__(self, seconds=None, minutes=None):
        if seconds:
            try:
                self.secs = int(seconds)
                if self.secs > self.MAX_SECONDS:
                    raise ValueError(
                        "Maximum seconds is {}".format(self.MAX_SECONDS)
                    )
            except Exception:
                raise ValueError("Seconds must be None or an integer value.")
        elif minutes:
            try:
                self.secs = int(minutes) * 60
                if self.secs > self.MAX_SECONDS:
                    raise ValueError("Maximum minutes is {}".format(
                        int(self.MAX_SECONDS) / 60
                    ))
            except Exception:
                raise ValueError("Minutes must be None or an integer value.")
        else:
            self.secs = 0

    @property
    def mins(self):
        if self.secs:
            return int(self.secs / 60)
        return 0

    @property
    def raw_value(self):
        return self.secs or 0

    def __str__(self):
        if not self.secs:
            return "Disable wake to hibernate timer"
        else:
            return "Wake after {} minutes ({} seconds)".format(
                self.mins, self.secs
            )

    def __repr__(self): return "WakeToHibernate({}s)".format(self.secs)
    
    def to_json(self):
        return {
            'seconds': self.secs or None,
            'minutes': self.mins or None,
        }

    @classmethod
    def from_json(cls, dict):
        return WakeHibernateTimer(
            seconds=dict.get('seconds'),
            minutes=dict.get('minutes')
        )   

class ParkedCores(SerializableResult):
    can_lock = False

    def __init__(self, max_parked, min_parked=0, max_cores=0):
        try:
            self.min_parked = int(min_parked)
        except Exception:
            raise ValueError("Minimum parked cores must be an integer value.")

        try:
            self.max_parked = int(max_parked)
        except Exception:
            raise ValueError("Maximum parked cores must be an integer value.")
    
        self.max_park = max_cores

        if self.min_parked > self.max_park:
            raise ValueError(
                "Minimum parked cores must be between 0 and {}".format(
                    self.max_park
                ))

        if self.max_parked > self.max_park:
            raise ValueError(
                "Maximum parked cores must be between 0 and {}".format(
                    self.max_park
                ))

        if self.max_parked < self.min_parked:
            raise ValueError(
                "Maximum number of parked cores must be greater than or equal "
                "to the minimum number of parked cores."
            )

    def __str__(self):
        return "Minimum Parked {} of {}, Maximum Parked {} of {}".format(
            self.min_parked, self.max_park, self.max_parked, self.max_park
        )

    def __repr__(self): return "Parking({})".format(self.__str__( ))

    def to_json(self):
        return {
            'min_parked': self.min_parked,
            'max_parked': self.max_parked,
            'avail_to_park': self.max_park
        }

    @classmethod
    def from_json(cls, dict, max_cores):
        return ParkedCores(
            min_parked=dict['min_parked'],
            max_parked=dict['max_parked'],
            max_cores=max_cores
        )
    
    def get_raw_min(self):
        return 100 - int((self.min_parked / self.max_park) * 100)
    def get_raw_max(self):
        return 100 - int((self.max_parked / self.max_park) * 100)

class WakeHibernateTimerRegister(PowerCfgRegister):
    '''
        Set the wake to hibernate timer setting.
    '''
    name = "Wake to Hibernate Timer"
    short_name = "WAKE_HIB"

    register = ["HIBERNATEIDLE"]

    def raw_to_dict(self, _timer_secs, register=None):
        return {
            'seconds': _timer_secs,
            'reg': register or self.get_register( ),
        }

    @classmethod
    def json_to_value(cls, json):
        if not isinstance(json, dict):
            raise ValueError(
                "Expects a dict-like object with 'seconds' or "
                "'minutes' keys."
            )
        
        if 'seconds' not in json and 'minutes' not in json:
            raise ValueError("Supply one of seconds or minutes keys.")

        return {
            'seconds': json.get('seconds'),
            'minutes': json.get('minutes')
        }

    def raw_to_value(self, dict_value, register=None):
        return WakeHibernateTimer(seconds=dict_value['seconds'])

    def write_value(self, value, verify=True, stop_on_fail=False):
        if not isinstance(value, dict):
            raise ValueError(
                "write_value expects a dict-like object with 'seconds' or "
                "'minutes' keys."
            )
        
        val = WakeHibernateTimer.from_json(value)
        fail_val = False
        for alias in self.get_register( ):
            res = self.controller.write_register(
                alias, val.raw_value, readback=verify
            )
            if not res and stop_on_fail:
                raise ValueError("Failed to set value, stopping.")

            if verify:
                fail_val = fail_val or res != val.raw_value
                if fail_val and stop_on_fail:
                    raise ValueError("Failed validation.")

        if fail_val and verify:
            raise ValueError("Failed validation.")
        return val

class MinProcessorFrequencyRegister(PowerCfgRegister):
    '''
        Set the minimum allowed processor frequency in MHz.

        There is no minimum frequency register within PowerCfg, so we use
        the minimum throttle percentage index control.

        Windows seems to set as a percentage of 'guaranteed' performance,
        which seems equivalent to MaxClockSpeed we obtain from WMI.
        Anything above that has to be controlled via HWP_REQ MSR.

        It seems to round the percentage to 2 DP and then floor to the
        nearest multiplier (bus speed), but not entirely confident this is
        perfect, so add on a 'fudge factor' couple of percentage points :-)
    '''
    name = "Minimum Processor Frequency (MHz)"
    short_name = "MIN_FREQ"

    register = ["PROCTHROTTLEMIN"]

    def raw_to_dict(self, factor):
        mhz = int(((factor / 100) * self.controller.max_clock) / 100) * 100
        return {'mhz': mhz}

    def raw_to_value(self, dict_value):
        if 'mhz' in dict_value:
            return {'mhz': dict_value['mhz']}
        else:
            return self.raw_to_dict(dict_value['factor'])

    @classmethod
    def json_to_value(cls, json, max_clock=None):
        if not isinstance(json, dict):
            raise ValueError(
                "Expects a dict-like object with 'mhz' key."
            )
        
        if 'mhz' not in json:
            if 'factor' not in json:
                raise ValueError("Supply the minimum frequency as mhz.")
            else:
                return {'factor': json['factor']}
        else:
            mhz = int(json['mhz'])

        '''
            If we don't have a max clock we are just verifying, so return the
            checked value.
        '''
        if not max_clock: return {'mhz': mhz}

        if not mhz: return {'factor': 0}
        # TODO, we assume bus speed of 100. Do we need to consider others?
        base_mhz = int(mhz / 100) * 100
        # Add on our fudge factor...
        base_mhz += 50
        factor = base_mhz / max_clock

        if factor >= 1: return {'factor': 100}
        return {'factor': int(round(factor, 2) * 100)}

    def write_value(self, value, verify=True, stop_on_fail=False):
        if not isinstance(value, dict):
            raise ValueError(
                "write_value expects a dict-like object with 'mhz' key."
            )
        
        if not hasattr(self.controller, 'max_clock') or (
            not self.controller.max_clock
        ):
            raise ValueError(
                "Max clock has not been determined, cannot set minimum "
                "processor frequency."
            )
        val = self.json_to_value(value, self.controller.max_clock)

        fail_val = False
        for alias in self.get_register( ):
            res = self.controller.write_register(
                alias, val['factor'], readback=verify
            )
            if not res and stop_on_fail:
                raise ValueError("Failed to set value, stopping.")

            if verify:
                fail_val = fail_val or res != val['factor']
                if fail_val and stop_on_fail:
                    raise ValueError("Failed validation.")

        if fail_val and verify:
            raise ValueError("Failed validation.")
        return val

class MaxProcessorFrequencyRegister(PowerCfgRegister):
    '''
        Set the maximum allowed processor frequency in MHz.
    '''
    name = "Maximum Processor Frequency (MHz)"
    short_name = "MAX_FREQ"

    register = ["PROCFREQMAX", "PROCTHROTTLEMAX"]

    @classmethod
    def json_to_value(cls, json):
        if not isinstance(json, dict):
            raise ValueError(
                "Expects a dict-like object with 'mhz' key."
            )
        
        if 'mhz' not in json:
            raise ValueError("Supply the maxmimum frequency as mhz.")

        return {
            'mhz': json.get('mhz')
        }

    def raw_to_dict(self, _mhz, register=None):
        return {'mhz': _mhz}

    def raw_to_value(self, dict_value, register=None):
        return MaxCPUFrequency(freq_mhz=dict_value['mhz'])

    def write_value(self, value, verify=True, stop_on_fail=False):
        if not isinstance(value, dict):
            raise ValueError(
                "write_value expects a dict-like object with 'mhz' key."
            )
        
        if not hasattr(self.controller, 'max_clock') or (
            not self.controller.max_clock
        ):
            raise ValueError(
                "Max clock has not been determined, cannot set maximum "
                "processor frequency."
            )

        val = MaxCPUFrequency.from_json(value)
        throttle = MinProcessorFrequencyRegister.json_to_value(
            {'mhz': val.raw_value}, self.controller.max_clock
        )['factor'] or 100
        
        fudge_factor = 50
        mhz = val.raw_value + fudge_factor if val.raw_value else 0
        fail_val = False
        for alias in self.get_register( ):
            if alias == "PROCFREQMAX":
                res = self.controller.write_register(
                    alias, mhz, readback=verify
                )
            elif alias == "PROCTHROTTLEMAX":
                res = self.controller.write_register(
                    alias, throttle, readback=verify
                )
            if not res and stop_on_fail:
                raise ValueError("Failed to set value, stopping.")

            if verify:
                if alias == "PROCFREQMAX":
                    fail_val = fail_val or res != mhz
                else:
                    fail_val = fail_val or res != throttle
                if fail_val and stop_on_fail:
                    raise ValueError("Failed validation.")

        if fail_val and verify:
            raise ValueError("Failed validation.")
        return val

class EPPPowerCfgRegister(PowerCfgRegister):
    '''
        Set the maximum allowed processor frequency in MHz.
    '''
    name = "EPP (Energy Performance Profile)"
    short_name = "EPP"

    register = ["PERFEPP"]

    @classmethod
    def json_to_value(cls, json):
        if not isinstance(json, dict):
            raise ValueError(
                "Expects a dict-like object with 'factor' key."
            )
        
        if 'factor' not in json:
            raise ValueError(
                "Supply the EPP factor (between 0 and 100) using the "
                "'factor' key."
            )

        try:
            fct = int(json['factor'])
            if fct < 0 or fct > 100: raise ValueError
        except Exception:
            raise ValueError("EPP Factor must be between 0 and 100.")

        return {
            'factor': fct
        }

    def raw_to_dict(self, factor, register=None):
        return {'factor': factor}

    def raw_to_value(self, dict_value, register=None):
        return {'factor': dict_value['factor']}

    def write_value(self, value, verify=True, stop_on_fail=False):
        val = self.json_to_value(value)
        fail_val = False

        for alias in self.get_register( ):
            res = self.controller.write_register(
                alias, val['factor'], readback=verify
            )
            if not res and stop_on_fail:
                raise ValueError("Failed to set value, stopping.")

            if verify:
                fail_val = fail_val or res != val['factor']
                if fail_val and stop_on_fail:
                    raise ValueError("Failed validation.")

        if fail_val and verify:
            raise ValueError("Failed validation.")
        return val

class ResetPowerDefaultsRegister(PowerCfgRegister):
    '''
        Set all of Windows power schemes and settings to the default.
        Technically not a read/write register but let's go with it.
    '''
    name = "Reset Power Settings"
    short_name = "PWR_RESET"

    def read_value(self, *args, **kwargs0):
        return {'read': 'NOOP'}

    def write_value(self, value, verify=True, stop_on_fail=False):
        if value.upper( ) == 'RESET':
            LOGGER.info("Attempting to reset via PowerRestoreDefaultPowerSchemes.")
            res = self.controller.dll.PowerRestoreDefaultPowerSchemes( )
            LOGGER.debug("PowerRestoreDefaultPowerSchemes result is {}".format(res))
            if res != 0:
                if stop_on_fail or verify:
                    raise ValueError(
                        "Reset to default not successful: {}".format(res)
                    )
            else:
                LOGGER.info("Restore of default power schemes was successful.")
                return 'RESET'
        else:
            LOGGER.warn(
                "Not resetting as write value '{}' is not = RESET".format(
                    value
                )
            )
            if verify or stop_on_fail:
                raise ValueError("Write value '{}' is not known".format(value))

class CoreParkingRegister(PowerCfgRegister):
    '''
        Set the number of parked cores according to those available on
        the system.
    '''
    name = "Parked Cores"
    short_name = "PARK"

    register = ["CPMINCORES", "CPMAXCORES"]

    def raw_to_dict(self, _min, _max, register=None):
        return {
            'min': _min, 'max': _max,
            'reg': register or self.get_register( ),
        }

    def raw_to_value(self, dict_value, register=None):
        #TODO: We only act on physical cores, worth adding logical?
        _min, _max = dict_value['min'], dict_value['max']
        _mind, _maxd = _min * 0.01, _max * 0.01
        core_p = self.controller.cores_p

        _minp = core_p - (core_p * _mind)
        _maxp = core_p - (core_p * _maxd)

        return ParkedCores(
            _maxp, _minp, core_p
        )

    @classmethod
    def json_to_value(cls, json):
        if not isinstance(json, dict):
            raise ValueError("Excepting a dict-like object with 'min_parked' "
                "and 'max_parked' keys.")
            
        if 'min_parked' not in json:
            raise ValueError("min_parked value must be specified.")
        if 'max_parked' not in json:
            raise ValueError("max_parked value must be specified.")
    
        return {
            'min_parked': json.get('min_parked'),
            'max_parked': json.get('max_parked')
        }

    def write_value(self, value, verify=True, stop_on_fail=False):
        if not isinstance(value, dict):
            raise ValueError(
                "write_value expects a dict-like object with 'min_parked' "
                "and 'max_parked' keys."
            )
        
        val = ParkedCores.from_json(value, self.controller.cores_p)
        fail_val = False
        for alias in self.get_register( ):
            rval = val.get_raw_min( ) if 'MIN' in alias else val.get_raw_max( )
            res = self.controller.write_register(
                alias, rval, readback=verify
            )
            if not res and stop_on_fail:
                raise ValueError("Failed to set value, stopping.")

            if verify:
                fail_val = fail_val or res != rval
                if fail_val and stop_on_fail:
                    raise ValueError("Failed validation.")

        if fail_val and verify:
            raise ValueError("Failed validation.")
        return val

# https://docs.microsoft.com/en-us/windows/win32/api/powrprof/nf-powrprof-powerwritevaluemax

class PowerProfController(BaseController, DllLoader):
    name = 'Windows Power Profile Controller'

    '''
        If power_profile is not NULL, the controller will attempt to 
        create the named power profile (or read the GUID if it already exists).
        All modifications will then be applied to this profile rather than
        the user's current one.
    '''
    power_profile = 'MSR Power'
    ''' This is the power profile to "duplicate". Ideally this needs to be
        run after restoredefaults, but I don't want to enforce that as it
        will reset power profiles, which rather defeats the point of creating
        a new one. Maybe we need a more comprehensive set of default values
        that we forcibly set once duplicated.
    '''

    '''
        PowerCFG resets some MSR values, so we need to apply all these before
        applying our MSRs.
    '''
    apply_priority = 10

    duplicate_power_profile = 'Balanced'
    duplicate_power_profile_guid = '381b4222-f694-41f0-9685-ff5bb260df2e'
    always_set_power_profile_defaults = False
    power_profile_defaults = POWER_DEFAULTS

    POWER_ACCESSOR_ACCESS_SCHEME = 16
    POWER_ACCESS_AC_POWER_SETTING_INDEX = 0
    POWER_ACCESS_DC_POWER_SETTING_INDEX = 0
    POWER_ACCESS_SCHEME = 16
    POWER_ACCESS_ACTIVE_SCHEME = 19
    POWER_ACCESS_CREATE_SCHEME = 20

    all_read_registers = all_registers = [
        ResetPowerDefaultsRegister,

        WakeHibernateTimerRegister,

        CoreParkingRegister, 
        
        MinProcessorFrequencyRegister, MaxProcessorFrequencyRegister,
        EPPPowerCfgRegister
    ]

    def init(self):
        if self.dll == None:
            raise RuntimeError("Unable to access DLL")

        self.cores_p, self.cores_l, self.max_clock = self.get_num_processors( )
        LOGGER.info(
            "Total physical cores {}, logical cores {}, max clock {}".format(
                self.cores_p, self.cores_l, self.max_clock
            )
        )

        self._PowerGetActiveScheme = self.dll.PowerGetActiveScheme
        self._PowerReadFriendlyName = self.dll.PowerReadFriendlyName
        self._PowerEnumerate = self.dll.PowerEnumerate
        self._PowerDuplicateScheme = self.dll.PowerDuplicateScheme
        self._PowerWriteFriendlyName = self.dll.PowerWriteFriendlyName
        '''
            Note, this is a pointer to a pointer, hence the reason we have to 
            pass in a reference to a pointer to a GUID, not the GUID pointer
            itself.
        '''

        self._PowerGetActiveScheme.argtypes = [
            wintypes.HKEY,
            ctypes.POINTER(ctypes.POINTER(GUID))
        ]
        self._PowerReadFriendlyName.argtypes = [
            wintypes.HKEY, 
            ctypes.POINTER(GUID),
            ctypes.POINTER(GUID),
            ctypes.POINTER(GUID),
            ctypes.POINTER(wintypes.WCHAR),
            ctypes.POINTER(wintypes.DWORD)
        ]
        self._PowerEnumerate.argtypes = [
            wintypes.HKEY, 
            ctypes.POINTER(GUID),
            ctypes.POINTER(GUID),
            wintypes.ULONG,
            wintypes.ULONG,
            ctypes.POINTER(wintypes.WCHAR),
            ctypes.POINTER(wintypes.DWORD)
        ]
        self._PowerDuplicateScheme.argtypes = [
            wintypes.HKEY,
            ctypes.POINTER(GUID),
            ctypes.POINTER(ctypes.POINTER(GUID))
        ]
        self._PowerWriteFriendlyName.argtypes = [
            wintypes.HKEY,
            ctypes.POINTER(GUID),
            ctypes.POINTER(GUID),
            ctypes.POINTER(GUID),
            ctypes.POINTER(wintypes.WCHAR),
            wintypes.DWORD
        ]

        self._active_set = False
        if not self.cores_p:
            raise RuntimeError("Cannot get physical core count.")

    def get_all_supported_registers(self):
        return self.get_supported_registers( )
    
    def get_all_supported_read_registers(self):
        return self.get_supported_registers( )

    @classmethod
    def get_dll_name(cls):
        return "powrprof.dll"

    @classmethod
    def get_num_processors(cls):
        phys, logic, max_clock = 0, 0, 0
        try:
            pythoncom.CoInitialize( )
            c = wmi.WMI( )
            procs = c.Win32_Processor([
                "Manufacturer", "ProcessorId", "MaxClockSpeed",
                "Name", "NumberOfCores", "NumberOfLogicalProcessors",
            ])
            for p in procs:
                pn = p.Name
                c, l = p.NumberOfCores, p.NumberOfLogicalProcessors
                mx = p.MaxClockSpeed
                if mx and mx > max_clock:
                    max_clock = mx
                LOGGER.info(
                    "Detected physical processor {}, {} cores, "
                    "{} threads, max clock: {}.".format(pn, c, l, mx)
                )
                phys += c
                logic += l
        except Exception as ex:
            LOGGER.warn("Unable to query CPUs via WMI: {}".format(
                ex
            ), exc_info=ex)
        finally:
            try:
                pythoncom.CoUninitialize( )
            except:
                pass

        if not phys:
            # Fallback
            phys = logic = os.getenv('NUMBER_OF_PROCESSORS')

        return (phys, logic, max_clock)

    def get_support_level(self):
        try:
            stat = self.dll.CanUserWritePwrScheme( )
            if not stat:
                msg = "User does not have permission to write power schemes." 
                LOGGER.warn(msg)
                self.support_hints.append(msg)
                return self.SUPPORT_LEVEL_UNSUPPORTED
            
            rgs = self.get_supported_registers( )

            if not rgs:
                return self.SUPPORT_LEVEL_UNSUPPORTED
            elif len(rgs) < len(self.all_read_registers):
                return self.SUPPORT_LEVEL_PARTIAL_SUPPORT
            else:
                return self.SUPPORT_LEVEL_FULL_SUPPORT
                    
        except Exception as ex:
            self.support_hints.append(
                "Error whilst calling PowrProf DLL function: {}".format(
                    ex
                )
            )
            LOGGER.warn("Unable to determine support level.", exc_info=ex)
            return self.SUPPORT_LEVEL_UNSUPPORTED
  
    def get_supported_registers(self):
        if hasattr(self, '_supported_registers'):
            return self._supported_registers

        self._supported_registers = []
        for reg in self.all_read_registers:
            rgs = getattr(reg, 'register', [])
            has_all = True
            if rgs:
                if not isinstance(rgs, (list, tuple)):
                    rgs = [rgs]
                
                for r in rgs:
                    guid = self.get_alias_info(r)
                    if not guid:
                        LOGGER.warn("Invalid register name {}".format(r))
                    else:
                        g = GUID('{%s}' % guid[1])
                        res = self.dll.PowerSettingAccessCheck(
                            self.POWER_ACCESS_AC_POWER_SETTING_INDEX,
                            ctypes.byref(g)
                        ) == 0 and self.dll.PowerSettingAccessCheck(
                            self.POWER_ACCESS_DC_POWER_SETTING_INDEX,
                            ctypes.byref(g)
                        ) == 0 and self.dll.PowerSettingAccessCheck(
                            self.POWER_ACCESS_ACTIVE_SCHEME,
                            ctypes.byref(g)
                        ) == 0
                        if not res:
                            m = "Policy blocks power change for {}/{}".format(
                                r, guid
                            )
                            has_all = False
                            self.unsupported_registers.append(reg)
                            self.support_hints.append(m)
                            LOGGER.warn(
                                m + " This register will be unsupported."
                            ) 
                        del g

            if has_all: self._supported_registers.append(reg)

        return self._supported_registers

    @classmethod
    def get_alias_info(cls, req_alias):
        for subvals in ALIAS_MAP.values( ):
            if req_alias in subvals:
                return (subvals['__'], subvals.get(req_alias))
        return None

    def _get_power_scheme_name(self, guid_struct):
        buff_size = wintypes.DWORD( )
        # Get the required buffer size.
        res = self._PowerReadFriendlyName(
            None, ctypes.byref(guid_struct),
            None, None, None, buff_size
        )
        if res != 0 and res != winerror.ERROR_MORE_DATA:
            raise ValueError("Unable to read PowerReadFriendlyName (rc != 0)")

        strbuf = (wintypes.WCHAR * buff_size.value)( )

        res = self._PowerReadFriendlyName(
            None, ctypes.byref(guid_struct),
            None, None,
            strbuf, buff_size
        )
        if res != 0:
            raise ValueError("Unable to read PowerReadFriendlyName (rc != 0)")

        return strbuf.value

    def _enumerate_power_schemes(self):
        buff_size = wintypes.DWORD( )
        schemes = {}

        for i in range(0, 255):
            # Get the buffer size for the accessor.
            res = self._PowerEnumerate(
                None, None, None, self.POWER_ACCESSOR_ACCESS_SCHEME,
                i, None, buff_size
            )
            if res == winerror.ERROR_NO_MORE_ITEMS:
                break
            
            if res != 0 and res != winerror.ERROR_MORE_DATA:
                raise ValueError("Unable to enumerate power schemes (rc != 0)")

            strbuf = (wintypes.WCHAR * buff_size.value)( )
            res = self._PowerEnumerate(
                None, None, None, self.POWER_ACCESSOR_ACCESS_SCHEME,
                i, strbuf, buff_size
            )
            if res != 0:
                raise ValueError("Unable to enumerate power schemes (rc != 0)")

            # Cast the buffer to get the GUID.
            guidp = ctypes.cast(strbuf, ctypes.POINTER(GUID))
            guid = guidp.contents
            schemes[str(guid)] =  self._get_power_scheme_name(guid)
            del guidp
            
        LOGGER.debug("Enumerated {} power schemes: {}".format(
            len(schemes), schemes
        ))
        return schemes

    def _get_profile_guid_by_name(self, *names, case_insensitive=True):
        pprofs = self._enumerate_power_schemes( )
        guids = {}

        for guid, pname in pprofs.items( ):
            for name in names:
                if name == pname or (
                    case_insensitive and name.lower( ) == pname.lower( )
                ): guids[name] = guid
        return guids or None

    def _set_power_profile_name(self, guid_str, name):
        guid = GUID(guid_str)
        strbuf = ctypes.create_unicode_buffer(name)

        res = self._PowerWriteFriendlyName(
            None, guid, None, None,
            strbuf, wintypes.DWORD(ctypes.sizeof(strbuf))
        )
        if res != 0:
            raise ValueError("Unable to set power profile name (rc != 0)")
    
        LOGGER.info("Power profile name for {} set to {}".format(
            guid_str, name
        ))
        return True


    def _duplicate_power_profile(self, guid_str, dup_name):
        ptr = ctypes.POINTER(GUID)( )
        guid = GUID(guid_str)
        res = self._PowerDuplicateScheme(
            None, guid, ctypes.byref(ptr)
        )
        if res != 0:
            raise ValueError("Unable to call PowerDuplicateScheme (rc != 0)")

        nguid = str(ptr.contents)
        del ptr
        LOGGER.info("Duplicate of {} created as {}, setting name to {}".format(
            guid_str, nguid, dup_name
        ))
        self._set_power_profile_name(nguid, dup_name)
        self._set_power_profile_defaults(str(nguid))
        return nguid

    def _create_power_profile(self, name, current_guid_str=None):
        guids = self._get_profile_guid_by_name(
            name, self.duplicate_power_profile
        ) or {}
        if name not in guids:
            dguid = guids.get(self.duplicate_power_profile)
            if not dguid:
                dguid = '{%s}' % self.duplicate_power_profile_guid.upper( )
                alld = self._enumerate_power_schemes( )
                if dguid not in alld:
                    LOGGER.warn(
                        "Unable to locate default power scheme {}/{} -".format(
                            self.duplicate_power_profile_guid.upper( ),
                            self.duplicate_power_profile
                        )+" using the first one I find."
                    )
                    if not len(alld):
                        raise ValueError(
                            "Cannot create power profile, no existing schemes "
                            "exist to be duplicated."
                        )
                    else:
                        dguid = list(alld.keys( ))[0]
            
            dupl = self._duplicate_power_profile(dguid, name)
            guid = GUID(dupl)
            # Apply the default settings.
            self.dll.PowerSetActiveScheme(None, ctypes.byref(guid))
            # Restore the current scheme if required.
            if current_guid_str:
                guid = GUID(current_guid_str)
                self.dll.PowerSetActiveScheme(None, ctypes.byref(guid))

        else:
            if self.always_set_power_profile_defaults and not self._active_set:
                LOGGER.info("Resetting power defaults for {}".format(name))
                self._set_power_profile_defaults(guids[name])
                self._active_set = True
            return guids[name]

    def get_power_profile_defaults(self):
        return self.power_profile_defaults
            
    def _set_power_profile_defaults(self, guid):
        LOGGER.debug("Setting power profile defaults for {}".format(guid))
        set_defs = {}
        for alias, value in self.get_power_profile_defaults( ).items( ):
            
            try:
                res = self.write_register(
                    alias, value, scheme_guid=guid, readback=False, no_set=True
                )
                LOGGER.debug("Write {} result = {}".format(alias, res))
                if res: set_defs[alias] = value
            except Exception as ex:
                LOGGER.warn("Unable to set default value {}={} for {}.".format(
                    alias, value, guid
                ), exc_info=ex)

        LOGGER.debug("Set power profile defaults for {} to: {}".format(
            guid, set_defs
        ))
        return set_defs

    def get_active_power_scheme(self):
        '''
            Return the power profile we wish to modify and activate. If 
            create_power_profile is specified, we will create and activate
            this named profile, otherwise we will just return the currently
            active system profile.
        '''
        ptr = ctypes.POINTER(GUID)( )
        res = self._PowerGetActiveScheme(None, ctypes.byref(ptr))
        if res != 0:
            raise ValueError("Unable to read PowerGetActiveScheme (rc != 0)")

        actv = ptr.contents
        name = self._get_power_scheme_name(actv)

        if self.power_profile and self.power_profile != name:
            # Create our power profile for application purposes.
            guid = self._create_power_profile(self.power_profile, None)
            self.dll.PowerSetActiveScheme(
                None, ctypes.byref(GUID(guid))
            )
    
            LOGGER.info("Power profile {} is {}".format(
                self.power_profile, guid
            ))
            return GUID(guid)

        guid = GUID(str(actv))
        del actv
        del ptr
        return guid

    def read_register(self, alias, scheme_guid=None):
        '''
            Given a PowrProf ALIAS and index value, read the configuration from
            the power scheme represented by scheme_guid. If scheme_guid is
            not specified, the current scheme will be used.
        
            We read the AC value as we assume we always set both. 
            TODO: Do we want to improve this and return AC/DC allowing the
            register to determine what it wants to use?
        '''
        info = self.get_alias_info(alias)
        if info == None:
            raise ValueError("Unknown power alias {}".format(alias))
        
        if not scheme_guid:
            try:
                sch_guid = self.get_active_power_scheme( )
            except Exception as ex:
                LOGGER.error("Unable to get active power scheme: {}".format(
                    ex
                ))
                raise
            LOGGER.info("Active power scheme GUID is {}".format(str(sch_guid)))
        else:
            if '{' not in scheme_guid: scheme_guid = '{%s}' % scheme_guid
            sch_guid = GUID(scheme_guid)
  
        group_guid, setting_guid = info
        gr_guid = GUID('{%s}' % group_guid)
        st_guid = GUID('{%s}' % setting_guid)

        LOGGER.debug(
            "Attempting to call PowerReadACValueIndex with {}, {} ({})".format(
                str(gr_guid), str(st_guid), alias
            )
        )
        value = wintypes.DWORD( )

        res = self.dll.PowerReadACValueIndex(
            None, ctypes.byref(sch_guid),
            ctypes.byref(gr_guid),
            ctypes.byref(st_guid),
            ctypes.byref(value)
        )
        if res != 0:
            raise ValueError(
                "Unable to read PowerReadACValueIndex (rc {} != 0)".format(
                    res
                )
            )
        LOGGER.debug("Result of call is {}".format(value.value))
        return value.value

    def write_register(
        self, alias, value, scheme_guid=None, readback=True, 
        no_set=False, **kwargs
    ):
        '''
            Given a PowrProf ALIAS and index value, write the configuration to
            the power scheme represented by scheme_guid. If scheme_guid is
            not specified, the current scheme will be used.
        '''
        info = self.get_alias_info(alias)
        if info == None:
            raise ValueError("Unknown power alias {}".format(alias))

        if not scheme_guid:
            try:
                sch_guid = self.get_active_power_scheme( )
            except Exception as ex:
                LOGGER.error("Unable to get active power scheme: {}".format(
                    ex
                ))
                raise

            LOGGER.info("Setting for active scheme {}".format(str(sch_guid)))
        else:
            LOGGER.info("Setting for passed in scheme {}".format(
                str(scheme_guid))
            )
            if '{' not in scheme_guid: scheme_guid = '{%s}' % scheme_guid
            sch_guid = GUID(scheme_guid)

        group_guid, setting_guid = info
        gr_guid = GUID('{%s}' % group_guid)
        st_guid = GUID('{%s}' % setting_guid)
        values = wintypes.DWORD(value)
        
        for type in ('AC', 'DC'):
            LOGGER.debug(
                "Attempting to call PowerWrite{}ValueIndex with"
                "{}, {} ({}) = {}".format(
                    type, str(gr_guid), str(st_guid), alias, values
                )
            )
            res = getattr(self.dll, 'PowerWrite{}ValueIndex'.format(type))(
                None, ctypes.byref(sch_guid),
                ctypes.byref(gr_guid),
                ctypes.byref(st_guid),
                values
            )
            if res != 0:
                msg = "Unable to PowerWriteACValueIndex (rc {} != 0)".format(
                    res
                )
                LOGGER.warn(msg)
                return False
            
        if readback: return self.read_register(alias, scheme_guid=scheme_guid)

        return True

    def apply_all(self):
        try:
            sch_guid = self.get_active_power_scheme( )
        except Exception as ex:
            LOGGER.error("Unable to get active power scheme: {}".format(
                ex
            ))
            raise

        LOGGER.debug(
            "Attempting to apply power scheme with PowerSetActiveScheme"
            " = {}".format(str(sch_guid))
        )
        res = self.dll.PowerSetActiveScheme(
            None, ctypes.byref(sch_guid)
        )
        if res != 0:
            msg = "Unable to PowerSetActiveScheme (rc {} != 0)".format(
                res
            )
            LOGGER.warn(msg)
            return False

        time.sleep(1)
        return True