from collections import namedtuple
import copy, ctypes, ctypes.wintypes, json, logging, os, shutil
import subprocess, sys

from .base import BaseController, DllLoader, SerializableResult
from .base import Register as _Register

'''
    Controller to handle settings within Intel's MSR system.

    Logic for registers is in Intel 64 and IA-32 Architectures
    Software Developer's Manual.

    Good explanation here:
    https://gist.github.com/Mnkai/5a8edd34bd949199224b33bd90b8c3d4
'''

LOGGER = logging.getLogger(__name__)

class Register(_Register):
    read_only = False

    def test(self):
        if not self.register:
            LOGGER.warn(
                "Testing register {} not possible, no register is defined."
                " Assuming supported".format(self.register)
            )
            return self.read_support_level if (
                self.read_only
            ) else self.write_support_level

        LOGGER.debug(
            "Testing register {} via read_value".format(self.short_name)
        )
        try:
            tv = self.read_value( )
            LOGGER.info(
                "Test read register value is {}, assuming R/W support.".format(
                    tv
                )
            )
            self._test_value = tv
            return self.read_support_level if (
                self.read_only
            ) else self.write_support_level
        except Exception as ex:
            LOGGER.warn(
                "Test read register failed for {}, assuming this is an "
                "unsupported register.".format(self.short_name)
            )
            return None

# Read-only registers 

class PlatformInfoRegister(Register):
    name = "Platform Info"
    short_name = "PINFO"
    register = 0xCE
    read_only = True

    def raw_to_value(self, raw):
        eax, edx = raw['eax'], raw['edx']
        # Bits 8-15 are maximum not turbo ratio
        max_non_turbo = (eax >> 8) & 0xff
        # Bit 28 = programmable turbo ratio
        p_rat = (eax >> 28) & 0x1
        # Bit 29 = programmable TDP ratio
        p_tdp = (eax >> 29) & 0x1
        # Bit 40-47 = max efficiency ratio
        max_effic = (eax >> 40) & 0x7f

        return {
            'max_non_turbo': max_non_turbo,
            'unlock_ratio': p_rat,
            'unlock_tdp': p_tdp,
            'max_effic': max_effic
        }


class HWPCapabilitiesRegister(Register):
    name = "HWP Performance Range and Dynamic Capabilities"
    short_name = "HWP"
    register = 0x771
    read_only = True

    def raw_to_value(self, raw):
        eax = raw['eax']
        if not raw.get('bus'):
            try:
                raw['bus'] = BusSpeedRegister(self.controller).read_value( )
            except Exception as ex:
                LOGGER.warn("Unable to read bus speed: {}".format(ex))
        # First 8 bits are highest performance
        high = eax & 0xff
        # Next 8 bits are guaranteed performance
        guar = (eax >> 8) & 0xff
        # Next 8 bits are most efficient performance
        effc = (eax >> 16) & 0xff
        # Next 8 bits are lowest performance
        low = (eax >> 24) & 0xff

        return {
            'lowest': low, 'highest': high,
            'guaranteed': guar, 'efficient': effc,
            'bus': raw.get('bus') or None
        }

class UncoreRatioLimitRegister(Register):
    name = "Uncore Ratio Limit"
    short_name = "UNCORE"
    register = 0x620
    read_only = True

    def raw_to_value(self, raw):
        eax = raw['eax']
        # First 7 bits are max ratio
        max = eax & 0x7f
        # Bits 8-14 are min ratio
        min = (eax >> 8) & 0x7f

        return {
            'max': max, 'min': min
        }

class BusSpeedRegister(Register):
    register = 0x2a
    read_only = True

    def raw_to_value(self, raw, **kw):
        eax = raw['eax']
        bus = (eax >> 18) & 0xff
        if bus == 0:
            return 100
        else:
            raise ValueError("Unknown bus speed in MSR {}: {}".format(
                self.register, raw
            ))

class UnitsRegister(Register):
    '''
        Power units from Intel register. Only really useful when used
        by the TDPRegister to convert values into relevant units.
    '''
    name = "Power Units"
    short_name = "PU"
    register = 0x606
    read_only = True

    def raw_to_value(self, raw):
        # 32-bit value, so only use EAX
        vl = raw['eax']
        # First 4 bits are power units
        pwr = 2 ** (vl & 0xf)
        # Next 5 bits are CPU energy units
        ener = 2 ** (vl >> 8 & 0x1f)
        # Bits 16-20 are time units
        time = 2 ** (vl >> 16 & 0xf)
        return {'power': pwr, 'time': time, 'energy': ener}

# Read-write registers 

class PLConfig(SerializableResult):
    can_lock = False

    def __init__(self, limit, enabled, clamped, duration, locked, original=None):
        try:
            self.limit = float(limit)
            if self.limit < 1 or self.limit > 1000:
                raise ValueError
        except Exception:
            raise ValueError(
                "Power limit must be a float value between 1 and 1000, "
                "representing the power limit in Watts."
            )
    
        self.enabled = bool(enabled)
        self.clamped = bool(clamped)
        try:
            self.duration = int(duration)
        except Exception:
            raise ValueError("Duration must be an integer value.")
        self.locked = bool(locked)
        self.original = original

    def __str__(self):
        return "Enabled={}, Limit={}, Clamped={}, Dur={}, Lck={}".format(
            self.enabled, self.limit, self.clamped, self.duration, self.locked
        )

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.__str__( ))

    def to_json(self):
        return {
            'enabled': self.enabled,
            'clamped': self.clamped,
            'locked': self.locked,
            'limit': self.limit,
            'duration': self.duration
        }

    @classmethod
    def from_json(cls, dict):
        return PLConfig(
            limit=dict['limit'],
            enabled=dict['enabled'],
            clamped=dict['clamped'],
            duration=dict['duration'],
            locked=dict.get('locked', False)
        )

    def to_raw(self, power, time=None, energy=None):
        if not self.original:
            raise ValueError(
                "Cannot convert to raw EAX/EDX as original bits are not set. "
            )

        LOGGER.debug("Converting {} to raw - original is {}".format(
            self, bin(self.original)
        ))
        orig = str(bin(self.original))[2:].zfill(32)

        lock_bit, clamp_bit, enable_bit = orig[0], "0", "0"

        if self.can_lock: lock_bit = "1" if self.locked else "0"

        limit = self.limit * power
        bin_limit = str(bin(int(round(limit))))[2:].zfill(15)

        if self.enabled: enable_bit = "1"
        if self.clamped: clamp_bit = "1"

        # Original bits (unknown)
        uk = orig[1:8]
        # Original bits, time window limit
        LOGGER.warn(
            "Duration is currently ignored for power limits, the bits "
            "currently stored in the register will be used instead of "
            "the value set."
        )
        tl = orig[8:15]

        bit_str = "".join((
            # Lock bit
            lock_bit,
            # Original bits 31 - 24 (Unknown)
            uk,
            # Original bits 24 - 16 (Power time limit)
            tl,
            # Clamp bit
            clamp_bit,
            # Enable bit
            enable_bit,
            # Power limit
            bin_limit
        ))

        return int(bit_str, base=2)

class PL1Config(PLConfig):
    pass

class PL2Config(PLConfig):
    can_lock = True

class PowerBalanceConfig(SerializableResult):
    type = 'PP0'

    def __init__(self, balance):
        try:
            self.balance = int(balance)
            if self.balance < 0 or self.balance > 31:
                raise ValueError
        except Exception:
            raise ValueError(
                "Power balance must be an integer between 0 and 31."
            )

    def to_raw(self):
        return int(self.balance)

    def to_json(self):
        return {
            'type': self.type,
            'balance': self.balance
        }

    @classmethod
    def from_json(cls, dict):
        try:
            type = dict['type']
            if type == 'PP0':
                kls = PP0Config
            elif type == 'PP1':
                kls = PP1Config
            else:
                kls = cls

            return kls(
                balance=dict['balance']
            )
        except KeyError:
            raise ValueError("Missing required parameter.")

    def __str__(self):
        return "{}_POLICY = {} of 31".format(self.type, self.balance)

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.balance)

class PP0Config(PowerBalanceConfig):
    pass

class PP1Config(PowerBalanceConfig):
    type = 'PP1'

class HWPConfig(SerializableResult):
    def __init__(self, min, max, desired, epp, raw=None):
        try:
            self.min = int(min)
            if self.min < 0: raise ValueError
        except Exception:
            raise ValueError(
                "Minimum requested performance must be an integer value."
            )

        try:
            self.max = int(max)
            if self.max < 0: raise ValueError
        except Exception:
            raise ValueError(
                "Maximum requested performance must be an integer value."
            )

        try:
            self.epp = int(epp)
            if self.epp < 0 or self.epp > 255: raise ValueError
        except Exception:
            raise ValueError("EPP must be an integer value between 0 and 255")

        self.desired = desired or 0
        if self.desired:
            try:
                self.desired = int(self.desired)
                if self.desired < 0: raise ValueError
            except Exception:
                raise ValueError(
                    "Desired performance must be an integer value."
                )

        LOGGER.debug("Configured: Min {}, max {}, desired {}, EPP {}".format(
            self.min, self.max, self.desired, self.epp
        ))
        if self.min > self.max:
            raise ValueError("Minimum performance must be lower than maximum.")

        if self.desired > self.max:
            raise ValueError("Desired performance must be lower than maximum.")

        self.raw = raw

    @property
    def epp_perc(self):
        return int(((self.epp or 0) / 255.0) * 100)

    def to_raw(self, bus=100):
        desired, max, min = map(lambda x: int(x / bus), (
            self.desired, self.max, self.min
        ))
        return (self.epp << 24) + (desired << 16) + (max << 8) + min

    def __str__(self):
        return "Minimum={}, Maximum={}, Desired={}, EPP={} ({}%) {}".format(
            self.min, self.max, self.desired or "-", self.epp, self.epp_perc,
            self.raw or ""
        )

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.__str__( ))

    def to_json(self):
        return {
            'minimum': self.min,
            'maximum': self.max,
            'desired': self.desired,
            'epp': self.epp
        }

    @classmethod
    def from_json(cls, dict):
        try:
            return cls(
                min=dict['minimum'],
                max=dict['maximum'],
                desired=dict['desired'],
                epp=dict['epp']
            )
        except KeyError:
            raise ValueError("Missing required parameter.")

class PowerPolicyRegister(Register):
    '''
        Request/set PP0/PP1 Power Policy values for the CPU package.

        Ability to set a balance value between 0 and 31.
    '''
    config_types = {
        'PP0': PP0Config,
        'PP1': PP1Config
    }
    config_type = None

    @property
    def config_obj_class(self): return self.config_types[self.config_type]

    def raw_to_value(self, raw, **kw):
        '''
            EAX: Bits 0 to 4 = balance value

            Between 0x0 and 0x1f (31).
        '''
        eax = raw['eax']

        return self.config_obj_class(eax)

    @classmethod
    def json_to_value(cls, json, verify=False, **defaults):
        if not isinstance(json, dict):
            raise ValueError("Object must be a dict-like object.")
    
        for k, v in defaults.items( ):
            if k not in json: json[k] = v

        if verify:
            try:
                self.config_obj_class.from_json(json)
            except TypeError:
                raise ValueError(
                    "Invalid parameters specified, balance value must be "
                    "given."
                )

        return json

    def write_value(self, value, verify=True, stop_on_fail=False):
        if isinstance(value, PowerBalanceConfig):
            dct = value.to_json( )
        else:
            dct = value

        if not isinstance(dct, dict):
            raise ValueError(
                "Value should be a dictionary or PowerBalanceConfig."
            )

        dct['type'] = self.config_obj_class.type
        config = self.config_obj_class.from_json(dct)
        eax = config.to_raw( )

        res = self.controller.write_register(
            self.get_register( ), eax, 0x0, readback=verify
        )
        if verify:
            return res and res[0] == eax
        return None

class PP0PolicyRegister(PowerPolicyRegister):
    name = "PP0 (CPU Core) Power Policy Register"
    short_name = "PP0_POLICY"
    register = 0x63a
    config_type = "PP0"

class PP1PolicyRegister(PowerPolicyRegister):
    name = "PP1 (Uncore) Power Policy Register"
    short_name = "PP1_POLICY"
    register = 0x642
    config_type = "PP1"

class HWPRequestRegister(Register):
    '''
        Request HWP Capabilities for the CPU Package.

        Ability to set minimum ratio, maximum ratio, desired ratio and
        EPP preference.
    '''
    name = "HWP Ratio Request"
    short_name = "HWP_REQ"
    register = 0x772

    def raw_to_value(self, raw, **kw):
        '''
            EAX: Bits 0 to 7 = Minimum
                 Bits 8 to 15 = Maximum
                 Bits 16 to 23 = Desired
                 Bits 24 to 31 = EPP

            EDX: Activity window (ignore for now)

            Backup other bits to restore if required.
        '''
          
        eax = raw['eax']
        
        #TODO: Read bus speed?
        bus = 100
        min = eax & 0xff
        max = (eax >> 8) & 0xff
        desire = (eax >> 16) & 0xff
        epp = (eax >> 24) & 0xff

        res = HWPConfig(min * bus, max * bus, desire * bus, epp, raw)
        return res

    @classmethod
    def json_to_value(cls, json, verify=False, **defaults):
        if not isinstance(json, dict):
            raise ValueError("Object must be a dict-like object.")
    
        for k, v in defaults.items( ):
            if k not in json: json[k] = v

        if verify:
            try:
                HWPConfig.from_json(json)
            except TypeError:
                raise ValueError(
                    "Invalid parameters specified, minimum, maximum, desired "
                    "and epp value MUST be given."
                )

        return json

    def write_value(self, value, verify=True, stop_on_fail=False):
        if isinstance(value, HWPConfig):
            dct = value.to_json( )
            dct['raw'] = value.raw
        else:
            dct = value

        if not isinstance(dct, dict):
            raise ValueError("Value should be a dictionary or HWPConfig.")

        query = []
        for key in ('minimum', 'maximum', 'epp'):
            resl = dct.get(key)
            q = not resl if key != 'epp' else resl == None
            if q: query.append(key)

        raw = dct.get('raw')
        if not raw or query:
            LOGGER.debug(
                "Querying existing HWP request for EDX/keys {}".format(query)
            )
            if not raw or 'epp' in query:
                current = self.read_value( )
                raw = current.raw
                if 'epp' in query: dct['epp'] = current.epp

            if query:
                res = HWPCapabilitiesRegister(self.controller).read_value( )
                LOGGER.debug("Default HWP capabilities are {}".format(res))
                for k in query:
                    if k == 'epp':
                        continue
                    elif k == 'minimum':
                        dct[k] = res['lowest'] * (res.get('bus') or 100)
                    elif k == 'maximum':
                        dct[k] = res['highest'] * (res.get('bus') or 100)
                
        if dct['minimum'] > dct['maximum']:
            dct['minimum'] = dct['maximum']

        config = HWPConfig.from_json(dct)
        eax = config.to_raw( )
        edx = raw['edx']

        res = self.controller.write_register(
            self.get_register( ), eax, edx, readback=verify
        )
        if verify:
            return res and res[0] == eax
        return None

class TDPRegister(Register):
    '''
        Power limits/settings from Intel register. 

        Will give PL1 (long) window info, and PL2 (short) window info,
        along with locking information.
    '''
    name = "Turbo Power Limits"
    short_name = "TPL"
    register = 0x610

    def read_value(self):
        # Grab our power units from the other register
        base = UnitsRegister(self.controller).read_value( )
        return self.raw_to_value(self._read_reg( ), **base)

    @classmethod
    def _pl_bits_to_values(cls, val, power_units, time_units, pl2=False):
        limit = val & 0x7fff
        on = (val >> 15) & 0x1
        clamp = (val >> 16) & 0x1
        time = (val >> 17) & 0x7f
        locked = (val >> 32) & 0x1

        kls = PL2Config if pl2 else PL1Config
        return kls(
            limit / power_units, bool(on), bool(clamp), time, bool(locked), val
        )

    @classmethod
    def json_to_value(cls, json):
        if not isinstance(json, dict):
            raise ValueError("Object must be a dict-like object.")
    
        pl1, pl2 = None, None
        for k, v in json.items( ):
            p = k.lower( ).strip( )
            if p == 'pl1':
                pl1 = v
            elif p == 'pl2':
                pl2 = v
            # For now we just ignore other keys, should we error?

        if not pl1 and not pl2:
            raise ValueError("Object must have a least a 'pl1' or 'pl2' key.")

        dct = {}
        for code, obj in [('pl1', pl1), ('pl2', pl2)]:
            if obj == None: continue
            if not isinstance(obj, dict):
                raise ValueError(
                    "'{}' must be specified as a dict of parameters.".format(
                        code
                    )
                )
            
            for req in ('limit', 'enabled', 'clamped', 'duration'):
                if req not in obj:
                    raise ValueError(
                        "'{}' is missing required parameter '{}'".format(
                            code, req
                        )
                    )

            try:
                dct[code] = PLConfig.from_json(obj)
            except Exception as ex:
                raise ValueError("Invalid specification for '{}': {}".format(
                    code, ex
                ))
        
        return dct

    def write_value(self, value, verify=True, stop_on_fail=False):
        '''
            Take a dict with pl1 and pl2 keys set. Setting to None will
            readback the existing value and set that instead (NB will not 
            change the power limit).

            If not set to None, pl1/pl2 must be an instance of PLConfig.
        '''

        if 'pl1' not in value and 'pl2' not in value:
            raise ValueError(
                "write_value expects a dict-like object with pl1 and pl2 keys"
            )
        
        pl1, pl2 = value.get('pl1'), value.get('pl2')
        if pl1 == None and pl2 == None:
            raise ValueError(
                "You must pass at least one of pl1 or pl2"
            )

        for p in [pl1, pl2]:
            if isinstance(p, dict):
                p = PLConfig.from_json(p)
            elif p != None and not isinstance(p, PLConfig):
                raise ValueError(
                    "pl1/pl2 must be NoneType or an instance of PLConfig"
                )

        current = None
        if not pl1 or not pl2 or not pl1.original or not pl2.original:
            # Get the current value so as not to override relevant bits.
            current = self.read_value( )
            if pl1 != None: pl1.original = current['pl1'].original
            if pl2 != None: pl2.original = current['pl2'].original

        # Get current power units
        base = UnitsRegister(self.controller).read_value( )
        if pl1 == None: pl1 = current['pl1']
        if pl2 == None: pl2 = current['pl2']

        eax, edx = pl1.to_raw(**base), pl2.to_raw(**base)
        res = self.controller.write_register(
            self.get_register( ), eax, edx, readback=verify
        )
        if verify:
            return res and res[0] == eax and res[1] == edx
        return None

    def raw_to_value(self, raw, **kw):
        '''
            EAX: Bits 0 to 14 = PL1 (Long) limit
                 Bit 15 = PL1 Enabled
                 Bit 16 = PL1 Clamping
                 Bits 17-23 = PL1 Time Window (complex formula, ignore)
            EDX:  First 15 bits = PL2 (Long) limit
                 Bit 15 = PL2 Enabled
                 Bit 16 = PL2 Clamping
                 Bits 17-23 = PL2 Time Window (complex formula, ignore)
                 Bit 32 = Configuration Locked (no changes allowed)

            Backup other bits to restore if required.
        '''
          
        eax, edx, power, time = raw['eax'], raw['edx'], kw['power'], kw['time']
        pl1 = self._pl_bits_to_values(eax, power, time, pl2=False)
        pl2 = self._pl_bits_to_values(edx, power, time, pl2=True)
    
        return {
            'pl1': pl1, 'pl2': pl2
        }

    def test(self):
        resl = super( ).test( )
        if not resl: return False

        pl1 = self._test_value.get('pl1')
        if pl1:
            lcked = pl1.locked
            if lcked:
                LOGGER.warn(
                    "PL1 is LOCKED, writeable TPL is unsupported."
                )
                return self.read_support_level
            return self.write_support_level
        else:
            return False

class PlaneValue(SerializableResult):
    ''' PlaneValue object representing an id, code and UV value.

        UV can be given in raw or mV - setting either of these will
        clear the other. The "other" value will then be dynamically
        recalculated. This allows you to set either raw or mV and 
        have this class work out the other.
    '''
    def __init__(self, id, code, raw=None, mV=None):
        self._raw = None
        self._mV = None
        if id not in [p.id for p in FIVRRegister.planes]:
            raise ValueError("Plane Id {} is not valid, use one of: {}".format(
                id, FIVRRegister.planes
            ))
        self.id, self.code = id, code
        if raw != None:
            self.raw = raw
        elif mV != None:
            self.mV = mV

        if self.raw == None and self.mV == None:
            raise ValueError(
                "One of raw or millivolt plane voltage offset value must be "
                "specified."
            )
        
        try:
            mv = float(self.mV)
            if mv < -999 or mv > 999: raise ValueError
        except Exception as ex:
            raise ValueError(
                "Plane voltage offset value must be between -999 and 999."
            )

        ''' Harcoded, fixes issue #3. We may want to consider relaxing this or
            making it a setting. '''

    def _get_mv(self):
        if self._mV != None:
            return self._mV
        if self._raw != None:
            return self._int_to_voltage(self._raw)
        return None

    def _get_raw(self):
        if self._raw != None:
            return self._raw
        if self._mV != None:
            return self._voltage_to_int(self._mV)
        return None

    def check_overvolt(self):
        if self.mV and self.mV > 0:
            raise ValueError("Positive offset (overvolting) is not permitted.")

    @property
    def mV(self):
        return self._get_mv( )
    @mV.setter
    def mV(self, mv):
        if mv == None:
            self._mV = None
            return
        self._mV = float(mv)
        self._raw = None

    @property
    def raw(self):
        return self._get_raw( )
    @raw.setter
    def raw(self, raw):
        if raw == None:
            self._raw = None
            return
        self._raw = int(raw)
        self._mV = None

    def to_json(self):
        return {
            'id': self.id, 'code': self.code,
            'raw': self.raw, 'mV': self.mV
        }

    @classmethod
    def from_json(cls, dict):
        pv = PlaneValue(
            id=dict['id'],
            code=dict.get('code') or 'Not Given',
            raw=dict.get('raw'),
            mV=dict.get('mV', dict.get('mv'))
        )
        pids = [p.id for p in FIVRRegister.planes]
        if pv.id not in pids:
            raise ValueError("Unknown plane Id {}".format(pv.id))
        if pv._raw == None and pv._mV == None:
            raise ValueError("Specify at least one of raw or mV.")
        return pv

    def __str__(self):
        return "PlaneValue(id={}, code={}, raw={}, ({}) mV={})".format(
            self.id, self.code, self.raw,
            hex(self.raw) if self.raw != None else "-", self.mV
        )

    def __repr__(self): return self.__str__( )

    @classmethod
    def _int_to_voltage(cls, eax):
        '''
            Convert to nearest millivolt. Logic taken from 
            https://github.com/georgewhewell/undervolt/blob/master/undervolt.py
        '''
        shft = eax >> 21
        return (shft if shft <= 1024 else - (2048 - shft)) / 1.024

    @classmethod
    def _voltage_to_int(cls, mv_float):
        x = int(round(mv_float * 1.024))
        return 0xFFE00000 & ((x & 0xFFF) << 21) 

class FIVRRegister(Register):
    '''
        Voltage offsets for Intel processors.

        "Planes" are set in the EDX value to determine which UV to action:

            + 0: Core Voltage
            + 1: GPU Voltage
            + 2: Cache Voltage
            + 4: System Agent Voltage

        EAX is used to determine the voltage to set. To set the register, 
        the plane is specified in EDX, and the voltage in EAX.

        This is a "read-back" register, in that to access the currently set
        voltage, the plane is specified in EDX as per the set, with the last
        bit as zero (read) or one (write), and zero is specified in the EAX.
        The register EAX and EDX values are then immediately read to get the
        offset.

    '''
    name = "Fully Integrated Voltage Regulators"
    short_name = "FIVR"
    register = 0x150

    Plane = namedtuple('Plane', ('id', 'code'))
    planes = [
        Plane(0, 'cpu'),
        Plane(1, 'gpu'),
        Plane(2, 'cache'),
        Plane(4, 'sys')
    ]
    read_eax = 0

    @classmethod
    def _pack_plane_id(cls, plane_id, write=False):
        '''
            EDX should be 0x80000p1w, where p = plane and w = write bit.
            Pack accordingly.
        '''
        return (1 << 31) | (plane_id << 8) | 0x10 | (1 if write else 0)

    def _read_plane(self, plane_id):
        # Ask for the current value by writing the special value to EAX.
        edx = self._pack_plane_id(plane_id, write=False)
        val = self._write_reg(edx=edx, eax=self.get_read_eax( ))
        return val['eax']

    def read_value(self, planes=[]):
        vals = {}
        for plane in self.planes:
            if planes and (plane.id not in planes and plane.code not in planes):
                continue
            try:
                raw = self._read_plane(plane.id)
            except Exception:
                raw = None
            vl = PlaneValue(id=plane.id, code=plane.code, raw=raw)
            vals[plane.code] = vl
        if len(vals) == 1: return tuple(vals.values())[0]
        LOGGER.debug("Read planes {} as {}".format(planes, vals))
        return vals

    def _write_plane(self, plane, verify=True):
        if not isinstance(plane, PlaneValue):
            raise ValueError("_write_plane requires an instance of PlaneValue")

        plane.check_overvolt( )
        raw = plane._get_raw( )
        if raw == None:
            raise ValueError("Unable to get raw value from {}".format(plane))

        # Get the EDX value for writing this plane.
        edx = self._pack_plane_id(plane.id, write=True)
        self._write_reg(raw, edx, readback=False)
        if verify:
            return self._read_plane(plane.id) == raw
        return None

    def write_value(self, value, verify=True, stop_on_fail=False):
        if not isinstance(value, (dict, list, tuple)):
            if not isinstance(value, PlaneValue):
                raise ValueError(
                    "write_value requires a list/dict of PlaneValue "
                    "objects, or a single PlaneValue object."
                )
            else:
                value = (value, )
        
        if isinstance(value, dict): value = tuple(value.values())

        if isinstance(value, (list, tuple)):
            for item in value:
                if isinstance(item, dict):
                    item = PlaneValue.from_json(item)
                elif not isinstance(item, PlaneValue):
                    raise ValueError(
                        "{} is not a PlaneValue object.".format(item)
                    )

        resl = []
        for plane in value:
            try:
                resl.append((plane, self._write_plane(plane, verify=verify)))
            except Exception as ex:
                if stop_on_fail: raise
                LOGGER.error("Unable to write/verify plane {}: {}".format(
                    plane, ex
                ))

        return resl[0] if len(resl) == 1 else resl
            
    @classmethod
    def json_to_value(cls, json):
        if not isinstance(json, dict):
            raise ValueError("Object must be a dict-like object.")
    
        plane_ids = dict([(str(p.id), p.code) for p in cls.planes])
        plane_codes = dict([(p.code, p.id) for p in cls.planes])
        
        planes = {}
        for k, v in json.items( ):
            if str(k) in plane_ids:
                planes[int(k)] = v
            elif str(k).lower( ) in plane_codes:
                planes[plane_codes[str(k).lower( )]] = v
            else:
                raise ValueError("Unknown plane '{}', choose from {}".format(
                    k, cls.planes
                ))

        for id, value in planes.items( ):
            if not isinstance(value, dict):
                # Assume MV
                try:
                    int(value)
                except Exception as ex:
                    raise ValueError(
                        "For each plane, you should pass a dict with either "
                        "raw OR mv key set, or an integer value to represent "
                        "the voltage offset in millivolts."
                    )
                planes[id] = value = {'mV': int(value)}
            value['code'] = plane_ids[str(id)]
            value['id'] = id
            
        dct = {}
        for id, value in planes.items( ):
            try:
                dct[id] = PlaneValue.from_json(value)
            except Exception as ex:
                raise ValueError("Invalid specification for '{}': {}".format(
                    id, ex
                ))
        
        return dct


class MSRController(BaseController, DllLoader):
    copy_sys_for_dll = True
    name = 'Intel MSR (Model Specific Register) Controller'

    info_register = PlatformInfoRegister
    all_registers = all_read_registers = [
        info_register, HWPCapabilitiesRegister, UncoreRatioLimitRegister,
        
        TDPRegister, FIVRRegister, HWPRequestRegister,

        PP0PolicyRegister, PP1PolicyRegister
    ]
    _supported_write_registers = []

    @classmethod
    def get_dll_name(cls):
        return "WinRing0.dll" if ctypes.sizeof(
            ctypes.c_voidp
        ) != 8 else "WinRing0x64.dll"    

    @classmethod
    def init_dll(cls, dll, potential):
        resl = dll.InitializeOls( )
        LOGGER.debug("InitializeOls is {}".format(resl))
        loaded = False
        resl = dll.GetDllStatus( )
        LOGGER.debug("GetDllStatus is {}".format(resl))
        if resl == 0:
            LOGGER.info(
                "DLL {} initialized successfully.".format(
                    potential
                )
            )
            loaded = True
        if not loaded:
            LOGGER.info("DLL {} cannot initialize: {}".format(
                potential, resl
            )+". This platform may not support MSR.")

            # Special case, DLL loaded but sys driver not.
            if resl == 3:
                LOGGER.error(
                    "Sys driver for {} cannot be found".format(
                        potential
                    )
                )
            raise RuntimeError("Cannot init DLL")

    def get_all_supported_registers(self):
        return self.get_supported_registers( )
    
    def get_all_supported_read_registers(self):
        self.get_supported_registers( )
        return self._supported_registers

    def get_support_level(self):
        try:
            stat = self.dll.GetDllStatus( ) 
            if stat != 0:
                self.support_hints.append(
                    "WinRing status ({}) not valid, MSR driver not "
                    "supported.".format(stat)
                )
                return self.SUPPORT_LEVEL_UNSUPPORTED

            regs = self.get_supported_registers( )
            if not regs:
                LOGGER.error("No registers supported.")
                return self.SUPPORT_LEVEL_UNSUPPORTED
            elif len(regs) < len(self.all_read_registers):
                return self.SUPPORT_LEVEL_PARTIAL_SUPPORT
            return self.SUPPORT_LEVEL_FULL_SUPPORT

        except Exception as ex:
            self.support_hints.append(
                "Unable to load WinRing driver, MSR driver not "
                "supported: {}".format(ex)
            )
            return self.SUPPORT_LEVEL_UNSUPPORTED
        
    def is_writeable(self, register_klass):
        return register_klass in self._supported_write_registers

    def get_supported_registers(self):
        if hasattr(self, '_supported_registers'):
            return self._supported_registers

        self._supported_registers = []
        self._supported_write_registers = []

        ret_list = []

        try:
            # TODO: We should probably make this better, look at CPUID?
            info = self.info_register(self).read_value( )
            self._supported_registers.append(self.info_register)

            for reg in self.all_read_registers:
                if reg != self.info_register and (
                    reg not in self.unsupported_registers
                ):
                    try:
                        resl = reg(self).test( )
                        if not resl:
                            self.support_hints.append(
                                "Testing register {} ({}/{}) failed, ".format(
                                    reg.name, reg.short_name, hex(reg.register)
                                )+"assuming this is unsupported."
                            )
                            self.unsupported_registers.append(reg)
                        else:
                            self._supported_registers.append(reg)
                            if resl == Register.write_support_level:
                                LOGGER.info(
                                    "Register {} is fully writeable.".format(
                                        reg.short_name
                                    )
                                )
                                self._supported_write_registers.append(reg)
                            else:
                                LOGGER.info(
                                    "Register {} is read-only.".format(
                                        reg.short_name
                                    )
                                )
                                if not reg.read_only:
                                    LOGGER.warn(
                                        "Register {} ({}/{}) is ".format(
                                            reg.name, reg.short_name, hex(reg.register)
                                        )+(
                                            "reported as read-only, could be "
                                            "locked or unsupported."
                                        )
                                    )

                    except Exception as ex:
                        LOGGER.warn(
                            "Unable to test register {}".format(reg.short_name),
                            exc_info=ex
                        )
        except Exception as ex:
            LOGGER.error(
                "Unhandled exception when detecting supported registers for "
                "{}. Assuming no support.".format(self.name), exc_info=ex
            )
            return []

        try:
            HWPRequestRegister(self).read_value( )
            self._supported_registers.append(HWPRequestRegister)
        except:
            self.support_hints.append(
                "HWP Requests are unavailable on this chipset, either due "
                "to the chipset being locked or lacking the underlying MSR "
                "requirement."
            )
            self.unsupported_registers.append(HWPRequestRegister)


        return self._supported_registers

    def get_writeable_registers(self):
        return self._supported_write_registers

    def deinit(self):
        self.dll.DeinitializeOls( )

    def read_register(self, reg_int, proc_thread=0, **kwargs):
        '''
            Given a register value return the 32-bit integers for EAX/EDX.
            Optionally specify a processor thread Id.
        '''
        eax, edx, reg, thrd = [ctypes.wintypes.DWORD( ) for i in range(0, 4)]
        reg.value = reg_int
        thrd.value = 1 << (proc_thread or 0)
        res = self.dll.RdmsrTx(reg, ctypes.byref(eax), ctypes.byref(edx), ctypes.byref(thrd))
        if not res:
            LOGGER.warn("Unable to read register {}=>{} from RdmsrTx".format(
                thrd.value, hex(reg.value)
            ))
            raise ValueError("Unable to read register {}".format(hex(reg.value)))

        eaxv, edxv = eax.value, edx.value
        LOGGER.debug("Register {}=>{} read EAX {}, EDX {}".format(
            thrd.value, hex(reg.value), hex(eaxv), hex(edxv)
        ))
        return (eaxv, edxv)

    def write_register(
        self, reg_int, eax_int, edx_int, proc_thread=0, readback=True, **kwargs
    ):
        '''
            Given a register value, and two 32-bit integers for EAX/EDX,
            write these to the MSR. Optionally specify readback=True to
            immediately read the register value from the processor.
        '''
        eax, edx, reg, thrd = [ctypes.wintypes.DWORD( ) for i in range(0, 4)]
        reg.value, eax.value, edx.value = reg_int, eax_int, edx_int
        thrd.value = 1 << (proc_thread or 0)

        res = self.dll.WrmsrTx(reg, eax, edx, ctypes.byref(thrd))
        reg_str = "{}=>{} = {},{}".format(
            thrd.value, hex(reg.value), hex(eax.value), hex(edx.value)
        )
        if not res:
            LOGGER.warn("Unable to write register {}".format(reg_str))
            return False
        
        LOGGER.debug("Wrote register {}".format(reg_str))
        
        if readback: return self.read_register(reg_int, proc_thread)

        return True
