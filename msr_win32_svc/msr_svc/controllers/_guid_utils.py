# Based on the comtypes project implementation.

from ctypes import *

BYTE, WORD, DWORD = c_byte, c_ushort, c_ulong

_StringFromCLSID = oledll.ole32.StringFromCLSID
_CLSIDFromString = oledll.ole32.CLSIDFromString

_CoTaskMemFree   = windll.ole32.CoTaskMemFree

class GUID(Structure):
    _fields_ = [("Data1", DWORD),
                ("Data2", WORD),
                ("Data3", WORD),
                ("Data4", BYTE * 8)]

    def __init__(self, name=None):
        if name is not None:
            _CLSIDFromString(str(name), byref(self))

    def __repr__(self):
        return u'GUID("%s")' % str(self)

    def __str__(self):
        p = c_wchar_p()
        _StringFromCLSID(byref(self), byref(p))
        result = p.value
        _CoTaskMemFree(p)
        return result