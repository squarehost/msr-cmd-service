import ctypes, ctypes.wintypes

import json, logging, os, shutil, sys

LOGGER = logging.getLogger(__name__)

class Register(object):
    ''' A friendly (user-facing) name for the register. '''
    name = None
    ''' A short system name for the register. '''
    short_name = None
    ''' The register (in hex).'''
    register = None
    ''' Some registers (e.g. undervolting) require writing a special
        'read' value to EAX and then pulling the result back.

        If so, specify the hex value to pass to EAX here.'''
    read_eax = None
    read_only = False
    
    ring_dll_name = "WinRing0x64.dll"
    powerprof_dll_name = "powrprof.dll"

    read_support_level = "r"
    write_support_level = "w"

    def __init__(self, controller, **kwargs):
        self.controller = controller
        for k, v in kwargs.items( ):
            setattr(self, k, v)

    def __str__(self):
        return "{}: {} (read via writeback: {})".format(
            self.get_short_name( ), self.get_name( ),
            "Yes" if self.is_readback else "No"
        )

    @classmethod
    def json_to_value(cls, json):
        '''
            Given a JSON object passed in by a client, convert this
            to the format required for write_reg. This should throw
            exceptions if the input format is incorrect/unacceptable or 
            cannot be validated.
        '''
        return json

    def raw_to_dict(self, eax, edx=None, register=None):
        if edx == None:
            eax, edx = eax
        return {
            'eax': eax, 'edx': edx,
            'reg': register or self.get_register( )
        }

    def _read_reg(self, proc_thread=None):
        eax, edx = self.controller.read_register(self.get_register( ))
        return self.raw_to_dict(eax, edx)

    def _write_reg(self, eax, edx, readback=True):
        reg = self.get_register( )
        resl = self.controller.write_register(
            reg, eax, edx, readback=readback
        )
        if readback: return self.raw_to_dict(resl, register=reg)
        return resl

    def get_name(self): return self.name
    def get_short_name(self): return self.short_name
    def get_register(self): return self.register
    def get_read_eax(self): return self.read_eax

    def raw_to_value(self, raw, **kwargs):
        raise NotImplementedError

    @property
    def is_readback(self):
        return True if self.get_read_eax( ) != None else False

    def read_value(self, **read_kwargs):
        if self.is_readback:
            # We need to write and read-back
            raise NotImplementedError
        else:
            # We just need vanilla read
            return self.raw_to_value(self._read_reg(**read_kwargs))

    def write_value(self, value, verify=True, stop_on_fail=False):
        ''' Write an updated value to the register. Values are dependent on
            the register, but are expected to be in the same format that
            read_value returns.

            raises an Exception if the register set fails. If verify=True is
            set, then this method should return True if verification is
            successful, False if it fails. Otherwise NoneType is returned.
        '''
        raise NotImplementedError

    def test(self):
        '''
            Test if the register is supported on the host system, returning
            a bool indicating this.
        '''
        return self.write_support_level

class RegisterJSONEncoder(json.JSONEncoder):
    '''
        Extend the standard JSON Encoder to serialize any returned value 
        classes using the to_json method within each.
    '''
    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return obj.to_json( )
        else:
            return str(obj)

class SerializableResult(object):
    ''' Basic stub class to allow "other" results to be serialized.
        Should be overriden to provide a proper object representation. '''
    def to_json(self): return str(self)

class DllLoader(object):
    system_wide_dll_path = "C:\\Windows\\System32"
    copy_sys_for_dll = False

    def __init__(self):
        self.dll = self.get_dll( )

    @classmethod
    def init_dll(cls, dll, potential):
        pass

    @classmethod
    def get_potential_dll_folders(cls):
        return [None, cls.system_wide_dll_path]

    @classmethod
    def get_dll_name(cls):
        raise NotImplementedError

    @classmethod
    def copy_sys_to_exec(cls, exec_path, sys_name):
        LOGGER.warn("Unable to find {} driver, attempting to copy {}.".format(
            sys_name, exec_path
        ))
        folders = list(cls.get_potential_dll_folders( ))
        if getattr(sys, 'frozen', False):
            mp = getattr(sys, '_MEIPASS', None)
            if mp: folders.insert(0, mp)

        
        for folder in folders:
            if not folder: continue
            sys_path = os.path.join(folder, sys_name)
            if os.path.exists(sys_path):
                try:
                    shutil.copyfile(
                        sys_path, os.path.join(exec_path, sys_name)
                    )
                    LOGGER.info("...sys file successfully copied from {}".format(
                        sys_path
                    ))
                    return
                except Exception as ex:
                    LOGGER.warn(
                        "Unable to copy file from path {}: {}".format(
                            folder, ex
                        )
                    )
        
        LOGGER.warn("Unable to locate/copy sysfile, proceeding but "
                    "init will likely fail.")

    @classmethod
    def copy_dll_sys(cls, dll_name):

        ''' The SYS file needs to be in the executable path for
            Windows to fine it, regardless of where the DLL is stored.

            Maybe I'm missing some sort of setting here, but I couldn't
            find any info other than looking at what ProcMon tells me.

            Sigh.
        '''
        exec_path = os.path.dirname(sys.executable)
        sys_name = dll_name.replace(".dll", ".sys")
        if not os.path.exists(
            os.path.join(exec_path, sys_name)
        ):
            cls.copy_sys_to_exec(exec_path, sys_name)

    @classmethod
    def load_dll(cls, dll_path, dll_name, test=False):
        ''' Do what is required (PATH modifications etc.) to locate
            and load the WinRing DLL. Also test that it can initialize.
            Return the ctypes reference if successful, else None.
        '''
        if dll_path:
            apath = os.path.abspath(dll_path)
            potential = os.path.join(apath, dll_name)
        else:
            potential = dll_name
        
        if cls.copy_sys_for_dll: cls.copy_dll_sys(dll_name)

        dll = None
        try:
            if dll_path:
                if not os.path.exists(potential):
                    return None

            dll = ctypes.WinDLL(potential)
            LOGGER.debug("Opened {} DLL: {}".format(potential, dll))
            cls.init_dll(dll, potential)
        except Exception:
            try:
                dll.DeinitializeOls( )
            except:
                pass
            dll = None
        return dll

    @classmethod
    def get_dll(cls):
        dll = None

        dll_name = cls.get_dll_name( )
        folders = cls.get_potential_dll_folders( )
        # Scan through relevant paths to try and find the DLL
        for path in folders:
            dll = cls.load_dll(path, dll_name)
            if dll:
                if not path: path = "<Executable Folder>"
                LOGGER.info("Using DLL {} from {}".format(dll_name, path))
                break
        return dll

class BaseController(object):
    ''' A controller is something that reads/writes a setting to the 
        appropriate underlying system. '''
    name = 'Unknown Controller'
    

    all_read_registers = all_registers = []

    '''
        When writing multiple registers, controllers will be grouped in
        descending order of their priority - so lower priority controllers
        will have all their registers applied first.
    '''
    apply_priority = 100

    SUPPORT_LEVEL_UNSUPPORTED = 100
    SUPPORT_LEVEL_PARTIAL_SUPPORT = 50
    SUPPORT_LEVEL_FULL_SUPPORT = 0

    def __init__(self, strict_mode, *args, **kwargs):
        self.strict = strict_mode
        self.support_hints = []
        self.unsupported_registers = []
        super( ).__init__(*args, **kwargs)

    def get_support_level(self):
        ''' Inidicates if the underlying system is supported on this platform.
            A value of SUPPORT_LEVEL_UNSUPPORTED will cause this module to be
            disregarded by the handler, and no functions will be called. Any
            value above this will cause the controller to be called 
            accordingly, but functions may fail depending on the underlying
            support.
        '''
        return self.SUPPORT_LEVEL_UNSUPPORTED

    def get_support_issues(self):
        ''' Return a list of strings indicating why certain aspects of the
            controller are not supported. '''
        return list(set(self.support_hints))

    def get_supported_registers(self):
        ''' Return a list of register classes indicating registers
            that are supported on the current platform.
            
            Even if a register is read-only it should be included in this list.
        '''
        return []

    def get_unsupported_registers(self):
        ''' Return a list of register classes that are not supported on the
            current platform. This is for both read/write. '''
        return self.unsupported_registers

    def is_writeable(self, register_klass):
        ''' If this register is writeable, this should return True. '''
        return not register_klass.read_only

    def get_writeable_registers(self):
        return self.get_supported_registers( )

    def init(self):
        ''' Post-init (e.g. DLL load) functions. '''
        pass

    def deinit(self):
        pass

    def get_all_supported_read_registers(self):
        ''' Return a list of supported registers that we should advertise. '''
        return self.all_read_registers

    def get_all_supported_registers(self):
        ''' Return a list of supported registers including non-advertised. '''
        return self.all_registers

    def read_register(self, *args, **kwargs):
        raise NotImplementedError

    def write_register(self, *args, **kwargs):
        raise NotImplementedError

    def apply_all(self):
        ''' Some controllers, for example PowrProf, requires all registers to be
            written before applying the profile. This command will be called at
            the end of a session if registers have been written using this
            controller. '''
        return