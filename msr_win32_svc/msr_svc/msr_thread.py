import glob, logging, os, pprint, shutil, subprocess, threading, time

from . import controllers, msr_listener

LOGGER = logging.getLogger("msr")

class MSRCommandThread(threading.Thread):
    protocol_version = "1.12.b4"
    listener_class = msr_listener.Win32PipeHandler
    listener_name = "msr"

    sleep_delay = 0.5

    controllers = [
        controllers.MSRController,
        controllers.PowerProfController
    ]
    controller_registers = []

    def __init__(self, service_klass, secure_mode=True, strict_mode=False):
        threading.Thread.__init__(self)
        self.service_klass = service_klass

        self._is_running = False
        self._can_run = False
        self.secure_mode = secure_mode
        self.strict_mode = strict_mode

        self.controller_instances = []
        self.register_instances = {}
        self.register_values = {}

    def get_supported_controllers(self):
        ''' Return a list of instantiated controller classes for supported
            controllers. '''
        return self.controller_instances

    def get_unsupported_controllers(self):
        ''' Return a tuple of (class, issues) for unsupported controllers. '''
        return getattr(self, 'unsupported_controllers', [])

    def load_all_registers(self):
        self.all_read_registers = []
        for ctrl in self.controller_instances:
            self.all_read_registers.extend(
                ctrl.get_all_supported_read_registers( )
            )
            self.controller_registers.append((
                ctrl, ctrl.get_all_supported_registers( )
            ))

    def get_read_registers(self, subset=None):
        if not subset:
            return self.all_read_registers
        else:
            return [r for r in self.all_read_registers if (
                r in subset or r.short_name in subset
            )]

    def get_unsupported_registers(self):
        unsup = []
        for ctrl in self.controller_instances:
            unsup.extend(ctrl.get_unsupported_registers( ))
        return unsup

    def get_controller_for_register(self, reg_klass):
        for ctrl, reg in self.controller_registers:
            if reg_klass in reg or reg_klass in [r.__name__ for r in reg]:
                return ctrl
        return None

    def setup_registers(self, register_classes, force=False):
        if not hasattr(self, '_setup_registers'):
            self._setup_registers = []

        for reg in register_classes:
            if reg not in self._setup_registers or force:
                self.service_klass.log(
                    "Instantianting register {}".format(reg.short_name),
                    self.service_klass.LOGLEVEL_DEBUG
                )
                ctrl = self.get_controller_for_register(reg)
                if reg.short_name not in self.register_instances:
                    self.register_instances[reg.short_name] = []
                self.register_instances[reg.short_name].append(reg(ctrl))
            self._setup_registers.append(reg)

    def read_register(self, klass_short_name, store=False):
        regs = self.register_instances.get(klass_short_name)
        vals = []

        if regs:
            for ri in regs:
                direct = not ri.is_readback
                if direct:
                    self.service_klass.log(
                        "Attempting to read direct register {}".format(ri),
                        level=self.service_klass.LOGLEVEL_DEBUG
                    )
                else:
                    self.service_klass.log(
                        "Attempting to read readback register {}".format(ri),
                        level=self.service_klass.LOGLEVEL_DEBUG
                    )

                try:
                    val = ri.read_value( )
                    self.service_klass.log(
                        "Current register value for {} is\n\t{}".format(
                            ri, pprint.pformat(val)
                        ), level=self.service_klass.LOGLEVEL_DEBUG
                    )
                    vals.append((val, ri.controller.name))
                except Exception as ex:
                    self.service_klass.log(
                        "Unable to read register {}: {}".format(ri, ex),
                        level=self.service_klass.LOGLEVEL_WARN
                    )
                    LOGGER.warn(
                        "Unable to read register {}".format(ri), exc_info=ex
                    )
        else:
            self.service_klass.log(
                "Unable to read register {}, it is unknown OR unsupported on"
                "this platform.".format(
                    klass_short_name, 
                    level=self.service_klass.LOGLEVEL_WARN
                )
            )
            return None

        LOGGER.info("All values read for register {} are {}".format(
            klass_short_name, vals
        ))
        if len(vals):
            if len(vals) == 1: return vals[0][0]
            return vals
        return None

    def read_registers(self, subset=None, store=True):
        rr = self.get_read_registers(subset=subset)
        self.setup_registers(rr)
        vals = {}
        for klass in rr:
            vals[klass.short_name] = self.read_register(
                klass.short_name, store=store
            )
        return vals

    def get_controllers_for_register(self, register_klass):
        rr = self.get_read_registers(subset=[register_klass])
        self.setup_registers(rr)

        return [r.controller for r in (
            self.register_instances.get(register_klass.short_name)
        ) or []]

    def write_register(
        self, register_klass, value, verify=True, test_mode=False
    ):
        rr = self.get_read_registers(subset=[register_klass])
        self.setup_registers(rr)
        
        regs = self.register_instances.get(register_klass.short_name)
        vals = []
        if len(regs) > 1:
            LOGGER.info(
                "Register {} is a compound register, "
                "regs will be applied by: {}".format(
                    register_klass.short_name,
                    ", ".join(["{}=>{}".format(
                        r.controller.name, r.name
                    ) for r in regs])
                )
            )
        for ri in regs:
            try:
                if not test_mode:
                    resl = ri.write_value(value, verify=verify)
                    self.written_controllers.add(ri.controller)
                else:
                    LOGGER.info(
                        "Not applying register {}/{} as we are in test mode.".format(
                            ri.short_name, ri.name
                        )+" Returning test dict constant."
                    )
                    resl = {'TEST': 'MODE'}
                vals.append((resl, ri.controller.name))
            except Exception as ex:
                LOGGER.error(
                    "Unable to write register {}".format(ri), exc_info=ex
                )
                if verify: raise
        if len(vals):
            if len(vals) == 1: return vals[0][0]
            return vals
        return None

    def start_session(self):
        self.written_controllers = set( )

    def finish_session(self):
        if hasattr(self, 'written_controllers'):
            if self.written_controllers:
                wc = list(self.written_controllers)
                wc.sort(key=lambda x: x.apply_priority)
                for ctrl in self.written_controllers:
                    ctrl.apply_all( )

        self.written_controllers = set( )
        
    def verify(self):
        self.unsupported_controllers = []
        for controller in self.controllers:
            try:
                ctrl = controller(self.strict_mode)
                ctrl.init( )
                sl = ctrl.get_support_level( )
                supported = ctrl.get_supported_registers( ) or []
                issues = ctrl.get_support_issues( ) or []

                if sl == ctrl.SUPPORT_LEVEL_UNSUPPORTED:
                    LOGGER.warn(
                        "Controller {} is not supported, issues: {}".format(
                            ctrl.name, ", ".join(issues)
                        )
                    )
                    self.service_klass.log(
                        "Controller {} is not supported, issues: {}".format(
                            ctrl.name, ", ".join(issues)
                        ), self.service_klass.LOGLEVEL_WARN
                    )
                    self.unsupported_controllers.append((controller, issues))
                    try:
                        ctrl.deinit( )
                    except:
                        pass
                    supported = None
                elif sl == ctrl.SUPPORT_LEVEL_PARTIAL_SUPPORT:
                    LOGGER.warn(
                        "Controller {} reports only partial support.".format(
                            ctrl.name
                        )
                    )
                elif sl == ctrl.SUPPORT_LEVEL_FULL_SUPPORT:
                    LOGGER.info(
                        "Controller {} reports full support.".format(ctrl.name)
                    )
                
                if not supported:
                    LOGGER.warn(
                        "Controller {} does not advertise support for any "
                        "registers, excluding.".format(ctrl.name)
                    )
                    try:
                        ctrl.deinit( )
                    except:
                        pass
                else:
                    wrt = ctrl.get_writeable_registers( )
                    LOGGER.info(
                        "Controller {} initalised, support level {}, "
                        "supported registers: {}".format(
                            ctrl.name, sl, supported
                        )+", supported writeable registers: {}".format(
                            ctrl.get_writeable_registers( )
                        )+", non-standard read-only registers: {}".format(
                            ["{}/{}".format(
                                r.short_name, r
                            ) for r in supported if (
                                r not in wrt and not r.read_only
                            )]
                        )
                    )
                    self.controller_instances.append(ctrl)
                    LOGGER.info("Initialised {} controller.".format(controller))
            except Exception as ex:
                LOGGER.error(
                    "{} support failure.".format(controller.__name__),
                    exc_info=ex
                )
                self.service_klass.log(
                    "This system does not have {} support: {}/{}.".format(
                        controller.__name__, ex.__class__, ex
                    )
                )
                self.unsupported_controllers.append((controller, [str(ex)]))
                try:
                    ctrl.deinit( )
                except:
                    pass

        self.load_all_registers( )
        self.listener = self.listener_class(
            self, self.listener_name, self.protocol_version,
            secure_mode=self.secure_mode, strict_mode=self.strict_mode
        )
        self._can_run = True

    def stop(self):
        # Signal our thread to terminate once any outstanding ops are done.
        for ctrl in self.controller_instances:
            try:
                ctrl.deinit( )
            except:
                pass
        try:
            self.listener.close( )
        except:
            pass

        self._is_running = False

    ''' Thread overrides. '''
    def run(self):
        if not self._is_running and self._can_run:
            self._is_running = True

            self.listener.listen( )

            
